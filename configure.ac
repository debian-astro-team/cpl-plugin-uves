# Process this file with autoconf to produce a configure script.

AC_INIT([FLAMES/UVES Pipeline], [6.1.8], [https://support.eso.org],[uves])
AC_PREREQ([2.59])

AC_CONFIG_SRCDIR([Makefile.am])
AC_CONFIG_AUX_DIR([admin])

AC_CANONICAL_HOST
AC_CANONICAL_TARGET

AM_INIT_AUTOMAKE
AC_CONFIG_SUBDIRS([hdrl])
AC_CONFIG_HEADERS([config.h])

AM_MAINTAINER_MODE([enable])

UVES_SET_PREFIX(${PIPE_HOME:-/usr/local})

# Immediately before every release do:
#-------------------------------------
#   if (the interface is totally unchanged from previous release)
#       REVISION++;
#   else {
#       /* interfaces have been added, removed or changed */
#       REVISION = 0;
#       CURRENT++;
#       if (any interfaces have been _added_ since last release)
#           AGE++;
#       if (any interfaces have been _removed_ or incompatibly changed)
#           AGE = 0;
#   }

UVES_SET_VERSION_INFO([$VERSION])

# Checks for programs.
CFLAGS=${CFLAGS=-O3 -g}
AC_PROG_CC
AC_PROG_CPP
AM_PROG_CC_C_O
AC_PROG_LN_S

ESO_ENABLE_DEBUG(no)
ESO_ENABLE_STRICT(no)
#This flag disables warnings about type-wrong calls of built-in functions like printf
#It also disallows cc to use its own effective implementations of built-in functions.
# ESO_PROG_CC_FLAG([fno-builtin], [CFLAGS="$CFLAGS -fno-builtin"])

ESO_PROG_CC_FLAG([-std=c99], [CFLAGS="$CFLAGS --std=c99"])
# FIXME: the 80 bit precision of the fpu is required to get correct results
# using amd64 sse registers (64 bit precision) causes
# significant rounding errors
# rounding issues are visibile when running the full science reduction and
# comparing the merged spectra on 32 and 64 bit
ESO_PROG_CC_FLAG([mfpmath=387], [CFLAGS="$CFLAGS -mfpmath=387"])
ESO_PROG_CC_FLAG([Werror=implicit-function-declaration],
                 [CFLAGS="-Werror=implicit-function-declaration $CFLAGS"])


ESO_CHECK_DOCTOOLS

AC_ENABLE_STATIC(no)
AC_ENABLE_SHARED(yes)

AC_PROG_LIBTOOL
AC_SUBST(LIBTOOL_DEPS)

# Checks for libraries.
AC_CHECK_LIB(m, pow, [LIBS="$LIBS -lm"])
AC_CHECK_LIB(socket, socket, [LIBS="$LIBS -lsocket"])
AC_CHECK_LIB(nsl, inet_ntoa, [LIBS="$LIBS -lnsl"])

CPL_CHECK_CFITSIO
#CHECK_FFTW
# Check for purify
CHECK_PURIFY
#ESO_ENABLE_PROFILE(no)

# Checks for header files.
AC_HEADER_STDC
AC_CHECK_HEADERS([string.h])

# Checks for typedefs, structures, and compiler characteristics.
AC_C_CONST
AC_C_INLINE

# Checks for library functions.
AC_CHECK_FUNCS([floor pow sqrt isinf isnan])
ESO_FUNC_STRDUP

# Check for function needed for plotting with irplib
AC_CHECK_FUNCS([setenv])
AC_CHECK_DECLS([setenv])

# Check for CPL presence and usability
#The next line should be eventually removed by removing cx_log in unit tests
CPL_CHECK_CEXT


CPL_CHECK_LIBS
HDRL_CHECK([hdrl])

UVES_SET_PATHS
UVES_CREATE_SYMBOLS

AM_WITH_DMALLOC


#AC_CONFIG_FILES(Makefile
#                doxygen/Doxyfile
#                irplib/Makefile
#                irplib/tests/Makefile
#                uves/Makefile
#                uves/tests/Makefile
#                recipes/Makefile)

AC_CONFIG_SUBDIRS(libqfits)

AC_CONFIG_FILES(Makefile
                Makefile.purify
                doxygen/Doxyfile
                irplib/Makefile
                irplib/tests/Makefile
                uves/Makefile
                uves/tests/Makefile
                flames/Makefile
                flames/tests/Makefile
                recipes/Makefile
                recipes/tests/Makefile
		reflex/Makefile
		reflex/uves.xml
		reflex/uves-dflats.xml
		reflex/uves-fibre.xml
                regtests/Makefile
                regtests/tests/Makefile)
AC_OUTPUT
