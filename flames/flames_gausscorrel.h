/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*
 * $Author: jtaylor $
 * $Date: 2012-11-19 09:16:16 $
 * $Revision: 1.4 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.3  2010/09/24 09:31:29  amodigli
 * put back QFITS dependency to fix problem spot by NRI on FIBER mode (with MIDAS calibs) data
 *
 * Revision 1.1  2007/07/30 09:04:38  amodigli
 * added to repository (removed from flames-uves.h)
 *
 * Revision 1.1  2007/07/27 06:44:09  amodigli
 * added to repository
 *
 */

#ifndef FLAMES_GAUSS_CORREL_H
#define FLAMES_GAUSS_CORREL_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <flames_uves.h>

/* The following function finds the optimal shift for the FF 
   in order to use them as a physical model for the perpendicular
   shape of the PSF on the CCD by calculating the correlation 
   between Science Frame and FF. The correlation takes place between 
   a synthetic frame with gaussian-shaped fibres centered on 
   shifted fibre positions and the actual, real frame on which the shift is
   to be found.
*/


flames_err
gausscorrel(flames_frame *ScienceFrame, 
        orderpos *Order, 
        int32_t maxcorriters,
        double shiftwindow,
        double shifttol,
        int32_t correlxstep, 
        const char *outtabname, 
        double *ydelta,
        const char *COR_MAX_FND,
        const float *COR_DEF_RNG,
        const int *COR_DEF_PNT,
        const float *COR_DEF_OFF,
        const char *COR_TAB_SHP_ID);


#endif
