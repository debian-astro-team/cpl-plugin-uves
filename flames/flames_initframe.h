/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */


#ifndef FLAMES_INIT_FRAME_H
#define FLAMES_INIT_FRAME_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <flames_uves.h>


/* the following function takes a pointer to a frame structure, a file
   name of a raw frame, a pointer to a template structure 
   (incomplete frame); it loads the raw frame, copies the bad pixel mask 
   from the template, calculates the variance frame to fill a complete
   frame structure */

flames_err initframe(flames_frame *myframe, char *filename, flames_frame *template, 
                     char satfilterswitch, frame_data *satthres);

/* CPL interface
flames_err 
initframe(cpl_image** frame_ima, 
                 uves_propertylist* header, 
                 int chip,
                 flames_frame *myframe, 
                 char* filename, 
                 flames_frame *template, 
                 char satfilterswitch,
                 frame_data *satthres);
 */

#endif
