/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
    Internet e-mail: midas@eso.org
    Postal address: European Southern Observatory
            Data Management Division 
            Karl-Schwarzschild-Strasse 2
            D 85748 Garching bei Muenchen 
            GERMANY
===========================================================================*/
/* Program  : Optimal.c                                                    */
/* Author   : G. Mulas  -  ITAL_FLAMES Consortium                          */
/* Date     :                                                              */
/*                                                                         */
/* Purpose  : Missing                                                      */
/*                                                                         */
/*                                                                         */
/* Input:  see interface                                                   */ 
/*                                                                      */
/* Output:                                                              */
/*                                                                         */
/* DRS Functions called:                                                   */
/* none                                                                    */ 
/*                                                                         */ 
/* Pseudocode:                                                             */
/* Missing                                                                 */ 
/*                                                                         */ 
/* Version  :                                                              */
/* Last modification date: 2002/08/05                                      */
/* Who     When        Why                Where                            */
/* AMo     02-08-05   Add header         header                            */
/*-------------------------------------------------------------------------*/
/* C functions include files */ 
/* 020716    KB */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* MIDAS include files */
#include <flames_midas_def.h>
/* FLAMES-UVES include files */ 
#include <flames_def_drs_par.h>
#include <flames_optimal.h>
#include <flames_quickoptimal.h>
#include <flames_quickoptextract.h>
#include <flames_uves.h>
#include <flames_opterrors.h>
#include <flames_sigma_clip.h>
#include <flames_newmatrix.h>

flames_err quickoptimal(flames_frame *ScienceFrame, allflats *SingleFF, 
                        orderpos *Order, int32_t ordsta, int32_t ordend,
                        double kappa2, frame_mask **mask,
                        frame_mask **newmask, frame_data **backframe,
                        int32_t miniters, int32_t maxiters,
                        int32_t xkillsize, int32_t ykillsize)
{

    int32_t nreject=0, nrejecttot=0;
    int32_t i=0, j=0;
    int32_t ijindex=0, ijuplimit=0;
    int32_t *fibrestosolve=0;
    int32_t *orderstosolve=0;
    int32_t numslices=0;
    int32_t numorders=0, iters=0, jrejects=0, arraysize=0;

    char output[100];
    double **aa=0, **xx=0;

    frame_mask *fmvecbuf1=0;
    frame_mask *fmvecbuf2=0;
    frame_mask *fmvecbuf3=0;

    int32_t numordersframe=0;
    int32_t firstno=0;
    int32_t lastno=0;
    int32_t firstnoifibre=0;
    int32_t lastnoifibre=0;
    int32_t noifibre=0;
    int32_t iuplimit=0;
    int32_t juplimit=0;

    int32_t ksubcols;


    static frame_mask *repeatflags=0;
    static frame_mask *badslices=0;
    static int old_juplimit=-1;


    int actvals=0;
    char drs_verbosity[10];
    int status=0;
    memset(drs_verbosity, 0, 10);
    if ((status=SCKGETC(DRS_VERBOSITY, 1, 3, &actvals, drs_verbosity))
                    != 0) {
        /* the keyword seems undefined, protest... */
        return(MAREMMA);
    }


    fmvecbuf1 = newmask[0];
    fmvecbuf2 = mask[0];
    numorders = 1+ordend-ordsta;
    numordersframe = ScienceFrame->lastorder-ScienceFrame->firstorder+1;
    firstno = ordsta-Order->firstorder;
    lastno = ordend-Order->firstorder;
    firstnoifibre = firstno*ScienceFrame->maxfibres;
    lastnoifibre = ((lastno+1)*ScienceFrame->maxfibres)-1;
    iuplimit = ScienceFrame->subrows-1;
    arraysize = numorders*ScienceFrame->maxfibres;
    fibrestosolve = lvector(1, arraysize);
    orderstosolve = lvector(1, arraysize);
    /* allocate aa and xx once and for all, as large as they may be
     possibly needed */
    aa = dmatrix(1, arraysize, 1, arraysize);
    xx = dmatrix(1, arraysize, 1, 1);

    ksubcols = ScienceFrame->subcols;
    juplimit = ksubcols-1;


    if (juplimit > old_juplimit)
    {
        if (old_juplimit > 0)
        {
            free_fmvector(repeatflags, 0, old_juplimit);
            free_fmvector(badslices, 0, old_juplimit);
        }
        repeatflags = fmvector(0,juplimit);
        badslices = fmvector(0,juplimit);
        old_juplimit = juplimit;
    }

    /* if miniters is >1, then we must repeat the extraction at first,
     therefore ...*/
    if (miniters>1)
        i = 1; /* ... initialise the repeatflags to 1, to begin with */
    else
        i = 0; /* initialise the repeatflags to 0, no iteration by default */

    for (j=0; j<=juplimit; j++)
    {
        repeatflags[j]=i;
        badslices[j]=0;        /* initialise the badslices mask */
    }

    /* reset the newmask */
    ijuplimit = (ScienceFrame->subrows*ksubcols)-1;
    for (ijindex=0; ijindex<=ijuplimit; ijindex++) fmvecbuf1[ijindex] = 0;

    nrejecttot=1;
    for (iters=1; (iters<=miniters)||(iters<=maxiters&&nrejecttot>0); iters++) {
        nrejecttot=0 ;
        for (j=0; j<=juplimit; j++) {
            /* should we iterate on this j? */
            if (repeatflags[j] != 0) {
                /* yes, do iterate */
                if (quickoptextract(ScienceFrame, SingleFF, Order, ordsta, ordend, j,
                                mask, aa, xx, arraysize, fibrestosolve,
                                orderstosolve, &numslices))
                    return 1;
                if (numslices>0) {
                    /* Let's do the sigma clipping */
                    if (sigma_clip(ScienceFrame, SingleFF, Order, kappa2, fibrestosolve,
                                    orderstosolve, numslices, j, &nreject,
                                    mask, newmask, backframe, xkillsize, ykillsize))
                        return 2;
                    nrejecttot += nreject;
                }
                else {
                    badslices[j]=1;
                    repeatflags[j]=0;
                    fmvecbuf3 = ScienceFrame->specmask[j][0];
                    for (noifibre=firstnoifibre; noifibre<=lastnoifibre; noifibre++)
                        fmvecbuf3[noifibre]=0;
                }
            }
        }
        /* if we must not iterate by default on all pixels, reset repeatflags */
        if (iters>=miniters)
            for (j=0; j<=juplimit; j++) repeatflags[j]=0;

        /* did I reject any pixels with sigma-clipping? */
        if (nrejecttot>0) {
            /* yes, I did; update the mask from newmask */
            nrejecttot=0;
            for (j=0; j<=juplimit; j++) {
                if (badslices[j]==0) {
                    jrejects=0;
                    ijindex = j;
                    for (i=0; i<=iuplimit; i++) {
                        if (fmvecbuf1[ijindex]!=0) {
                            fmvecbuf1[ijindex]=0;
                            if (fmvecbuf2[ijindex]==0) {
                                /* this pixel is being newly marked bad now*/
                                jrejects++;
                                fmvecbuf2[ijindex]=4;
                            }
                        }
                        ijindex += ksubcols;
                    }
                    if (jrejects>0) {
                        nrejecttot += jrejects;
                        /* mark this x for another iteration */
                        repeatflags[j]=1;
                    }
                }
            }
            if ( strcmp(drs_verbosity,"LOW") == 0 ) {
            } else {
                sprintf(output,"%d pixels sigma-clipped in iteration %d",nrejecttot,
                                iters);
                SCTPUT(output);
            }
        }
        else if (iters>=miniters) {
            if ( strcmp(drs_verbosity,"LOW") == 0 ) {
            } else {
                sprintf(output,"Convergence reached in all slices after %d \
iterations", iters);
                SCTPUT(output);
            }
        }
    }        /* end of `iters' loop */


    /* here compute the errors */
    if ( strcmp(drs_verbosity,"LOW") == 0 ){
    } else {
        SCTPUT("Detailed errors computation...");
    }
    for (j=0; j<=(ksubcols-1); j++) {
        /* was this a bad slice? */
        if (badslices[j]!=0) {
            if ( strcmp(drs_verbosity,"LOW") == 0 ) {
            } else {
                sprintf(output,"bad slice at %d-th column", j);
                SCTPUT(output);
            }
        }

        /* did the extraction converge here? */
        else if (repeatflags[j] != 0) {
            /* no, it did not */

            if ( strcmp(drs_verbosity,"LOW") == 0 ) {
            } else {
                sprintf(output,"no convergence after %d iterations at %d-th column",
                                iters-1, j);
                SCTPUT(output);
            }
            fmvecbuf3 = ScienceFrame->specmask[j][0];
            for (noifibre=firstnoifibre; noifibre<=lastnoifibre; noifibre++)
                fmvecbuf3[noifibre]=0;
        }
        else {
            /* this is a good slice, compute the final pixel values and errors */
            if (quickoptextract(ScienceFrame, SingleFF, Order, ordsta, ordend, j,
                            mask, aa, xx, arraysize, fibrestosolve,
                            orderstosolve, &numslices))
                return 1;

            /* here put the heavy part of the final error estimate, which we do
     once and for all only when we finished extracting the slice, 
     instead of repeating it over and over in the loop */
            if (numslices>0)
                if (opterrors(ScienceFrame, SingleFF, Order, j, mask, aa, xx,
                                fibrestosolve, orderstosolve, numslices, arraysize)
                                != NOERR)
                    return 3;
        }
    }

    free_lvector(fibrestosolve, 1, arraysize);
    free_lvector(orderstosolve, 1, arraysize);
    free_dmatrix(aa, 1, arraysize, 1, arraysize);
    free_dmatrix(xx, 1, arraysize, 1, 1);

    return 0;

}
