/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

#ifndef FLAMES_DO_QUICK_OPTIMAL_H
#define FLAMES_DO_QUICK_OPTIMAL_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <flames_uves.h>

/* the following function performs the actual optimal extraction on a frame,
   "quick and dirty" no y shift correction flavour */
flames_err 
doquickoptimal(flames_frame *ScienceFrame, 
               orderpos *Order, 
               allflats *Shifted_FF,
               double kappa2, 
               frame_mask **mask,
               frame_data **backframe, 
               int32_t minoptitersint,
               int32_t maxoptitersint, 
               int32_t xkillsize,
               int32_t ykillsize);

#endif
