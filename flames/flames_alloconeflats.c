/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
  Internet e-mail: midas@eso.org
  Postal address: European Southern Observatory
  Data Management Division 
  Karl-Schwarzschild-Strasse 2
  D 85748 Garching bei Muenchen 
  GERMANY
  ===========================================================================*/
/*-------------------------------------------------------------------------*/
/**
 * @defgroup flames_alloconeflats   Substep: Initialize a flat frame
 *
 */
/*-------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdlib.h>
#include <flames_midas_def.h>
#include <flames_uves.h>
#include <flames_newmatrix.h>
#include <flames_alloconeflats.h>
/*---------------------------------------------------------------------------
  Implementation
  ---------------------------------------------------------------------------*/
/**
   @name  flames_alloconeflats()  
   @short  Initialize a flat frame
   @author G. Mulas  -  ITAL_FLAMES Consortium. Ported to CPL by A. Modigliani

   @param myflats pointer to a frame

   @return initialized frame

   DRS Functions called:                                                  
   fd3tensor                                                  
   fm3tensor                                                  
   l3tensor                                                   
   cvector                                                   
   lvector                                                   
   ivector                                                   
                                                                         
   Pseudocode:                                                            
   use fd3tensor,fm3tensor,l3tensor,cvector,lvector,ivector   
   to initialize to NULL data                                 

   @note
*/

flames_err alloconeflats(allflats *myflats)
{
  int32_t iframe=0;

  /* allocate the flatdata member array itself */
  myflats->flatdata = 
    (singleflat *) calloc((size_t) myflats->nflats, sizeof(singleflat));
  /* allocate only one submember of flatdata */
  myflats->flatdata[0].data = 
    fdmatrix(0, myflats->subrows-1, 0, myflats->subcols-1);
  myflats->flatdata[0].sigma = 
    fdmatrix(0, myflats->subrows-1, 0, myflats->subcols-1);
  myflats->flatdata[0].badpixel = 
    fmmatrix(0, myflats->subrows-1, 0, myflats->subcols-1);
  myflats->flatdata[0].framename = cvector(0, CATREC_LEN);
  myflats->flatdata[0].sigmaname = cvector(0, CATREC_LEN);
  myflats->flatdata[0].badname = cvector(0, CATREC_LEN);
  myflats->flatdata[0].fibres = lvector(0,myflats->maxfibres-1);

  for (iframe=1; iframe<=myflats->nflats-1; iframe++) {
    /* replicate the pointers for the other members of flatdata */
    myflats->flatdata[iframe].data = myflats->flatdata[0].data;
    myflats->flatdata[iframe].sigma = myflats->flatdata[0].sigma;
    myflats->flatdata[iframe].badpixel = myflats->flatdata[0].badpixel;
    myflats->flatdata[iframe].framename = myflats->flatdata[0].framename;
    myflats->flatdata[iframe].sigmaname = myflats->flatdata[0].sigmaname;
    myflats->flatdata[iframe].badname = myflats->flatdata[0].badname;
    myflats->flatdata[iframe].fibres = myflats->flatdata[0].fibres;
  }
  /* now allocate allflats members */
  myflats->fibremask = ivector(0,myflats->maxfibres-1);
  myflats->fibre2frame = ivector(0,myflats->maxfibres-1);
  myflats->normfactors = 
    fd3tensor(0, myflats->lastorder-myflats->firstorder, 
	      0, myflats->maxfibres-1, 0, myflats->subcols-1);
  myflats->normsigmas = 
    fd3tensor(0, myflats->lastorder-myflats->firstorder, 
	      0, myflats->maxfibres-1, 0, myflats->subcols-1);
  myflats->goodfibres = 
    fm3tensor(0, myflats->lastorder-myflats->firstorder, 
	      0, myflats->maxfibres-1, 0, myflats->subcols-1);
  myflats->lowfibrebounds = 
    l3tensor(0, myflats->lastorder-myflats->firstorder, 
	     0, myflats->maxfibres-1, 0, myflats->subcols-1);
  myflats->highfibrebounds = 
    l3tensor(0, myflats->lastorder-myflats->firstorder, 
	     0, myflats->maxfibres-1, 0, myflats->subcols-1);
  /* ok, finished */
  return(NOERR);

}
/**@}*/






