/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
    Internet e-mail: midas@eso.org
    Postal address: European Southern Observatory
            Data Management Division 
            Karl-Schwarzschild-Strasse 2
            D 85748 Garching bei Muenchen 
            GERMANY
===========================================================================*/
/*-------------------------------------------------------------------------*/
/**
 * @defgroup flames_freeback  Substep: free allocated memory for back 
 *
 */
/*-------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
                          Includes
 --------------------------------------------------------------------------*/

/**@{*/

/*---------------------------------------------------------------------------
                            Implementation
 ---------------------------------------------------------------------------*/
/**
 @name  flames_checksize()  
 @short  free allocated memory for back 
 @author G. Mulas  -  ITAL_FLAMES Consortium. Ported to CPL by A. Modigliani

 @param back  input structure

 @return success or failure code

 DRS Functions called:          
   free_dmatrix                                          
   free_dvector                                                               
 Pseudocode:                                                             
     free allocated memory                                      

@note
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdlib.h>
#include <flames_midas_def.h>
#include <flames_freeback.h>
#include <flames_uves.h>
#include <flames_newmatrix.h>


flames_err 
freeback(flames_background *back)
{

    free_dvector(back->x, 1, (int32_t) back->Window_Number);
    free_dvector(back->y, 1, (int32_t) back->Window_Number);
    free_dmatrix(back->window, 1, (int32_t) back->Window_Number, 1, 5);
    free_dvector(back->coeff, 1,
                 (int32_t)((back->xdegree+1)*(back->ydegree+1)));
    free_dmatrix(back->expon, 1, 2, 1,
                 (int32_t)((back->xdegree+1)*(back->ydegree+1)));

    return(NOERR);

}
/**@}*/
