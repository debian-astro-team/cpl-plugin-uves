/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

#ifndef FLAMES_MAINSTANDQUICKFF_H
#define FLAMES_MAINSTANDQUICKFF_H

#include <cpl.h>

int flames_mainstandquickFF(const char *IN_A,
                            const cpl_frameset* IN_B,
                            const char *IN_D,
                            const char *IN_E,
                            const char *IN_F,
                            const double *DECENTSNR,
                            const double *MAXDISCARDFRACT,
                            const int *MAXBACKITERS,
                            const int *BKGPOL,
                            const char *BKGFITINLINE,
                            const char *BKGFITMETHOD,
                            const char *BKGBADSCAN,
                            const int *BKGBADWIN,
                            const double *BKGBADMAXFRAC,
                            const int *BKGBADMAXTOT,
                            const double *SIGMA,
                            const double *WINDOW,
                            double *OUTPUTD,
                            int *OUTPUTI);

#endif
