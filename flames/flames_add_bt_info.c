/* @(#)flames_tab_dump_bt_info.c */
/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
  Internet e-mail: midas@eso.org
  Postal address: European Southern Observatory
  Data Management Division 
  Karl-Schwarzschild-Strasse 2
  D 85748 Garching bei Muenchen 
  GERMANY
  ===========================================================================*/
/*-------------------------------------------------------------------------*/
/**
 * @defgroup flames_add_bt_info   Substep: add binary table information
 *
 */
/*-------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <flames_midas_tblsys.h>
#include <flames_midas_tblerr.h>
#include <flames_midas_tbldef.h>
#include <flames_midas_macrogen.h>
#include <flames_midas_atype.h>
#include <flames_freeordpos.h>
#include <math.h>
#include <flames_uves.h>
#include <flames_midas_def.h>
#include <flames_add_bt_info.h>

#define MAX_POINTER_FREE_STORE 1000
#define MAX_LENGTH_TABLE_LAB     20
#define MAX_LENGTH_STRING        80
#define DUMMY_FIB_POS_VAL       200



/**@{*/

/*---------------------------------------------------------------------------
  Implementation
  ---------------------------------------------------------------------------*/


/**
   @name  flames_Std_Extract()  
   @short  Dump info stored in a table to screen or to ima descriptors
   @author A. Modigliani  

   @param IMA_IN          Input image filename
   @param TAB_IN_OFPOS    Input fiber-order position table
   @param TAB_IN_BT_INFO  User-defined name of the input binary tab fibre info
   @param FIB_ID          Fiber id
   @param MODE            Dumping mode switch
   @return Bin tab info dumped on screen or on OUTIMA descriptors  
   DRS function called: none                             
   Pseudocode:                       
   select on INTAB the fibre corresponding to INFIB and INIMA's plate   
   of and dump to screen or on INIMA the found information              

   @note
 */

int 
flames_add_bt_info(
		const char *IMA_IN,
		const char *TAB_IN_OFPOS,
		const char *TAB_IN_BT_INFO,
		int *FIB_ID,
		const char *MODE)
{
	char tab_in_bt_info[CATREC_LEN+5];
	char tab_in_ofpos[CATREC_LEN+5];
	char ima_in[CATREC_LEN+5];
	char output[160];
	char string_obs_plate[MAX_LENGTH_TABLE_LAB];
	char mode[MAX_LENGTH_TABLE_LAB];
	char** pointer_free_store;

	char lab_slit[MAX_LENGTH_TABLE_LAB];
	char lab_retractor[MAX_LENGTH_TABLE_LAB];
	char lab_bn[MAX_LENGTH_TABLE_LAB];
	char lab_fbn[MAX_LENGTH_TABLE_LAB];
	char lab_rp[MAX_LENGTH_TABLE_LAB];
	char lab_fps[MAX_LENGTH_TABLE_LAB];

	char lab_e370[MAX_LENGTH_TABLE_LAB];
	char lab_e400[MAX_LENGTH_TABLE_LAB];
	char lab_e420[MAX_LENGTH_TABLE_LAB];
	char lab_e450[MAX_LENGTH_TABLE_LAB];
	char lab_e500[MAX_LENGTH_TABLE_LAB];
	char lab_e600[MAX_LENGTH_TABLE_LAB];
	char lab_e700[MAX_LENGTH_TABLE_LAB];
	char lab_e870[MAX_LENGTH_TABLE_LAB];

	char info_type[MAX_LENGTH_STRING];
	char info_object[MAX_LENGTH_STRING];
	char info_in_tol[MAX_LENGTH_STRING];
	char info_comments[MAX_LENGTH_STRING];
	char info_slit[MAX_LENGTH_STRING];
	char info_ssn[MAX_LENGTH_STRING];
	char info_pssn[MAX_LENGTH_STRING];
	char info_retractor[MAX_LENGTH_STRING];
	char info_fbn[MAX_LENGTH_STRING];

	char info_e400[MAX_LENGTH_STRING];
	char info_e420[MAX_LENGTH_STRING];
	char info_e500[MAX_LENGTH_STRING];
	char info_e700[MAX_LENGTH_STRING];

	char info_prefix[MAX_LENGTH_STRING];

	char info_bn[MAX_LENGTH_STRING];
	char info_rp[MAX_LENGTH_STRING];
	char info_fps[MAX_LENGTH_STRING];


	int n_pointer_free_store=0;
	int actvals = 0;
	int obs_plate = 0;
	int unit = 0;
	int null = 0;
	int tab_in_bt_id =0;
	int tab_in_ofpos_id =0;
	int ima_in_id =0;
	int fib_id=0;
	int select_status=0;


	int i=0;




	int col_fib_pos=0;
	int col_fib_msk=0;
	int col_fib_ord=0;
	int col_object=0;
	int col_ra=0;
	int col_dec=0;
	int col_r=0;
	int col_r_err=0;
	int col_theta=0;
	int col_theta_err=0;
	int col_type=0;
	int col_button=0;
	int col_priority=0;
	int col_orient=0;
	int col_in_tol=0;
	int col_magnitude=0;
	int col_comments=0;
	int col_slit=0;
	int col_retractor=0;
	int col_bn=0;
	int col_fbn=0;
	int col_rp=0;
	int col_fps=0;

	int col_e370=0;
	int col_e400=0;
	int col_e420=0;
	int col_e450=0;
	int col_e500=0;
	int col_e600=0;
	int col_e700=0;



	int info_fib_msk=0;
	int info_fib_ord=0;
	int info_button=0;
	int info_priority=0;




	int n_col=0;
	int n_row=0;
	int raw_sel=0;

	float info_fib_pos=0;
	float info_ra=0;
	float info_dec=0;
	float info_r=0;
	float info_r_err=0;
	float info_theta=0;
	float info_theta_err=0;
	float info_orient=0;
	float info_magnitude=0;


	double fib_pos_tmp_val=0;
	double fib_pos[N_FIBRES_MAX];
	double fib_pos_val[N_FIBRES_MAX];

	double precision=0.00005;


	memset(ima_in, 0, CATREC_LEN+5);
	memset(tab_in_ofpos, 0, CATREC_LEN+5);
	memset(tab_in_bt_info, 0, CATREC_LEN+5);
	memset(fib_pos, 0, N_FIBRES_MAX);
	memset(fib_pos_val, 0, N_FIBRES_MAX);

	memset(lab_slit,      '\0', MAX_LENGTH_TABLE_LAB);
	memset(lab_retractor, '\0', MAX_LENGTH_TABLE_LAB);
	memset(lab_bn,        '\0', MAX_LENGTH_TABLE_LAB);
	memset(lab_fbn,       '\0', MAX_LENGTH_TABLE_LAB);
	memset(lab_rp,        '\0', MAX_LENGTH_TABLE_LAB);
	memset(lab_fps,       '\0', MAX_LENGTH_TABLE_LAB);
	memset(lab_e370,      '\0', MAX_LENGTH_TABLE_LAB);
	memset(lab_e400,      '\0', MAX_LENGTH_TABLE_LAB);
	memset(lab_e420,      '\0', MAX_LENGTH_TABLE_LAB);
	memset(lab_e450,      '\0', MAX_LENGTH_TABLE_LAB);
	memset(lab_e500,      '\0', MAX_LENGTH_TABLE_LAB);
	memset(lab_e600,      '\0', MAX_LENGTH_TABLE_LAB);
	memset(lab_e700,      '\0', MAX_LENGTH_TABLE_LAB);
	memset(lab_e870,      '\0', MAX_LENGTH_TABLE_LAB);

	memset(info_type,     '\0',MAX_LENGTH_STRING);
	memset(info_object,   '\0',MAX_LENGTH_STRING);
	memset(info_in_tol,   '\0',MAX_LENGTH_STRING);
	memset(info_comments, '\0',MAX_LENGTH_STRING);
	memset(info_slit,     '\0',MAX_LENGTH_STRING);
	memset(info_ssn,      '\0',MAX_LENGTH_STRING);
	memset(info_pssn,     '\0',MAX_LENGTH_STRING);
	memset(info_retractor,'\0',MAX_LENGTH_STRING);
	memset(info_fbn,      '\0',MAX_LENGTH_STRING);
	memset(info_prefix,   '\0',MAX_LENGTH_STRING);
	memset(info_bn,       '\0',MAX_LENGTH_STRING);
	memset(info_rp,       '\0',MAX_LENGTH_STRING);
	memset(info_fps,      '\0',MAX_LENGTH_STRING);


	memset(string_obs_plate, '\0', MAX_LENGTH_TABLE_LAB);
	memset(mode, '\0', MAX_LENGTH_TABLE_LAB);



	SCSPRO("flames_add_bt_info");


	if ((pointer_free_store = (char **) calloc(MAX_POINTER_FREE_STORE,
			sizeof(char *))) == NULL) {
		SCTPUT("Error allocating pointer_free_store in flames_add_bt_info");
		return flames_midas_fail();
	}
	else {
		pointer_free_store[0] = (char *) pointer_free_store;
		n_pointer_free_store++;
	}


	if ((SCKGETC(IMA_IN,1,CATREC_LEN+4,&actvals,ima_in)!=0) || (actvals==0)) {
		SCTPUT("Error reading IMA_IN in flames_add_bt_info()");
		return flames_midas_fail();
	}

	if ((SCKGETC(TAB_IN_OFPOS,1,CATREC_LEN+4,&actvals,tab_in_ofpos)!=0) ||
			(actvals==0)) {
		SCTPUT("Error reading TAB_IN_OFPOS in flames_add_bt_info()");
		return flames_midas_fail();
	}

	if ((SCKGETC(TAB_IN_BT_INFO,1,CATREC_LEN+4,&actvals,tab_in_bt_info)!=0) ||
			(actvals==0)) {
		SCTPUT("Error reading TAB_IN_BT_INFO in flames_add_bt_info()");
		return flames_midas_fail();
	}


	if ((SCKRDI(FIB_ID,1,1,&actvals,&fib_id,&unit,&null)!=0) || (actvals==0)) {
		SCTPUT("Error reading FIB_ID in flames_add_bt_info()");
		return flames_midas_fail();
	}


	if ((SCKGETC(MODE,1,1,&actvals,mode)!=0) || (actvals==0)) {
		SCTPUT("Error reading MODE in flames_add_bt_info()");
		return flames_midas_fail();
	}


	/* open the input table */
	if (SCFOPN(ima_in, D_OLD_FORMAT, 0, F_IMA_TYPE, &ima_in_id) != 0) {
		sprintf(output, "Error opening ima %d", ima_in_id);
		SCTPUT(output);
		for (i=(n_pointer_free_store-1); i>=0; i--) free(pointer_free_store[i]);
		return flames_midas_fail();
	}


	if ((SCDRDI(ima_in_id, "ESO.INS.OBSPLATE", 1, 1, &actvals, &obs_plate,
			&unit, &null)!=0) ||
			(actvals==0)) {
		SCTPUT("Error reading descriptor ESO.INS.OBSPLATE in flames_dump_bt_info()");
		return flames_midas_fail();
	}

	if ( TCTOPN(tab_in_ofpos,F_IO_MODE,&tab_in_ofpos_id) != 0) {
		sprintf(output, "Error opening tab %d", tab_in_ofpos_id);
		SCTPUT(output);
	}

	if (TCTOPN(tab_in_bt_info,F_IO_MODE,&tab_in_bt_id) != 0) {
		sprintf(output, "Error opening tab %d", tab_in_bt_id);
		SCTPUT(output);
	}


	if ((SCDRDD(tab_in_ofpos_id, "FIBREPOS", 1, N_FIBRES_MAX, &actvals,fib_pos,
			&unit, &null)!=0) ||
			(actvals==0)) {
		SCTPUT("Error reading descriptor FIBREPOS in flames_dump_bt_info()");
		return flames_midas_fail();
	}

	sprintf(string_obs_plate, "%3s%1d", "_pt",obs_plate);


	strcat(lab_slit,"SLIT");
	strcat(lab_retractor,"RETRACTOR");
	strcat(lab_bn,"BN");
	strcat(lab_fbn,"FBN");
	strcat(lab_rp,"RP");
	strcat(lab_fps,"FPS");

	strcat(lab_e370,"_370");
	strcat(lab_e400,"_400");
	strcat(lab_e420,"_420");
	strcat(lab_e450,"_450");
	strcat(lab_e500,"_500");
	strcat(lab_e600,"_600");
	strcat(lab_e700,"_700");
	strcat(lab_e870,"_870");


	strcat(lab_slit,string_obs_plate);
	strcat(lab_retractor,string_obs_plate);
	strcat(lab_bn,string_obs_plate);
	strcat(lab_fbn,string_obs_plate);
	strcat(lab_rp,string_obs_plate);
	strcat(lab_fps,string_obs_plate);

	strcat(lab_e370,string_obs_plate);
	strcat(lab_e400,string_obs_plate);
	strcat(lab_e420,string_obs_plate);
	strcat(lab_e450,string_obs_plate);
	strcat(lab_e500,string_obs_plate);
	strcat(lab_e600,string_obs_plate);
	strcat(lab_e700,string_obs_plate);
	strcat(lab_e870,string_obs_plate);

	TCIGET(tab_in_bt_id,&n_col,&n_row);


	TCLSER(tab_in_bt_id,"FIBREPOS",   &col_fib_pos);
	TCLSER(tab_in_bt_id,"FIBREMASK",  &col_fib_msk);
	TCLSER(tab_in_bt_id,"FIBREORD",   &col_fib_ord);
	TCLSER(tab_in_bt_id,"OBJECT",     &col_object);
	TCLSER(tab_in_bt_id,"RA",         &col_ra);
	TCLSER(tab_in_bt_id,"DEC",        &col_dec);
	TCLSER(tab_in_bt_id,"R",          &col_r);
	TCLSER(tab_in_bt_id,"R_ERROR",    &col_r_err);
	TCLSER(tab_in_bt_id,"THETA",      &col_theta);
	TCLSER(tab_in_bt_id,"THETA_ERROR",&col_theta_err);
	TCLSER(tab_in_bt_id,"TYPE",       &col_type);
	TCLSER(tab_in_bt_id,"BUTTON",     &col_button);
	TCLSER(tab_in_bt_id,"PRIORITY",   &col_priority);
	TCLSER(tab_in_bt_id,"ORIENT",     &col_orient);
	TCLSER(tab_in_bt_id,"IN_TOL",     &col_in_tol);
	TCLSER(tab_in_bt_id,"MAGNITUDE",  &col_magnitude);
	TCLSER(tab_in_bt_id,"COMMENTS",   &col_comments);

	TCLSER(tab_in_bt_id,lab_slit,       &col_slit);
	TCLSER(tab_in_bt_id,lab_retractor,  &col_retractor);
	TCLSER(tab_in_bt_id,lab_bn,         &col_bn);
	TCLSER(tab_in_bt_id,lab_fbn,        &col_fbn);
	TCLSER(tab_in_bt_id,lab_rp,         &col_rp);
	TCLSER(tab_in_bt_id,lab_fps,        &col_fps);

	TCLSER(tab_in_bt_id,lab_e370,       &col_e370);
	TCLSER(tab_in_bt_id,lab_e400,       &col_e400);
	TCLSER(tab_in_bt_id,lab_e420,       &col_e420);
	TCLSER(tab_in_bt_id,lab_e450,       &col_e450);
	TCLSER(tab_in_bt_id,lab_e500,       &col_e500);
	TCLSER(tab_in_bt_id,lab_e600,       &col_e600);
	TCLSER(tab_in_bt_id,lab_e700,       &col_e700);

	for (i=1;i<=N_FIBRES_MAX;i++){
		TCSGET(tab_in_bt_id,i,&select_status);
		if (select_status) {
			TCERDD(tab_in_bt_id,i,col_fib_pos,&fib_pos_tmp_val,&null);
			/*
	printf("fib_pos_tmp_val=%g\n",fib_pos_tmp_val);
	printf("fib_pos_check_val=%g\n",fib_pos[fib_id]);
			 */
			double fib_pos_sel_val=0;
			if (fabs(fib_pos_tmp_val-fib_pos[fib_id]) <= precision){
				fib_pos_sel_val=fib_pos_tmp_val;
				raw_sel=i;
			} else if (fabs(fib_pos_tmp_val-DUMMY_FIB_POS_VAL) <= precision) {
				fib_pos_sel_val=fib_pos_tmp_val;
				raw_sel=i;
			}
		}
	}

	TCERDR(tab_in_bt_id,raw_sel,col_fib_pos,  &info_fib_pos,   &null);
	TCERDI(tab_in_bt_id,raw_sel,col_fib_msk,  &info_fib_msk,   &null);
	TCERDI(tab_in_bt_id,raw_sel,col_fib_ord,  &info_fib_ord,   &null);

	TCERDR(tab_in_bt_id,raw_sel,col_ra,       &info_ra,        &null);
	TCERDR(tab_in_bt_id,raw_sel,col_dec,      &info_dec,       &null);
	TCERDR(tab_in_bt_id,raw_sel,col_r,        &info_r,         &null);
	TCERDR(tab_in_bt_id,raw_sel,col_r_err,    &info_r_err,     &null);
	TCERDR(tab_in_bt_id,raw_sel,col_theta,    &info_theta,     &null);
	TCERDR(tab_in_bt_id,raw_sel,col_theta_err,&info_theta_err, &null);


	TCERDI(tab_in_bt_id,raw_sel,col_button,   &info_button,    &null);
	TCERDI(tab_in_bt_id,raw_sel,col_priority, &info_priority,  &null);
	TCERDR(tab_in_bt_id,raw_sel,col_orient,   &info_orient,    &null);


	TCERDR(tab_in_bt_id,raw_sel,col_magnitude,&info_magnitude, &null);

	TCERDC(tab_in_bt_id,raw_sel,col_object,    info_object,    &null);
	TCERDC(tab_in_bt_id,raw_sel,col_type,      info_type,      &null);
	TCERDC(tab_in_bt_id,raw_sel,col_in_tol,    info_in_tol,    &null);
	TCERDC(tab_in_bt_id,raw_sel,col_e400,     info_e400,      &null);
	TCERDC(tab_in_bt_id,raw_sel,col_e420,     info_e420,      &null);
	TCERDC(tab_in_bt_id,raw_sel,col_e500,     info_e500,      &null);
	TCERDC(tab_in_bt_id,raw_sel,col_e700,     info_e700,      &null);

	/*
    TCERDC(tab_in_bt_id,raw_sel,col_e370,     info_e370,      &null);
    TCERDC(tab_in_bt_id,raw_sel,col_e450,     info_e450,      &null);
    TCERDC(tab_in_bt_id,raw_sel,col_e600,     info_e600,      &null);
    TCERDC(tab_in_bt_id,raw_sel,col_e870,     info_e870,      &null);
	 */


	TCERDC(tab_in_bt_id,raw_sel,col_comments,  info_comments,  &null);
	TCERDC(tab_in_bt_id,raw_sel,col_slit,      info_slit,      &null);
	TCERDC(tab_in_bt_id,raw_sel,col_retractor, info_retractor, &null);
	TCERDC(tab_in_bt_id,raw_sel,col_fbn,       info_fbn,       &null);

	TCERDC(tab_in_bt_id,raw_sel,col_bn,        info_bn,        &null);
	TCERDC(tab_in_bt_id,raw_sel,col_rp,        info_rp,        &null);
	TCERDC(tab_in_bt_id,raw_sel,col_fps,       info_fps,       &null);

	strcat(info_prefix,"INFO    [dump_bt_info]:   ");

	if (strcmp(mode,"I") !=0 ) {

		sprintf(output,"%s FIBREPOS: %f",info_prefix,info_fib_pos);
		SCTPUT(output);
		sprintf(output,"%s FIBREMASK: %d",info_prefix,info_fib_msk);
		SCTPUT(output);
		sprintf(output,"%s FIBREORD: %d",info_prefix,info_fib_ord);
		SCTPUT(output);
		sprintf(output,"%s OBJECT: %s",info_prefix,info_object);
		SCTPUT(output);
		sprintf(output,"%s MAGNITUDE: %f",info_prefix,info_magnitude);
		SCTPUT(output);
		sprintf(output,"%s RA: %f",info_prefix,info_ra);
		SCTPUT(output);
		sprintf(output,"%s DEC: %f",info_prefix,info_dec);
		SCTPUT(output);
		sprintf(output,"%s R: %f",info_prefix,info_r);
		SCTPUT(output);
		sprintf(output,"%s R_ERR: %f",info_prefix,info_r_err);
		SCTPUT(output);
		sprintf(output,"%s THETA: %f",info_prefix,info_theta);
		SCTPUT(output);
		sprintf(output,"%s THETA_ERR: %f",info_prefix,info_theta_err);
		SCTPUT(output);
		sprintf(output,"%s TYPE: %s",info_prefix,info_type);
		SCTPUT(output);
		sprintf(output,"%s BUTTON: %d",info_prefix,info_button);
		SCTPUT(output);
		sprintf(output,"%s PRIORITY: %d",info_prefix,info_priority);
		SCTPUT(output);
		sprintf(output,"%s ORIENT: %f",info_prefix,info_orient);
		SCTPUT(output);
		sprintf(output,"%s IN_TOL: %s",info_prefix,info_in_tol);
		SCTPUT(output);
		sprintf(output,"%s Slit:%s",info_prefix,info_slit);
		SCTPUT(output);
		sprintf(output,"%s FPS:%s",info_prefix,info_fps);
		SCTPUT(output);
		sprintf(output,"%s Retractor: %s",info_prefix,info_retractor);
		SCTPUT(output);
		sprintf(output,"%s BN: %s",info_prefix,info_bn);
		SCTPUT(output);
		sprintf(output,"%s FBN: %s",info_prefix,info_fbn);
		SCTPUT(output);
		sprintf(output,"%s RP: %s",info_prefix,info_rp);
		SCTPUT(output);
		sprintf(output,"%s _e400: %s",info_prefix,info_e400);
		SCTPUT(output);
		sprintf(output,"%s _e420: %s",info_prefix,info_e420);
		SCTPUT(output);
		sprintf(output,"%s _e500: %s",info_prefix,info_e500);
		SCTPUT(output);
		sprintf(output,"%s _e700: %s",info_prefix,info_e700);
		SCTPUT(output);
		sprintf(output,"%s comments: %s",info_prefix,info_comments);
		SCTPUT(output);




	} else {


		SCDWRI(ima_in_id,"ESO.INF.FIBREMASK",&info_fib_msk,  1,1,&unit);
		SCDWRI(ima_in_id,"ESO.INF.FIBREORD", &info_fib_ord,  1,1,&unit);
		SCDWRI(ima_in_id,"ESO.INF.BUTTON",   &info_button,   1,1,&unit);
		SCDWRI(ima_in_id,"ESO.INF.PRIORITY", &info_priority, 1,1,&unit);


		SCDWRR(ima_in_id,"ESO.INF.FIBREPOS", &info_fib_pos,  1,1,&unit);
		SCDWRR(ima_in_id,"ESO.INF.RA",       &info_ra,       1,1,&unit);
		SCDWRR(ima_in_id,"ESO.INF.DEC",      &info_dec,      1,1,&unit);
		SCDWRR(ima_in_id,"ESO.INF.R",        &info_r,        1,1,&unit);
		SCDWRR(ima_in_id,"ESO.INF.R_ERR",    &info_r_err,    1,1,&unit);
		SCDWRR(ima_in_id,"ESO.INF.THETA",    &info_theta,    1,1,&unit);
		SCDWRR(ima_in_id,"ESO.INF.THETA_ERR",&info_theta_err,1,1,&unit);
		SCDWRR(ima_in_id,"ESO.INF.ORIENT",   &info_orient,   1,1,&unit);
		SCDWRR(ima_in_id,"ESO.INF.MAGNITUDE",&info_magnitude,1,1,&unit);

		SCDWRC(ima_in_id,"ESO.INF.BN",1,info_bn,1,9,&unit);
		SCDWRC(ima_in_id,"ESO.INF.RP",1,info_rp,1,9,&unit);
		SCDWRC(ima_in_id,"ESO.INF.FPS",1,info_fps,1,9,&unit);

		SCDWRC(ima_in_id,"ESO.INF._e420",1,info_e420,1,4,&unit);
		SCDWRC(ima_in_id,"ESO.INF._e500",1,info_e500,1,4,&unit);
		SCDWRC(ima_in_id,"ESO.INF._e700",1,info_e700,1,4,&unit);

	}



	/*
    if ((TCCSER(tab_in_bt_id, "ORDER", &inordercol)!=0) || (inordercol==-1)) {
    sprintf(output, "Error searching ORDER column in %s table", inordtab);
    SCTPUT(output);
    if (freeordpos(ordpos)!=NOERR) {
    SCTPUT("Error returned by freeordpos() to create_full_ordertable()");
    }
    for (i=(npointerstofree-1); i>=0; i--) free(pointerstofree[i]);
    return flames_midas_fail();
    }
	 */
	/* ok, it's over */
	return SCSEPI();

}
/**@}*/




