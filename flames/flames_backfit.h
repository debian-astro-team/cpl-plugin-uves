/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
    Internet e-mail: midas@eso.org
    Postal address: European Southern Observatory
            Data Management Division 
            Karl-Schwarzschild-Strasse 2
            D 85748 Garching bei Muenchen 
            GERMANY
===========================================================================*/



#ifndef FLAMES_BACKFIT_H
#define FLAMES_BACKFIT_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif



int flames_backfit(const double *MAXDISCARDFRACT,
                   const int *MAXBACKITERS,
                   const char *FRAMENAME,
                   const char *BACKTABLE,
                   const char *ORDERTABLE,
                   const char *BACKFRAME,
                   const int *BKGPOL,
                   const char *BKGFITMETHOD,
                   const char *BKGBADSCAN,
                   const int *BKGBADWIN,
                   const double *BKGBADMAXFRAC,
                   const int *BKGBADMAXTOT,
                   const double *SIGMA,
                   int *OUTPUTI);

#endif
