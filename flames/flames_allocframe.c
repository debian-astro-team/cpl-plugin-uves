/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
  Internet e-mail: midas@eso.org
  Postal address: European Southern Observatory
  Data Management Division 
  Karl-Schwarzschild-Strasse 2
  D 85748 Garching bei Muenchen 
  GERMANY
  ===========================================================================*/

/*-------------------------------------------------------------------------*/
/**
 * @defgroup flames_allocframe   Substep: Initialize a frame
 *
 */
/*-------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <flames_midas_def.h>
#include <flames_uves.h>
#include <flames_newmatrix.h>
#include <flames_allocframe.h>


/**@{*/

/*---------------------------------------------------------------------------
  Implementation
  ---------------------------------------------------------------------------*/
/**
   @name  flames_allocframe()  
   @short  Dump info stored in a table to screen or to ima descriptors
   @author G. Mulas  -  ITAL_FLAMES Consortium. Ported to CPL by A. Modigliani

   @param myframe input/output frame

   @return initialized frame
   DRS function called: fdmatrix,dvector
   Pseudocode:            
   use fdmatrix,dvector to initialize to NULL data           

   @note
*/

flames_err allocframe(flames_frame *myframe)
{
  
  myframe->frame_array = 
    fdmatrix(0, myframe->subrows-1, 0, myframe->subcols-1);
  memset(&myframe->frame_array[0][0], '\0', 
	 myframe->subrows*myframe->subcols*sizeof(frame_data));

  myframe->frame_sigma = 
    fdmatrix(0, myframe->subrows-1, 0, myframe->subcols-1); 
  memset(&myframe->frame_sigma[0][0], '\0', 
	 myframe->subrows*myframe->subcols*sizeof(frame_data));

  myframe->badpixel = 
    fmmatrix(0, myframe->subrows-1, 0, myframe->subcols-1);
  memset(&myframe->badpixel[0][0], '\0', 
	 myframe->subrows*myframe->subcols*sizeof(frame_mask));
  
  myframe->framename = cvector(0, CATREC_LEN+1);
  myframe->sigmaname = cvector(0, CATREC_LEN+1);
  myframe->badname = cvector(0, CATREC_LEN+1);
  if (myframe->maxfibres>0) {
    myframe->fibremask = cvector(0, myframe->maxfibres-1);
    myframe->ind_lit_fibres = lvector(0, myframe->maxfibres-1);
  }
  /* do not allocate spec*** structures here because it is a mistake 
     If you don't use these structures you suffer of a memory leakage 
     otherwise you might allocate them before you know the right dimension
     (i.e. myframe->lastorder-myframe->firstorder) and no check help you
     to reveal this error because the memory is allocated but with 
     wrong parameters 
  */
  /* do not allocate back arrays here: it is a mistake, since we cannot 
     possibly know yet Window_Number */
  if (myframe->nflats > 0) {
    myframe->yshift = dvector(0, myframe->nflats-1);
  }
  else {
    myframe->yshift = 0;
  }
  
  /* initialize to zero the pointers in the the back structure */

  myframe->back.x = 0;
  myframe->back.y = 0;
  myframe->back.window = 0;
  myframe->back.coeff = 0;
  myframe->back.expon = 0;

  return(NOERR);

}
/**@}*/
