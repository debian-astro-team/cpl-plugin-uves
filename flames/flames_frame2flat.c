/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
    Internet e-mail: midas@eso.org
    Postal address: European Southern Observatory
            Data Management Division 
            Karl-Schwarzschild-Strasse 2
            D 85748 Garching bei Muenchen 
            GERMANY
===========================================================================*/

/*-------------------------------------------------------------------------*/
/**
 * @defgroup flames_frame2flat  
   this function takes a pointer to a frame structure, a pointer to an 
   allflats structure and a frame index, and copies the pointers to the
   frame, sigma, badpixel et al. to the specified single flat in myflats 
 *
 */
/*-------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
                          Includes
 --------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include <stdlib.h>
#include <flames_midas_def.h>
#include <flames_frame2flat.h>
#include <flames_uves.h>
#include <flames_newmatrix.h>
/**@{*/

/*---------------------------------------------------------------------------
                            Implementation
 ---------------------------------------------------------------------------*/
/**
 @name  flames_frame2flat()  
 @short fill an allflats structure
 @author G. Mulas  -  ITAL_FLAMES Consortium. Ported to CPL by A. Modigliani

 @param myframe frame to be converted to flat
 @param myflats output flat frame
 @param iframe  frame slice to copy (CHECK AGAIN)


 @return success or failure code

 DRS Functions called:          
       none                                         

 Pseudocode:                                                             
    copy in structure in out structure                                      

@doc
   this function takes a pointer to a frame structure, a pointer to an 
   allflats structure and a frame index, and copies the pointers to the
   frame, sigma, badpixel et al. to the specified single flat in myflats 

 */


flames_err 
frame2flat(
                flames_frame *myframe,
                allflats *myflats,
                int32_t iframe)
{
    int32_t ifibre=0;
    int32_t i=0;
    frame_data *fdvecbuf1=0;

    /* copy pointers from myframe to myflats */
    myflats->flatdata[iframe].data = myframe->frame_array;
    myflats->flatdata[iframe].sigma = myframe->frame_sigma;
    myflats->flatdata[iframe].badpixel = myframe->badpixel;
    myflats->flatdata[iframe].framename = myframe->framename;
    myflats->flatdata[iframe].sigmaname = myframe->sigmaname;
    myflats->flatdata[iframe].badname = myframe->badname;
    /* generate numfibres and fibres */
    myflats->flatdata[iframe].numfibres = 0;
    for (ifibre = 0; ifibre <= myframe->maxfibres-1; ifibre++){
        if (myframe->fibremask[ifibre] == TRUE) {
            myflats->flatdata[iframe].fibres[myflats->flatdata[iframe].numfibres] =
                            ifibre;
            myflats->fibremask[ifibre] = TRUE;
            myflats->fibre2frame[ifibre] = (int) iframe;
            myflats->flatdata[iframe].numfibres++;
        }
    }
    /* update global numfibres */
    myflats->numfibres += myflats->flatdata[iframe].numfibres;

    /* check to find pixmax */
    fdvecbuf1 = myflats->flatdata[iframe].data[0];
    for (i=0; i<=((myflats->subrows*myflats->subcols)-1); i++) {
        if (fdvecbuf1[i] > myflats->pixmax) {
            myflats->pixmax = fdvecbuf1[i];
        }
    }

    return(NOERR);

}
/**@}*/







