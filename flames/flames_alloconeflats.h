/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:31:28 $
 * $Revision: 1.3 $
 * $Name: not supported by cvs2svn $
 *
 */

#ifndef FLAMES_ALLOC_ONE_FLATS_H
#define FLAMES_ALLOC_ONE_FLATS_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <flames_uves.h>

/* The following function takes a pointer to an allflats structure 
   and allocates memory for part of the arrays therein. The sizes are taken
   from the scalar members of the allflats structure, and must have 
   been set in advance */

flames_err 
alloconeflats(allflats *myflats);


#endif
