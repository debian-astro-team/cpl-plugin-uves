/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

#ifndef FLAMES_GET_FLAT_SIZE_H
#define FLAMES_GET_FLAT_SIZE_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*-----------------------------------------------------------------------------
                    Includes
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                             Defines
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                                   Prototypes
 -----------------------------------------------------------------------------*/
int 
flames_get_flat_size(const char *IN_A,
                     const char *IN_B,
                     //             const int *MESS_LEVEL,
                     const float *SLIT,
                     const int *ORD_MIN,
                     const int *ORD_MAX,
                     const int *DEFPOL,
                     const  int SAV_BORD_SZ,
                     const  int DRS_SFF_HW_MIN,
                     const  int X_WIND_SIZE,
                     const  int Y_WIND_SIZE,
                     const  int Y_SEARCH_WIND,
                     const  int ORD_TRESH,
                     const  int N_CLIP_MED,
                     const  int N_CLIP_AVG,
                     const  float INT_TRESH);

#endif
