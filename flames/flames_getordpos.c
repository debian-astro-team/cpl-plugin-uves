/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
  Internet e-mail: midas@eso.org
  Postal address: European Southern Observatory
  Data Management Division 
  Karl-Schwarzschild-Strasse 2
  D 85748 Garching bei Muenchen 
  GERMANY
  ===========================================================================*/

/*-------------------------------------------------------------------------*/
/**
 * @defgroup flames_getordpos  this function calculates the y position of the 
 center of a given order "m" at a given "x", 
 and returns it in the double variable to which "y" points.
 *
 */
/*-------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <math.h>
#include <flames_midas_def.h>
#include <flames_uves.h>
#include <flames_getordpos.h>
#include <flames_newmatrix.h>

/**@{*/

/*---------------------------------------------------------------------------
  Implementation
  ---------------------------------------------------------------------------*/
/**
   @name  flames_getordpos()  
   @short  this function calculates the y position of the 
   center of a given order "m" at a given "x", 
   and returns it in the double variable to which "y" points.

   @author G. Mulas  -  ITAL_FLAMES Consortium. Ported to CPL by A. Modigliani

   @param orderstruct 
   @param m 
   @param x 
   @param y

   @return success or failure code

   DRS Functions called:          
   singlecorrel 
   dvector 
   free_dvector

   Pseudocode:                                                             

   @doc
   the following function calculates the y position of the 
   center of a given order "m" at a given "x", 
   and returns it in the double variable to which "y" points. 
   Beware: both "m" and "n" are double! 


 */


flames_err 
get_ordpos(
                orderpos *orderstruct,
                double m,
                double x,
                double *y)
{
    /*TO DO: check for sanity of the input parameters m and x? */
    int ordx=0;
    int ordm=0;
    double *mpowers=0;
    double *xpowers=0;

    mpowers = dvector(0, orderstruct->mdegree);
    xpowers = dvector(0, orderstruct->xdegree);

    /* build the vectors of powers */
    mpowers[0] = xpowers[0] = 1;
    for (ordm=1; ordm <= orderstruct->mdegree; ordm++)
        mpowers[ordm] = mpowers[ordm-1]*m;
    for (ordx=1; ordx <= orderstruct->xdegree; ordx++)
        xpowers[ordx] = xpowers[ordx-1]*x;

    /* loop over powers of the polynomial */
    /* treat the 0 degree parts separately, to avoid indeterminate results */
    *y=(orderstruct->orderpol)[0][0];
    for (ordm=1; ordm <= orderstruct->mdegree; ordm++) {
        /* add the contribution of each power to the order position */
        *y += ((orderstruct->orderpol)[ordm][0])*mpowers[ordm];
        /* printf("y=%f\n",*y); */
    }
    for (ordx=1; ordx <= orderstruct->xdegree; ordx++) {
        *y += ((orderstruct->orderpol)[0][ordx])*xpowers[ordx];
        /* printf("y1=%f\n",*y); */
        for (ordm=1; ordm <= orderstruct->mdegree; ordm++) {
            /* add the contribution of each power to the order position */
            *y += ((orderstruct->orderpol)[ordm][ordx])*xpowers[ordx]*mpowers[ordm];
            /* printf("y2=%f\n",*y); */
        }
        /* printf("y3=%f\n",*y); */
    }
    /* printf("y4=%f\n",*y); */

    free_dvector(mpowers, 0, orderstruct->mdegree);
    free_dvector(xpowers, 0, orderstruct->xdegree);

    return(NOERR);
}
/**@}*/






