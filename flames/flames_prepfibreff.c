/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
    Internet e-mail: midas@eso.org
    Postal address: European Southern Observatory
            Data Management Division 
            Karl-Schwarzschild-Strasse 2
            D 85748 Garching bei Muenchen 
            GERMANY
===========================================================================*/
/* Program  : prepfibreff.c                                                */
/* Author   : G. Mulas  -  ITAL_FLAMES Consortium                          */
/* Date     :                                                              */
/*                                                                         */
/* Purpose  : See later doxygen                                            */
/*                                                                         */
/*                                                                         */
/* Input:  See later doxygen                                               */
/*                                                                         */
/* Output: See later doxygen                                               */
/*                                                                         */
/* DRS Functions called:                                                   */
/* none                                                                    */ 
/*                                                                         */ 
/* Pseudocode:                                                             */
/* Missing                                                                 */ 
/*-------------------------------------------------------------------------*/


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <flames_midas_def.h>
#include <flames_writeallff.h>
#include <flames_gaussselfcorrel.h>
#include <flames_frame2flat.h>
#include <flames_prepfibreff.h>
#include <flames_newmatrix.h>
#include <flames_shiftcommon.h>
#include <flames_fillholes.h>
#include <flames_readordpos.h>
#include <flames_allocallflats2.h>
#include <flames_freeframe2.h>
#include <flames_stripfitsext.h>
#include <flames_freeallflats.h>

#include <flames_readframe.h>
#include <flames_freeordpos.h>

#include <uves_msg.h>

#include <ctype.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
/*---------------------------------------------------------------------------*/
/**
   @brief    Prepare fibre flat field frames

   @param INFIBREFFCAT input fibre flat field data set
   @param OUTFIBREFFCAT output fibre flat field data set
   @param MYORDER     order table filename
   @param SLITFFCAT   input slit flat field data set
   @param BACKTABLE   backgraund table filename
   @param MAXSINGLEPXFRC  maximum acceptable fraction of the flux in a given slices in one single pixel; a higher fraction than this means
                          that there was some numerical instability and/or unmasked bad pixel, and that it must be discarded
   @param MAXCLEANITERS   This is the maximum number of cleaning iterations to be tried on a given slice in flames_prep_fibre, before
                          giving up on its normalise-ability and falling back to the second cleaning strategy
   @param FRACSLICESTHRES defines the minimum fraction of slices that must be good, for a fibre to be considered covered enough at the end
                          of fillholes, in flames_prep_fibreff, to avoid stupid instabilities in gaussselfcorrel if a fibre is only very
                          barely covered by the slit FF frames
   @param GAUSSCORRELSCL  this is the scaling factor with which we define the Gaussian width of synthetic fibres in correlation
   @param GAUSSCORRELWND  This is the scaling factor with which we define window out of which the Gaussian synthetic fibres are considered
                          negligible in correlation
   @param GAUSSFIBRESIGMA Gaussian pseudo-fibre HWHM for correlation
   @param GAUSSHALFWIDTH  Gaussian pseudo-fibre total half-width for correlation
   @param MINFIBREFRAC    Minimum fibre fraction coverage for extraction
   @param BASENAME        base file name of the newly produced FF frames
   @param DECENTSNR       How large must the SNR on a fibre be in a calibration frame, at a given order and x, for that slice to be
                          considered "good"?
   @param MAXDISCARDFRACT This is the maximum fraction of windows/pixels which we are willing to  discard by kappa-sigma clipping in each
                          iteration of the background fitting loop
   @param MAXBACKITERS    This is the maximum number of kappa-sigma clipping iterations which we are willing to perform in background
                          fitting
   @param MAXCORRITERS    This is the maximum number of iterations which we are willing to perform in the correlation
   @param BKGPOL          polynomial degree (X,Y)
   @param BKGFITMETHOD    Background fitting method
   @param BKGBADSCAN      Background table bad pixel frame scanning switch (Values:none, fraction, absolute)
   @param BKGBADWIN       Background table bad pixel frame scanning window size (sizex=50,sizey=50)
   @param BKGBADMAXFRAC   Background table bad pixel frame scanning threshold fraction
   @param BKGBADMAXTOT    Background table bad pixel frame scanning threshold number
   @param SIGMA           Sigma in kappa-sigma
   @param MAXYSHIFT       Half width of the interval to scan for correlation, when determining y shift
   @param CORRELTOL       This is the absolute accuracy with which we require in the correlation to determine the y shift
   @param CORRELXSTEP     This is the x step to use while computing the correlation: it must be a positive integer,
                          1 means "use all pixels", 2 means "use every other pixel", 3 means "use one every three" etc.

   @param OUTPUTI         output
   @ doc -read all input params and data
         -fill holes: tries to fill missing (bad) pixels with interpolated values. Moreover, in the 2D frames each pixel is normalised
                      so that for any given x, and order the integrated value over one fibre equals 1, and the normalisation factor is
                      stored in appropriate structure members, to make shifting more efficient later.

         -define self-shift corrections for Gaussian cross-correlation (finally determines GAUSSFIBRESIGMA, GAUSSHALFWIDTH)
         -computes order centre between first and last order fibre
         -corrects order centre by computed self-correlation
         -move fibres in opposite direction
         -write results in FIBREMQASK, FIBREPOS, COEFFD, TAB_IN_OUT_YSHIFT, GAUSSELFSHIFT

   @return   0 if everything is ok
 */
/*---------------------------------------------------------------------------*/
int flames_prepfibreff(const cpl_frameset  * INFIBREFFCAT,
                       cpl_frameset  ** OUTFIBREFFCAT,
                       const char *MYORDER,
                       const cpl_frameset *SLITFFCAT,
                       const char *BACKTABLE,
                       const double *MAXSINGLEPXFRC,
                       const int *MAXCLEANITERS,
                       const double *FRACSLICESTHRES,
                       const double *GAUSSCORRELSCL,
                       const double *GAUSSCORRELWND,
                       const double *GAUSSFIBRESIGMA,                       
                       const double *GAUSSHALFWIDTH,                       
                       const double *MINFIBREFRAC,
                       const char *BASENAME,
                       const double *DECENTSNR,
                       const double *MAXDISCARDFRACT,
                       const int *MAXBACKITERS,
                       const int *MAXCORRITERS,
                       const int *BKGPOL,
                       const char *BKGFITMETHOD,
                       const char *BKGBADSCAN,
                       const int *BKGBADWIN,
                       const double *BKGBADMAXFRAC,
                       const int *BKGBADMAXTOT,
                       const double *SIGMA,
                       const double *MAXYSHIFT,
                       const double *CORRELTOL,
                       const int *CORRELXSTEP,
                       int *OUTPUTI)
{
    char output[CATREC_LEN+1];
    int status=0;
    int tid=0;
    int actvals=0;
    int unit=0;
    int null=0;
    int entrynum=0;
    int i=0;
    int maxbackiters=0;

    double maxdiscardfract=0;

    const cpl_frameset *catname;
    const cpl_frameset *slitsname;

    cpl_frameset **outcatname;

    char backname[CATREC_LEN+1];
    char filename[CATREC_LEN+5];
    char basename[CATREC_LEN+1];
    char ordername[CATREC_LEN+1];
    char identifier[CATREC_LEN+1];

    int bxdegree=0, bydegree=0;

    double kappa=0;
    double kappa2=0;

    scatterswitch bkgswitch=USEALL;
    scatterswitch2 bkgswitch2=NOBADSCAN;

    int badwinxsize=0;
    int badwinysize=0;
    double badfracthres=0;
    int badtotthres=0;
    char keytype=0;
    int nval=0;
    int noelem=0;
    int bytelem=0;
    double dbuf=0;

    int32_t ifibre=0;
    int imaxcorriters=0;
    int32_t maxcorriters=0;
    double shiftwindow=0;
    double shifttol=0;
    double gausscorrelscale=0;
    double gausscorrelwindow=0;
    int icorrelxstep=0;
    int32_t correlxstep=0;
    int imaxcleaniters=0;
    int32_t maxcleaniters=0;
    double maxsinglepixelfrac=0;
    double fracslicesthres=0;
    char bkgfitmethod[CATREC_LEN+1];
    int32_t firstifibre=0;
    int32_t lastifibre=0;
    double newcentre=0;
    double decentsnr=0;
    allflats *myflats=0;
    flames_frame *framebuffer=0;
    orderpos *ordpos=0;


    memset(output, 0, CATREC_LEN+1);
    memset(backname, 0, CATREC_LEN+1);
    memset(filename, 0, CATREC_LEN+5);
    memset(basename, 0, CATREC_LEN+1);
    memset(ordername, 0, CATREC_LEN+1);
    memset(identifier, 0, CATREC_LEN+1);
    memset(bkgfitmethod, 0, CATREC_LEN+1);

    bkgswitch = USEALL;

    /* allocate pointers */
    myflats = calloc(1, sizeof(allflats));
    framebuffer = calloc(1, sizeof(flames_frame));
    ordpos = calloc(1, sizeof(orderpos));

    /* enter the MIDAS environment */
    SCSPRO("prepfibreff");

    /* read the INFIBREFFCAT keyword to know the name of the catalog file
     containing the list of input fibre FF frames */
    if ((status=SCKGETC_fs(INFIBREFFCAT, 1, CATREC_LEN, &actvals, &catname))
                    != 0) {
        /* the keyword seems undefined, protest... */
        free(myflats);
        free(framebuffer);
        free(ordpos);

        return flames_midas_fail();
    }

    /* read the SLITFFCAT keyword to know the name of the catalog file
     containing the list of int32_t slit FF frames */
    if ((status=SCKGETC_fs(SLITFFCAT, 1, CATREC_LEN, &actvals, &slitsname))
                    != 0) {
        /* the keyword seems undefined, protest... */
        free(myflats);
        free(framebuffer);
        free(ordpos);
        return flames_midas_fail();
    }


    /* read the OUTFIBREFFCAT keyword to know the name of the catalog file
     containing the list of output fibre FF frames */
    if ((status=SCKGETC_fsp(OUTFIBREFFCAT, 1, CATREC_LEN, &actvals, &outcatname))
                    != 0) {
        /* the keyword seems undefined, protest... */
        free(myflats);
        free(framebuffer);
        free(ordpos);
        return flames_midas_fail();
    }

    /* read the MYORDER keyword to know the name of the order table */
    if ((status=SCKGETC(MYORDER, 1, CATREC_LEN, &actvals, ordername))
                    != 0) {
        /* the keyword seems undefined, protest... */
        free(myflats);
        free(framebuffer);
        free(ordpos);
        return flames_midas_fail();
    }

    /* read the BASENAME keyword to know the base file name of the newly
     produced FF frames */
    if ((status=SCKGETC(BASENAME, 1, CATREC_LEN, &actvals, filename))
                    != 0) {
        /* the keyword seems undefined, protest... */
        free(myflats);
        free(framebuffer);
        free(ordpos);
        return flames_midas_fail();
    }
    /* strip filename of the .fits extension, if it has one */
    if ((status = stripfitsext(filename, basename)) != NOERR) {
        /* error stripping extension */
        free(myflats);
        free(framebuffer);
        free(ordpos);
        return flames_midas_fail();
    }

    /* read the BACKTABLE keyword to know the name of background table file */
    if ((status=SCKGETC(BACKTABLE, 1, CATREC_LEN, &actvals, backname))
                    != 0) {
        /* the keyword seems undefined, protest... */
        free(myflats);
        free(framebuffer);
        free(ordpos);
        return flames_midas_fail();
    }
    /* initialise DECENTSNR from keyword */
    if (SCKRDD(DECENTSNR, 1, 1, &actvals, &decentsnr, &unit, &null) != 0) {
        /* problems reading DECENTSNR */
        SCTPUT("Error reading the minimum acceptable SNR");
        free(myflats);
        free(framebuffer);
        free(ordpos);
        return flames_midas_fail();
    }
    /* initialise MAXDISCARDFRACT from keyword */
    if (SCKRDD(MAXDISCARDFRACT, 1, 1, &actvals, &maxdiscardfract, &unit,
                    &null) != 0) {
        /* problems reading MAXDISCARDFRACT */
        SCTPUT("Error reading the MAXDISCARDFRACT keyword");
        free(myflats);
        free(framebuffer);
        free(ordpos);
        return flames_midas_fail();
    }
    /* initialise MAXBACKITERS from keyword */
    if (SCKRDI(MAXBACKITERS, 1, 1, &actvals, &maxbackiters, &unit,
                    &null) != 0) {
        /* problems reading MAXBACKITERS */
        SCTPUT("Error reading the MAXBACKITERS keyword");
        free(myflats);
        free(framebuffer);
        free(ordpos);
        return flames_midas_fail();
    }
    /* initialise MAXCORRITERS from keyword */
    if (SCKRDI(MAXCORRITERS, 1, 1, &actvals, &imaxcorriters, &unit,
                    &null) != 0) {
        /* problems reading MAXCORRITERS */
        SCTPUT("Error reading the MAXCORRITERS keyword");
        free(myflats);
        free(framebuffer);
        free(ordpos);
        return flames_midas_fail();
    }
    /* initialise MAXSINGLEPXFRC from keyword */
    if (SCKRDD(MAXSINGLEPXFRC, 1, 1, &actvals, &maxsinglepixelfrac, &unit,
                    &null) != 0) {
        /* problems reading MAXSINGLEPXFRC */
        SCTPUT("Error reading the MAXSINGLEPXFRC keyword");
        free(myflats);
        free(framebuffer);
        free(ordpos);
        return flames_midas_fail();
    }
    /* initialise MAXCLEANITERS from keyword */
    if (SCKRDI(MAXCLEANITERS, 1, 1, &actvals, &imaxcleaniters, &unit,
                    &null) != 0) {
        /* problems reading MAXCLEANITERS */
        SCTPUT("Error reading the MAXCLEANITERS keyword");
        free(myflats);
        free(framebuffer);
        free(ordpos);
        return flames_midas_fail();
    }
    maxcleaniters = (int32_t) imaxcleaniters;
    /* initialise FRACSLICESTHRES from keyword */
    if (SCKRDD(FRACSLICESTHRES, 1, 1, &actvals, &fracslicesthres, &unit,
                    &null) != 0) {
        /* problems reading FRACSLICESTHRES */
        SCTPUT("Error reading the FRACSLICESTHRES keyword");
        free(myflats);
        free(framebuffer);
        free(ordpos);
        return flames_midas_fail();
    }
    /* initialise background fitting scalars */
    if (SCKRDI(BKGPOL, 1, 1, &actvals, &bxdegree, &unit, &null)
                    != 0) {
        /* problems reading xdegree */
        SCTPUT("Error reading the x degree of the background polynomial");
        free(myflats);
        free(framebuffer);
        free(ordpos);
        return flames_midas_fail();
    }
    if (SCKRDI(BKGPOL, 2, 1, &actvals, &bydegree, &unit, &null)
                    != 0) {
        /* problems reading xdegree */
        SCTPUT("Error reading the y degree of the background polynomial");
        free(myflats);
        free(framebuffer);
        free(ordpos);
        return flames_midas_fail();
    }
    /* before trying to read it, make sure that BKGFITMETHOD exists and it is
     of the appropriate type */
    if (SCKFND_string(BKGFITMETHOD, &keytype, &noelem, &bytelem) != 0) {
        /* SCKFND failed, give up! */
        SCTPUT("Internal MIDAS error in prepfibreff: SCKFND failed");
        free(myflats);
        free(framebuffer);
        free(ordpos);
        return flames_midas_fail();
    }
    switch(keytype) {
    case 'C':
        /* it exists and it is a character keyword, go ahead and read it */
        if (SCKGETC(BKGFITMETHOD, 1, CATREC_LEN, &nval, bkgfitmethod)
                        != 0) {
            /* problems reading BKGFITMETHOD */
            SCTPUT("Warning: error reading the BKGFITMETHOD keyword, falling back to \
default");
        }
        else {
            /* is the string long enough to be unambiguous? */
            if (nval<2) {
                /* no, fall back to the default */
                SCTPUT("Warning: BKGFITMETHOD is ambiguous, falling back to default");
            }
            else {
                /* convert bkgfitmethod to upper case, to ease the subsequent check */
                for (i=0; i<=(nval-1); i++) bkgfitmethod[i] = toupper(bkgfitmethod[i]);
                /* compare as many letters as we have with the expected values */
                if (strncmp("ALL", bkgfitmethod, (size_t) nval) == 0)
                    bkgswitch = USEALL;
                else if (strncmp("MEDIAN", bkgfitmethod, (size_t) nval)
                                == 0) bkgswitch = USEMEDIAN;
                else if (strncmp("MINIMUM", bkgfitmethod, (size_t) nval)
                                == 0) bkgswitch = USEMINIMUM;
                else if (strncmp("AVERAGE", bkgfitmethod, (size_t) nval)
                                == 0) bkgswitch = USEAVERAGE;
                else {
                    SCTPUT("Warning: unsupported BKGFITMETHOD value, falling back to \
default");
                }
            }
        }
        break;
    case ' ':
        /* the keyword does not exist at all */
        SCTPUT("Warning: BKGFITMETHOD is undefined, falling back to default");
        break;
    default:
        /* the keyword is of the wrong type */
        SCTPUT("Warning: BKGFITMETHOD is not a string, falling back to default");
        break;
    }

    /* before trying to read it, make sure that BKGFITMETHOD exists and it is
     of the appropriate type */
    if (SCKFND_string(BKGBADSCAN, &keytype, &noelem, &bytelem) != 0) {
        /* SCKFND failed, give up! */
        SCTPUT("Internal MIDAS error in mainopt: SCKFND failed");
        free(myflats);
        free(framebuffer);
        free(ordpos);
        return flames_midas_fail();
    }
    switch(keytype) {
    case 'C':
        /* it exists and it is a character keyword, go ahead and read it */
        if (SCKGETC(BKGBADSCAN, 1, CATREC_LEN, &nval, bkgfitmethod)
                        != 0) {
            /* problems reading BKGFITMETHOD */
            SCTPUT("Warning: error reading the BKGBADSCAN keyword, falling back to \
default");
        }
        else {
            /* is the string long enough to be unambiguous? */
            if (nval<1) {
                /* no, fall back to the default */
                SCTPUT("Warning: BKGBADSCAN is ambiguous, falling back to default");
            }
            else {
                /* convert bkgfitmethod to upper case, to ease the subsequent check */
                for (i=0; i<=(nval-1); i++) bkgfitmethod[i] = toupper(bkgfitmethod[i]);
                /* compare as many letters as we have with the expected values */
                if (strncmp("NONE", bkgfitmethod, (size_t) nval) == 0)
                    bkgswitch2 = NOBADSCAN;
                else if (strncmp("FRACTION", bkgfitmethod, (size_t) nval)
                                == 0) bkgswitch2 = FRACBADSCAN;
                else if (strncmp("ABSOLUTE", bkgfitmethod, (size_t) nval)
                                == 0) bkgswitch2 = ABSBADSCAN;
                else {
                    SCTPUT("Warning: unsupported BKGBADSCAN value, falling back to \
default");
                }
            }
        }
        break;
    case ' ':
        /* the keyword does not exist at all */
        SCTPUT("Warning: BKGBADSCAN is undefined, falling back to default");
        break;
    default:
        /* the keyword is of the wrong type */
        SCTPUT("Warning: BKGBADSCAN is not a string, falling back to default");
        break;
    }

    /* if neighborhood bad pixel scanning was requested, read the other
     keywords needed */
    if (bkgswitch2 == FRACBADSCAN) {
        if ((SCKRDI(BKGBADWIN, 1, 1, &actvals, &badwinxsize, &unit, &null) != 0)
                        || (SCKRDI(BKGBADWIN, 2, 1, &actvals, &badwinysize, &unit, &null)
                                        != 0)) {
            SCTPUT("Error reading the BKGBADWIN keyword");
            free(myflats);
            free(framebuffer);
            free(ordpos);
            return flames_midas_fail();
        }
        if (SCKRDD(BKGBADMAXFRAC, 1, 1, &actvals, &badfracthres, &unit,
                        &null) != 0) {
            SCTPUT("Error reading the BKGBADMAXFRAC keyword");
            free(myflats);
            free(framebuffer);
            free(ordpos);
            return flames_midas_fail();
        }
        /* check the values read for consistency */
        if ((badwinxsize < 0) || (badwinysize < 0)) {
            SCTPUT("Warning: BKGBADWIN values must be non negative, disabling \
BKGBADSCAN");
            bkgswitch2 = NOBADSCAN;
        }
        if (badfracthres < 0) {
            SCTPUT("Warning: BKGBADMAXFRAC value must be non negative, disabling \
BKGBADSCAN");
            bkgswitch2 = NOBADSCAN;
        }
    }
    else if (bkgswitch2 == ABSBADSCAN) {
        if ((SCKRDI(BKGBADWIN, 1, 1, &actvals, &badwinxsize, &unit, &null) != 0)
                        || (SCKRDI(BKGBADWIN, 2, 1, &actvals, &badwinysize, &unit, &null)
                                        != 0)) {
            SCTPUT("Error reading the BKGBADWIN keyword");
            free(myflats);
            free(framebuffer);
            free(ordpos);
            return flames_midas_fail();
        }
        if (SCKRDI(BKGBADMAXTOT, 1, 1, &actvals, &badtotthres, &unit,
                        &null) != 0) {
            SCTPUT("Error reading the BKGBADMAXTOT keyword");
            free(myflats);
            free(framebuffer);
            free(ordpos);
            return flames_midas_fail();
        }
        /* check the values read for consistency */
        if ((badwinxsize < 0) || (badwinysize < 0)) {
            SCTPUT("Warning: BKGBADWIN values must be non negative, disabling \
BKGBADSCAN");
            bkgswitch2 = NOBADSCAN;
        }
        if (badtotthres < 0) {
            SCTPUT("Warning: BKGBADMAXTOT value must be non negative, disabling \
BKGBADSCAN");
            bkgswitch2 = NOBADSCAN;
        }
    }

    /* read the kappa factor to be used later in kappa-sigma clipping */
    if ((status=SCKRDD(SIGMA, 1, 1, &actvals, &kappa, &unit, &null))!=0) {
        /* something went wrong while reading the kappa-sigma factor */
        SCTPUT("Error while reading SIGMA keyword");
        free(myflats);
        free(framebuffer);
        free(ordpos);
        return flames_midas_fail();
    }
    /* compute once and for all the square of kappa, as we will be using that */
    kappa2 = kappa*kappa;

    /* first open the catalog and count the FF frames present */
    myflats->nflats = 0;
    entrynum = 0;
    do {
        if ((status = SCCGET(catname, 0 , filename, identifier, &entrynum)) != 0) {
            /* error getting catalog entry */
            return flames_midas_fail();
        }
        /* did I get a valid catalog entry? */
        if (filename[0] != ' ') {
            myflats->nflats++;
        }
    } while (filename[0] != ' ');

    /* check that nflats > 0, otherwise, well... */
    if (myflats->nflats == 0) {
        return flames_midas_fail();
    }

    /* read the first frame */
    entrynum = 0;
    if ((status = SCCGET(catname, 0 , filename, identifier, &entrynum)) != 0) {
        /* error getting catalog entry */
        return flames_midas_fail();
    }
    if ((status = readframe(framebuffer, filename)) != NOERR) {
        /* error reading frame */
        return flames_midas_fail();
    }
    /* copy some scalars from framebuffer to myflats */
    myflats->subrows = framebuffer->subrows;
    myflats->subcols = framebuffer->subcols;
    myflats->maxfibres = framebuffer->maxfibres;
    myflats->substartx = framebuffer->substartx;
    myflats->substarty = framebuffer->substarty;
    myflats->substepx = framebuffer->substepx;
    myflats->substepy = framebuffer->substepy;
    myflats->chipchoice = framebuffer->chipchoice;
    myflats->ron = framebuffer->ron;
    myflats->gain = framebuffer->gain;

    /* read in ordpos, in order to fill in the last scalars in myflats */
    /* initialise the ordpos structure from the dummy table descriptors */
    if((status = readordpos(ordername, ordpos)) != NOERR) {
        /* something went wrong in the initialisation */
        return flames_midas_fail();
    }

    /* read the GAUSSCORRELSCL keyword */
    if (SCKRDD(GAUSSCORRELSCL, 1, 1, &actvals, &gausscorrelscale, &unit,
                    &null) != 0) {
        /* problems reading GAUSSCORRELSCL */
        SCTPUT("Error reading the GAUSSCORRELSCL keyword");
        return flames_midas_fail();
    }
    if (gausscorrelscale<0) {
        SCTPUT("Error: illegal (negative) value for GAUSSCORRELSCL");
        return flames_midas_fail();
    }
    /* read the GAUSSCORRELWND keyword */
    if (SCKRDD(GAUSSCORRELWND, 1, 1, &actvals, &gausscorrelwindow, &unit,
                    &null) != 0) {
        /* problems reading GAUSSCORRELWND */
        SCTPUT("Error reading the GAUSSCORRELWND keyword");
        return flames_midas_fail();
    }
    if (gausscorrelwindow<0) {
        SCTPUT("Error: illegal (negative) value for GAUSSCORRELWND");
        return flames_midas_fail();
    }
    /* we define the default pgausswidth to be proportional to halfibrewidth,
     say a quarter of it or so, and this can be tuned by a parameter in 
     flames_uves.h */
    ordpos->pgausssigma = fabs(gausscorrelscale*ordpos->halfibrewidth/
                    framebuffer->substepy);
    ordpos->pgausshalfwidth = gausscorrelwindow*ordpos->pgausssigma;
    maxcorriters = (int32_t) imaxcorriters;

    /* read the MAXYSHIFT keyword */
    if (SCKRDD(MAXYSHIFT, 1, 1, &actvals, &shiftwindow, &unit, &null) != 0) {
        /* problems reading MAXYSHIFT */
        SCTPUT("Error reading the MAXYSHIFT (P8) parameter");
        return flames_midas_fail();
    }
    if (shiftwindow<0) {
        SCTPUT("Warning: illegal value for MAXYSHIFT (P8), fall back to 0");
        shiftwindow = 0;
    }
    /* read the CORRELTOL keyword */
    if (SCKRDD(CORRELTOL, 1, 1, &actvals, &shifttol, &unit, &null) != 0) {
        /* problems reading CORRELTOL */
        SCTPUT("Error reading the CORRELTOL keyword");
        return flames_midas_fail();
    }
    if (shifttol<0) {
        SCTPUT("Error: illegal (negative) value for CORRELTOL");
        return flames_midas_fail();
    }
    /* read the CORRELXSTEP keyword */
    if (SCKRDI(CORRELXSTEP, 1, 1, &actvals, &icorrelxstep, &unit, &null) != 0){
        /* problems reading CORRELXSTEP */
        SCTPUT("Error reading the CORRELXSTEP keyword");
        return flames_midas_fail();
    }
    if (icorrelxstep<1) {
        SCTPUT("Warning: illegal (<1) value for CORRELTOL, falling back to 1");
        icorrelxstep=1;
    }
    correlxstep = (int32_t) icorrelxstep;


    for (ifibre=0; ifibre<=(ordpos->maxfibres-1); ifibre++)
        ordpos->gaussselfshift[ifibre]=0;

    /* before trying to read it, make sure that GAUSSFIBRESIGMA exists and it is
     of the appropriate type */
    if (SCKFND_double(GAUSSFIBRESIGMA, &keytype, &noelem, &bytelem) != 0) {
        /* SCKFND failed, give up! */
        SCTPUT("Internal MIDAS error in prepfibreff: SCKFND failed");
        return flames_midas_fail();
    }

    assert (keytype == 'D' || keytype == ' ');
    //AMo: removed support of 'R' and 'I' types.
    switch(keytype) {
    case 'D':
        /* it exists and it is a double keyword, go ahead and read it */
        if (SCKRDD(GAUSSFIBRESIGMA, 1, 1, &actvals, &dbuf,
                        &unit, &null) != 0) {
            /* problems reading GAUSSFIBRESIGMA */
            SCTPUT("Error reading the GAUSSFIBRESIGMA keyword");
            return flames_midas_fail();
        }
        if (dbuf<=0) {
            SCTPUT("Warning: GAUSSFIBRESIGMA must be >0, reverting to default");
        }
        else {
            ordpos->pgausssigma = dbuf;
        }
        break;
    case ' ':
        /* the keyword does not exist at all, use the default value */
        break;
    default:
        /* the keyword is of the wrong type */
        SCTPUT("Warning: GAUSSFIBRESIGMA is not a number, falling back to \
default");
        break;
    }

    /* before trying to read it, make sure that GAUSSHALFWIDTH exists and it is
     of the appropriate type */
    if (SCKFND_double(GAUSSHALFWIDTH, &keytype, &noelem, &bytelem) != 0) {
        /* SCKFND failed, give up! */
        SCTPUT("Internal MIDAS error in prepfibreff: SCKFND failed");
        return flames_midas_fail();
    }

    assert (keytype == 'D' || keytype == ' ');
    //AMo: removed support of 'R' and 'I' types.


    switch(keytype) {
    case 'D':
        /* it exists and it is a double keyword, go ahead and read it */
        if (SCKRDD(GAUSSHALFWIDTH, 1, 1, &actvals, &dbuf,
                        &unit, &null) != 0) {
            /* problems reading GAUSSHALFWIDTH */
            SCTPUT("Error reading the GAUSSHALFWIDTH keyword");
            return flames_midas_fail();
        }
        if (dbuf<=0) {
            SCTPUT("Warning: GAUSSHALFWIDTH must be >0, reverting to default");
        }
        else {
            ordpos->pgausshalfwidth = dbuf;
        }
        break;
    case ' ':
        /* the keyword does not exist at all, use the default value */
        break;
    default:
        /* the keyword is of the wrong type */
        SCTPUT("Warning: GAUSSHALFWIDTH is not a number, falling back to \
default");
        break;
    }


    /* check whether frames and order chip choices match */
    if (ordpos->chipchoice != myflats->chipchoice) {
        /* no, they don't match */
        SCTPUT("Error: chip mismatch between frames and order table");
        return flames_midas_fail();
    }

    /* is this an already corrected order table? */
    if (ordpos->corrected == 't') {
        /* yes, it is, and it must not! */
        SCTPUT("Error: this procedure must use an uncorrected order table");
        return flames_midas_fail();
    }
    else if (ordpos->corrected != 'f') {
        /* meaningless value of the corrected flag */
        SCTPUT("Error: the CORRECTED descriptor must be either t or f");
        return flames_midas_fail();
    }

    /* initialise scalars in myflats from values in ordpos */
    myflats->halfibrewidth = ordpos->halfibrewidth;
    myflats->firstorder = ordpos->firstorder;
    myflats->lastorder = ordpos->lastorder;
    myflats->tab_io_oshift = ordpos->tab_io_oshift;
    myflats->shiftable = 'y';
    myflats->normalised ='n';

    /* allocate some internal arrays of myflats */
    if ((status = allocallflats2(myflats)) != NOERR) {
        /* error allocating myflats arrays */
        return flames_midas_fail();
    }
    /* copy pointers from framebuffer to first myflats single flat frame */
    if ((status = frame2flat(framebuffer, myflats, 0)) != NOERR) {
        /* error copying data */
        return flames_midas_fail();
    }
    /* free unused arrays in framebuffer, so that it can be reused */
    if ((status = freeframe2(framebuffer)) != NOERR) {
        /* error freing memory */
        return flames_midas_fail();
    }

    /* now run a loop to fetch the other FF frames */
    for (entrynum=1; entrynum<=myflats->nflats-1; entrynum++) {
        i = entrynum;
        /* get filename */
        if ((status = SCCGET(catname, 0 , filename, identifier, &i)) != 0) {
            /* error getting catalog entry */
            return flames_midas_fail();
        }
        /* read frame */
        if ((status = readframe(framebuffer, filename)) != NOERR) {
            /* error reading frame */
            return flames_midas_fail();
        }
        /* copy pointers and data from framebuffer to first myflats single flat
       frame */
        if ((status = frame2flat(framebuffer, myflats, entrynum)) != NOERR) {
            /* error copying data */
            return flames_midas_fail();
        }
        /* free unused arrays in framebuffer, so that it can be reused */
        if ((status = freeframe2(framebuffer)) != NOERR) {
            /* error freing memory */
            return flames_midas_fail();
        }
    }

    /* as last touch, initialise scalars taken from MIDAS context */
    if ((status = SCKRDD(MINFIBREFRAC, 1, 1, &actvals, &myflats->minfibrefrac,
                    &unit, &null)) != 0) {
        /* problems getting value from MINFIBREFRAC keyword */
        return flames_midas_fail();
    }

    /* everything ready, finally do our job: fill those darn holes */
    if ((status = fillholes(myflats, ordpos, slitsname, backname, bxdegree,
                    bydegree, bkgswitch, bkgswitch2, badwinxsize,
                    badwinysize, badfracthres, badtotthres, kappa2,
                    decentsnr, (int32_t) maxbackiters,
                    maxdiscardfract, maxcleaniters, maxsinglepixelfrac,
                    fracslicesthres, OUTPUTI))
                    != NOERR) {
        /* problems filling holes */
        return flames_midas_fail();
    }

    /* insert here the code to first determine the self-shift correction
     for gausscorrel and friends */
    SCTPUT("Finding self-shift corrections for gaussian cross-correlation");
    if (gaussselfcorrel(myflats,
                    ordpos,
                    maxcorriters,
                    shiftwindow,
                    shifttol,
                    correlxstep,
                    "middumma.fits")) {
        SCTPUT("Error during cross-correlation calculation");
        return flames_midas_fail();
    }

    /* write the self-shift corrections to disk, as descriptors of the order
     table */
    if (TCTOPN(ordername, F_D_MODE, &tid)!= 0) {
        /* I could not open the input order table: protest... */
        sprintf(output, "Error: I couldn't open the %s table\n", ordername);
        SCTPUT(output);
        return flames_midas_fail();
    }
    /* write the scalars first */
    if (SCDWRD(tid, "GAUSSFIBRESIGMA", &ordpos->pgausssigma, 1, 1, &unit) != 0) {
        sprintf (output, "Error writing GAUSSFIBRESIGMA descriptor in %s table",
                        ordername);
        SCTPUT(output);
        return flames_midas_fail();
    }
    if (SCDWRD(tid, "GAUSSHALFWIDTH", &ordpos->pgausshalfwidth, 1, 1, &unit)
                    != 0) {
        sprintf (output, "Error writing GAUSSHALFWIDTH descriptor in %s table",
                        ordername);
        SCTPUT(output);
        return flames_midas_fail();
    }
    /* Find the first and last fibre which were left standing */
    for (ifibre=0;
    		ifibre<=(ordpos->maxfibres-1) && ordpos->fibremask[ifibre]==FALSE;
                    ifibre++);
    if (ordpos->fibremask[ifibre]==FALSE) {
        /* ouch, no fibre was left standing, bail out */
        SCTPUT("Error: no fibre with adequate coverage is left!");
        return flames_midas_fail();
    }
    firstifibre = lastifibre = ifibre;
    for (ifibre++; ifibre<=(ordpos->maxfibres-1); ifibre++) {
        if (ordpos->fibremask[ifibre]==TRUE) lastifibre = ifibre;
    }


    /* compute the order centre between the first and last lit fibre */
    newcentre = (ordpos->fibrepos[firstifibre]+ordpos->fibrepos[lastifibre])/2;

    uves_msg_debug("first, last, newcentre = %d, %d, %f\n", firstifibre, lastifibre, newcentre);

    /* move the order position and related data accordingly */
    ordpos->orderpol[0][0] += newcentre;
    ordpos->tab_io_yshift += newcentre;
    /* move the fibre positions in the opposite direction, so that the sum
     does not change, of course */
    for (ifibre=0; ifibre<=(ordpos->maxfibres-1); ifibre++) {
        ordpos->fibrepos[ifibre] -= newcentre;
    }
    /* write this stuff appropriately to the order table */
    if (SCDWRI(tid, "FIBREMASK", ordpos->fibremask, 1,
                    ordpos->maxfibres, &unit) != 0) {
        sprintf (output, "Error writing FIBREMASK descriptor in %s table",
                        ordername);
        SCTPUT(output);
        return flames_midas_fail();
    }
    if (SCDWRD(tid, "FIBREPOS", ordpos->fibrepos, 1, ordpos->maxfibres,
                    &unit)!=0) {
        sprintf (output, "Error writing FIBREPOS descriptor in %s table",
                        ordername);
        SCTPUT(output);
        return flames_midas_fail();
    }
    if (SCDWRD(tid, "COEFFD", ordpos->orderpol[0], 1, 1, &unit)!=0) {
        sprintf (output, "Error writing COEFFD descriptor in %s table",
                        ordername);
        SCTPUT(output);
        return flames_midas_fail();
    }
    if (SCDWRD(tid, "TAB_IN_OUT_YSHIFT", &ordpos->tab_io_yshift, 1, 1,
                    &unit)!=0) {
        sprintf (output, "Error writing TAB_IN_OUT_YSHIFT descriptor in %s table",
                        ordername);
        SCTPUT(output);
        return flames_midas_fail();
    }
    if (SCDWRD(tid, "GAUSSSELFSHIFT", ordpos->gaussselfshift, 1,
                    ordpos->maxfibres, &unit) != 0) {
        sprintf (output, "Error writing GAUSSSELFSHIFT descriptor in %s table",
                        ordername);
        SCTPUT(output);
        return flames_midas_fail();
    }
    /* we can close the table now */
    if (TCTCLO(tid)!=0) {
        /* I could not close the order table: protest... */
        sprintf(output, "Error: I couldn't close the %s table\n", ordername);
        SCTPUT(output);
        return flames_midas_fail();
    }


    /* dump myflats with filled holes to disk */
    if ((status = writeallff(myflats, basename, outcatname)) != NOERR) {
        /* error writing myflats to disk */
        return flames_midas_fail();
    }

    /* time to free memory */
    /* free the internal arrays of myflats */
    if ((status = freeallflats(myflats)) != NOERR) {
        /* error freeing myflats arrays */
        return flames_midas_fail();
    }
    /* free myflats itself */
    free(myflats);
    /* the pointers to the internal arrays of framebuffer were moved to
     myflats, and therefore have been freed already; I only need to 
     free framebuffer itself */
    free(framebuffer);
    /* free the internal members of ordpos */
    if ((status = freeordpos(ordpos)) != NOERR) {
        /* error freeing ordpos arrays */
        return flames_midas_fail();
    }
    /* free ordpos itself */
    free(ordpos);

    /* it's over, exit MIDAS and return*/
    return SCSEPI();

}
