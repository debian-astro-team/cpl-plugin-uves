## Process this file with automake to produce Makefile.in

##   This file is part of the UVES Pipeline
##   Copyright (C) 2002,2003 European Southern Observatory

##   This library is free software; you can redistribute it and/or modify
##   it under the terms of the GNU General Public License as published by
##   the Free Software Foundation; either version 2 of the License, or
##   (at your option) any later version.

##   This program is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##   GNU General Public License for more details.

##   You should have received a copy of the GNU General Public License
##   along with this program; if not, write to the Free Software
##   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

AUTOMAKE_OPTIONS = 1.8 foreign

DISTCLEANFILES = *~

SUBDIRS = . tests

LIBQFITS=$(top_builddir)/libqfits/src/libqfits.la
QFITS_INCLUDES = -I$(top_srcdir)/libqfits/src/


if MAINTAINER_MODE

MAINTAINERCLEANFILES = Makefile.in

endif


AM_CPPFLAGS = $(all_includes) $(QFITS_INCLUDES)

pkginclude_HEADERS =

noinst_HEADERS     = flames_cal_prep_sff_ofpos_impl.h  \
                     flames_utils.h        \
                     flames_utils_science.h        \
                     flames_pfits.h        \
                     flames.h              \
                     flames_dfs.h          \
                     flames_crea_bp_ima.h  \
                     flames_creamask.h     \
		     flames_preppa_impl.h  \
		     flames_prepframe.h    \
                     flames_def_drs_par.h  \
                     flames_uves.h         \
                     flames_fillholes.h    \
                     flames_fillnormfactors.h \
			flames_doquickoptimal.h           \
			flames_doquickstandard.h           \
			flames_gauss_jordan.h           \
                     flames_mvfit.h        \
                     flames_nrmacros.h     \
                     flames_shiftall.h     \
		     flames_midas_tblsys.h \
		     flames_midas_tblerr.h \
		     flames_midas_tbldef.h \
		     flames_midas_macrogen.h \
		     flames_midas_atype.h  \
                     flames_preordpos.h    \
                     flames_ordpos.h       \
                     flames_multimatch.h   \
                     flames_matchorders.h  \
                     flames_create_ordertable.h \
                     flames_create_backtable.h \
                     flames_create_full_ordertable.h \
                     flames_fitting.h \
                     flames_tracing.h \
                     flames_add_extra_des.h \
                     flames_prepfibreff.h \
                     flames_mainoptFF.h \
                     flames_mainstandFF.h \
                     flames_mainoptquickFF.h \
                     flames_mainstandquickFF.h \
                     flames_fastprepfibreff.h \
                     flames_get_flat_size.h \
                     flames_prepslitff.h \
                     flames_mainshift.h \
                     flames_fileutils.h \
                     flames_dointerpolate.h \
                     flames_mainslitdivide.h \
                     flames_backfit.h \
			flames_add_bt_info.h \
			flames_dointerpolate.h \
                     flames_covariance_reorder.h \
                     flames_mainopt.h \
                     flames_mainoptquick.h \
                     flames_mainoptfast.h \
                     flames_mainstand.h \
                     flames_mainstandquick.h \
                     flames_mainstandfast.h \
                     flames_cubify.h \
                     flames_shiftcommon.h \
			flames_checksize.h \
			flames_checksize2.h \
			flames_checksize3.h \
			flames_Stand_Extract.h \
			flames_standard.h \
			flames_freespectrum.h \
			flames_allocspectrum.h \
			flames_computeback.h \
			flames_merge.h \
			flames_getordpos.h \
			flames_getordslope.h \
			flames_shift_all_FF.h \
			flames_shift_FF_n.h \
			flames_copy_FF_n.h \
			flames_fastfillholes.h \
			flames_fastlocatefibre.h \
			flames_readordpos.h \
			flames_readslitflats.h \
			flames_allocslitflats.h \
			flames_allocallflats.h \
			flames_allocallflats2.h \
			flames_alloconeflats.h \
			flames_initemplate.h \
			flames_alloctemplate.h \
			flames_allocframe.h \
			flames_initbadpixel.h \
			flames_allocback.h \
			flames_mergebadpixels.h \
			flames_readback.h \
			flames_readframe.h \
			flames_allocordpos.h \
			flames_freeordpos.h \
			flames_slitdivide.h \
			flames_ffslitmultiply.h \
			flames_freeslitflats.h \
			flames_freeframe.h \
			flames_freetemplate.h \
			flames_writeframe.h \
			flames_writesigma.h \
			flames_writeback.h \
			flames_initframe.h \
			flames_frame2flat.h \
			flames_freeframe2.h \
			flames_medianfilterframe.h \
			flames_stripfitsext.h \
			flames_striptblext.h \
			flames_freeallflats.h \
			flames_freeallflats2.h \
			flames_freeoneflats.h \
			flames_writeallff.h \
			flames_writenormfactors.h \
			flames_readallff.h \
			flames_freeback.h           \
			flames_readallff0.h \
			flames_readslit0.h \
			flames_readslit.h \
			flames_initallflatsout.h \
			flames_initshiftedff.h \
			flames_prepextract.h \
			flames_quickprepextract.h \
			flames_prepstand.h \
			flames_quickprepstand.h \
			flames_ordselect.h \
			flames_doptimal.h \
			flames_dostandard.h \
			flames_gausscorrelFF.h \
			flames_gausscorrel.h \
			flames_gaussselfcorrel.h \
			flames_singlecorrel.h \
			flames_optimal.h \
			flames_quickoptimal.h \
			flames_optsynth.h           \
			flames_quickoptextract.h \
			flames_Opt_Extract.h \
			flames_opterrors.h \
			flames_sigma_clip.h \
			flames_writesynth.h           \
			flames_correl.h \
			flames_scatter.h \
			flames_trimback.h \
			flames_write_spectra.h \
			flames_cveltab.h \
			flames_writeslitff.h \
			flames_lsfit.h 



privatelib_LTLIBRARIES = libflames.la

#		        flames_extract_impl.c
#		        flames_fillordtab_impl.c
#		        flames_mes_slitff_size_impl.c
#		        flames_prepbkg_impl.c
#		        flames_prepfibreff_impl.c
#		        flames_prepnorm_impl.c
#		        flames_prepslitff_impl.c

libflames_la_SOURCES = 	flames_cal_orderpos_impl.c        \
                        flames_cal_mkmaster_impl.c         \
                        flames_cal_predict_impl.c         \
                        flames_cal_wavecal_impl.c         \
                        flames_obs_scired_impl.c          \
		        flames_cal_prep_sff_ofpos_impl.c  \
		        flames_redchain_impl.c  \
		        flames_add_extra_des.c            \
                        flames_utils.c                    \
			flames_utils_science.c        \
                        flames_pfits.c                    \
			flames_dfs.c                      \
			flames_crea_bp_ima.c              \
                        flames_def_drs_par.c              \
		        flames_preppa_impl.c              \
			flames_Opt_Extract.c              \
			flames_Optimal.c                  \
			flames_Stand_Extract.c            \
			flames_Standard.c                 \
			flames_allocallflats.c            \
			flames_allocallflats2.c           \
			flames_allocback.c           \
			flames_allocframe.c           \
			flames_alloconeflats.c           \
			flames_allocordpos.c           \
			flames_allocslitflats.c           \
			flames_allocspectrum.c           \
			flames_alloctemplate.c           \
			flames_calcfillshifts.c           \
			flames_calcshifts.c           \
			flames_checksize.c           \
			flames_checksize2.c           \
			flames_checksize3.c           \
			flames_cloneallflats.c           \
			flames_computeback.c           \
			flames_covariance_reorder.c           \
			flames_creamask.c           \
			flames_create_backtable.c           \
			flames_create_full_ordertable.c           \
			flames_add_bt_info.c           \
			flames_create_ordertable.c           \
			flames_dointerpolate.c           \
			flames_doptimal.c           \
			flames_doquickoptimal.c           \
			flames_doquickstandard.c           \
			flames_dostandard.c           \
			flames_fastfillholes.c           \
			flames_fastlocatefibre.c           \
			flames_fastprepfibreff.c           \
			flames_ffslitmultiply.c           \
			flames_fillholes.c           \
			flames_fillnormfactors.c           \
			flames_gauss_jordan.c           \
			flames_fitting.c           \
			flames_merge.c           \
			flames_tracing.c           \
			flames_frame2flat.c           \
			flames_freeallflats.c           \
			flames_freeback.c           \
			flames_freeframe.c           \
			flames_freeframe2.c           \
			flames_freeoneflats.c           \
			flames_freeordpos.c           \
			flames_freeslitflats.c           \
			flames_freespectrum.c           \
			flames_freetemplate.c           \
			flames_gausscorrel.c           \
			flames_gausscorrelFF.c           \
			flames_gaussselfcorrel.c         \
			flames_getordpos.c           \
			flames_getordslope.c           \
			flames_initallflatsout.c           \
			flames_initbadpixel.c           \
			flames_initemplate.c           \
			flames_initfillfibre.c           \
			flames_initframe.c           \
			flames_initshiftedff.c           \
			flames_locatefibre.c           \
			flames_locatefillfibre.c           \
			flames_mainopt.c           \
			flames_mainoptfast.c           \
			flames_mainoptquick.c         \
			flames_mainshift.c           \
			flames_mainslitdivide.c           \
			flames_mainstand.c           \
			flames_mainstandquick.c        \
			flames_mainstandfast.c          \
			flames_matchorders.c           \
			flames_medianfilterframe.c           \
			flames_mergebadpixels.c           \
			flames_multimatch.c           \
			flames_mvfit.c           \
			flames_opterrors.c           \
			flames_optsynth.c           \
			flames_ordselect.c           \
			flames_preordpos.c           \
			flames_prepextract.c           \
			flames_prepfibreff.c           \
			flames_prepframe.c           \
			flames_prepslitff.c           \
			flames_prepstand.c           \
			flames_quickoptextract.c           \
			flames_quickoptimal.c           \
			flames_quickprepextract.c           \
			flames_quickprepstand.c           \
			flames_readallff.c           \
			flames_readback.c           \
			flames_readframe.c           \
			flames_readordpos.c           \
			flames_readslit.c           \
			flames_readslit0.c           \
			flames_readslitflats.c       \
			flames_scatter.c           \
			flames_selectavail.c           \
			flames_selectfillavail.c           \
			flames_shiftall.c           \
			flames_shiftone.c           \
			flames_sigma_clip.c           \
			flames_singlecorrel.c           \
			flames_slitdivide.c           \
			flames_stripbdfext.c           \
			flames_striptblext.c           \
			flames_trimback.c           \
			flames_writeallff.c           \
			flames_writeback.c           \
			flames_writeframe.c           \
			flames_writenormfactors.c           \
			flames_writesigma.c           \
			flames_writeslitff.c           \
			flames_writespectrum.c           \
			flames_writesynth.c           \
			flames_get_flat_size.c           \
			flames_cubify.c           \
			flames_fileutils.c         \
			flames_mainoptquickFF.c      \
			flames_mainstandFF.c         \
			flames_mainstandquickFF.c    \
			flames_mainstripbdfext.c     \
			flames_mainoptFF.c           \
                        flames_ordpos.c              \
			flames_backfit.c           \
			flames_lsfit.c 







#			flames_ipercube_io.c
#not used		flames_correl.c


libflames_la_LDFLAGS = $(LDFLAGS) $(CPL_LDFLAGS) $(CFITSIO_LDFLAGS) -version-info $(LT_CURRENT):$(LT_REVISION):$(LT_AGE)
libflames_la_LIBADD = $(LIBUVES) $(LIBCPLDFS) $(LIBCPLUI) $(LIBCPLDRS) $(LIBCPLCORE) $(LIBCFITSIO) $(LIBQFITS)
libflames_la_DEPENDENCIES = $(LIBUVES)
