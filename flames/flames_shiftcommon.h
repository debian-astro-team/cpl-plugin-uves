/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2 of
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with this program; if not, write to the Free
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge,
  MA 02139, USA.

  Corresponding concerning ESO-MIDAS should be addressed as follows:
      Internet e-mail: midas@eso.org
      Postal address: European Southern Observatory
                      Data Management Division
                      Karl-Schwarzschild-Strasse 2
                       D 85748 Garching bei Muenchen
                       GERMANY
 ===========================================================================*/
#ifndef FLAMES_SHIFTCOMMON_H
#define FLAMES_SHIFTCOMMON_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/* The following structure contains all data necessary for fibre shifting. 
   The data are supposed to be valid for one specified order and x position, 
   hence arrays of these structures may be used in certain cases. */
typedef struct _shiftstruct
{
    int32_t *ixoffsets; /* the ix offsets to be used (pixel units) */
    double *yfracoffsets; /* the y offsets corresponding to the above ix
                 offsets */
    int32_t *yintoffsets; /* the integer y offsets to be used to access
                  the pixels (pixel units)*/
    int32_t numoffsets; /* the number of offsets actually stored
                   in the above vectors */
    double ordercentre; /* the centre of this order at this ix
             (world coordinates)*/
    double orderslope; /* the slope of this order at this ix
            (world coordinates) */
    double *normfactor; /* the relative normalisation factors */
    double *normsigma; /* the sigmas of the above */
    char *goodoverlap; /* which shifted ixs have good (0) or
                   bad (1) overlaps */
} shiftstruct;

/* The following structure contains all pixel values from which a bad 
   pixel will be interpolated */
typedef struct _fitstruct
{
    int32_t availpixels;
    double *offset; /* the y offsets relative to the precise requested
               position */
    double *value; /* the pixel values from which we interpolate */
    double *sigma; /* sigmas of the above pixel values */
} fitstruct;

/* The following structure contains data necessary for fibre FF cleansing. 
   The data are supposed to be valid for one specified order, fibre and x 
   position, hence arrays of these structures will be used. */
typedef struct _badixstruct
{
    int32_t *badiy; /* this vector contains the list of bad iys
                   for the given fibre/ix */
    int32_t badiycount; /* the number of bad iys in this given
                   fibre/ix */
    int32_t badix; /* the ix containing these bad pixels to be
                  cleaned */
    int32_t nextbadindex; /* the index of the next ix containing
                     bad pixels in this fibre */
    int32_t prevbadindex; /* the index of the next ix containing
                     bad pixels in this fibre */
} badixstruct;

/* The following structure contains data necessary for fibre FF cleansing. 
   The data are supposed to be valid for one specified order and fibre, 
   hence arrays of these structures will be used. */
typedef struct _badifibrestruct
{
    badixstruct *badixs; /* a vector of badystruct structures, one for each
              ix containing bad pixels to be cleaned*/
    int32_t badixcount; /* the number of ixs containing bad pixels
                   in this fibre */
    int32_t firstbadixindex; /* the index of the first ix
                    containing bad pixels in this fibre */
    int32_t lastbadixindex; /* the index of the last ix containing
                       bad pixels in this fibre */
} badifibrestruct;

/* this structure contains the normalisation factors, their sigmas and a 
   flag signalling good/bad (0/1) overlap between the ix slices to be used 
   for for bad pixel filling in the fibre FF frames; one issue of this 
   structure will be used for each offset to be used for filling, hence an
   array of these structures will be necessary */
typedef struct _normstruct
{
    double normfactor;
    double normsigma;
    char goodoverlap;
} normstruct;

#endif
