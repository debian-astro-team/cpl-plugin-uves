/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
    Internet e-mail: midas@eso.org
    Postal address: European Southern Observatory
            Data Management Division 
            Karl-Schwarzschild-Strasse 2
            D 85748 Garching bei Muenchen 
            GERMANY
===========================================================================*/

/*-------------------------------------------------------------------------*/
/**
 * @defgroup flames_dotandard
 *
 */
/*---------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/
/**
   @name  flames_dostandard()  
   @short this function performs the actual standard extraction on a frame,
   "quick and dirty" no y shift correction flavour
   @author G. Mulas  -  ITAL_FLAMES Consortium. Ported to CPL by A. Modigliani

   @param ScienceFrame  all fibre frame to be optimally extracted
   @param Order         fibre-order position table
   @param Shifted_FF    shifted flat field structure
   @param mask          bad pixel mask frame
   @param pfibrecentre  array describing fibre centre traces 
   @param phalfwinsize  half extraction window size
   @param realfirstorder first order to be extracted
   @param reallastorder  last order to be extracted

   @return success or failure code, generates the Extracted science frame 

   DRS Functions called:          
   ordselect()
   standard()

   Pseudocode:                                                             
   for ord_start< ord <ord_end                                           
   if (no adjacent order overlapping)                                
   standard extract ord                                           
   endif                                                             
   endfor                                                               


   @note
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <flames_midas_def.h>
#include <flames_standard.h>
#include <flames_def_drs_par.h>
#include <flames_ordselect.h>
#include <flames_dostandard.h>
#include <flames_uves.h>


flames_err 
dostandard(flames_frame *ScienceFrame, 
           orderpos *Order, 
           allflats *Shifted_FF, 
           frame_mask **mask, 
           double ***pfibrecentre, 
           double phalfwinsize, 
           int32_t realfirstorder, 
           int32_t reallastorder)
{

    int32_t ordsta=0, ordend=0;
    char output[100];

    int actvals=0;
    char drs_verbosity[10];

    memset(drs_verbosity, 0, 10);
    if ( SCKGETC(DRS_VERBOSITY, 1, 3, &actvals, drs_verbosity) != 0) {
        /* the keyword seems undefined, protest... */
        return(MAREMMA);
    }

    for (ordsta=ordend=realfirstorder; ordsta<=reallastorder;
                    ordsta=++ordend) {
        /* at least we extract from ordsta to ordend */
        /* select a set of orders which is disjoint from other orders */
        if (ordselect(Order, ScienceFrame, Shifted_FF, &ordend)!=NOERR) {
            SCTPUT("Error selecting an order subset to estract\n");
            return flames_midas_error(MAREMMA);
        }

        /* A little bit of advertising */
        sprintf(output, "Extracting orders from nr. %d to nr. %d....",
                ordsta, ordend);
        SCTPUT(output);
        memset(output, '\0', 70);

        /* let's make the standard extraction */
        if (standard(ScienceFrame, Shifted_FF, Order, ordsta, ordend,
                        mask, pfibrecentre, phalfwinsize)==NOERR) {

            if ( strcmp(drs_verbosity,"LOW") == 0 ){
            } else {

                sprintf(output,
                                "Standard extraction for orders from nr. %d to %d completed\n",
                                ordsta, ordend);
                SCTPUT(output);
            }
            memset(output, 0, 70);
        }
        else {
            sprintf(output, "Error in standard extraction\n");
            SCTPUT(output);
            return flames_midas_error(MAREMMA);
        }
    }

    return NOERR;

}
/**@}*/
