/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.

  Corresponding concerning ESO-MIDAS should be addressed as follows:
    Internet e-mail: midas@eso.org
    Postal address: European Southern Observatory
            Data Management Division 
            Karl-Schwarzschild-Strasse 2
            D 85748 Garching bei Muenchen 
            GERMANY
===========================================================================*/
/* Program  : writesynth.c                                                 */
/* Author   : G. Mulas  -  ITAL_FLAMES Consortium                          */
/* Date     :                                                              */
/*                                                                         */
/* Purpose  : Missing                                                      */
/*                                                                         */
/*                                                                         */
/* Input:  see interface                                                   */ 
/*                                                                      */
/* Output:                                                              */
/*                                                                         */
/* DRS Functions called:                                                   */
/* none                                                                    */ 
/*                                                                         */ 
/* Pseudocode:                                                             */
/* Missing                                                                 */ 
/*                                                                         */ 
/* Version  :                                                              */
/* Last modification date: 2002/08/05                                      */
/* Who     When        Why                Where                            */
/* AMo     02-08-05   Add header         header                            */
/*-------------------------------------------------------------------------*/


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <flames_midas_def.h>
#include <flames_writesynth.h>
#include <flames_uves.h>
#include <string.h>

flames_err writesynth(flames_frame *ScienceFrame, 
                      const char *synthname, 
                      const char *sigmaname, 
                      const char *badname)
{

    int naxis=0;
    int npix[2]={0,0};
    int unit=0;
    int synthid=0;
    int sigmaid=0;
    int badid=0;

    float cuts[4]={0,0,0,0};
    frame_data fdminimum=0, fdmaximum=0;

    double start[2]={0,0};
    double step[2]={0,0};

    char ident[73];
    char cunit[3][16];

    frame_data *fdvecbuf1=0;
    frame_data fdbuf1=0;
    int32_t iyixindex=0;
    int32_t iyixend=0;


    memset(ident, '\0', 73);
    memset(cunit[0], '\0', 48);

    iyixend = (ScienceFrame->subrows*ScienceFrame->subcols)-1;
    npix[0] = ScienceFrame->subcols;
    npix[1] = ScienceFrame->subrows;
    naxis = 2;
    cuts[0] = 0;
    cuts[1] = 0;
    start[0] = ScienceFrame->substartx;
    start[1] = ScienceFrame->substarty;
    step[0] = ScienceFrame->substepx;
    step[1] = ScienceFrame->substepy;

    strncpy(cunit[0], "                ", 16);
    strncpy(cunit[1], "PIXEL           ", 16);
    strncpy(cunit[2], "PIXEL           ", 16);

    if(SCFCRE(synthname, FLAMESDATATYPE, F_O_MODE, F_IMA_TYPE,
                    ScienceFrame->subcols*ScienceFrame->subrows,
                    &synthid)) {
        /* could not create the data file */
        SCFCLO(synthid);
        return MAREMMA;
    }

    /* write standard descriptors of file */
    if (SCDWRC(synthid, "IDENT", 1, ident, 1, 72, &unit)) {
        SCFCLO(synthid);
        return MAREMMA;
    }
    if (SCDWRI(synthid, "NAXIS", &naxis, 1, 1, &unit)) {
        SCFCLO(synthid);
        return MAREMMA;
    }
    if (SCDWRI(synthid, "NPIX", npix, 1, 2, &unit)) {
        SCFCLO(synthid);
        return MAREMMA;
    }
    if (SCDWRD(synthid, "START", start, 1, 2, &unit)) {
        /* error writing descriptor */
        SCFCLO(synthid);
        return MAREMMA;
    }
    if (SCDWRD(synthid, "STEP", step, 1, 2, &unit)) {
        SCFCLO(synthid);
        return MAREMMA;
    }
    if (SCDWRC(synthid, "CUNIT", 1, cunit[0], 1, 48, &unit)) {
        SCFCLO(synthid);
        return MAREMMA;
    }
    fdvecbuf1 = ScienceFrame->frame_array[0];
    fdminimum = fdmaximum = fdvecbuf1[0];
    for (iyixindex=1; iyixindex<=iyixend; iyixindex++) {
        fdbuf1 = fdvecbuf1[iyixindex];
        if (fdbuf1<fdminimum) fdminimum = fdbuf1;
        if (fdbuf1>fdmaximum) fdmaximum = fdbuf1;
    }
    cuts[2] = (float) fdminimum;
    cuts[3] = (float) fdmaximum;
    if (SCDWRR(synthid, "LHCUTS", cuts, 1, 4, &unit)) {
        SCFCLO(synthid);
        return MAREMMA;
    }

    if(SCFPUT(synthid, 1, ScienceFrame->subcols*ScienceFrame->subrows,
                    (char *) ScienceFrame->frame_array[0])) {
        SCFCLO(synthid);
        return MAREMMA;
    }
    SCFCLO(synthid);


    if(SCFCRE(sigmaname, FLAMESDATATYPE, F_O_MODE, F_IMA_TYPE,
                    ScienceFrame->subcols*ScienceFrame->subrows,
                    &sigmaid)) {
        /* could not create the data file */
        SCFCLO(sigmaid);
        return MAREMMA;
    }

    /* write standard descriptors of file */
    if (SCDWRC(sigmaid, "IDENT", 1, ident, 1, 72, &unit)) {
        SCFCLO(sigmaid);
        return MAREMMA;
    }
    if (SCDWRI(sigmaid, "NAXIS", &naxis, 1, 1, &unit)) {
        SCFCLO(sigmaid);
        return MAREMMA;
    }
    if (SCDWRI(sigmaid, "NPIX", npix, 1, 2, &unit)) {
        SCFCLO(sigmaid);
        return MAREMMA;
    }
    if (SCDWRD(sigmaid, "START", start, 1, 2, &unit)) {
        /* error writing descriptor */
        SCFCLO(sigmaid);
        return MAREMMA;
    }
    if (SCDWRD(sigmaid, "STEP", step, 1, 2, &unit)) {
        SCFCLO(sigmaid);
        return MAREMMA;
    }
    if (SCDWRC(sigmaid, "CUNIT", 1, cunit[0], 1, 48, &unit)) {
        SCFCLO(sigmaid);
        return MAREMMA;
    }
    fdvecbuf1 = ScienceFrame->frame_sigma[0];
    fdminimum = fdmaximum = fdvecbuf1[0];
    for (iyixindex=1; iyixindex<=iyixend; iyixindex++) {
        fdbuf1 = fdvecbuf1[iyixindex];
        if (fdbuf1<fdminimum) fdminimum = fdbuf1;
        if (fdbuf1>fdmaximum) fdmaximum = fdbuf1;
    }
    cuts[2] = (float) fdminimum;
    cuts[3] = (float) fdmaximum;
    if (SCDWRR(sigmaid, "LHCUTS", cuts, 1, 4, &unit)) {
        SCFCLO(sigmaid);
        return MAREMMA;
    }

    if(SCFPUT(sigmaid, 1, ScienceFrame->subcols*ScienceFrame->subrows,
                    (char *) ScienceFrame->frame_sigma[0])) {
        SCFCLO(sigmaid);
        return MAREMMA;
    }
    SCFCLO(sigmaid);


    if(SCFCRE(badname, FLAMESMASKTYPE, F_O_MODE, F_IMA_TYPE,
                    ScienceFrame->subcols*ScienceFrame->subrows,
                    &badid)) {
        /* could not create the data file */
        SCFCLO(badid);
        return MAREMMA;
    }

    /* write standard descriptors of file */
    if (SCDWRC(badid, "IDENT", 1, ident, 1, 72, &unit)) {
        SCFCLO(badid);
        return MAREMMA;
    }
    if (SCDWRI(badid, "NAXIS", &naxis, 1, 1, &unit)) {
        SCFCLO(badid);
        return MAREMMA;
    }
    if (SCDWRI(badid, "NPIX", npix, 1, 2, &unit)) {
        SCFCLO(badid);
        return MAREMMA;
    }
    if (SCDWRD(badid, "START", start, 1, 2, &unit)) {
        /* error writing descriptor */
        SCFCLO(badid);
        return MAREMMA;
    }
    if (SCDWRD(badid, "STEP", step, 1, 2, &unit)) {
        SCFCLO(badid);
        return MAREMMA;
    }
    if (SCDWRC(badid, "CUNIT", 1, cunit[0], 1, 48, &unit)) {
        SCFCLO(badid);
        return MAREMMA;
    }
    cuts[2] = 0;
    cuts[3] = 5;
    if (SCDWRR(badid, "LHCUTS", cuts, 1, 4, &unit)) {
        SCFCLO(badid);
        return MAREMMA;
    }

    if(SCFPUT(badid, 1, ScienceFrame->subcols*ScienceFrame->subrows,
                    (char *) ScienceFrame->badpixel[0])) {
        SCFCLO(badid);
        return MAREMMA;
    }
    SCFCLO(badid);


    return(NOERR);

}

flames_err write_flames_frame_data(flames_frame *ScienceFrame,const char *data_name)
{
    int synthid=0;
    int naxis=0;
    int unit=0;
    int npix[2]={0,0};
    double start[2]={0,0};
    double step[2]={0,0};
    char ident[73];
    char cunit[3][16];
    float cuts[4]={0,0,0,0};
    frame_data *fdvecbuf1=0;
    frame_data fdminimum=0, fdmaximum=0;

    int32_t iyixindex=0;
    int32_t iyixend=0;

    memset(ident, '\0', 73);
    memset(cunit[0], '\0', 48);

    naxis = 2;
    npix[0] = ScienceFrame->subcols;
    npix[1] = ScienceFrame->subrows;

    start[0] = ScienceFrame->substartx;
    start[1] = ScienceFrame->substarty;
    step[0] = ScienceFrame->substepx;
    step[1] = ScienceFrame->substepy;

    strncpy(cunit[0], "                ", 16);
    strncpy(cunit[1], "PIXEL           ", 16);
    strncpy(cunit[2], "PIXEL           ", 16);

    cuts[0] = 0;
    cuts[1] = 0;


    if(SCFCRE(data_name, FLAMESDATATYPE, F_O_MODE, F_IMA_TYPE,
                    ScienceFrame->subcols*ScienceFrame->subrows,
                    &synthid)) {
        /* could not create the data file */
        SCFCLO(synthid);
        return MAREMMA;
    }

    /* write standard descriptors of file */
    if (SCDWRC(synthid, "IDENT", 1, ident, 1, 72, &unit)) {
        SCFCLO(synthid);
        return MAREMMA;
    }
    if (SCDWRI(synthid, "NAXIS", &naxis, 1, 1, &unit)) {
        SCFCLO(synthid);
        return MAREMMA;
    }
    if (SCDWRI(synthid, "NPIX", npix, 1, 2, &unit)) {
        SCFCLO(synthid);
        return MAREMMA;
    }
    if (SCDWRD(synthid, "START", start, 1, 2, &unit)) {
        /* error writing descriptor */
        SCFCLO(synthid);
        return MAREMMA;
    }
    if (SCDWRD(synthid, "STEP", step, 1, 2, &unit)) {
        SCFCLO(synthid);
        return MAREMMA;
    }
    if (SCDWRC(synthid, "CUNIT", 1, cunit[0], 1, 48, &unit)) {
        SCFCLO(synthid);
        return MAREMMA;
    }
    fdvecbuf1 = ScienceFrame->frame_array[0];
    fdminimum = fdmaximum = fdvecbuf1[0];
    for (iyixindex=1; iyixindex<=iyixend; iyixindex++) {
        frame_data fdbuf1 = fdvecbuf1[iyixindex];
        if (fdbuf1<fdminimum) fdminimum = fdbuf1;
        if (fdbuf1>fdmaximum) fdmaximum = fdbuf1;
    }
    cuts[2] = (float) fdminimum;
    cuts[3] = (float) fdmaximum;
    if (SCDWRR(synthid, "LHCUTS", cuts, 1, 4, &unit)) {
        SCFCLO(synthid);
        return MAREMMA;
    }

    if(SCFPUT(synthid, 1, ScienceFrame->subcols*ScienceFrame->subrows,
                    (char *) ScienceFrame->frame_array[0])) {
        SCFCLO(synthid);
        return MAREMMA;
    }
    SCFCLO(synthid);

    return(NOERR);
}


flames_err write_flames_frame_sigma(flames_frame *ScienceFrame,const char *sigma_name)
{
    int sigmaid=0;
    int naxis=0;
    int unit=0;
    int npix[2]={0,0};
    double start[2]={0,0};
    double step[2]={0,0};
    char ident[73];
    char cunit[3][16];
    float cuts[4]={0,0,0,0};
    frame_data *fdvecbuf1=0;
    frame_data fdminimum=0, fdmaximum=0;

    int32_t iyixindex=0;
    int32_t iyixend=0;

    memset(ident, '\0', 73);
    memset(cunit[0], '\0', 48);

    naxis = 2;
    npix[0] = ScienceFrame->subcols;
    npix[1] = ScienceFrame->subrows;

    start[0] = ScienceFrame->substartx;
    start[1] = ScienceFrame->substarty;
    step[0] = ScienceFrame->substepx;
    step[1] = ScienceFrame->substepy;

    strncpy(cunit[0], "                ", 16);
    strncpy(cunit[1], "PIXEL           ", 16);
    strncpy(cunit[2], "PIXEL           ", 16);

    cuts[0] = 0;
    cuts[1] = 0;
    if(SCFCRE(sigma_name, FLAMESDATATYPE, F_O_MODE, F_IMA_TYPE,
                    ScienceFrame->subcols*ScienceFrame->subrows,
                    &sigmaid)) {
        /* could not create the data file */
        SCFCLO(sigmaid);
        return MAREMMA;
    }

    /* write standard descriptors of file */
    if (SCDWRC(sigmaid, "IDENT", 1, ident, 1, 72, &unit)) {
        SCFCLO(sigmaid);
        return MAREMMA;
    }
    if (SCDWRI(sigmaid, "NAXIS", &naxis, 1, 1, &unit)) {
        SCFCLO(sigmaid);
        return MAREMMA;
    }
    if (SCDWRI(sigmaid, "NPIX", npix, 1, 2, &unit)) {
        SCFCLO(sigmaid);
        return MAREMMA;
    }
    if (SCDWRD(sigmaid, "START", start, 1, 2, &unit)) {
        /* error writing descriptor */
        SCFCLO(sigmaid);
        return MAREMMA;
    }
    if (SCDWRD(sigmaid, "STEP", step, 1, 2, &unit)) {
        SCFCLO(sigmaid);
        return MAREMMA;
    }
    if (SCDWRC(sigmaid, "CUNIT", 1, cunit[0], 1, 48, &unit)) {
        SCFCLO(sigmaid);
        return MAREMMA;
    }
    fdvecbuf1 = ScienceFrame->frame_sigma[0];
    fdminimum = fdmaximum = fdvecbuf1[0];
    for (iyixindex=1; iyixindex<=iyixend; iyixindex++) {
        frame_data fdbuf1 = fdvecbuf1[iyixindex];
        if (fdbuf1<fdminimum) fdminimum = fdbuf1;
        if (fdbuf1>fdmaximum) fdmaximum = fdbuf1;
    }
    cuts[2] = (float) fdminimum;
    cuts[3] = (float) fdmaximum;
    if (SCDWRR(sigmaid, "LHCUTS", cuts, 1, 4, &unit)) {
        SCFCLO(sigmaid);
        return MAREMMA;
    }

    if(SCFPUT(sigmaid, 1, ScienceFrame->subcols*ScienceFrame->subrows,
                    (char *) ScienceFrame->frame_sigma[0])) {
        SCFCLO(sigmaid);
        return MAREMMA;
    }
    SCFCLO(sigmaid);

    return(NOERR);
}


flames_err write_flames_frame_mask(flames_frame *ScienceFrame,const char *mask_name)
{
    int badid=0;
    int naxis=0;
    int unit=0;
    int npix[2]={0,0};
    double start[2]={0,0};
    double step[2]={0,0};
    char ident[73];
    char cunit[3][16];
    float cuts[4]={0,0,0,0};


    memset(ident, '\0', 73);
    memset(cunit[0], '\0', 48);

    naxis = 2;
    npix[0] = ScienceFrame->subcols;
    npix[1] = ScienceFrame->subrows;

    start[0] = ScienceFrame->substartx;
    start[1] = ScienceFrame->substarty;
    step[0] = ScienceFrame->substepx;
    step[1] = ScienceFrame->substepy;

    strncpy(cunit[0], "                ", 16);
    strncpy(cunit[1], "PIXEL           ", 16);
    strncpy(cunit[2], "PIXEL           ", 16);

    cuts[0] = 0;
    cuts[1] = 0;

    if(SCFCRE(mask_name, FLAMESMASKTYPE, F_O_MODE, F_IMA_TYPE,
                    ScienceFrame->subcols*ScienceFrame->subrows,
                    &badid)) {
        /* could not create the data file */
        SCFCLO(badid);
        return MAREMMA;
    }

    /* write standard descriptors of file */
    if (SCDWRC(badid, "IDENT", 1, ident, 1, 72, &unit)) {
        SCFCLO(badid);
        return MAREMMA;
    }
    if (SCDWRI(badid, "NAXIS", &naxis, 1, 1, &unit)) {
        SCFCLO(badid);
        return MAREMMA;
    }
    if (SCDWRI(badid, "NPIX", npix, 1, 2, &unit)) {
        SCFCLO(badid);
        return MAREMMA;
    }
    if (SCDWRD(badid, "START", start, 1, 2, &unit)) {
        /* error writing descriptor */
        SCFCLO(badid);
        return MAREMMA;
    }
    if (SCDWRD(badid, "STEP", step, 1, 2, &unit)) {
        SCFCLO(badid);
        return MAREMMA;
    }
    if (SCDWRC(badid, "CUNIT", 1, cunit[0], 1, 48, &unit)) {
        SCFCLO(badid);
        return MAREMMA;
    }
    cuts[2] = 0;
    cuts[3] = 5;
    if (SCDWRR(badid, "LHCUTS", cuts, 1, 4, &unit)) {
        SCFCLO(badid);
        return MAREMMA;
    }

    if(SCFPUT(badid, 1, ScienceFrame->subcols*ScienceFrame->subrows,
                    (char *) ScienceFrame->badpixel[0])) {
        SCFCLO(badid);
        return MAREMMA;
    }
    SCFCLO(badid);



    return(NOERR);
}
