/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
  Internet e-mail: midas@eso.org
  Postal address: European Southern Observatory
  Data Management Division 
  Karl-Schwarzschild-Strasse 2
  D 85748 Garching bei Muenchen 
  GERMANY
  ===========================================================================*/
/*-------------------------------------------------------------------------*/
/**
 * @defgroup flames_allocslitflats   Substep: Initialize a slitflats frame
 *
 */
/*-------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <flames_midas_def.h>
#include <flames_uves.h>
#include <flames_newmatrix.h>
#include <flames_allocslitflats.h>
/**@{*/

/*---------------------------------------------------------------------------
  Implementation
  ---------------------------------------------------------------------------*/
/**
   @name  flames_allocslitflats()  
   @short   Initialize a slitflat frame
   @author G. Mulas  -  ITAL_FLAMES Consortium. Ported to CPL by A. Modigliani

   @param slitflats pointer to a allslitflat frame

   @return initialized frame


   DRS Functions called:                                                  
   fdmatrix                                                    
   fmmatrix                                                    
   lmatrix                                                     
                                                                        
   Pseudocode:                                                            
   use fdmatrix,fmmatrix,lmatrix                              
   to initialize to NULL data                                 

   @note
*/

flames_err allocslitflats(allslitflats *slitflats)
{
  int32_t iframe=0;

  /* allocate the slit member array itself */
  slitflats->slit = 
    (slitFF *) calloc((size_t)slitflats->nflats, sizeof(slitFF));
  /* allocate submembers of slit */
  for (iframe=0; iframe<=slitflats->nflats-1; iframe++) {
    slitflats->slit[iframe].data = 
      fdmatrix(0, slitflats->subrows-1, 0, slitflats->subcols-1);
    slitflats->slit[iframe].sigma = 
      fdmatrix(0, slitflats->subrows-1, 0, slitflats->subcols-1);
    slitflats->slit[iframe].badpixel = 
      fmmatrix(0, slitflats->subrows-1, 0, slitflats->subcols-1);
    slitflats->slit[iframe].framename = cvector(0, CATREC_LEN);
    slitflats->slit[iframe].sigmaname = cvector(0, CATREC_LEN);
    slitflats->slit[iframe].badname = cvector(0, CATREC_LEN);
    slitflats->slit[iframe].boundname = cvector(0, CATREC_LEN);
    slitflats->slit[iframe].lowbound = 
      lmatrix(0, slitflats->lastorder-slitflats->firstorder, 
	      0, slitflats->subcols-1);
    slitflats->slit[iframe].highbound = 
      lmatrix(0, slitflats->lastorder-slitflats->firstorder, 
	      0, slitflats->subcols-1);
  }
  /* now allocate allslitflats members */
  slitflats->normfactor = 
    fdmatrix(0, slitflats->lastorder-slitflats->firstorder, 
	     0, slitflats->subcols-1);
  slitflats->lowbound = 
    lmatrix(0, slitflats->lastorder-slitflats->firstorder, 
	    0, slitflats->subcols-1);
  slitflats->highbound = 
    lmatrix(0, slitflats->lastorder-slitflats->firstorder, 
	    0, slitflats->subcols-1);
  slitflats->goodx = 
    fmmatrix(0, slitflats->lastorder-slitflats->firstorder, 
	     0, slitflats->subcols-1);

  /* ok, finished */
  return(NOERR);

}
/**@}*/
