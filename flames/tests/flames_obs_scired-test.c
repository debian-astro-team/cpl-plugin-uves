/*                                                                            *
 *   This file is part of the ESO UVES Pipeline                               *
 *   Copyright (C) 2004,2005 European Southern Observatory                    *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                           */
 
/*
 * $Author: amodigli $
 * $Date: 2012-12-04 16:30:12 $
 * $Revision: 1.26 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.25  2012/11/19 09:16:16  jtaylor
 * replace all (u)long ints with (u)int32_t in flames
 *
 * except the fitsio arguments in load_frame() from uves/flames_midas_def.c
 * the rest of uves is unchanged
 * (uves/uves_deque.c must not be changed for compatibility with cpl)
 *
 * required for 64 bit compatibility
 * the midas fits io does not support 8 byte long integers.
 * longs are 4 byte on 32 bit and 8 byte on 64 bit so the simplest fix is
 * to replace them all with the size which is known to work.
 * An alternative would be to check and fix all io related functions
 * this would require adding a 4 byte integer tensor
 *
 * Revision 1.24  2012/07/09 08:41:53  amodigli
 * fixed cppcheck (memleak) error
 *
 * Revision 1.23  2012/04/27 09:17:37  amodigli
 * fixed mem leak indicated by jenkins static checks
 *
 * Revision 1.22  2010/12/10 10:25:18  amodigli
 * changed call to function uves_merge_orders() interface to reflect recent changes
 *
 * Revision 1.21  2009/06/05 05:55:26  amodigli
 * updated init/end to cpl5
 *
 * Revision 1.20  2009/04/14 07:20:22  amodigli
 * fixed API change compiler errors
 *
 * Revision 1.19  2008/12/23 08:29:54  amodigli
 * replace // with proper C comments
 *
 * Revision 1.18  2008/09/29 08:25:09  amodigli
 * fixed compiler warning
 *
 * Revision 1.17  2008/06/20 14:18:39  amodigli
 * fixed bug due to API change
 *
 * Revision 1.16  2008/04/21 06:49:38  amodigli
 * added test_unpack
 *
 * Revision 1.15  2008/01/17 17:27:22  amodigli
 * case with simcal fiber
 *
 * Revision 1.14  2008/01/10 16:32:36  amodigli
 * added test on spectra packing
 *
 * Revision 1.13  2007/12/15 09:53:11  amodigli
 * updated to reflech change function interface
 *
 * Revision 1.12  2007/10/15 06:59:09  amodigli
 * added check on linetable
 *
 * Revision 1.11  2007/10/01 17:09:51  amodigli
 * test_extract changes to fit new function interface and input location
 *
 * Revision 1.10  2007/09/19 11:53:26  amodigli
 * added test_flames_insert
 *
 * Revision 1.9  2007/09/17 16:06:06  amodigli
 * fixed flames_merge test
 *
 * Revision 1.8  2007/09/17 08:30:46  amodigli
 * added test_flames_merge
 *
 * Revision 1.7  2007/09/13 16:01:57  amodigli
 * added tests
 *
 * Revision 1.6  2007/08/31 14:18:42  amodigli
 * added test on cubify
 *
 * Revision 1.5  2007/08/30 08:18:47  amodigli
 * fixed some doxygen warnings
 *
 * Revision 1.4  2007/08/27 07:08:56  amodigli
 * added test_readordpos
 *
 * Revision 1.3  2007/08/23 14:19:17  amodigli
 * commented test
 *
 * Revision 1.2  2007/08/16 06:58:43  amodigli
 * fixed typo
 *
 * Revision 1.1  2007/08/13 12:12:22  amodigli
 * added to repository
 *
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*----------------------------------------------------------------------------
                                Includes
 ----------------------------------------------------------------------------*/
#include <cpl.h>
#include <cpl_test.h>

#include <flames_utils_science.h>
#include <flames_readslitflats.h>
#include <flames_readallff.h>
#include <flames_merge.h>
#include <flames_def_drs_par.h>
#include <flames_midas_def.h>
#include <flames_readordpos.h>
#include <flames_merge.h>
#include <flames.h>
#include <flames_uves.h>
#include <flames_dfs.h>
#include <uves_msg.h>
#include <uves_dfs.h>
#include <uves_chip.h>
#include <uves_globals.h>
#include <uves_pfits.h>
#include <uves_utils_wrappers.h>
#include <uves_error.h>

#include <uves_error.h>
#include <uves_merge.h>

#include <string.h>
/*----------------------------------------------------------------------------
                                Defines
 ----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
                            Functions prototypes
 ----------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/**
 * @defgroup flames_obs_scired-test  FLAMES science recipe unit tests
 */
/*---------------------------------------------------------------------------*/
/**@{*/


/**
          @brief test version function
*/

static int
flames_add_frame(cpl_frameset** set,const char* name)
{
  cpl_frame* frm=NULL;
  uves_msg_debug("add file=%s",name);

  check_nomsg(frm=cpl_frame_new());
  check_nomsg(cpl_frame_set_filename(frm,name));
  check_nomsg(cpl_frame_set_group(frm,CPL_FRAME_GROUP_CALIB));
  check_nomsg(cpl_frame_set_type(frm,CPL_FRAME_TYPE_IMAGE));
  check_nomsg(cpl_frame_set_level(frm,CPL_FRAME_LEVEL_INTERMEDIATE));
  check_nomsg(cpl_frame_set_tag(frm,"PIPPO"));
  check_nomsg(cpl_frameset_insert(*set,frm));

cleanup:

  if (cpl_error_get_code() != CPL_ERROR_NONE) {
    return -1;
  } else {
    return 0;
  }


}



/*---------------------------------------------------------------------------*/
/**
  @brief   Test flames_load_linetable
  This test exists just to verify that function works
**/
/*---------------------------------------------------------------------------*/


static int
test_flames_load_linetable(void)
{

  char file[MED_NAME_SIZE];
  enum uves_chip chip=UVES_CHIP_REDL;
  cpl_frame* frame=NULL;

  cpl_frameset* frames=NULL;
  bool flames=true;
  const char* chip_name=NULL;
  polynomial *order_locations=NULL;
  int ord_min=106;
  int ord_max=127;
  const char* linetable_filename=NULL;
  cpl_table* parLinTab=NULL;
  uves_propertylist* linetable_header=NULL;
  polynomial  *dispersion_relation=NULL;
  polynomial  *absolute_order=NULL;
    int it=2;
  int abs_ord_min=0;
  int abs_ord_max=0;


  sprintf(file,"linetable_redl.fits");
  frame=cpl_frame_new();
  check_nomsg(cpl_frame_set_tag(frame,"FIB_LINE_TABLE_REDL"));
  check_nomsg(cpl_frame_set_filename(frame,file));
  check_nomsg(linetable_header=uves_propertylist_load(file,0));
  check_nomsg(chip_name=uves_pfits_get_chipid(linetable_header,chip));
  check_nomsg(cpl_frame_set_group(frame,CPL_FRAME_GROUP_CALIB));
  frames=cpl_frameset_new();
  cpl_frameset_insert(frames,frame);


  check_nomsg(uves_load_linetable(frames, flames,chip_name,
                                  order_locations,ord_min, 
                                  ord_max,&linetable_filename,
                                  &parLinTab,&linetable_header,
                                  &dispersion_relation,
                                  &absolute_order, chip, it, -1));

  check_nomsg(abs_ord_min=(int)cpl_table_get_column_min(parLinTab,"Order"));
  check_nomsg(abs_ord_max=(int)cpl_table_get_column_max(parLinTab,"Order"));


  if(cpl_table_has_column(parLinTab,"FIBRE")) {

    check_nomsg(cpl_table_and_selected_float(parLinTab,"FIBRE",
					     CPL_EQUAL_TO,it));
  } else {

    cpl_table_dump(parLinTab,1,3,stdout);

    check_nomsg(cpl_table_and_selected_float(parLinTab,"Fibre",
					     CPL_EQUAL_TO,it));

  }





 cleanup:

  if (cpl_error_get_code() != CPL_ERROR_NONE) {
    uves_msg_warning("error");
    return -1;
  } else {
    uves_msg_warning("success");
    return 0;
  }

}

static int
test_flames_unpack(void)
{
  const char* src_dir="/media/disk/flames/flames_uves_demo/test/";
  const char* pfile="fxb_l_pack.fits";
  char  ifile[MED_NAME_SIZE];
  char  cfile[MED_NAME_SIZE];
  char  tfile[MED_NAME_SIZE];

  int naxis=0;
  int naxis1=0;
  int naxis2=0;
  int naxis3=0;

  uves_propertylist* plist=NULL;

  const int fmin=0;
  const int fmax=9;
  int i=0;
  int pno=1;

  cpl_table* xtbl=NULL;

  sprintf(ifile,"%s%s",src_dir,pfile);
  check_nomsg(plist=uves_propertylist_load(ifile,0));
  check_nomsg(naxis=uves_pfits_get_naxis(plist));
  check_nomsg(naxis1=uves_pfits_get_naxis1(plist));
  check_nomsg(naxis2=uves_pfits_get_naxis2(plist));

  check_nomsg(xtbl=cpl_table_load(tfile,1,0));
  ck0_nomsg(flames_spectra_to_image(ifile,"",cfile,fmin,fmax,xtbl,pno));

 cleanup:

  uves_free_table(&xtbl);

  if (cpl_error_get_code() != CPL_ERROR_NONE) {
    uves_msg_warning("error");
    return -1;
  } else {
    uves_msg_warning("success");
    return 0;
  }



}


static int
test_flames_pack(void)
{

  const char* file="mwfxb_l_";
  const char* cfile="cube.fits";
  const char* tfile="m_tbl_redl.fits";
  const int fmin=2;
  const int fmax=9;
  int i=0;
  int pno=1;

  cpl_table* xtbl=NULL;

  check_nomsg(xtbl=cpl_table_load(tfile,1,0));
  ck0_nomsg(flames_spectra_to_image(file,"",cfile,fmin,fmax,xtbl,pno));

 cleanup:

  uves_free_table(&xtbl);

  if (cpl_error_get_code() != CPL_ERROR_NONE) {
    uves_msg_warning("error");
    return -1;
  } else {
    uves_msg_warning("success");
    return 0;
  }



}

/*---------------------------------------------------------------------------*/
/**
  @brief   Test flames_merge
  This test exists just to verify that function works
**/
/*---------------------------------------------------------------------------*/


static int
test_flames_insert(void)
{

  char sci_file[MED_NAME_SIZE];
  char file[MED_NAME_SIZE];
  char cfile[MED_NAME_SIZE];
  char src[MED_NAME_SIZE];
  enum uves_chip chip=UVES_CHIP_REDU;
  cpl_frameset* frames=NULL;
  const char* start_time="start";
  uves_propertylist* ref_header=NULL;
  const char* recipe_id="test";
  cpl_parameterlist *parameters=NULL;
  cpl_frame* frame=NULL;
  cpl_parameter *p=NULL;;

  strcpy(sci_file,"sci.fits");
  strcpy(file,"fxb_u");
  sprintf(cfile,"%s%s",file,"_pack.fits");
  check_nomsg(ref_header=uves_propertylist_load(sci_file,0));
  uves_msg_warning("file=%s cfile=%s",file,cfile);
  
  /* define a frameset */
  check_nomsg(frames=cpl_frameset_new());

  /* add a raw frame to the frameset */
  check_nomsg(frame=cpl_frame_new());
  check_nomsg(cpl_frame_set_filename(frame,sci_file));
  check_nomsg(cpl_frame_set_tag(frame,"TEST"));
  check_nomsg(cpl_frame_set_group(frame,CPL_FRAME_GROUP_RAW));
  check_nomsg(cpl_frameset_insert(frames,frame));

  /* define one parameter */
  parameters=cpl_parameterlist_new();
  p = cpl_parameter_new_value("parameter",
			      CPL_TYPE_INT,
                              "par_int",
                              "test",
                              2);

  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"test-parameter");
  cpl_parameterlist_append(parameters, p);



  check( flames_frameset_insert(
             frames,
             CPL_FRAME_GROUP_PRODUCT,
             CPL_FRAME_TYPE_IMAGE,
             CPL_FRAME_LEVEL_INTERMEDIATE,
             cfile,
             FLAMES_XB_SCI(chip),
             ref_header,
             parameters,
             recipe_id,
             PACKAGE "/" PACKAGE_VERSION,
             NULL, start_time, true, 0), 
             "Could not add frame '%s' (%s) to frameset",
             cfile, FLAMES_XB_SCI(chip));
      
      uves_msg("Frame '%s' (%s) added to frameset",
           file, FLAMES_XB_SCI(chip));


 cleanup:

  if (cpl_error_get_code() != CPL_ERROR_NONE) {
 uves_msg_warning("error");
    return -1;
  } else {
 uves_msg_warning("success");
    return 0;
  }

}

/*---------------------------------------------------------------------------*/
/**
  @brief   Test flames_merge
  This test exists just to verify that function works
**/
/*---------------------------------------------------------------------------*/


static int
test_flames_merge(void)
{

  char file_ima[MED_NAME_SIZE];
  char file_reb[MED_NAME_SIZE];
  char file_mer[MED_NAME_SIZE];
  char file_sig[MED_NAME_SIZE];
  char file_msk[MED_NAME_SIZE];

  char base_reb[MED_NAME_SIZE];
  char base_mer[MED_NAME_SIZE];

  char inter_ima[MED_NAME_SIZE];
  char inter_sig[MED_NAME_SIZE];
  char inter_msk[MED_NAME_SIZE];

  char des_name[MED_NAME_SIZE];


  char in_ima[MED_NAME_SIZE];
  char in_sig[MED_NAME_SIZE];
  char in_msk[MED_NAME_SIZE];

  int ima_id=0;
  int sig_id=0;
  int msk_id=0;
  int unit=0;
  int null=0;
  int actvals=0;
  int naxis2=0;
  int i=0;
  double* wstart=NULL;
  double* wend=NULL;
  int* nptot=NULL;
  double wstep=0;

  SCSPRO("test_flames_merge");

  strcpy(in_ima,"fxb_l_0002.fits");

  if (SCFOPN(in_ima, FLAMESDATATYPE, 0, F_IMA_TYPE, &ima_id) != 0) {
    SCTPUT("Error opening the extracted spectrum");
    return flames_midas_fail();
  }
  if (SCDRDC(ima_id,"SIGMAFRAME",1,1,48,&actvals,in_sig,&unit,&null)!=0) {
    SCTPUT("Error getting SIGMAFRAME");
    return flames_midas_fail();
  }
  if (SCDRDC(ima_id,"MASKFRAME",1,1,48,&actvals,in_msk,&unit,&null)!=0) {
    SCTPUT("Error getting SIGMAFRAME");
    return flames_midas_fail();
  }

  strcpy(file_reb,"wfxb_l_0002.fits");
  strcpy(file_mer,"mwfxb_l_0002.fits");

  strcpy(base_reb,"wfxb_l_0002");
  strcpy(base_mer,"mwfxb_l_0002");

  sprintf(inter_ima,"%s","wfxb_l_0002.fits");
  sprintf(inter_sig,"%s","wfxb_l_sig0002.fits");
  sprintf(inter_msk,"%s","wfxb_l_extco0002.fits");
  uves_msg_warning("in_sig=%s",in_sig);
  uves_msg_warning("in_msk=%s",in_msk);


  if (SCFOPN(inter_ima, FLAMESDATATYPE, 0, F_IMA_TYPE, &ima_id) != 0) {
    SCTPUT("Error opening the extracted spectrum");
    return flames_midas_fail();
  }
  if (SCDWRC(ima_id, "SIGMAFRAME", 1,inter_sig, 1,48, &unit)) {
    SCFCLO(ima_id); 
    return flames_midas_fail();
  }
  if (SCDWRC(ima_id, "MASKFRAME", 1,inter_sig, 1,48, &unit)) {
    SCFCLO(ima_id); 
    return flames_midas_fail();
  }
  if (SCDRDI(ima_id,"NAXIS2",1,1,&actvals,&naxis2,&unit,&null)){
    SCFCLO(ima_id);
    return flames_midas_fail();
  }


  /* Add some missing descriptor files: WSTART,NPTOT */
  wstart = calloc(naxis2, sizeof(double));
  wend   = calloc(naxis2, sizeof(double));
  nptot  = calloc(naxis2, sizeof(int));
  if (SCDRDD(ima_id,"CDELT1",1,1,&actvals,&wstep,&unit,&null)){
    SCFCLO(ima_id);
    free(nptot);
    free(wstart);
    free(wend);
    return flames_midas_fail();
  }
  for(i=0;i<naxis2;i++) {
    sprintf(des_name,"%s%d","WSTART",i+1);
    if (SCDRDD(ima_id,des_name,1,1,&actvals,&wstart[i],&unit,&null)){
      SCFCLO(ima_id);
      free(nptot);
      return flames_midas_fail();
    }
    /* uves_msg_warning("WSTART[%d]=%f",i,wstart[i]); */
    sprintf(des_name,"%s%d","WEND",i+1);
    if (SCDRDD(ima_id,des_name,1,1,&actvals,&wend[i],&unit,&null)){
      SCFCLO(ima_id);
      free(nptot);
      return flames_midas_fail();
    }
    /* uves_msg_warning("WEND[%d]=%f",i,wend[i]); */
    nptot[i]=(wend[i]-wstart[i])/wstep;
    uves_msg_warning("NPTOT[%d]=%d",i,nptot[i]);
  }
  if (SCDWRD(ima_id, "WSTART", wstart, 1, naxis2, &unit)) {
    SCFCLO(ima_id); 
    free(nptot);
    return flames_midas_fail();
  }
  if (SCDWRI(ima_id, "NPTOT", nptot, 1, naxis2, &unit)) {
    SCFCLO(ima_id); 
    free(nptot);
    return flames_midas_fail();
  }
  SCFCLO(ima_id);

  if (SCFOPN(inter_sig, FLAMESDATATYPE, 0, F_IMA_TYPE, &sig_id) != 0) {
    SCTPUT("Error opening the extracted spectrum");
    free(nptot);
    return flames_midas_fail();
  }
  if (SCDWRC(sig_id, "SCIENCEFRAME", 1,inter_ima, 1,48, &unit)) {
    SCFCLO(sig_id); 
    free(nptot);
    return flames_midas_fail();
  }

  /* Add some missing descriptor files: WSTART,NPTOT */
  if (SCDWRD(ima_id, "WSTART", wstart, 1, naxis2, &unit)) {
    SCFCLO(ima_id); 
    free(nptot);
    return flames_midas_fail();
  }
  if (SCDWRI(ima_id, "NPTOT", nptot, 1, naxis2, &unit)) {
    SCFCLO(ima_id); 
    free(nptot);
    return flames_midas_fail();
  }
  SCFCLO(sig_id);

  if (SCFOPN(inter_msk, FLAMESDATATYPE, 0, F_IMA_TYPE, &msk_id) != 0) {
    SCTPUT("Error opening the extracted spectrum");
    free(nptot);
    return flames_midas_fail();
  }
  if (SCDWRC(msk_id, "SCIENCEFRAME", 1,inter_msk, 1,48, &unit)) {
    SCFCLO(msk_id); 
    free(nptot);
    return flames_midas_fail();
  }

  
  /* Add some missing descriptor files: WSTART,NPTOT */
  if (SCDWRD(ima_id, "WSTART", wstart, 1, naxis2, &unit)) {
    SCFCLO(ima_id); 
    free(nptot);
    return flames_midas_fail();
  }
  if (SCDWRI(ima_id, "NPTOT", nptot, 1, naxis2, &unit)) {
    SCFCLO(ima_id); 
    free(nptot);
    return flames_midas_fail();
  }
  SCFCLO(msk_id);
  SCSEPI();

 uves_msg_warning("mr1");

 ck0_nomsg(flames_merge(inter_ima,inter_sig,inter_msk,base_mer,&MASKTHRES,0,0));

 uves_msg_warning("mr2");


 cleanup:

  if (cpl_error_get_code() != CPL_ERROR_NONE) {
 uves_msg_warning("error");
    return -1;
  } else {
 uves_msg_warning("success");
    return 0;
  }

}

/*---------------------------------------------------------------------------*/
/**
  @brief   Test flames_rebin_loop
  This test exists just to verify that function works
**/
/*---------------------------------------------------------------------------*/


static int
test_flames_rebin_loop(void)
{

  int it=0;
  int fib_min=0;
  int fib_max=0;

  int* fibre_mask_lin=NULL;
  int* fibre_mask_frm=NULL;
  int fib_max_lit=-1;
  int fib_min_lit=10;
  int ord_min=0;
  int ord_max=0;
  const char* filename="ordef_l.fits";
  cpl_table* parFibOrdTab=NULL;
  uves_propertylist* header=NULL;
  const char* chip_name=NULL;
  enum uves_chip chip=UVES_CHIP_REDL;
  cpl_type fibre_mask_type=0;
  int fibre_mask_length=0;



  check_nomsg(parFibOrdTab=cpl_table_load(filename,1,0));
  /* cpl_table_dump(parFibOrdTab,1,3,stdout); */
  check_nomsg(ord_min=(int)cpl_table_get_column_min(parFibOrdTab,"Order"));
  check_nomsg(ord_max=(int)cpl_table_get_column_max(parFibOrdTab,"Order"));
  check_nomsg(fib_min=(int)cpl_table_get_column_min(parFibOrdTab,"FIBRE"));
  check_nomsg(fib_max=(int)cpl_table_get_column_max(parFibOrdTab,"FIBRE"));

  check_nomsg(header=uves_propertylist_load(filename,0));
  check_nomsg(chip_name=uves_pfits_get_chipid(header,chip));
  uves_msg_warning("ok8");
  
  check( fibre_mask_lin = uves_read_midas_array(header, 
                                            "FIBREMASK", 
                                            &fibre_mask_length,
                                            &fibre_mask_type, 
                                            NULL),
	 "Error reading FIBREMASK");


  check( fibre_mask_frm = uves_read_midas_array(header, 
                                            "FIBREMASK", 
                                            &fibre_mask_length,
                                            &fibre_mask_type, 
                                            NULL),
	 "Error reading FIBREMASK");


  for(it = fib_min; it< fib_max; it++) {


    if ( (fibre_mask_lin[it] == 1) && 
         (fibre_mask_frm[it] == 1) ) {
               

      if(it<fib_min_lit) {
	fib_min_lit=it;
      }
      if(it>fib_max_lit) {
	fib_max_lit=it;
      }


      uves_msg_warning("it=%d",it);

    }
  }
  uves_msg_warning("fib lit. Min=%d Max=%d",fib_min_lit,fib_max_lit);

 cleanup:

  if (cpl_error_get_code() != CPL_ERROR_NONE) {
    return -1;
  } else {
    return 0;
  }

}


/*---------------------------------------------------------------------------*/
/**
  @brief   Test flames_readslitflat
  This test exists just to verify that function works
**/
/*---------------------------------------------------------------------------*/


static int
test_flames_readslitflats(void)
{
  char file_com[MED_NAME_SIZE];
  char file_nor[MED_NAME_SIZE];
  char file_bpm[MED_NAME_SIZE];
  char file_bnd[MED_NAME_SIZE];
  char file_dat[MED_NAME_SIZE];
  char file_sig[MED_NAME_SIZE];
  char file_ima[MED_NAME_SIZE];
  int32_t* lowbound=NULL;
  int32_t* highbound=NULL;
  int32_t index=0;
  int j=0;

  const char* src_dir="/data1/flames/flames_demo/";
  const char* inp_base_format="%2.2d";
  cpl_frameset* slitff_set=NULL;
  allslitflats* Slit_FF=NULL;
  int nflats=2;
  int i=0;
  flames_err status=0;
  int32_t iframe=0;
  int sz=0;
  cpl_imagelist* iml=NULL;
  cpl_image* img=NULL;
  double min=0;
  double max=0;
  double med=0;
  double avg=0;
  double std=0;

  SCSPRO("optimal"); 
  sprintf(file_com,"%s%s",src_dir,"slitff_l_common.fits");
  sprintf(file_nor,"%s%s",src_dir,"slitff_l_norm.fits");

  check_nomsg(slitff_set=cpl_frameset_new());

  check_nomsg(flames_add_frame(&slitff_set,file_com));
  check_nomsg(flames_add_frame(&slitff_set,file_nor));
  check_nomsg(iml=cpl_imagelist_load(file_com,CPL_TYPE_FLOAT,0));
  check_nomsg(sz=cpl_imagelist_get_size(iml));
  for(i=0;i<sz;i++) {
    img=cpl_imagelist_get(iml,i);
    sprintf(file_ima,"%s%d%s","check",i,".fits");
    cpl_image_save(img, file_ima, CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT);
    min=cpl_image_get_min(img);
    max=cpl_image_get_max(img);
    avg=cpl_image_get_mean(img);
    std=cpl_image_get_stdev(img);
    uves_msg_warning("statistics: min=%g max=%g avg=%g med=%g std=%g",
	     min,max,avg,med,std);
  }
 
  for(i=1;i<=nflats;i++) {

    sprintf(file_bnd,"%s%s%2.2d%s",src_dir,"slitff_l_bound",i,".fits");
    check_nomsg(flames_add_frame(&slitff_set,file_bnd));

    sprintf(file_dat,"%s%s%2.2d%s",src_dir,"slitff_l_data",i,".fits");
    check_nomsg(flames_add_frame(&slitff_set,file_dat));

    sprintf(file_sig,"%s%s%2.2d%s",src_dir,"slitff_l_sigma",i,".fits");
    check_nomsg(flames_add_frame(&slitff_set,file_sig));

    sprintf(file_bpm,"%s%s%2.2d%s",src_dir,"slitff_l_badpixel",i,".fits");
    check_nomsg(flames_add_frame(&slitff_set,file_bpm));

  }

   /* now allocate the Slit_FF structure, we'll be using it from now on */
   if(!(Slit_FF = calloc(1, sizeof(allslitflats)))) {
     SCTPUT("Allocation error during Slit_FF memory allocation");
     return flames_midas_fail();
   }

  if ((status = readslitflats(slitff_set, Slit_FF)) != NOERR) {
     /* problems reading slitflats members from disk */
     SCTPUT("Error while reading the slit Flat Field frames");
     return flames_midas_fail();
   }

 
  slitFF *myslit=0;
  /* Added check on bounds
  for (iframe=0; (iframe<=Slit_FF->nflats-1) ;iframe++) {
  */
  for (iframe=2; iframe<=2 ;iframe++) {
    myslit = Slit_FF->slit+iframe;
    lowbound=myslit->lowbound[0];
    highbound=myslit->highbound[0];
    for(j=0;j<4;j++) {
      for(index=j*4096+20; index<j*4096+30 ;index++) {
	uves_msg_warning("x=%d y=%d index=%d lowbound=%d highbound=%d\n",
			 index-j*4096,j,index,lowbound[index],highbound[index]);
      }
    }
  }


 cleanup:
  if (cpl_error_get_code() != CPL_ERROR_NONE) {
    return -1;
  } else {
    return 0;
  }

}



/*---------------------------------------------------------------------------*/
/**
  @brief   Test flames_readfibreflats
  This test exists just to verify that function works
**/
/*---------------------------------------------------------------------------*/


static int
test_flames_readfibreflats(void)
{
  char file_com[MED_NAME_SIZE];
  char file_nor[MED_NAME_SIZE];
  char file_nsg[MED_NAME_SIZE];
  char file_bpm[MED_NAME_SIZE];
  char file_bnd[MED_NAME_SIZE];
  char file_dat[MED_NAME_SIZE];
  char file_sig[MED_NAME_SIZE];
  char file_ima[MED_NAME_SIZE];
  int32_t* lowbound=NULL;
  int32_t* highbound=NULL;
  int32_t index=0;
  int j=0;

  const char* src_dir="/data1/flames/flames_demo/";
  const char* inp_base_format="%2.2d";
  cpl_frameset* fibreff_set=NULL;
  allflats* Single_FF=NULL;
  int nflats=2;
  int i=0;
  flames_err status=0;
  int32_t iframe=0;
  int sz=0;
  cpl_imagelist* iml=NULL;
  cpl_image* img=NULL;
  double min=0;
  double max=0;
  double med=0;
  double avg=0;
  double std=0;
  uves_propertylist* plist=NULL;

  SCSPRO("optimal"); 
  sprintf(file_com,"%s%s",src_dir,"fibreff_l_common.fits");
  sprintf(file_nor,"%s%s",src_dir,"fibreff_l_norm.fits");
  sprintf(file_nsg,"%s%s",src_dir,"fibreff_l_nsigma.fits");

  check_nomsg(fibreff_set=cpl_frameset_new());

  check_nomsg(flames_add_frame(&fibreff_set,file_com));
  check_nomsg(flames_add_frame(&fibreff_set,file_nor));
  check_nomsg(flames_add_frame(&fibreff_set,file_nsg));
  check_nomsg(plist=uves_propertylist_load(file_com,0));
  check_nomsg(nflats = uves_propertylist_get_int(plist, "NFLATS"));

 
  for(i=1;i<=nflats;i++) {


    sprintf(file_dat,"%s%s%2.2d%s",src_dir,"fibreff_l_data",i,".fits");
    check_nomsg(flames_add_frame(&fibreff_set,file_dat));

    sprintf(file_sig,"%s%s%2.2d%s",src_dir,"fibreff_l_sigma",i,".fits");
    check_nomsg(flames_add_frame(&fibreff_set,file_sig));

    sprintf(file_bpm,"%s%s%2.2d%s",src_dir,"fibreff_l_badpixel",i,".fits");
    check_nomsg(flames_add_frame(&fibreff_set,file_bpm));

  }
  uves_msg_warning("rk1");

   /* now allocate the Single_FF structure, we'll be using it from now on */
   if(!(Single_FF = calloc(1, sizeof(allflats)))) {
     SCTPUT("Allocation error during Single_FF memory allocation");
     return flames_midas_fail();
   }

  if ((status = readallff(fibreff_set, Single_FF)) != NOERR) {
     /* problems reading fibreflats members from disk */
     SCTPUT("Error while reading the fibre Flat Field frames");
     return flames_midas_fail();
   }

 

 cleanup:
  if (cpl_error_get_code() != CPL_ERROR_NONE) {
    return -1;
  } else {
    return 0;
  }

}

/*---------------------------------------------------------------------------*/
/**
  @brief   Test flames_my_decubify
  This test exists just to verify that function works
**/
/*---------------------------------------------------------------------------*/


static int
test_flames_reset_desc3(void)
{

  char cname[MED_NAME_SIZE];
  char iname[MED_NAME_SIZE];
  int cub_id;
  int ima_id;


  SCSPRO("test");
  sprintf(cname,"%s","slitff_l_dtc.fits");
  sprintf(iname,"%s","slitff_l_data01.fits");

  SCFOPN(cname, FLAMESDATATYPE, 0, F_IMA_TYPE, &cub_id);
  SCFOPN(iname, FLAMESDATATYPE, 0, F_IMA_TYPE, &ima_id);
  uves_msg_warning("This is supposed to work");
  check_nomsg(flames_reset_desc_set2(ima_id,cub_id,1));
  uves_msg_warning("The following should crash");

  /* Set SLITFF */
  check_nomsg(flames_reset_desc_set3(ima_id,cub_id,2,1));
  check_nomsg(flames_reset_desc_set0(ima_id,cub_id,1,1));
  uves_msg_warning("now fibff");
  /* Set FIBFF */

  uves_msg_warning("The following should crash");
  check_nomsg(flames_reset_desc_set4(ima_id,cub_id,2,1));

  uves_msg_warning("Descriptor changed");
  SCFCLO(cub_id);
  SCFCLO(ima_id);



  sprintf(cname,"%s","slitff_l_bpc.fits");
  sprintf(iname,"%s","slitff_l_badpixel01.fits");

  SCFOPN(cname, FLAMESDATATYPE, 0, F_IMA_TYPE, &cub_id);
  SCFOPN(iname, FLAMESDATATYPE, 0, F_IMA_TYPE, &ima_id);

  check_nomsg(flames_reset_desc_set3(ima_id,cub_id,2,1));
  uves_msg_warning("now fibff");

  uves_msg_warning("The following should crash");
  check_nomsg(flames_reset_desc_set4(ima_id,cub_id,2,1));

  uves_msg_warning("Descriptor changed");
  SCFCLO(cub_id);
  SCFCLO(ima_id);



  sprintf(cname,"%s","slitff_l_sgc.fits");
  sprintf(iname,"%s","slitff_l_sigma01.fits");

  SCFOPN(cname, FLAMESDATATYPE, 0, F_IMA_TYPE, &cub_id);
  SCFOPN(iname, FLAMESDATATYPE, 0, F_IMA_TYPE, &ima_id);
  check_nomsg(flames_reset_desc_set3(ima_id,cub_id,2,1));
  uves_msg_warning("now fibff");
  /* Set FIBFF */

  uves_msg_warning("The following should crash");
  check_nomsg(flames_reset_desc_set4(ima_id,cub_id,2,1));

  uves_msg_warning("Descriptor changed");
  SCFCLO(cub_id);
  SCFCLO(ima_id);


  sprintf(cname,"%s","slitff_l_bnc.fits");
  sprintf(iname,"%s","slitff_l_bound01.fits");

  SCFOPN(cname, FLAMESDATATYPE, 0, F_IMA_TYPE, &cub_id);
  SCFOPN(iname, FLAMESDATATYPE, 0, F_IMA_TYPE, &ima_id);
  uves_msg_warning("This is supposed to work");
  check_nomsg(flames_reset_desc_set2(ima_id,cub_id,1));
  uves_msg_warning("The following should crash");

  /* Set SLITFF */
  check_nomsg(flames_reset_desc_set3(ima_id,cub_id,2,1));
  uves_msg_warning("now fibff");
  /* Set FIBFF */

  uves_msg_warning("The following should crash");
  check_nomsg(flames_reset_desc_set4(ima_id,cub_id,2,1));

  uves_msg_warning("Descriptor changed");
  SCFCLO(cub_id);
  SCFCLO(ima_id);



  uves_msg_warning("now fibff");



  sprintf(cname,"%s","fibreff_l_dtc.fits");
  sprintf(iname,"%s","fibreff_l_data01.fits");

  SCFOPN(cname, FLAMESDATATYPE, 0, F_IMA_TYPE, &cub_id);
  SCFOPN(iname, FLAMESDATATYPE, 0, F_IMA_TYPE, &ima_id);

  uves_msg_warning("This is supposed to work");
  check_nomsg(flames_reset_desc_set2(ima_id,cub_id,1));
  uves_msg_warning("The following should crash");
  check_nomsg(flames_reset_desc_set3(ima_id,cub_id,2,2));
  uves_msg_warning("The following should crash");
  check_nomsg(flames_reset_desc_set4(ima_id,cub_id,2,2));
  uves_msg_warning("Descriptor changed");
  SCFCLO(cub_id);
  SCFCLO(ima_id);



  sprintf(cname,"%s","fibreff_l_sgc.fits");
  sprintf(iname,"%s","fibreff_l_sigma01.fits");

  SCFOPN(cname, FLAMESDATATYPE, 0, F_IMA_TYPE, &cub_id);
  SCFOPN(iname, FLAMESDATATYPE, 0, F_IMA_TYPE, &ima_id);

  uves_msg_warning("This is supposed to work");
  check_nomsg(flames_reset_desc_set3(ima_id,cub_id,2,2));
  uves_msg_warning("The following should crash");
  check_nomsg(flames_reset_desc_set4(ima_id,cub_id,2,2));
  uves_msg_warning("Descriptor changed");
  SCFCLO(cub_id);
  SCFCLO(ima_id);



  sprintf(cname,"%s","fibreff_l_bpc.fits");
  sprintf(iname,"%s","fibreff_l_badpixel01.fits");

  SCFOPN(cname, FLAMESDATATYPE, 0, F_IMA_TYPE, &cub_id);
  SCFOPN(iname, FLAMESDATATYPE, 0, F_IMA_TYPE, &ima_id);

  uves_msg_warning("This is supposed to work");
  check_nomsg(flames_reset_desc_set3(ima_id,cub_id,2,2));
  uves_msg_warning("The following should crash");
  check_nomsg(flames_reset_desc_set4(ima_id,cub_id,2,2));
  uves_msg_warning("Descriptor changed");
  SCFCLO(cub_id);
  SCFCLO(ima_id);


 cleanup:

  if (cpl_error_get_code() != CPL_ERROR_NONE) {
    return -1;
  } else {
    return 0;
  }

}


/*---------------------------------------------------------------------------*/
/**
  @brief   Test flames_my_decubify
  This test exists just to verify that function works
**/
/*---------------------------------------------------------------------------*/


static int
test_flames_my_decubify(void)
{
  char tag[MED_NAME_SIZE];
  char cname[MED_NAME_SIZE];
  char ibase[MED_NAME_SIZE];
  char file_com[MED_NAME_SIZE];
  char file_dat[MED_NAME_SIZE];
  const char* src_dir="/data1/flames/midas2cpl/";
  int nflats=0;
  uves_propertylist* plist=NULL;
  enum uves_chip chip = UVES_CHIP_REDL;
  const int frm_type=1;
  cpl_frame* frm=NULL;
  cpl_frameset* set=NULL;
  cpl_frameset* set_out=NULL;


  sprintf(file_dat,"%s%s",src_dir,"slitff_l_dtc.fits");
  sprintf(file_com,"%s%s",src_dir,"slitff_l_common.fits");
  sprintf(ibase,"%s","slitff_l_data");
  sprintf(cname,"%s","slitff_l_dtc.fits");
  uves_msg_warning("cname=%s",cname);

  check_nomsg(plist=uves_propertylist_load(file_com,0));
  check_nomsg(nflats=uves_flames_pfits_get_nflats(plist));
  uves_free_propertylist(&plist);

  strcpy(tag,FLAMES_SLIT_FF_DTC(chip));
  check_nomsg(frm=cpl_frame_new());
  cpl_frame_set_tag(frm,tag);
  cpl_frame_set_filename(frm,file_dat);
  cpl_frame_set_group(frm,CPL_FRAME_GROUP_CALIB);

  
  check_nomsg(set=cpl_frameset_new());
  check_nomsg(set_out=cpl_frameset_new());
  cpl_frameset_insert(set,frm);
  ck0_nomsg(flames_extract_ima_from_cube(set,tag,chip,cname,&set_out));
 
  uves_free_frameset(&set);
  check_nomsg(flames_reset_desc_data(cname,nflats,ibase,frm_type,chip));
  uves_msg_warning("ok6");
   
  cleanup:

  uves_free_propertylist(&plist);
  uves_free_frameset(&set);
  uves_free_frameset(&set_out);

  if (cpl_error_get_code() != CPL_ERROR_NONE) {
    return -1;
  } else {
    return 0;
  }
}





/*---------------------------------------------------------------------------*/
/**
  @brief   Test flames_my_cubify
  This test exists just to verify that function works
**/
/*---------------------------------------------------------------------------*/


static int
test_flames_my_cubify(void)
{
  char file_com[MED_NAME_SIZE];
  char file_dat[MED_NAME_SIZE];
  char file_sig[MED_NAME_SIZE];
  char file_bpm[MED_NAME_SIZE];
  char file_bnd[MED_NAME_SIZE];
  char file_out[MED_NAME_SIZE];

  const char* src_dir="/home/amodigli/pipeline/uvesp/flames/tests/";
  const char* inp_base_format="%2.2d";

  uves_propertylist* head=NULL;
  int nflats=0;

  /* SLITFF_COMMON */
  
  sprintf(file_com,"%s%s",src_dir,"slitff_l_common.fits");
  uves_msg_debug("file=%s",file_com);
  check_nomsg(head=uves_propertylist_load(file_com,0));
  check_nomsg(nflats=uves_flames_pfits_get_nflats(head));
  uves_free_propertylist(&head);
  uves_msg_debug("nflats=%d",nflats);

  sprintf(file_dat,"%s%s",src_dir,"slitff_l_data");
  sprintf(file_bpm,"%s%s",src_dir,"slitff_l_badpixel");
  sprintf(file_sig,"%s%s",src_dir,"slitff_l_sigma");
  sprintf(file_bnd,"%s%s",src_dir,"slitff_l_bound");

  uves_msg_warning("file=%s",file_dat);

  sprintf(file_out,"%s","slitff_l_data_pck.fits");
  ck0_nomsg(flames_images_to_cube(file_dat,file_out,inp_base_format,1,nflats));
 

  /* sprintf(file_dat,"%s%s",src_dir,"slitff_l_data01.fits"); */

  uves_msg_warning("file_dat=%s",file_dat);
  check_nomsg(flames_add_desc_data(file_dat,file_out,nflats,1));
  
  sprintf(file_out,"%s","slitff_l_badpixel_pck.fits");
  ck0_nomsg(flames_images_to_cube(file_bpm,file_out,inp_base_format,1,nflats));
  uves_msg_warning("ok1");

  sprintf(file_out,"%s","slitff_l_sigma_pck.fits");
  ck0_nomsg(flames_images_to_cube(file_sig,file_out,inp_base_format,1,nflats));
  uves_msg_warning("ok2");

  sprintf(file_out,"%s","slitff_l_bound_pck.fits");
  ck0_nomsg(flames_cubes_to_supercube(file_bnd,file_out,inp_base_format,1,nflats));
  uves_msg_warning("ok3");
  
  

  /*
  FIBREFF_COMMON
  uves_msg_debug("ok1");
  sprintf(file_com,"%s%s",src_dir,"fibreff_l_common.fits");
  check_nomsg(head=uves_propertylist_load(file_com,0));
  uves_msg_debug("ok1");
  check_nomsg(nflats=uves_flames_pfits_get_nflats(head));
  uves_msg_debug("ok1");
  uves_free_propertylist(&head);
  uves_msg_debug("nflats=%d",nflats);

  sprintf(file_dat,"%s%s",src_dir,"fibreff_l_data");
  sprintf(file_bpm,"%s%s",src_dir,"fibreff_l_badpixel");
  sprintf(file_sig,"%s%s",src_dir,"fibreff_l_sigma");

  sprintf(file_out,"%s","fibreff_l_data_pck.fits");
  ck0_nomsg(flames_images_to_cube(file_dat,file_out,inp_base_format,1,nflats));
  sprintf(file_out,"%s","fibreff_l_badpixel_pck.fits");
  ck0_nomsg(flames_images_to_cube(file_bpm,file_out,inp_base_format,1,nflats));
  sprintf(file_out,"%s","fibreff_l_sigma_pck.fits");
  ck0_nomsg(flames_images_to_cube(file_sig,file_out,inp_base_format,1,nflats));
  */

  cleanup:

  if (cpl_error_get_code() != CPL_ERROR_NONE) {
    return -1;
  } else {
    return 0;
  }

}


/*---------------------------------------------------------------------------*/
/**
  @brief   Test start & npix reset
  This test exists just to verify that function works
**/
/*---------------------------------------------------------------------------*/


static int
test_reset_start_and_npix(void)
{

  char file[MED_NAME_SIZE];
  char tag[MED_NAME_SIZE];
  enum uves_chip chip=UVES_CHIP_REDL;
  const char* src_dir="/data1/flames/midas2cpl/";
  sprintf(file,"%s%s",src_dir,"fibreff_l_sigma01.fits");
  strcpy(tag,FLAMES_FIB_FF_SG1(chip));
  uves_msg_warning("ok1");
  ck0_nomsg(flames_reset_start_and_npix(file,tag));
  uves_msg_warning("ok2");

  cleanup:

  if (cpl_error_get_code() != CPL_ERROR_NONE) {
    return -1;
  } else {
    return 0;
  }

}


/*---------------------------------------------------------------------------*/
/**
  @brief   Test readordpos
  This test exists just to verify that function works
**/
/*---------------------------------------------------------------------------*/



static int
test_readordpos(void)
{

  char file[MED_NAME_SIZE];
  char tag[MED_NAME_SIZE];
  orderpos* ordpos=0;
  enum uves_chip chip=UVES_CHIP_REDL;
  const char* src_dir="/data1/flames/midas2cpl/";
  sprintf(file,"%s%s",src_dir,"ordef.fits");
  uves_msg_warning("ok1");
  ck0_nomsg(readordpos(file,tag));
  uves_msg_warning("ok2");

  cleanup:

  if (cpl_error_get_code() != CPL_ERROR_NONE) {
    return -1;
  } else {
    return 0;
  }

}


/*---------------------------------------------------------------------------*/
/**
  @brief   Test of frame merging (using UVES-FLAMES-like procedure)

  This test exists just to verify that the flames library can be loaded/run
**/
/*---------------------------------------------------------------------------*/


static int
test_merge_flames(void)
{

  char file_ima[MED_NAME_SIZE];
  char file_sig[MED_NAME_SIZE];
  char file_msk[MED_NAME_SIZE];


  char wfile[MED_NAME_SIZE];


  char efile[MED_NAME_SIZE];

  char mfile[MED_NAME_SIZE];

  cpl_image* wimg=NULL;
  cpl_image* eimg=NULL;
  cpl_image* mimg=NULL;
  cpl_image* werr=NULL;
  cpl_image* merr=NULL;

  uves_propertylist* wheader=NULL;
  uves_propertylist* eheader=NULL;
  uves_propertylist* mheader=NULL;
  merge_method m_method=MERGE_OPTIMAL; /* MERGE_SUM */
  enum uves_chip chip=UVES_CHIP_REDL;
  const char* src_dir="/home/amodigli/test/fluves/ex/rb/sof/";

  uves_msg_debug("ok1");
 
  sprintf(file_ima,"%s%s",src_dir,"wfxb_l_0002.fits");
  sprintf(file_sig,"%s%s",src_dir,"wfxb_l_sig0002.fits");
  sprintf(file_msk,"%s%s",src_dir,"wfxb_l_extco0002.fits");
  sprintf(wfile,"%s","wfxb");
  sprintf(mfile,"%s","mwfxb");

  uves_msg_debug("ok2");

  ck0_nomsg(flames_merge(file_ima,file_sig,file_msk,mfile,&MASKTHRES,0,0));
  uves_msg_debug("ok3");

  cleanup:

  if (cpl_error_get_code() != CPL_ERROR_NONE) {
    return -1;
  } else {
    return 0;
  }


}    

/*---------------------------------------------------------------------------*/
/**
  @brief   Test of frame merging (using UVES-ECHELLE-like procedure)

  This test exists just to verify that the flames library can be loaded/run
**/
/*---------------------------------------------------------------------------*/


static int
test_merge_echelle(void)
{

  char file[MED_NAME_SIZE];
  char wfile[MED_NAME_SIZE];
  char efile[MED_NAME_SIZE];
  char mfile[MED_NAME_SIZE];

  cpl_image* wimg=NULL;
  cpl_image* eimg=NULL;
  cpl_image* mimg=NULL;
  cpl_image* werr=NULL;
  cpl_image* merr=NULL;

  uves_propertylist* wheader=NULL;
  uves_propertylist* eheader=NULL;
  uves_propertylist* mheader=NULL;
  merge_method m_method=MERGE_OPTIMAL; /* MERGE_SUM */
  enum uves_chip chip=UVES_CHIP_REDL;
  const char* src_dir="/home/amodigli/test/fluves/ex/rb/sof/";

 
  sprintf(file,"%s%s",src_dir,"fxb_l_0002.fits");
  sprintf(wfile,"%s%s",src_dir,"wfxb_l_0002.fits");
  sprintf(mfile,"%s","mwfxb_l_0002.fits");


  sprintf(efile,"%s%s",src_dir,"wfxb_l_sig0002.fits");
  uves_msg_debug("wfile=%s",wfile);
  check_nomsg(wimg=cpl_image_load(wfile,CPL_TYPE_FLOAT,0,0));

  check_nomsg(werr=cpl_image_load(efile,CPL_TYPE_FLOAT,0,0));

  check_nomsg(cpl_image_abs(werr));
  check_nomsg(cpl_image_power(werr,0.5));
/*
  check_nomsg(cpl_image_divide(wimg,wimg));
  check_nomsg(cpl_image_multiply_scalar(wimg,1.e-6));
  */
  check_nomsg(cpl_image_multiply_scalar(werr,1.e6));
  check_nomsg( uves_save_image(wimg,"wcheck.fits",NULL,true, true) );
 
  check_nomsg(wheader=uves_propertylist_load(wfile,0));

  check(mimg=uves_merge_orders(wimg, werr,wheader,m_method,1,
                               &mheader,0,0,chip,&merr),
	"Error merging frame");


  check_nomsg( uves_save_image(mimg,mfile,mheader,true, true) );


  cleanup:

  if (cpl_error_get_code() != CPL_ERROR_NONE) {
    return -1;
  } else {
    return 0;
  }


}    





/*---------------------------------------------------------------------------*/
/**
  @brief   Test of FLAMES library

  This test exists just to verify that the flames library can be loaded/run
**/
/*---------------------------------------------------------------------------*/

int main(void)
{
    cpl_test_init(PACKAGE_BUGREPORT, CPL_MSG_WARNING);
    cpl_errorstate ESTATE=cpl_errorstate_get();
    /*  
      Temporary commented out (this is used to debug a module depending on
      local fits files
      ck0( test_readordpos(), "readordpos test failed");
      ck0( test_reset_start_and_npix(), "reset start and npix test failed");
      ck0( test_merge_echelle(), "merge-echelle test failed");
      ck0( test_merge_flames(), "merge-flames test failed");
      ck0( test_flames_my_cubify(), "flames_my_cubify test failed");
      ck0( test_flames_my_decubify(), "flames_my_decubify test failed");
      ck0( test_flames_readslitflats(), "flames_readslitflats test failed");
      ck0( test_flames_readfibreflats(), "flames_readfibreflats test failed");
      ck0( test_flames_my_decubify(),"test_flames_my_decubify failed");
      ck0( test_flames_reset_desc3(),"test_flames_reset_desc3 failed");
      ck0( test_flames_rebin_loop(),"test_flames_rebin_loop failed");
      ck0( test_flames_merge(),"test_flames_merge failed");
      ck0( test_flames_insert(),"test_flames_insert failed");
      ck0(test_flames_load_linetable(),"test_flames_load_linetable failed");
      ck0(test_flames_pack(),"test_flames_pack failed");
    */

  cleanup:
    cpl_errorstate_dump(ESTATE, CPL_FALSE, cpl_errorstate_dump_one);
    return cpl_test_end(0);
}

/**@}*/
