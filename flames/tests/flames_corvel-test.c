/*                                                                            *
 *   This file is part of the ESO UVES Pipeline                               *
 *   Copyright (C) 2004,2005 European Southern Observatory                    *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */
 
/*
 * $Author: amodigli $
 * $Date: 2009-06-05 05:55:26 $
 * $Revision: 1.13 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.12  2009/03/04 08:16:34  amodigli
 * fixed compiler error due to API change
 *
 * Revision 1.11  2008/12/23 08:34:00  amodigli
 * replace // with proper C comments
 *
 * Revision 1.10  2008/09/29 08:25:09  amodigli
 * fixed compiler warning
 *
 * Revision 1.9  2008/06/26 08:25:10  amodigli
 * fixed some bugs
 *
 * Revision 1.8  2008/06/20 14:18:51  amodigli
 * updated
 *
 * Revision 1.7  2008/01/30 17:13:21  amodigli
 * cleaned
 *
 * Revision 1.6  2008/01/22 07:56:32  amodigli
 * fixed warnings and disactivated tests requiring external input
 *
 * Revision 1.5  2008/01/17 17:28:12  amodigli
 * Monitor fix some bugs on corvel
 *
 * Revision 1.4  2007/12/15 09:52:45  amodigli
 * commented test
 *
 * Revision 1.3  2007/08/30 08:18:47  amodigli
 * fixed some doxygen warnings
 *
 * Revision 1.2  2007/08/10 12:08:14  amodigli
 * temporary suppressed test
 *
 * Revision 1.1  2007/08/10 12:06:34  amodigli
 * added to repository
 *
 * Revision 1.3  2007/06/22 07:10:43  amodigli
 * shorten lines length
 *
 * Revision 1.2  2007/06/05 06:54:10  jmlarsen
 * Removed unused variables
 *
 * Revision 1.1  2007/03/15 12:27:23  jmlarsen
 * Moved unit tests to ./uves/tests and ./flames/tests
 *
 * Revision 1.2  2007/02/27 14:04:14  jmlarsen
 * Move unit test infrastructure to IRPLIB
 *
 * Revision 1.1  2007/02/21 12:38:26  jmlarsen
 * Renamed _test -> -test
 *
 * Revision 1.3  2007/01/29 12:17:54  jmlarsen
 * Support setting verbosity from command line
 *
 * Revision 1.2  2006/11/16 09:49:25  jmlarsen
 * Fixed doxygen bug
 *
 * Revision 1.1  2006/11/07 14:02:20  jmlarsen
 * Added FLAMES library unit test
 *
 * Revision 1.18  2006/11/06 15:30:54  jmlarsen
 * Added missing includes
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*----------------------------------------------------------------------------
                                Includes
 ----------------------------------------------------------------------------*/

#include <flames_utils.h>
#include <flames_corvel.h>
#include <flames_def_drs_par.h>
#include <uves_msg.h>
#include <uves_chip.h>
#include <uves_error.h>
#include <uves_dfs.h>
#include <uves_utils_wrappers.h>
#include <uves_qclog.h>
#include <flames_utils_science.h>
#include <cpl_test.h>

#include <cpl.h>

#include <float.h>
/*----------------------------------------------------------------------------
                                Defines
 ----------------------------------------------------------------------------*/
#define MAX_SIZE 256
#define MIN_SIZE 80


/*----------------------------------------------------------------------------
                            Functions prototypes
 ----------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/**
 * @defgroup flames_corvel_test  FLAMES corvel unit tests
 */
/*---------------------------------------------------------------------------*/
/**@{*/
/**
          @brief test version function
*/


static void
test_vcorrel(void)
{

  char mname[MAX_SIZE];
  char wname[MAX_SIZE];
  char tname[MAX_SIZE];
  char cvel_name[MAX_SIZE];
  enum uves_chip chip=UVES_CHIP_REDL;
  
 
  uves_propertylist* mhead=NULL;
  uves_propertylist* whead=NULL;
  cpl_vector* vec=NULL;
  cpl_image* img=NULL;
  cpl_table* order_tbl=NULL;
  const char* tab_base_name="ordef_";
  const char* cvel_base_name="mask_corvel.tfits";
 
  int nord=0; 
  int ord_min=1; 
  int ord_max=1; 
  double avg_cnt=0;
  double sig_cnt=0;
  cpl_table* qclog=NULL;
  const char* prefid="l";

  const char* src_dir="/media/disk/flames/spr/SFLAT/107336/";
  const char* cdb_dir="/cal/uves/mos/cal/";

  double old_cvel_max = DRS_CVEL_MAX;
  double old_cvel_min = DRS_CVEL_MIN;
  double drs_cvel_max = DRS_CVEL_MAX;
  double drs_cvel_min = DRS_CVEL_MIN;
  double cvel_min=0;
  double cvel_max=0;
  double ccf_posmax_zero_point=0;
  const char* drs_base_name="fxb_l";
  cpl_frame* cvel_tab=NULL;
  cpl_errorstate initial_errorstate = cpl_errorstate_get();

  sprintf(wname,"%s%s%s%s",src_dir,"w",drs_base_name,"_raw0001.fits");
  sprintf(mname,"%s%s%s%s",src_dir,"mw",drs_base_name,"_raw0001.fits");
  sprintf(tname,"%s%s%s%s",src_dir,tab_base_name,prefid,".fits");
  sprintf(cvel_name,"%s%s",cdb_dir,cvel_base_name);
  cvel_tab=cpl_frame_new();
  cpl_frame_set_filename(cvel_tab,cvel_name);
  uves_msg_warning("wname=%s",wname);
  uves_msg_warning("mname=%s",mname);
  uves_msg_warning("tname=%s",tname);


  check_nomsg(whead=uves_propertylist_load(wname,0));
  check_nomsg(mhead=uves_propertylist_load(mname,0));
  check_nomsg(vec=cpl_vector_load(mname,0));
  check_nomsg(img=uves_vector_to_image(vec,CPL_TYPE_FLOAT));


  uves_free_vector(&vec);
  check_nomsg(order_tbl=cpl_table_load(tname,1,0));
  check_nomsg(ord_min=cpl_table_get_column_min(order_tbl,"Order"));
  check_nomsg(ord_max=cpl_table_get_column_max(order_tbl,"Order"));
  uves_free_table(&order_tbl);
  nord=ord_max-ord_min+1;
  ck0_nomsg(flames_reduce_add_wstart(whead,&mhead,nord));
  check_nomsg(uves_save_image(img,mname,mhead,true,true));
  uves_free_image(&img);

  qclog=cpl_table_new(0);
  check_nomsg(flames_reduce_vcorrel(drs_base_name,
				    "cvel2", 
				    prefid,
				    ord_max,
				    cvel_tab, 
				    "_raw0001", 
				    "_raw0001", 
				    DRS_CVEL_MIN,
				    DRS_CVEL_MAX,
				    &ccf_posmax_zero_point,
				    &avg_cnt,
				    &sig_cnt,
				    qclog));


  uves_msg_warning("POSAVG=%f POSRMS=%f",avg_cnt,sig_cnt);
  drs_cvel_max +=cvel_max;
  drs_cvel_min +=cvel_min;
  drs_cvel_max=old_cvel_max;
  drs_cvel_min=old_cvel_min;



 cleanup:
  uves_free_table(&order_tbl);
  uves_free_image(&img);
  uves_free_vector(&vec);
  uves_free_frame(&cvel_tab);
  uves_free_table(&qclog);
  uves_free_propertylist(&whead);
  uves_free_propertylist(&mhead);

  cpl_errorstate_dump(initial_errorstate, CPL_FALSE, NULL);
  return;
}
    


static void
test_corvel(void)
{

  char mfile[MAX_SIZE];
  char cvel_file[MAX_SIZE];
  char out_cvel_tab[MIN_SIZE];
  char out_tot_ima[MIN_SIZE];
  char out_nrm_ima[MIN_SIZE];
 
  int ord=0; 
  int ord_min=1; 
  int ord_max=1; 

  const char* prefid="l";
  const char* src_dir="/home/amodigli/test/sof/";
  const char* cdb_dir="/cal/uves/mos/cal/";

  sprintf(mfile,"%s%s%s%s",src_dir,"mwscience_",prefid,"_raw0001.fits");
  sprintf(cvel_file,"%s%s",cdb_dir,"mask_corvel.tfits");


  for (ord=ord_min;ord<=ord_max;ord++) {
    sprintf(out_cvel_tab,"%s%s%s%d%s","tab_",prefid,"_",ord,".fits");
    sprintf(out_tot_ima, "%s%s%s%d%s","tot_",prefid,"_",ord,".fits");
    sprintf(out_nrm_ima, "%s%s%s%d%s","nrm_",prefid,"_",ord,".fits");

    uves_msg_warning("mfile=%s",mfile);
    uves_msg_warning("cvel_file=%s",cvel_file);
    uves_msg_warning("ord=%d",ord);

    ck0(flames_corvel(mfile,
                  cvel_file,
                  ord,
                  out_cvel_tab,
                  out_tot_ima,
                  out_nrm_ima,
		  DRS_CVEL_MIN,
                  DRS_CVEL_MAX,
		  DRS_CVEL_STEP),"Fail of flames_corvel()");

  }

  cleanup:
    return;
}
    
/*---------------------------------------------------------------------------*/
/**
  @brief   Test of FLAMES library

  This test exists just to verify that the flames library can be loaded/run
**/
/*---------------------------------------------------------------------------*/

int main(void)
{
    cpl_test_init(PACKAGE_BUGREPORT, CPL_MSG_WARNING);
    /* Temporary commented out (this is used to debug a module depending on
    local fits files
    */
    /*check( test_vcorrel(), "corvel test failed"); */

  cleanup:
    return cpl_test_end(0);
}

/**@}*/
