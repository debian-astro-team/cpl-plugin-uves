/*                                                                            *
 *   This file is part of the ESO UVES Pipeline                               *
 *   Copyright (C) 2004,2005 European Southern Observatory                    *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */
 
/*
 * $Author: amodigli $
 * $Date: 2009-06-05 05:55:26 $
 * $Revision: 1.4 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.3  2007/06/22 07:10:43  amodigli
 * shorten lines length
 *
 * Revision 1.2  2007/06/05 06:54:10  jmlarsen
 * Removed unused variables
 *
 * Revision 1.1  2007/03/15 12:27:23  jmlarsen
 * Moved unit tests to ./uves/tests and ./flames/tests
 *
 * Revision 1.2  2007/02/27 14:04:14  jmlarsen
 * Move unit test infrastructure to IRPLIB
 *
 * Revision 1.1  2007/02/21 12:38:26  jmlarsen
 * Renamed _test -> -test
 *
 * Revision 1.3  2007/01/29 12:17:54  jmlarsen
 * Support setting verbosity from command line
 *
 * Revision 1.2  2006/11/16 09:49:25  jmlarsen
 * Fixed doxygen bug
 *
 * Revision 1.1  2006/11/07 14:02:20  jmlarsen
 * Added FLAMES library unit test
 *
 * Revision 1.18  2006/11/06 15:30:54  jmlarsen
 * Added missing includes
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*----------------------------------------------------------------------------
                                Includes
 ----------------------------------------------------------------------------*/

#include <flames_utils.h>

#include <uves_error.h>
#include <cpl_test.h>

#include <cpl.h>

#include <float.h>
/*----------------------------------------------------------------------------
                                Defines
 ----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
                            Functions prototypes
 ----------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/**
 * @defgroup flames_test  FLAMES unit tests
 */
/*---------------------------------------------------------------------------*/
/**@{*/
/**
          @brief test version function
*/
static void
test_version(void)
{
    assure( flames_get_version_binary() == UVES_BINARY_VERSION,
	    CPL_ERROR_ILLEGAL_OUTPUT, "Got: %d, expected: %d",
	    flames_get_version_binary(), UVES_BINARY_VERSION);

  cleanup:
    return;
}
    
/*---------------------------------------------------------------------------*/
/**
  @brief   Test of FLAMES library

  This test exists just to verify that the flames library can be loaded/run
**/
/*---------------------------------------------------------------------------*/

int main(void)
{
    cpl_test_init(PACKAGE_BUGREPORT, CPL_MSG_WARNING);

    check( test_version(),
	   "Version test failed");

  cleanup:
    return cpl_test_end(0);
}

/**@}*/
