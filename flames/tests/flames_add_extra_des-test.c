/*                                                                            *
 *   This file is part of the ESO UVES Pipeline                               *
 *   Copyright (C) 2004,2005 European Southern Observatory                    *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */
 
/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:31:05 $
 * $Revision: 1.9 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.7  2009/06/05 05:55:26  amodigli
 * updated init/end to cpl5
 *
 * Revision 1.6  2007/08/30 08:18:47  amodigli
 * fixed some doxygen warnings
 *
 * Revision 1.5  2007/08/13 12:11:32  amodigli
 * added support CPL4
 *
 * Revision 1.4  2007/06/22 14:53:26  jmlarsen
 * Expanded, again, interface of uves_save_image()
 *
 * Revision 1.3  2007/06/22 09:34:47  jmlarsen
 * Adapted to new funcionality
 *
 * Revision 1.2  2007/06/22 07:10:43  amodigli
 * shorten lines length
 *
 * Revision 1.1  2007/06/06 07:19:52  jmlarsen
 * Added unit test
 *
 * Revision 1.3  2007/04/24 12:49:34  jmlarsen
 * Replaced cpl_propertylist -> uves_propertylist which is much faster
 *
 * Revision 1.2  2007/04/10 07:04:44  jmlarsen
 * Fixed memory leak
 *
 * Revision 1.1  2007/03/15 15:07:20  jmlarsen
 * Added flames_dfs-test
 *
 * Revision 1.1  2007/03/15 12:27:23  jmlarsen
 * Moved unit tests to ./uves/tests and ./flames/tests
 *
 * Revision 1.3  2007/03/05 10:11:52  jmlarsen
 * Added test
 *
 * Revision 1.2  2007/02/27 14:04:14  jmlarsen
 * Move unit test infrastructure to IRPLIB
 *
 * Revision 1.1  2007/01/29 12:17:26  jmlarsen
 * Added
 *
 * Revision 1.2  2006/11/16 09:49:25  jmlarsen
 * Fixed doxygen bug
 *
 * Revision 1.1  2006/11/07 14:02:20  jmlarsen
 * Added FLAMES library unit test
 *
 * Revision 1.18  2006/11/06 15:30:54  jmlarsen
 * Added missing includes
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*----------------------------------------------------------------------------
                                Includes
 ----------------------------------------------------------------------------*/

#include <flames_add_extra_des.h>

#include <uves_pfits.h>
#include <uves_dfs.h>
#include <uves_utils_wrappers.h>
#include <uves_error.h>
#include <cpl_test.h>

#include <cpl.h>

/*----------------------------------------------------------------------------
                                Defines
 ----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                            Functions prototypes
 ----------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/**
 * @defgroup flames_add_extra_des_test unit tests for flames descriptor additions
 */
/*---------------------------------------------------------------------------*/
/**@{*/
static void
test_add_extra_des(void)
{
    const char *ref_filename = "flames_add_extra_des_ref.fits";
    const char *out_filename = "flames_add_extra_des_out.fits";

    cpl_frame *ref_frm = cpl_frame_new();
    cpl_frame *out_frm = cpl_frame_new();
    
    cpl_image *image = cpl_image_new(1, 1, CPL_TYPE_FLOAT);
    uves_propertylist *header = uves_propertylist_new();

    uves_propertylist_update_string(header, UVES_DPR_TYPE, "hello");

    uves_save_image(image, ref_filename, header, true, true);
    uves_save_image(image, out_filename, header, true, true);
    
    cpl_frame_set_filename(ref_frm, ref_filename);
    cpl_frame_set_filename(out_frm, out_filename);
    cpl_frame_set_type(ref_frm, CPL_FRAME_TYPE_IMAGE);
    cpl_frame_set_type(out_frm, CPL_FRAME_TYPE_IMAGE);

    flames_add_extra_des(ref_frm, out_frm);
    
    uves_free_image(&image);
    uves_free_propertylist(&header);
    uves_free_frame(&ref_frm);
    uves_free_frame(&out_frm);
    return;
}
    
/*---------------------------------------------------------------------------*/
/**
  @brief   Unit test of flames_add_extra_des()
**/
/*---------------------------------------------------------------------------*/

int main(void)
{
    cpl_test_init(PACKAGE_BUGREPORT, CPL_MSG_WARNING);
    cpl_errorstate initial_errorstate = cpl_errorstate_get();
    test_add_extra_des();

    if (cpl_error_get_code() != CPL_ERROR_NONE)
        {

	  cpl_errorstate_dump(initial_errorstate,CPL_FALSE,NULL);

        }
   return cpl_test_end(0); 
}

/**@}*/
