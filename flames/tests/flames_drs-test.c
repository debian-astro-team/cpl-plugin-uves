/*                                                                            *
 *   This file is part of the ESO UVES Pipeline                               *
 *   Copyright (C) 2004,2005 European Southern Observatory                    *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                              */
 
/*
 * $Author: amodigli $
 * $Date: 2009-06-05 05:55:26 $
 * $Revision: 1.3 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.2  2008/12/23 08:34:39  amodigli
 * replace // with proper C comments
 *
 * Revision 1.1  2007/12/15 09:53:52  amodigli
 * added to repository
 *
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*----------------------------------------------------------------------------
                                Includes
 ----------------------------------------------------------------------------*/


#include <uves_utils_wrappers.h>
#include <uves_dfs.h>
#include <uves_error.h>
#include <uves_parameters.h>
#include <cpl_test.h>

#include <cpl.h>

/*----------------------------------------------------------------------------
                                Defines
 ----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
                            Functions prototypes
 ----------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/**
 * @defgroup flames_drs_test  FLAMES DFS unit tests
 */
/*---------------------------------------------------------------------------*/
/**@{*/

static int
flames_align_table_column(cpl_table** m_tbl,cpl_table** r_tbl,const char* col)
{

  double* pmw=NULL;
  double* prw=NULL;
  int* pmc=NULL;
  int* prc=NULL;
  int* pmo=NULL;
  int* pro=NULL;
  int nm=0;
  int nr=0;
  int i=0;
  int j=0;

 
  check_nomsg(pmw=cpl_table_get_data_double(*m_tbl,col));
  check_nomsg(prw=cpl_table_get_data_double(*r_tbl,col));
  check_nomsg(pmc=cpl_table_get_data_int(*m_tbl,"CHECK"));
  check_nomsg(prc=cpl_table_get_data_int(*r_tbl,"CHECK"));
  check_nomsg(pmo=cpl_table_get_data_int(*m_tbl,"CHECK"));
  check_nomsg(pro=cpl_table_get_data_int(*r_tbl,"CHECK"));
  check_nomsg(nm=cpl_table_get_nrow(*m_tbl));
  check_nomsg(nr=cpl_table_get_nrow(*r_tbl));
  for(i=0;i<nm;i++) {
    for(j=0;j<nr;j++) {

      if((pmw[i]==prw[j]) && (pmo[i]==pro[j])) {
	pmc[i]=1;
	prc[j]=1;
      }

    }
  }
 cleanup:

  return 0;

}
/*---------------------------------------------------------------------------*/
/**
 * @brief   test filename
 */
/*---------------------------------------------------------------------------*/
static void
test_align_tables(void)
{
  const char *rfile = "r_tbl.fits";
  const char *mfile = "m_tbl.fits";
  const char *rout = "align_r_tbl.fits";
  const char *mout = "align_m_tbl.fits";
  cpl_table* r_tbl=NULL;
  cpl_table* m_tbl=NULL;
  cpl_table* tmp=NULL;

  uves_propertylist* plist=NULL;
  int ord_min=0;
  int ord_max=0;
  double wav_min=0;
  double wav_max=0;
  int i=0;
  int nm=0;
  int nr=0;
  int nsel=0;


  check_nomsg(r_tbl = cpl_table_load(rfile,1,0));
  check_nomsg(m_tbl = cpl_table_load(mfile,1,0));
  check_nomsg(nm=cpl_table_get_nrow(m_tbl));
  check_nomsg(nr=cpl_table_get_nrow(r_tbl));

  check_nomsg(plist=uves_propertylist_new());
  check_nomsg(uves_propertylist_append_bool(plist,"ORDER",0)); /* 1 for descending order */
  check_nomsg(uves_propertylist_append_bool(plist,"WAVE",0));  /* 0 for ascending order */
  check_nomsg(uves_table_sort(m_tbl,plist));
  check_nomsg(uves_table_sort(r_tbl,plist));
  uves_free_propertylist(&plist);


  ord_min=(cpl_table_get_column_min(m_tbl,"ORDER")>
           cpl_table_get_column_min(r_tbl,"ORDER")) ?  
    cpl_table_get_column_min(m_tbl,"ORDER") :
    cpl_table_get_column_min(r_tbl,"ORDER");



  ord_max=(cpl_table_get_column_max(m_tbl,"ORDER")<
           cpl_table_get_column_max(r_tbl,"ORDER")) ?  
    cpl_table_get_column_max(m_tbl,"ORDER") :
    cpl_table_get_column_max(r_tbl,"ORDER");


    

  uves_msg_warning("ord_min=%d",ord_min);
  uves_msg_warning("ord_max=%d",ord_max);


 wav_min=(cpl_table_get_column_min(m_tbl,"WAVE")>
           cpl_table_get_column_min(r_tbl,"WAVE")) ?  
    cpl_table_get_column_min(m_tbl,"WAVE") :
    cpl_table_get_column_min(r_tbl,"WAVE");



  wav_max=(cpl_table_get_column_max(m_tbl,"WAVE")<
           cpl_table_get_column_max(r_tbl,"WAVE")) ?  
    cpl_table_get_column_max(m_tbl,"WAVE") :
    cpl_table_get_column_max(r_tbl,"WAVE");


  uves_msg_warning("wav_min=%g",wav_min);
  uves_msg_warning("wav_max=%g",wav_max);


  check_nomsg(nsel=cpl_table_and_selected_int(m_tbl,"ORDER",CPL_NOT_LESS_THAN,ord_min));
  check_nomsg(nsel=cpl_table_and_selected_int(m_tbl,"ORDER",CPL_NOT_GREATER_THAN,ord_max));
  check_nomsg(nsel=cpl_table_and_selected_double(m_tbl,"WAVE",CPL_NOT_LESS_THAN,wav_min));
  check_nomsg(nsel=cpl_table_and_selected_double(m_tbl,"WAVE",CPL_NOT_GREATER_THAN,wav_max));
  uves_msg_warning("nsel=%d",nsel);
  check_nomsg(tmp=cpl_table_extract_selected(m_tbl));
  uves_free_table(&m_tbl);
  check_nomsg(m_tbl=cpl_table_duplicate(tmp));




  check_nomsg(nsel=cpl_table_and_selected_int(r_tbl,"ORDER",CPL_NOT_LESS_THAN,ord_min));
  check_nomsg(nsel=cpl_table_and_selected_int(r_tbl,"ORDER",CPL_NOT_GREATER_THAN,ord_max));
  check_nomsg(nsel=cpl_table_and_selected_double(m_tbl,"WAVE",CPL_NOT_LESS_THAN,wav_min));
  check_nomsg(nsel=cpl_table_and_selected_double(m_tbl,"WAVE",CPL_NOT_GREATER_THAN,wav_max));
  uves_msg_warning("nsel=%d",nsel);
  check_nomsg(tmp=cpl_table_extract_selected(r_tbl));
  uves_free_table(&r_tbl);
  check_nomsg(r_tbl=cpl_table_duplicate(tmp));



  cpl_table_new_column(m_tbl,"CHECK",CPL_TYPE_INT);
  cpl_table_new_column(r_tbl,"CHECK",CPL_TYPE_INT);
  cpl_table_fill_column_window_int(r_tbl,"CHECK",0,nr,0);
  cpl_table_fill_column_window_int(m_tbl,"CHECK",0,nm,0);


  for(i=ord_min;i<=ord_max;i++) {

    cpl_table_and_selected_int(m_tbl,"ORDER",CPL_EQUAL_TO,i);
    cpl_table_and_selected_int(r_tbl,"ORDER",CPL_EQUAL_TO,i);
    flames_align_table_column(&m_tbl,&r_tbl,"WAVE");


  }
  cpl_table_select_all(m_tbl);
  cpl_table_select_all(r_tbl);


 wav_min=(cpl_table_get_column_min(m_tbl,"WAVE")>
           cpl_table_get_column_min(r_tbl,"WAVE")) ?  
    cpl_table_get_column_min(m_tbl,"WAVE") :
    cpl_table_get_column_min(r_tbl,"WAVE");



  wav_max=(cpl_table_get_column_max(m_tbl,"WAVE")<
           cpl_table_get_column_max(r_tbl,"WAVE")) ?  
    cpl_table_get_column_max(m_tbl,"WAVE") :
    cpl_table_get_column_max(r_tbl,"WAVE");


  uves_msg_warning("wav_min=%g",wav_min);
  uves_msg_warning("wav_max=%g",wav_max);

  check_nomsg(nsel=cpl_table_and_selected_int(m_tbl,"ORDER",CPL_NOT_LESS_THAN,ord_min));
  check_nomsg(nsel=cpl_table_and_selected_int(m_tbl,"ORDER",CPL_NOT_GREATER_THAN,ord_max));
  check_nomsg(nsel=cpl_table_and_selected_double(m_tbl,"WAVE",CPL_NOT_LESS_THAN,wav_min));
  check_nomsg(nsel=cpl_table_and_selected_double(m_tbl,"WAVE",CPL_NOT_GREATER_THAN,wav_max));
  check_nomsg(nsel=cpl_table_and_selected_int(m_tbl,"CHECK",CPL_EQUAL_TO,1));
  uves_msg_warning("nsel=%d",nsel);
  check_nomsg(tmp=cpl_table_extract_selected(m_tbl));
  uves_free_table(&m_tbl);
  check_nomsg(m_tbl=cpl_table_duplicate(tmp));

  check_nomsg(nsel=cpl_table_and_selected_int(r_tbl,"ORDER",CPL_NOT_LESS_THAN,ord_min));
  check_nomsg(nsel=cpl_table_and_selected_int(r_tbl,"ORDER",CPL_NOT_GREATER_THAN,ord_max));
  check_nomsg(nsel=cpl_table_and_selected_double(m_tbl,"WAVE",CPL_NOT_LESS_THAN,wav_min));
  check_nomsg(nsel=cpl_table_and_selected_double(m_tbl,"WAVE",CPL_NOT_GREATER_THAN,wav_max));
  check_nomsg(nsel=cpl_table_and_selected_int(r_tbl,"CHECK",CPL_EQUAL_TO,1));
  uves_msg_warning("nsel=%d",nsel);
  check_nomsg(tmp=cpl_table_extract_selected(r_tbl));
  uves_free_table(&r_tbl);
  check_nomsg(r_tbl=cpl_table_duplicate(tmp));
 
  
  cpl_table_save(m_tbl,NULL,NULL,mout,CPL_IO_DEFAULT);
  cpl_table_save(r_tbl,NULL,NULL,rout,CPL_IO_DEFAULT);



 cleanup:


  uves_free_table(&r_tbl);
  uves_free_table(&m_tbl);

  return;
}

    
/*---------------------------------------------------------------------------*/
/**
  @brief   Test of dfs module
**/
/*---------------------------------------------------------------------------*/

int main(void)
{
    cpl_test_init(PACKAGE_BUGREPORT, CPL_MSG_WARNING);
    cpl_errorstate initial_errorstate = cpl_errorstate_get();
 
  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE)
        {
	  cpl_errorstate_dump(initial_errorstate,CPL_FALSE,NULL);

        }
    return cpl_test_end(0);
}

/**@}*/
