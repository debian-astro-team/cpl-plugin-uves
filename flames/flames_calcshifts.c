/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
    Internet e-mail: midas@eso.org
    Postal address: European Southern Observatory
            Data Management Division 
            Karl-Schwarzschild-Strasse 2
            D 85748 Garching bei Muenchen 
            GERMANY
===========================================================================*/
/*-------------------------------------------------------------------------*/
/**
 * @defgroup flames_calcshift   Substep: calculates frames shift
 *
 */
/*-------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
                          Includes
 --------------------------------------------------------------------------*/


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif


#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <flames_midas_def.h>
#include <flames_uves.h>
#include <flames_newmatrix.h>
#include <flames_shiftcommon.h>
#include <flames_shiftall.h>

/**@{*/

/*---------------------------------------------------------------------------
                            Implementation
 ---------------------------------------------------------------------------*/
/**
 @name  flames_calcfillshift()  
 @short   ???
 @author G. Mulas  -  ITAL_FLAMES Consortium. Ported to CPL by A. Modigliani

 @param allflatsin pointer to allflat frame 
 @param shiftdata  pointer to shift structure
 @param iframe, 
 @param ix         
 @param yshift computed shift

 @return success or failure code

 DRS Functions called:          
       none                                         
                                                                         
 Pseudocode:                                                             
 convert realyshift to pixel coordinates,                                
 split integer and fractional part                                       
 for inf(value <= pyshift_ip <= sup(value)                               
     find the precise corresponding ix shift: pxshift                    
     find the range in integer ix shifts corresponding to the pxshift    
     if pxshift is not integer,                                          
        i_min= floor(pxshift), i_max= ceil(pxshift);                     
     else if pxshift is integer,                                         
        i_min= pxshift-1, i_max= pxshift+1;                              
     for (i=i_min;i<=i_max;i++)                                          
        if the shifted ix is within frame boundaries                     
           save the absolute shifted ix for later use (not the offset!)  
        endif                                                            
     endfor                                                              
 endfor                                                                  

@note
*/


flames_err 
calcshifts(
           allflats *allflatsin, 
           shiftstruct *shiftdata,
           int32_t iframe, 
           int32_t ix, 
           double yshift)
{
  double realyshift=0, pyshift=0, pyshift_ip=0;
  int32_t i=0, numoffsets;
  shiftstruct *myshiftdata=0;

  myshiftdata = shiftdata+ix;
  /* compute the real shift to be applied to this FF frame, taking into account
     that it is probably slightly shifted itself with respect to the 
     "all fibres" FF frame, and "yshift" is referred to the latter frame */
  realyshift = yshift-(((allflatsin->flatdata)[iframe]).yshift);
  /* convert realyshift to pixel coordinates, and split it in the two 
     possible ways between integer part and fractional part */
  pyshift = realyshift/(allflatsin->substepy);
  numoffsets = 0;
  for (pyshift_ip=floor(pyshift); pyshift_ip<=ceil(pyshift)+DEPSILON; 
       pyshift_ip++) {
    double pyshift_fp = pyshift-pyshift_ip;
    /* find the precise corresponding ix shift */
    double pxshift = (pyshift_fp*(allflatsin->substepy))/
      ((allflatsin->substepx)*(myshiftdata->orderslope));
    /* find the range in integer ix shifts corresponding to the above 
       pxshift */
    /* this is a bit subtle: if pxshift is not integer, i will range from 
       floor(pxshift) to ceil(pxshift); otherwise, if pxshift is integer, 
       it will range from pxshift-1 to pxshift+1 */
    for (i = (int32_t)(ceil(pxshift))-1; i <= (int32_t)(floor(pxshift))+1; 
     i++) {
      if ((ix+i)>=0 && (ix+i)<=(allflatsin->subcols-1)) {
    /* the shifted ix is within frame boundaries */
    /* save the absolute shifted ix for later use (not the offset!) */
    (myshiftdata->ixoffsets)[numoffsets] = (ix+i);
    (myshiftdata->yintoffsets)[numoffsets] = (int32_t) pyshift_ip;
    (myshiftdata->yfracoffsets)[numoffsets] = 
      ((shiftdata[ix+i]).ordercentre)-(myshiftdata->ordercentre)-
      pyshift_fp;
    numoffsets++;
      }
    }
  }

  myshiftdata->numoffsets = numoffsets;

  return(NOERR);
}
/**@}*/
