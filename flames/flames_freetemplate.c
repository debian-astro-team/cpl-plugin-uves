/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
  Internet e-mail: midas@eso.org
  Postal address: European Southern Observatory
  Data Management Division 
  Karl-Schwarzschild-Strasse 2
  D 85748 Garching bei Muenchen 
  GERMANY
  ===========================================================================*/
/*-------------------------------------------------------------------------*/
/**
 * @defgroup flames_freetemplate  free allocated memory for template frm
 *
 */
/*-------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdlib.h>
#include <flames_midas_def.h>
#include <flames_uves.h>
#include <flames_freetemplate.h>
#include <flames_newmatrix.h>
/**@{*/

/*---------------------------------------------------------------------------
  Implementation
  ---------------------------------------------------------------------------*/
/**
   @name  flames_freetemplate()  
   @short free allocated memory for template frm
   @author G. Mulas  -  ITAL_FLAMES Consortium. Ported to CPL by A. Modigliani

   @param myframe

   @return success or failure code

   DRS Functions called:          
   free_fmmatrix                                         
                                                                         
   Pseudocode:                                                             
   free allocated memory                                      

   @note
*/

flames_err 
freetemplate(flames_frame *myframe)
{

  free_fmmatrix(myframe->badpixel, 0, myframe->subrows-1, 
		0, myframe->subcols-1);
  /* Pasquale, I cannot free dynamic submembers of the back structure here
     since I do not know their sizes. I suppose it should be done here 
     though, this is a reminder to this effect */

  return(NOERR);

}

/**@}*/



