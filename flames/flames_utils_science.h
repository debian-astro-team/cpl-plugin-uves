/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

#ifndef FLAMES_UTILS_SCIENCE_H
#define FLAMES_UTILS_SCIENCE_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include <uves_cpl_size.h>
/*-----------------------------------------------------------------------------
                    Includes
 ----------------------------------------------------------------------------*/
#include <cpl.h>
#include <uves_chip.h>
#include <uves_propertylist.h>
#include <flames_uves.h>
#include <flames_midas_def.h>
#include <flames_reduce_vcorrel.h>

/*-----------------------------------------------------------------------------
                             Defines
 ----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                                   Prototypes
 ----------------------------------------------------------------------------*/
flames_err
flames_add_desc_data(const char* file_ref,
                     const char* file_out,
                     const int ndat,
                     const int in_frm_id);



flames_err
flames_add_desc_bpmap(const char* file_ref,
                      const char* file_out,
                      const int ndat,
                      const int in_frm_id);

flames_err
flames_add_desc_sigma(const char* file_ref,
                      const char* file_out,
                      const int ndat,
                      const int in_frm_id);

flames_err
flames_add_desc_bound(const char* file_ref,
                      const char* file_out,
                      const int ndat,
                      const int in_frm_id);


int
flames_extract_ima_from_cube(cpl_frameset* frames,
                             const char* tag,
                             enum uves_chip chip,
                             const char* cub_name,
                             cpl_frameset** set_out);

int
flames_drs_merge(const char* inp_ima, 
                 const char* out_base_reb, 
                 const char* out_base_mer,
                 const int fibre_id,
                 const int raw_switch,
                 const double delta1,
                 const double delta2);

flames_err
flames_reset_desc_data(const char* name_inp,
                       const int nflats,
                       const char* base_out,
                       const int in_frm_id,
                       enum uves_chip chip);


flames_err
flames_reset_desc_set4(const int out_id,
                       const int dat_id, 
                       const int it,
                       const int in_frm_id);

flames_err
flames_reset_desc_set2(const int dat_id, 
                       const int it,
                       const int in_frm_id);

flames_err
flames_reset_desc_set0(const int out_id,
                       const int dat_id, 
                       const int it,
                       const int in_frm_id);


flames_err
flames_reset_desc_set3(const int out_id,
                       const int dat_id, 
                       const int it,
                       const int in_frm_id);

int
flames_fix_start_and_npix(const char* name, const int format);

int
flames_reset_start_and_npix(const char* name, const char* tag);


int
flames_reduce_reset_descriptors(uves_propertylist* head_ref,
                                uves_propertylist** file_out,
                                const int i);


int
flames_my_decubify(cpl_frameset* set,
                   enum uves_chip chip,
                   const int frm_type,
                   cpl_frameset** set_out);

int
flames_my_cubify(enum uves_chip chip,
                 const int frm_type,
                 cpl_frameset** set_out);

int
flames_my_cubify2(enum uves_chip chip,
                  const int frm_type,
                  cpl_frameset** set_out,
                  cpl_frameset *frames,
                  const uves_propertylist *raw_header,
                  const cpl_parameterlist *parameters,
                  const char *recipe_id,
                  const char *starttime);

int
flames_spectra_to_image(
                const char* inp_basename,
                const char* inp_filename_qual,
                const char* out_filename,
                const int min,
                const int max,
                const cpl_table* xtab,
                const int pno);
int
flames_spectra_to_image_check(
                const char* inp_basename,
                const char* inp_filename_qual,
                const char* out_filename,
                const int min,
                const int max,
                const cpl_table* xtab,
                const int pno);
int
flames_images_to_cube(
                const char* inp_basename,
                const char* out_filename,
                const char* inp_format_digit,
                const int min,
                const int max);
int
flames_images_to_cube_check(
                const char* inp_basename,
                const char* out_filename,
                const char* inp_format_digit,
                int* fibre_mask_frm,
                const int min,
                const int max);
int
flames_cubes_to_supercube(
                const char* inp_basename,
                const char* out_filename,
                const char* inp_format_digit,
                const int min,
                const int max);

int
flames_replicate_frame(cpl_frameset* frames,
                       const char* tag,
                       const char* name,
                       cpl_frameset** set);



int
flames_reduce_add_wstart(uves_propertylist* head_ref,
                         uves_propertylist** head_out,
                         const int nord);


#endif
