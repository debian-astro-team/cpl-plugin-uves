/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
  Internet e-mail: midas@eso.org
  Postal address: European Southern Observatory
  Data Management Division 
  Karl-Schwarzschild-Strasse 2
  D 85748 Garching bei Muenchen 
  GERMANY
  ===========================================================================*/

/*-------------------------------------------------------------------------*/
/**
 * @defgroup flames_freeordpos  free allocated memory for ordpos frm 
 *
 */
/*-------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include <stdlib.h>
#include <flames_midas_def.h>
#include <flames_freeordpos.h>
#include <flames_uves.h>
#include <flames_newmatrix.h>
/**@{*/

/*---------------------------------------------------------------------------
  Implementation
  ---------------------------------------------------------------------------*/
/**
   @name  flames_freeordpos()  
   @short  c free allocated memory for ordpos frm 
   @author G. Mulas  -  ITAL_FLAMES Consortium. Ported to CPL by A. Modigliani

   @param ordpos

   @return success or failure code
   DRS Functions called:                                               
   free_dmatrix                                                        
   free_dvector                                                        
   free_ivector                                                        
                                                                      
                                                                         
   Pseudocode:                                                             
   cfree allocated memory                                     

   @note
*/

flames_err 
freeordpos(orderpos *ordpos)
{

  free_dvector(ordpos->start, 0, 1);
  free_dvector(ordpos->step, 0, 1);
  free_ivector(ordpos->npix, 0, 1);
  free_dmatrix(ordpos->orderpol, 0, ordpos->mdegree, 0, ordpos->xdegree);
  free_dvector(ordpos->fibrepos, 0, ordpos->maxfibres-1);
  free_ivector(ordpos->fibremask, 0, ordpos->maxfibres-1);
  free_dvector(ordpos->gaussselfshift, 0, ordpos->maxfibres-1);

  return(NOERR);

}

/**@}*/
