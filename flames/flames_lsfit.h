/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

#include <stdlib.h>
void lsqfit_nr(
                double x[], double y[], double sig[], int ndat, double a[],
                int ma,
                void (*funcs)(double, double [], int));

void
flames_lfit(cpl_vector* vx, cpl_vector* vy, cpl_vector* vs, int32_t ndat,
            double* a, int* ia, int ma, double **covar, double *chisq,
            void (*model)(double, double [], int));
