/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
    Internet e-mail: midas@eso.org
    Postal address: European Southern Observatory
            Data Management Division 
            Karl-Schwarzschild-Strasse 2
            D 85748 Garching bei Muenchen 
            GERMANY
===========================================================================*/
/* Program  : freeframe2.c                                                 */
/* Author   : G. Mulas  -  ITAL_FLAMES Consortium                          */
/* Date     :                                                              */
/*                                                                         */
/* Purpose  : Free memory allocation                                       */
/*                                                                         */
/*                                                                         */
/* Input:  a frame                                                         */ 
/*                                                                      */
/* Output: none                                                             */
/*                                                                         */
/* DRS Functions called:                                                   */
/* free_cvector,free_lvector,free_dvector                                  */ 
/*                                                                         */ 
/* Pseudocode:                                                             */
/* Call library functions to free allocated space                          */ 
/*                                                                         */ 
/* Version  :                                                              */
/* Last modification date: 2002/08/05                                      */
/* Who     When        Why                Where                            */
/* AMo     02-08-05   Add header         header                            */
/*-------------------------------------------------------------------------*/


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include <stdlib.h>
#include <flames_midas_def.h>
#include <flames_uves.h>
#include <flames_freeframe2.h>
#include <flames_newmatrix.h>


flames_err freeframe2(flames_frame *myframe)
{

  free_cvector(myframe->fibremask, 0, myframe->maxfibres-1);
  free_lvector(myframe->ind_lit_fibres, 0, myframe->maxfibres-1);
  if (myframe->nflats > 0) {
    free_dvector(myframe->yshift, 0, myframe->nflats-1);
  }
  
  return(NOERR);

}


