/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.

  Corresponding concerning ESO-MIDAS should be addressed as follows:
    Internet e-mail: midas@eso.org
    Postal address: European Southern Observatory
            Data Management Division 
            Karl-Schwarzschild-Strasse 2
            D 85748 Garching bei Muenchen 
            GERMANY
===========================================================================*/
#ifndef FLAMES_MAINOPTFAST_H
#define FLAMES_MAINOPTFAST_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*----------------------------------------------------------------------------
                             Includes
 ----------------------------------------------------------------------------*/

#include <cpl.h>

/*-----------------------------------------------------------------------------
                             Defines
 ----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                                   Prototypes
 ----------------------------------------------------------------------------*/

int flames_mainoptfast(const char *IN_A,
                       const cpl_frameset *fibff_set,
                       const cpl_frameset *slitff_set,
                       const char *IN_D,
                       const char *IN_E,
                       const char *BASENAME,
                       const double *MAXDISCARDFRACT,
                       const int *MAXBACKITERS,
                       const int *MINOPTITERSINT,
                       const int *MAXOPTITERSINT,
                       const int *XKILLSIZE,
                       const int *YKILLSIZE,
                       const int *BKGPOL,
                       const char *BKGFITINLINE, //was int
                       const char *BKGFITMETHOD,
                       const char *BKGBADSCAN,  //was int
                       const int *BKGBADWIN,
                       const double *BKGBADMAXFRAC,
                       const int *BKGBADMAXTOT,
                       const double *SIGMA,
                       double *OUTPUTD,
                       int *OUTPUTI);

#endif
