/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:31:27 $
 * $Revision: 1.3 $
 * $Name: not supported by cvs2svn $
 */
#ifndef FLAMES_ADD_EXTRA_DES_H
#define FLAMES_ADD_EXTRA_DES_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <cpl.h>

void
flames_add_extra_des(const cpl_frame *ref_frm,
                     cpl_frame *out_frm);
#endif
