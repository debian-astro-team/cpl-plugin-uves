/*
 * This file is part of the UVES Pipeline
 * Copyright (C) 2002, 2003, 2004, 2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2013-08-08 12:31:00 $
 * $Revision: 1.21 $
 * $Name: not supported by cvs2svn $
 *
 */

#ifndef FLAMES_DFS_H
#define FLAMES_DFS_H

/*-----------------------------------------------------------------------------
                                Includes
  -----------------------------------------------------------------------------*/
#include <uves_propertylist.h>
#include <uves_chip.h>

#include <cpl.h>

#include <stdbool.h>

/*-----------------------------------------------------------------------------
                                Defines
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                                Functions prototypes
 -----------------------------------------------------------------------------*/

void
flames_frameset_insert(cpl_frameset *frames, 
		       cpl_frame_group group, 
		       cpl_frame_type type, 
		       cpl_frame_level level,
		       const char *filename,
		       const char *tag, 
		       const uves_propertylist *raw_header,
		       const cpl_parameterlist *parameters, 
		       const char *recipe, 
		       const char *pipeline,
		       cpl_table **qc,
		       const char *start_time,
		       bool dump_paf,
		       unsigned stats_mask);

void
flames_dfs_write_descr(uves_propertylist *header, enum uves_chip chip,
                       bool warn_tech, cpl_table* ozpoz);

void
flames_dfs_set_history_val(uves_propertylist *header, char type,
               const char *name, const char *val);

void
flames_load_table(const cpl_frameset *frames, 
          const char **filename, 
          cpl_table **table, 
          uves_propertylist **pheader,
          uves_propertylist **theader,
          const int extention, 
          const char* tag);

void
flames_load_ordef(const cpl_frameset *frames, const char *chip_name,
          const char **filename, cpl_image **ordef,
          uves_propertylist **ordef_header, enum uves_chip chip);

cpl_error_code
flames_load_ofpos(const cpl_frameset *frames,
          const char **raw_filename,
          cpl_image *raw_image[2],
          uves_propertylist *raw_header[2], 
                  uves_propertylist *rotated_header[2], 
                  cpl_table *ext_tbl[2], 
                  bool *blue);

cpl_error_code
flames_load_frame(const cpl_frameset *frames,
          const char **raw_filename,
          cpl_image *raw_image[2],
          uves_propertylist *raw_header[2], 
                  uves_propertylist *rotated_header[2], 
                  cpl_table *ext_tbl[2], 
          const char* tag);

cpl_error_code
flames_load_frame_index(const cpl_frameset *frames,
            const char **raw_filename,
            cpl_image *raw_image[2],
            uves_propertylist *raw_header[2], 
            uves_propertylist *rotated_header[2], 
            cpl_table *ext_tbl[2], 
            const int indx);

char *
flames_fix_estention(const char* filename);

void 
flames_set_header_char_1(uves_propertylist *header,
			 const char *name, char val);
cpl_error_code
flames_clean_tmp_products_ofpos(enum uves_chip chip, const int out_cubify);

cpl_error_code
flames_clean_tmp_products_sci(enum uves_chip chip, const int out_cubify);

void flames_set_fibremask(cpl_table* table, char* fibremask,
                          const int size, const int mode);
#endif
