/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.

  Corresponding concerning ESO-MIDAS should be addressed as follows:
    Internet e-mail: midas@eso.org
    Postal address: European Southern Observatory
            Data Management Division 
            Karl-Schwarzschild-Strasse 2
            D 85748 Garching bei Muenchen 
            GERMANY
===========================================================================*/
/* Program  : selectavail.c                                                */
/* Author   : G. Mulas  -  ITAL_FLAMES Consortium                          */
/* Date     :                                                              */
/*                                                                         */
/* Purpose  : Missing                                                      */
/*                                                                         */
/*                                                                         */
/* Input:  see interface                                                   */ 
/*                                                                      */
/* Output:                                                              */
/*                                                                         */
/* DRS Functions called:                                                   */
/* none                                                                    */ 
/*                                                                         */ 
/* Pseudocode:                                                             */
/* Missing                                                                 */ 
/*                                                                         */ 
/* Version  :                                                              */
/* Last modification date: 2002/08/05                                      */
/* Who     When        Why                Where                            */
/* AMo     02-08-05   Add header         header                            */
/*-------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <flames_midas_def.h>
#include <flames_uves.h>
#include <flames_newmatrix.h>
#include <flames_shiftcommon.h>
#include <flames_shiftall.h>

flames_err selectavail(allflats *allflatsin, shiftstruct *shiftdata, 
                       fitstruct *fitdata, int32_t iorder,
                       int32_t iframe, int32_t ix,
                       int32_t iy)
{
    int32_t i=0;

    shiftstruct *myshiftdata=0;
    singleflat *myflat=0;
    frame_data *fdvecbuf1=0;
    frame_data *fdvecbuf2=0;
    frame_mask *fmvecbuf1=0;
    int32_t availpixels=0;
    int32_t *myyintoffsets=0;
    int32_t *myixoffsets=0;
    double *myyfracoffsets=0;
    int32_t shiftediy=0;
    int32_t shiftedix=0;
    int32_t shiftediyixindex=0;

    myshiftdata = shiftdata+ix;
    myyintoffsets = myshiftdata->yintoffsets;
    myyfracoffsets = myshiftdata->yfracoffsets;
    myixoffsets = myshiftdata->ixoffsets;
    myflat = allflatsin->flatdata+iframe;
    fdvecbuf1 = myflat->data[0];
    fdvecbuf2 = myflat->sigma[0];
    fmvecbuf1 = myflat->badpixel[0];

    availpixels = 0;
    /* go fetch the appropriate shifted pixels and add them to the list to be
     used for interpolation */
    for (i=0; i<=myshiftdata->numoffsets-1; i++){
        shiftediy = iy-myyintoffsets[i];
        /* does this integer y shift end within frame boundaries */
        if ((shiftediy >= 0) && (shiftediy <= (allflatsin->subrows)-1)) {
            shiftedix = myixoffsets[i];
            shiftediyixindex = (shiftediy*allflatsin->subcols)+shiftedix;
            /* is the shifted one a good pixel? */
            if (fmvecbuf1[shiftediyixindex] == 0) {
                /* yes, it is indeed good: enroll it in the list */
                fitdata->offset[availpixels] = myyfracoffsets[i];
                fitdata->value[availpixels] = fdvecbuf1[shiftediyixindex];
                fitdata->sigma[availpixels] = fdvecbuf2[shiftediyixindex];
                availpixels++;
            }
        }
    }
    fitdata->availpixels = availpixels;

    return(NOERR);

}
