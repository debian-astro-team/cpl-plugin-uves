/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */


#ifndef FLAMES_FF_SLIT_MULTIPLY_H
#define FLAMES_FF_SLIT_MULTIPLY_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <flames_uves.h>


/* the following function takes pointers to an allslitflats structure, an
   orderpos structure, an input allflats and an output allflats and 
   multiplies the input allflats by the appropriate slit FF frame */
flames_err 
ffslitmultiply(allslitflats *slitflats, 
               orderpos *ordpos, 
               allflats *inflats,
               allflats *outflats);


#endif
