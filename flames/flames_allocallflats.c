/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
  Internet e-mail: midas@eso.org
  Postal address: European Southern Observatory
  Data Management Division 
  Karl-Schwarzschild-Strasse 2
  D 85748 Garching bei Muenchen 
  GERMANY
  ===========================================================================*/

/*-------------------------------------------------------------------------*/
/**
 * @defgroup flames_allocallflats   Substep: Initialize allflats structure
 *
 */
/*-------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/



#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdlib.h>
#include <flames_midas_def.h>
#include <flames_uves.h>
#include <flames_newmatrix.h>
#include <flames_allocallflats.h>
/**@{*/

/*---------------------------------------------------------------------------
  Implementation
  ---------------------------------------------------------------------------*/

/**
   @name  flames_allocateflats()  
   @short   Initialize fibre FF frames
   @author G. Mulas  -  ITAL_FLAMES Consortium. Ported to CPL by A. Modigliani

   @param myflats input/output allflats structure

   @return initialized allflats structure
   DRS function called: 
   l3tensor                                                   
   fd3tensor                                                  
   ivector                                                    
   cvector                                                    

   Pseudocode:            
   use calloc,ivector,cvector to initialize to NULL data       

   @note
*/

flames_err allocallflats(allflats *myflats)
{
  int32_t iframe=0;

  /* allocate the flatdata member array itself */
  myflats->flatdata = 
    (singleflat *) calloc((size_t) myflats->nflats, sizeof(singleflat));
  /* allocate submembers of flatdata */
  for (iframe=0; iframe<=myflats->nflats-1; iframe++) {
    myflats->flatdata[iframe].data = 
      fdmatrix(0, myflats->subrows-1, 0, myflats->subcols-1);
    myflats->flatdata[iframe].sigma = 
      fdmatrix(0, myflats->subrows-1, 0, myflats->subcols-1);
    myflats->flatdata[iframe].badpixel = 
      fmmatrix(0, myflats->subrows-1, 0, myflats->subcols-1);
    myflats->flatdata[iframe].framename = cvector(0, CATREC_LEN);
    myflats->flatdata[iframe].sigmaname = cvector(0, CATREC_LEN);
    myflats->flatdata[iframe].badname = cvector(0, CATREC_LEN);
    myflats->flatdata[iframe].fibres =
      lvector(0,myflats->maxfibres-1);
    /* do not allocate back members here, I have no clue as to their
       sizes */
  }
  /* now allocate allflats members */
  myflats->fibremask = ivector(0,myflats->maxfibres-1);
  myflats->fibre2frame = ivector(0,myflats->maxfibres-1);
  myflats->normfactors = 
    fd3tensor(0, myflats->lastorder-myflats->firstorder, 
	      0, myflats->maxfibres-1, 0, myflats->subcols-1);
  myflats->normsigmas = 
    fd3tensor(0, myflats->lastorder-myflats->firstorder, 
	      0, myflats->maxfibres-1, 0, myflats->subcols-1);
  myflats->goodfibres = 
    fm3tensor(0, myflats->lastorder-myflats->firstorder, 
	      0, myflats->maxfibres-1, 0, myflats->subcols-1);
  myflats->lowfibrebounds = 
    l3tensor(0, myflats->lastorder-myflats->firstorder, 
	     0, myflats->maxfibres-1, 0, myflats->subcols-1);
  myflats->highfibrebounds = 
    l3tensor(0, myflats->lastorder-myflats->firstorder, 
	     0, myflats->maxfibres-1, 0, myflats->subcols-1);
  /* ok, finished */
  /* don't allocate the back structure: as I said in a comment above, you 
     can't possibly know here Window_Number, you would only pick up
     uninitialised memory garbage! */

  return(NOERR);

}
/**@}*/
