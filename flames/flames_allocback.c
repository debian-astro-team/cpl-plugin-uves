/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
  Internet e-mail: midas@eso.org
  Postal address: European Southern Observatory
  Data Management Division 
  Karl-Schwarzschild-Strasse 2
  D 85748 Garching bei Muenchen 
  GERMANY
  ===========================================================================*/
/*-------------------------------------------------------------------------*/
/**
 * @defgroup flames_allocback   Substep: Initialize background frame
 *
 */
/*-------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <flames_midas_def.h>
#include <flames_uves.h>
#include <flames_newmatrix.h>
#include <flames_allocback.h>

/**@{*/

/*---------------------------------------------------------------------------
  Implementation
  ---------------------------------------------------------------------------*/

/**
   @name  flames_allocateback()  
   @short   Initialize background frames
   @author G. Mulas  -  ITAL_FLAMES Consortium. Ported to CPL by A. Modigliani

   @param back input/output background structure

   @return initialized allflats structure
   DRS function called: 
   dvector                                                    
   dmatrix                                                    

   Pseudocode:            
   use dvector, dmatrix to initialize to NULL data             

   @note
*/

flames_err allocback(flames_background *back)
{
  int32_t i,j,k;
  double dj;
 
  back->x = dvector(1, (int32_t) back->Window_Number);
  memset(back->x+1, 0, (size_t)back->Window_Number*sizeof(double));
  back->y = dvector(1, (int32_t) back->Window_Number);
  memset(back->y+1, 0, (size_t)back->Window_Number*sizeof(double));
  back->window = dmatrix(1, (int32_t) back->Window_Number, 1, 5);
  for (i=1; i<=(int32_t)back->Window_Number; i++) {
    for (j=1; j<=5; j++) {
      back->window[i][j] = 0;
    }
  }
  back->coeff = dvector(1, (int32_t)((back->xdegree+1)*(back->ydegree+1)));
  memset(back->coeff+1, 0, (size_t)((back->xdegree+1)*(back->ydegree+1))*
	 sizeof(double));
  back->expon = dmatrix(1, 2, 1, 
			(int32_t)((back->xdegree+1)*(back->ydegree+1)));
  k = 1;
  for (i=0; i<=(int32_t)back->ydegree; i++) {
    double di = (double) i;
    for (j=0; j<=(int32_t)back->xdegree; j++) {
      dj = (double) j;
      back->expon[1][k] = di;
      back->expon[2][k] = dj;
      k++;
    }
  }

  return(NOERR);

}

/**@}*/


