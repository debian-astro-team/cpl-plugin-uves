/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
  Internet e-mail: midas@eso.org
  Postal address: European Southern Observatory
  Data Management Division 
  Karl-Schwarzschild-Strasse 2
  D 85748 Garching bei Muenchen 
  GERMANY
  ===========================================================================*/
/*-------------------------------------------------------------------------*/
/**
 * @defgroup flames_freespectrum  free allocated memory for science frm 
 *
 */
/*-------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif


#include <math.h>
#include <stdio.h>
#include <stdlib.h>
/* MIDAS include files */
#include <flames_midas_def.h>
/* FLAMES-UVES include files */ 
#include <flames_uves.h>
#include <flames_freespectrum.h>
#include <flames_newmatrix.h>
/**@{*/

/*---------------------------------------------------------------------------
  Implementation
  ---------------------------------------------------------------------------*/
/**
   @name  flames_freespectrum()  
   @short free allocated memory for science frm
   @author G. Mulas  -  ITAL_FLAMES Consortium. Ported to CPL by A. Modigliani

   @param ScienceFrame

   @return success or failure code

   DRS Functions called:                                                  
   free_fd3tensor                                                         
   free_fm3tensor                                                         

   Pseudocode:                                                            
   free allocated memory                                                   

   @note
 */

flames_err 
free_spectrum(flames_frame *ScienceFrame)
{ 
    free_fd3tensor(ScienceFrame->spectrum, 0, ScienceFrame->subcols-1,
                    0, ScienceFrame->lastorder-ScienceFrame->firstorder,
                    0, ScienceFrame->maxfibres-1);
    free_fd3tensor(ScienceFrame->specsigma, 0, ScienceFrame->subcols-1,
                   0, ScienceFrame->lastorder-ScienceFrame->firstorder,
                   0, ScienceFrame->maxfibres-1);
    free_fd3tensor(ScienceFrame->normspec, 0, ScienceFrame->subcols-1,
                   0, ScienceFrame->lastorder-ScienceFrame->firstorder,
                   0, ScienceFrame->maxfibres-1);
    free_fd3tensor(ScienceFrame->normsigma, 0, ScienceFrame->subcols-1,
                   0, ScienceFrame->lastorder-ScienceFrame->firstorder,
                   0, ScienceFrame->maxfibres-1);
    free_fd3tensor(ScienceFrame->speccovar, 0, ScienceFrame->subcols-1,
                   0, ScienceFrame->lastorder-ScienceFrame->firstorder,
                   0, ScienceFrame->maxfibres-1);
    free_fm3tensor(ScienceFrame->specmask, 0, ScienceFrame->subcols-1,
                   0, ScienceFrame->lastorder-ScienceFrame->firstorder,
                   0, ScienceFrame->maxfibres-1);
    free_fm3tensor(ScienceFrame->normmask, 0, ScienceFrame->subcols-1,
                   0, ScienceFrame->lastorder-ScienceFrame->firstorder,
                   0, ScienceFrame->maxfibres-1);

    return NOERR;
}
/**@}*/
