/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */
 
/*
 * $Author: amodigli $
 * $Date: 2013-07-22 07:27:49 $
 * $Revision: 1.15 $
 * $Name: not supported by cvs2svn $
 *
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*----------------------------------------------------------------------------*/
/**
 * @defgroup flames_orderpos  Recipe: Order Position
 *
 * This recipe determines the echelle order locations.
 * See man-page for details.
 */
/*----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#include <flames.h>

#include <uves_orderpos_body.h>
#include <uves_recipe.h>
#include <uves.h>
#include <uves_error.h>

#include <cpl.h>

/**@{*/
/*-----------------------------------------------------------------------------
                            Forward declarations
 -----------------------------------------------------------------------------*/
static int flames_cal_orderpos_define_parameters(cpl_parameterlist *parameters);
const char * const flames_orderpos_desc_short = "Defines uves-echelle-simultaneous calibration fibre order positions";



const char * const flames_orderpos_desc =
"The recipe defines the simultaneus calibration fibre order positions in a \n"
"fibre-echelle image. The orders are initially detected by means of a Hough \n"
"transformation, the orders are then traced, and the positions are finally \n"
"fitted with a global polynomial.\n"
"\n"
"Expected input frames are simultaneous calibration fibre flat fields, \n"
"FIB_ORDEF_RED and optionally for each \n"
"chip a DRS setup table, DRS_TABLE_(REDL|REDU), for backward \n"
"compatibility, and optionally a guess order table\n"
" FIB_ORD_GUE_(REDL|REDU) and a master bias, MASTER_BIAS_(REDL|REDU).\n"
"The recipe processes only the first raw frame found.\n"
"\n"
"Output are two order tables FIB_ORD_TAB_(REDL|REDU) contaning\n"
"the columns:\n"
"X                : Position along x\n"
"Order            : Relative order number\n"
"Y                : Order line centroid location\n"
"Yfit             : The fitted order location\n"
"dY               : Uncertainty of Y\n"
"dYfit_Square     : Variance of Yfit\n"
"Residual         : Y - Yfit\n"
"Residual_Square  : Residual^2\n"
"OrderRMS         : Root mean squared residual of initial\n"
"                   one-dimensional linear fit of order\n"
"\n"
"The bivariate fit polynomial itself is stored in table extension no. 2.\n"
"The 3rd table extension contains a table that defines the active fibre traces\n"
"and their positions (for support of FLAMES/UVES) and 2 images frames \n"
"FIB_ORDEF_(REDL|REDU)\n";


/*-----------------------------------------------------------------------------
                            Recipe standard code
 -----------------------------------------------------------------------------*/
#define cpl_plugin_get_info flames_cal_orderpos_get_info
UVES_RECIPE_DEFINE(
    FLAMES_CAL_ORDERPOS_ID, FLAMES_CAL_ORDERPOS_DOM, 
    flames_cal_orderpos_define_parameters,
    "Jonas M. Larsen", "cpl@eso.org",
    flames_orderpos_desc_short,
    flames_orderpos_desc);
/*-----------------------------------------------------------------------------
                              Functions code
 -----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/**
  @brief    Setup the recipe options    
  @param    parameters        the parameterlist to fill
  @return   0 if everything is ok
 */
/*----------------------------------------------------------------------------*/
static int
flames_cal_orderpos_define_parameters(cpl_parameterlist *parameters)
{
    return uves_orderpos_define_parameters_body(parameters, make_str(FLAMES_CAL_ORDERPOS_ID));
}
/*----------------------------------------------------------------------------*/
/**
  @brief    Get the command line options and execute the data reduction
  @param    parameters  the parameters list
  @param    frames      the frames list
  @return   CPL_ERROR_NONE if everything is ok
 */
/*----------------------------------------------------------------------------*/
static void
flames_cal_orderpos_exe(cpl_frameset *frames, const cpl_parameterlist *parameters,
            const char *starttime)
{
    bool flames = true;
    uves_orderpos_exe_body(frames, flames, make_str(FLAMES_CAL_ORDERPOS_ID),
               parameters, starttime);
    return;
}
/**@}*/


