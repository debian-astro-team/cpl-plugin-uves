/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
  Internet e-mail: midas@eso.org
  Postal address: European Southern Observatory
  Data Management Division 
  Karl-Schwarzschild-Strasse 2
  D 85748 Garching bei Muenchen 
  GERMANY
  ===========================================================================*/
/*-------------------------------------------------------------------------*/
/**
 * @defgroup flames_allocframe   Substep: Initialize a frame
 *
 */
/*-------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdlib.h>
#include <flames_midas_def.h>
#include <flames_uves.h>
#include <flames_allocordpos.h>
#include <flames_newmatrix.h>
/*---------------------------------------------------------------------------
  Implementation
  ---------------------------------------------------------------------------*/
/**
   @name  flames_allocordpos()  
   @short  Initialize an ordpos frame
   @author G. Mulas  -  ITAL_FLAMES Consortium. Ported to CPL by A. Modigliani

   @param ordpos pointer to a frame

   @return initialized frame

   DRS Functions called:                                                  
   dmatrix                                                    
   dvector                                                    
   ivector                                                    
                                                                         
   Pseudocode:                                                             
   use dmatrix,dvector,lvector                                 
   to initialize to NULL data                                  

   @note
*/
flames_err allocordpos(orderpos *ordpos)
{

  ordpos->start = dvector(0,1);
  ordpos->step = dvector(0,1);
  ordpos->npix = ivector(0,1);
  ordpos->orderpol = dmatrix(0,ordpos->mdegree, 0, ordpos->xdegree);
  ordpos->fibrepos = dvector(0, ordpos->maxfibres-1);
  ordpos->fibremask = ivector(0, ordpos->maxfibres-1);
  ordpos->gaussselfshift = dvector(0, ordpos->maxfibres-1);

  return(NOERR);

}
/**@}*/
