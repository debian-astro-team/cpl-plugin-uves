/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

#ifndef FLAMES_CORREL_H
#define FLAMES_CORREL_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include<flames_uves.h>


/* 
   The following function gives the optimal shift for the FF 
   in order to use them as a physical model for the perpendicular
   shape of the PSF on the CCD by calculating the correlation 
   between Science Frame and FF.
*/

flames_err 
correl(flames_frame *ScienceFrame, 
       allflats *SingleFF, 
       orderpos *Order, 
       double ytolerance, 
       int32_t maxshift, 
       double *finaldeltas,
       frame_mask *finalmask, 
       char *outtabname);

#endif












