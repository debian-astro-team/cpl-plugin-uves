/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

#ifndef FLAMES_STAND_EXTRACT_H
#define FLAMES_STAND_EXTRACT_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <flames_uves.h>

/* This function makes the dirty work for the standard extraction */

flames_err 
Std_Extract(flames_frame *ScienceFrame, 
            allflats *SingleFF, 
            orderpos *Order,
            int32_t ordsta, 
            int32_t ordend, 
            int32_t ix,
            frame_mask **mask, 
            double **aa, 
            double **varaa,
            double **xx, 
            double *xx2, 
            double **covarxx,
            double **covariance, 
            int32_t *fibrestosolve,
            int32_t *orderstosolve, 
            int32_t *numslices,
            double ***pfibrecentre, 
            double phalfwinsize,
            double *pylow, 
            double *pyup, 
            int32_t *ylow,
            int32_t *yup, 
            int32_t arraysize);

#endif
