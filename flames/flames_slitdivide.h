/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

#ifndef FLAMES_SLIT_DIVIDE_H
#define FLAMES_SLIT_DIVIDE_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <flames_uves.h>

/* the following function takes pointers to an allslitflats structure, an
   orderpos structure, an input frame and an output frame and divides the
   input frame by the appropriate slit FF frame */
flames_err 
slitdivide(allslitflats *slitflats, 
           orderpos *ordpos, 
           flames_frame *inframe,
           flames_frame *outframe);

#endif
