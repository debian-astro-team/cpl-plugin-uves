/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
  Internet e-mail: midas@eso.org
  Postal address: European Southern Observatory
  Data Management Division 
  Karl-Schwarzschild-Strasse 2
  D 85748 Garching bei Muenchen 
  GERMANY
  ===========================================================================*/
/*-------------------------------------------------------------------------*/
/**
 * @defgroup flames_initemplate   Substep: initialize frm
 *
 */
/*-------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/
/**@{*/

/*---------------------------------------------------------------------------
  Implementation
  ---------------------------------------------------------------------------*/


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif


#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <flames_midas_def.h>
#include <flames_uves.h>
#include <flames_newmatrix.h>
#include <flames_initemplate.h>
#include <uves_msg.h>
#include <flames_alloctemplate.h>

/**
   @name  flames_initemplate()  
   @short  initialize frm
   @author G. Mulas  -  ITAL_FLAMES Consortium. Ported to CPL by A. Modigliani

   @param frameid
   @param slitflats

   @return success or failure code

   DRS Functions called:          
   none                                         

   Pseudocode:                                                             
   check several MIDAS descriptors                                     

   @note
 */

flames_err 
initemplate(flames_frame *myframe, 
            char *filename,
            const char *H_RON_L,
            const char *H_RON_U,
            const char *H_GAIN_L,
            const char *H_GAIN_U)
{
    char chipchoice='\0';

    char h_ron_l[17];
    char h_ron_u[17];

    char h_gain_l[18];
    char h_gain_u[18];

    int status=0;
    int fileid=0;
    int actvals=0;
    int unit=0;
    int null=0;
    int naxis=0;
    int maxfibres=0;
    double ron=0;
    double gain=0;
    int32_t ix=0;
    int32_t iyixlimit=0;
    double start[2]={0,0};
    double step[2]={0,0};
    int npix[2]={0,0};
    frame_mask *fmvecbuf1=0;

    memset(h_ron_l, 0, 17);
    memset(h_ron_u, 0, 17);

    memset(h_gain_l, 0, 18);
    memset(h_gain_u, 0, 18);
    /* try to open the frame */
    if (0 != SCFOPN(filename, FLAMESDATATYPE, 0, F_IMA_TYPE, &fileid)) {
        /* I could not open the frame */
        return(MAREMMA);
    }
    /* is it a 2D image? */
    if (0 != SCDRDI(fileid, "NAXIS", 1, 1, &actvals, &naxis, &unit, &null)) {
        /* something went wrong in SCDRDI */
        return(MAREMMA);
    }
    if (naxis != 2) {
        /* wrong dimensions, wrong frames, I suppose... */
        return(MAREMMA);
    }

    /* read all relevant scalar descriptors */
    /* read start, step and npix from the frame */
    if (0 != SCDRDD(fileid, "START", 1, naxis, &actvals, start, &unit, &null)) {
        /* something went wrong in SCDRDD */
        return(status);
    }
    if (0 != SCDRDD(fileid, "STEP", 1, naxis, &actvals, step, &unit, &null)) {
        /* something went wrong in SCDRDD */
        return(status);
    }
    if (0 != SCDRDI(fileid, "NPIX", 1, naxis, &actvals, npix, &unit, &null)) {
        /* something went wrong in SCDRDI */
        return(status);
    }
    /* Where should the maxfibres information be stored? The most sensible
     way would perhaps be a frame descriptor in the ESO.* hierarchy.
     Remember to talk to amodigli about this */
    if (0 != SCDRDC(fileid, "CHIPCHOICE", 1, 1, 1, &actvals, &chipchoice, &unit,
    		&null)) {
        /* something went wrong in SCDRDC */
        return(status);
    }




    if (0 != SCKRDC(H_RON_L,16,1,1,&actvals, h_ron_l, &unit, &null)) {
        /* something went wrong in SCDRDC */
        return(status);
    }
    if ( 0 != SCKRDC(H_RON_U,16,1,1,&actvals, h_ron_u, &unit, &null)) {
        /* something went wrong in SCDRDC */
        return(status);
    }


    if (0 != SCKRDC(H_GAIN_L,17,1,1,&actvals, h_gain_l, &unit, &null)) {
        /* something went wrong in SCDRDC */
        return(status);
    }

    if (0 != SCKRDC(H_GAIN_U,17,1,1,&actvals, h_gain_u, &unit, &null)) {
        /* something went wrong in SCDRDC */
        return(status);
    }

    switch(chipchoice) {
    case 'u':
        /* upper chip, read the corresponding data */
        if (0 !=SCDRDD(fileid, h_ron_l, 1, 1, &actvals, &ron, &unit, &null)) {
            /* something went wrong in SCDRDD */
            return(status);
        }
        if (0 !=SCDRDD(fileid, h_gain_l, 1, 1, &actvals, &gain, &unit, &null)) {
            /* something went wrong in SCDRDD */
            return(status);
        }
        break;

    case 'l':
        /* lower chip, read the corresponding data */
        /* why h_ron_u=OUT4 is used in lower chip case */
        if (0 !=SCDRDD(fileid, h_ron_u, 1, 1, &actvals, &ron, &unit, &null)) {
            /* something went wrong in SCDRDD */
            return(status);
        }
        /* why h_gain_u=OUT4 is used in lower chip case */
        if (0 !=SCDRDD(fileid, h_gain_u, 1, 1, &actvals, &gain, &unit, &null)) {
            /* something went wrong in SCDRDD */
            return(status);
        }
        break;
    default:
        /* nothing to do here, actually the switch must have one of the
       above 2 values, if it doesn't just complain and quit */
        return(MAREMMA);
        break;
    }
    if (0 !=SCDRDI(fileid, "MAXFIBRES", 1, 1, &actvals, &maxfibres, &unit,
                    &null)) {
        /* something went wrong in SCDRDI */
        return(status);
    }
    /* set up scalar member values in myframe */
    myframe->subcols = (int32_t) npix[0];
    myframe->subrows = (int32_t) npix[1];
    myframe->maxfibres = maxfibres; /* dummy value */
    myframe->chipchoice = chipchoice;
    myframe->substartx = start[0];
    myframe->substarty = start[1];
    myframe->substepx = step[0];
    myframe->substepy = step[1];
    myframe->ron = ron;
    myframe->gain = gain;
    uves_msg("subcols=%d subrows=%d maxfibres=%d chipchoice=%c substartx=%f substarty=%f substepx=%f substepy=%f ron=%f gain=%f",
             myframe->subcols,
             myframe->subrows,
             myframe->maxfibres,
             myframe->chipchoice,
             myframe->substartx,
             myframe->substarty,
             myframe->substepx,
             myframe->substepy,
             myframe->ron,
             myframe->gain);

    /* these will be set up during the reduction, they are still
     undefined here */
    myframe->firstorder = 0;
    myframe->lastorder = 0;
    myframe->tab_io_oshift = 0;
    myframe->nflats = 0;
    myframe->back.Window_Number = 0;

    /* allocate dynamic submembers here */
    if (NOERR != alloctemplate(myframe)) {
        /* alloctemplate returned an error */
        return(MAREMMA);
    }

    /* fill the bad pixel mask with zeros */
    iyixlimit = (myframe->subrows*myframe->subcols)-1;
    fmvecbuf1 = myframe->badpixel[0];
    for (ix=0; ix<=iyixlimit; ix++) {
        fmvecbuf1[ix] = 0;
    }

    /* close image frame */
    if (0 != SCFCLO(fileid)) {
        /* error closing image file */
        return(MAREMMA);
    }

    /* template frame successfully setup from image frame */

    return(NOERR);

}
/**@}*/










