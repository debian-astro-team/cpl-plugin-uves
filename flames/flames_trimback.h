/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

#ifndef FLAMES_TRIM_BACK_H
#define FLAMES_TRIM_BACK_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <flames_uves.h>


/* the following function trims the background table contained in
   Frame->back to ensure it does not contain any pixels covered by fibres */
flames_err 
trimback(flames_frame *Frame, 
         orderpos *Order, 
         int32_t **wxlow, 
         int32_t **wxup,
         int32_t **wylow, 
         int32_t **wyup,
         scatterswitch2 bkgswitch2,
         int badwinxsize, 
         int badwinysize,
         double badfracthres, 
         int badtotthres);


#endif
