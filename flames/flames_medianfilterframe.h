/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

#ifndef FLAMES_MEDIAN_FILTER_FRAME_H
#define FLAMES_MEDIAN_FILTER_FRAME_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <flames_uves.h>


/* the following function takes a pointer to a frame structure, two int32_t
   integers xhalfwindow and yhalfwindow which define the neighborhood of 
   each pixel to be considered, a maximum number of iterations, a 
   kappa-sigma clipping constant kappa2; for each good pixel, it computes
   the median of its neighborhood, as defined by xhalfwindow and yhalfwindow, 
   and compare it to the pixel value: if the squared difference is larger
   than kappa2 times the pixel variance, the pixel is marked bad in a 
   temporary bad pixel mask; after all the frame has been scanned, the 
   temporary bad pixel mask is copied onto the one of the frame, and the
   whole cleanup procedure iterated until no more bad pixels are found or
   the maximum number of iterations has been reached. */
flames_err 
medianfilterframe(flames_frame *myframe, 
                  int32_t xhalfwindow,
                  int32_t yhalfwindow,
                  int32_t maxiters,
                  double kappa2);


#endif
