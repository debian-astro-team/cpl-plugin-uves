/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
    Internet e-mail: midas@eso.org
    Postal address: European Southern Observatory
            Data Management Division 
            Karl-Schwarzschild-Strasse 2
            D 85748 Garching bei Muenchen 
            GERMANY
===========================================================================*/
/* Program  : fastprepfibreff.c                                            */
/* Author   : G. Mulas  -  ITAL_FLAMES Consortium                          */
/* Date     :                                                              */
/*                                                                         */
/* Purpose  : Missing                                                      */
/*                                                                         */
/*                                                                         */
/* Input:  see interface                                                   */ 
/*                                                                      */
/* Output:                                                              */
/*                                                                         */
/* DRS Functions called:                                                   */
/* none                                                                    */ 
/*                                                                         */ 
/* Pseudocode:                                                             */
/* Missing                                                                 */ 
/*                                                                         */ 
/* Version  :                                                              */
/* Last modification date: 2002/08/05                                      */
/* Who     When        Why                Where                            */
/* AMo     02-08-05   Add header         header                            */
/*-------------------------------------------------------------------------*/
#ifndef FLAMES_FASTPREPFIBREFF_H
#define FLAMES_FASTPREPFIBREFF_H
#include <cpl.h>

int flames_fastprepfibreff(const cpl_frameset *INFIBREFFCAT,
               cpl_frameset **OUTFIBREFFCAT,
                           const char *MYORDER,
                           const char *BASENAME,
                           const char *BACKTABLE,
                           const double *MAXDISCARDFRACT,
                           const int *MAXBACKITERS,
                           const double *FRACSLICESTHRES,
                           const int *BKGPOL,
                           const char *BKGFITMETHOD,
                           const char *BKGBADSCAN,
                           const int *BKGBADWIN,
                           const double *BKGBADMAXFRAC,
                           const int *BKGBADMAXTOT,
                           const double *SIGMA,
                           const double *MINFIBREFRAC,
                           int *OUTPUTI);

#endif
