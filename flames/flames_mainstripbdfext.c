/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.

  Corresponding concerning ESO-MIDAS should be addressed as follows:
    Internet e-mail: midas@eso.org
    Postal address: European Southern Observatory
            Data Management Division 
            Karl-Schwarzschild-Strasse 2
            D 85748 Garching bei Muenchen 
            GERMANY
===========================================================================*/
/* Program  : mainstripfitsast.c                                            */
/* Author   : G. Mulas  -  ITAL_FLAMES Consortium                          */
/* Date     :                                                              */
/*                                                                         */
/* Purpose  : Missing                                                      */
/*                                                                         */
/*                                                                         */
/* Input:  none                                                            */ 
/*                                                                      */
/* Output:                                                              */
/*                                                                         */
/* DRS Functions called:                                                   */
/* stripfitsext()                                                           */ 
/*                                                                         */ 
/* Pseudocode:                                                             */
/* Missing                                                                 */ 
/*                                                                         */ 
/* Version  :                                                              */
/* Last modification date: 2002/08/05                                      */
/* Who     When        Why                Where                            */
/* AMo     02-08-05   Add header         header                            */
/*-------------------------------------------------------------------------*/
/* C functions include files */ 

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif


#include <stdlib.h>
#include <string.h>
/* MIDAS include files */
#include <flames_midas_def.h>
#include <flames_stripfitsext.h>
/* FLAMES-UVES include files */ 
#include <flames_uves.h>

int flames_mainstripfitsext(const char *FILENAME,
                            char *BASENAME);

int flames_mainstripfitsext(const char *FILENAME,
                            char *BASENAME)

{

    int actvals=0;
    char *filename=0;
    char *basename=0;
    int baselength=0;
    int unit=0;

    filename = calloc(CATREC_LEN, sizeof(char));
    basename = calloc(CATREC_LEN, sizeof(char));

    SCSPRO("stripfitsext"); /* Get into MIDAS Env. */

    /* Read once and for all, here at the beginning, what MIDAS keywords we
    need */

    /* initialise FILENAME from keyword */
    if (SCKGETC(FILENAME, 1, 160, &actvals, filename) != 0) {
        /* problems reading FILENAME */
        SCTPUT("Error reading the FILENAME keyword");
        free(basename);
        free(filename);
        return flames_midas_fail();
    }

    /* hopefully, filename contains something... */
    if (strlen(filename)<=0) {
        SCTPUT("Error: zero length FILENAME");
        free(basename);
        free(filename);
        return flames_midas_fail();
    }

    /* strip the fits extension from filename */
    if (stripfitsext(filename, basename)!=NOERR) {
        SCTPUT("Error in stripfitsext()");
        free(basename);
        free(filename);
        return flames_midas_fail();
    }

    if ((baselength=(strlen(basename)+1))>160) baselength=160;

    /* write the stripped basename to the BASENAME keyword */
    if (SCKWRC(BASENAME, 160, basename, 1, 1, &unit)!=0) {
        SCTPUT("Error writing BASENAME keyword");
        free(basename);
        free(filename);
        return flames_midas_fail();
    }

    /* all is well, end gracefully */
    free(basename);
    free(filename);
    return SCSEPI();

}
