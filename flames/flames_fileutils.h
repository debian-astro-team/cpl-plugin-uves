/* $Id: flames_fileutils.h,v 1.1 2007-05-07 06:59:43 amodigli Exp $
 *
 *   This file is part of the ESO Common Pipeline Library
 *   Copyright (C) 2001-2006 European Southern Observatory
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef FLAMES_FILEUTILS_H
#define FLAMES_FILEUTILS_H


CPL_BEGIN_DECLS

const char *flames_fileutils_tilde_replace ( const char * );
const char * flames_fileutils_dot_replace ( const char *name );
int flames_fileutils_directory_exists(const char *);
int flames_fileutils_file_exists(const char *);
char *flames_fileutils_create_fqfname(char *, char *);
char *flames_fileutils_fqfname_filename(const char *);
char *flames_fileutils_fqfname_dirname(const char *);
int flames_fileutils_copy(const char *, const char *);
int flames_fileutils_move(const char *, const char *);
int flames_fileutils_link(const char *, const char *);


CPL_END_DECLS

#endif /* FLAMES_FILEUTILS_H */
