/*                                                                            *
 *   This file is part of the ESO UVES Pipeline                               *
 *   Copyright (C) 2004,2005 European Southern Observatory                    *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                           */
/*
 * $Author: amodigli $
 * $Date: 2013-07-22 07:24:32 $
 * $Revision: 1.11 $
 * $Name: not supported by cvs2svn $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*---------------------------------------------------------------------------*/
/**
 * @defgroup flames_add_extra_des  copy descriptors
 */
/*---------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
  Includes
  ----------------------------------------------------------------------------*/

#include <flames_add_extra_des.h>

#include <uves_dfs.h>
#include <uves_utils_wrappers.h>
#include <uves_dump.h>
#include <uves_pfits.h>
#include <uves_error.h>
#include <uves_msg.h>
#include <string.h>
/*-----------------------------------------------------------------------------
  Functions prototypes
  ----------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/**
   @brief    copy all descriptors from in_frm to out_frm

   @param    ref_frm        source
   @param    out_frm        destination table
  
**/
/*---------------------------------------------------------------------------*/
void
flames_add_extra_des(const cpl_frame *ref_frm,
                     cpl_frame *out_frm)
{

  const char *dprtype;
  uves_propertylist *ref_frm_header = NULL;
  uves_propertylist *out_frm_header = NULL;
  cpl_table *t = NULL;
  cpl_image *image = NULL;

  check( ref_frm_header = uves_propertylist_load(cpl_frame_get_filename(ref_frm), 0),
	 "Could not load %s header", cpl_frame_get_filename(ref_frm));

  check( out_frm_header = uves_propertylist_load(cpl_frame_get_filename(out_frm), 0),
	 "Could not load %s header", cpl_frame_get_filename(out_frm));

  check_nomsg( dprtype = uves_pfits_get_dpr_type(ref_frm_header));

  uves_msg_debug("Propagating keywords from %s to %s...",
		 cpl_frame_get_filename(ref_frm),
		 cpl_frame_get_filename(out_frm));

  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, "ORIGIN"));
  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, UVES_DATE));
  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, "TELESCOPE"));
  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, "TELESCOP"));

  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, "INSTRUME"));
  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, UVES_OBJECT));
  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, UVES_RA));
  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, UVES_DEC));
  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, "EQUINOX"));//O_POS

  //check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, "RADECSYS"));
  if (!uves_propertylist_has(out_frm_header, "RADECSYS") && !uves_propertylist_has(out_frm_header, "RADESYS")) {
    for (char *l[] = {"RADESYS", "RADECSYS", 0}, **i = l; *i; ++i) {
      if (uves_propertylist_has(ref_frm_header, *i)) {
        char * radeval = cpl_strdup(uves_propertylist_get_string(ref_frm_header, *i));
        char * radecmt = cpl_strdup(uves_propertylist_get_comment(ref_frm_header, *i));
        uves_propertylist_update_string(out_frm_header, "RADESYS", radeval);
        uves_propertylist_set_comment(out_frm_header, "RADESYS", radecmt);
        cpl_free(radeval);
        cpl_free(radecmt);
        break;
      }
    }
  }

  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, UVES_EXPTIME));
  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, "DATE-OBS"));//O_TIME
  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, UVES_AIRMASS));//O_AIRM
  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, "EPOCH"));//O_POS
  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, UVES_UTC));
  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, "LST"));
  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, UVES_UT));
  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, UVES_ST));

  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, "PI-COI"));
  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, "OBSERVER"));
  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, UVES_CTYPE1));
  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, UVES_CTYPE2));
  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, UVES_CRVAL1));
  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, UVES_CRVAL2));
  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, UVES_CRPIX1));
  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, UVES_CRPIX2));
  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, UVES_CDELT1));
  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, UVES_CDELT2));
  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, "EXTEND"));
  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, "ARCFILE"));
  check_nomsg( uves_copy_if_possible(out_frm_header, ref_frm_header, "ORIGFILE"));

  check( uves_propertylist_copy_property_regexp(
						out_frm_header, ref_frm_header, "^ESO.*", 0),
	 "Could propagate ESO.* keywords");

  if (strstr(dprtype, "SimCal") != NULL) {
    check_nomsg( uves_pfits_set_ocs_simcal(out_frm_header, 1));
  }

  /* Save the header */
  switch(cpl_frame_get_type(out_frm)) {
  case CPL_FRAME_TYPE_TABLE:
    check( t = cpl_table_load(cpl_frame_get_filename(out_frm), 1, 1),
	   "Could not load table %s", cpl_frame_get_filename(out_frm));
    check( uves_table_save(t,
			   out_frm_header,
			   NULL,
			   cpl_frame_get_filename(out_frm),
			   CPL_IO_DEFAULT),
	   "Could not update table %s header", cpl_frame_get_filename(out_frm));
    break;
  case CPL_FRAME_TYPE_IMAGE:
    check( image = uves_load_image(out_frm,
				   0, 0, NULL),
	   "Could not load image %s", cpl_frame_get_filename(out_frm));
    check( uves_save_image(image, cpl_frame_get_filename(out_frm), 
			   out_frm_header, true, true),
	   "Could not update image %s header", cpl_frame_get_filename(out_frm));
    break;
  default:
    assure( false, CPL_ERROR_UNSUPPORTED_MODE,
	    "Frame type is %s", 
	    uves_tostring_cpl_frame_type(cpl_frame_get_type(out_frm)));
    break;
  }
    
  uves_msg_debug("Done propagating keywords from %s to %s...",
		 cpl_frame_get_filename(ref_frm),
		 cpl_frame_get_filename(out_frm));


 cleanup:
  uves_free_table(&t);
  uves_free_image(&image);
  uves_free_propertylist(&ref_frm_header);
  uves_free_propertylist(&out_frm_header);
  return;
}

/**@}*/
