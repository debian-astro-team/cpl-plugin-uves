/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)

  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.

  Corresponding concerning ESO-MIDAS should be addressed as follows:
    Internet e-mail: midas@eso.org
    Postal address: European Southern Observatory
            Data Management Division 
            Karl-Schwarzschild-Strasse 2
            D 85748 Garching bei Muenchen 
            GERMANY
===========================================================================*/
/* Program  : mainstandFF.c                                                */
/* Author   : G. Mulas  -  ITAL_FLAMES Consortium                          */
/* Date     :                                                              */
/*                                                                         */
/* Purpose  : Missing                                                      */
/*                                                                         */
/*                                                                         */
/* Input:  see interface                                                   */ 
/*                                                                      */
/* Output:                                                              */
/*                                                                         */
/* DRS Functions called:                                                   */
/* none                                                                    */ 
/*                                                                         */ 
/* Pseudocode:                                                             */
/* Missing                                                                 */ 
/*                                                                         */ 
/* Version  :                                                              */
/* Last modification date: 2002/08/05                                      */
/* Who     When        Why                Where                            */
/* AMo     02-08-05   Add header         header                            */
/*-------------------------------------------------------------------------*/
/* C functions include files */ 
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/* MIDAS include files */
#include <uves_msg.h>
#include <flames_writenormfactors.h>

#include <flames_prepstand.h>
#include <flames_optsynth.h>
#include <flames_writesynth.h>
#include <flames_gausscorrelFF.h>
#include <flames_scatter.h>
#include <flames_dostandard.h>
#include <flames_readallff.h>
#include <flames_initshiftedff.h>
#include <flames_freeoneflats.h>
#include <flames_readback.h>
#include <flames_shift_FF_n.h>
#include <flames_readframe.h>
#include <flames_freeordpos.h>
#include <flames_slitdivide.h>
#include <flames_ffslitmultiply.h>
#include <flames_freeslitflats.h>
#include <flames_writeback.h>
#include <flames_striptblext.h>
#include <flames_freeallflats.h>
#include <flames_freeframe.h>
#include <flames_copy_FF_n.h>
#include <flames_midas_def.h>
#include <flames_computeback.h>
#include <flames_freespectrum.h>
/* FLAMES-UVES include files */ 
#include <flames_mainstandFF.h>
#include <flames_uves.h>
#include <flames_newmatrix.h>
#include <flames_readordpos.h>
#include <flames_readslitflats.h>

int flames_mainstandFF(const char *IN_A,
                       const cpl_frameset* IN_B,
                       const cpl_frameset* IN_C,
                       const char *IN_D,
                       const char *IN_E,
                       const char *IN_F,
                       const double *DECENTSNR,
                       const double *MAXDISCARDFRACT,
                       const int *MAXBACKITERS,
                       const int *MAXCORRITERS,
                       const int *BKGPOL,
                       const char *BKGFITINLINE,
                       const char *BKGFITMETHOD,
                       const char *BKGBADSCAN,
                       const int *BKGBADWIN,
                       const double *BKGBADMAXFRAC,
                       const int *BKGBADMAXTOT,
                       const double *SIGMA,
                       const double *MAXYSHIFT,
                       const double *CORRELTOL,
                       const int *CORRELXSTEP,
                       const double *WINDOW,
                       double *OUTPUTD,
                       int *OUTPUTI)
{
    char output[200];
    char repeatflag=0;
    int i=0;
    int tid=0;
    int fileid=0;
    int nflats=0;
    int unit=0;
    int actvals=0;
    frame_mask **mask=0;
    int bxdegree=0, bydegree=0;
    int maxbackiters=0;
    double maxdiscardfract=0;

    double kappa=0;
    double kappa2=0;
    int chisqpixels=0, nfittedparams=0;
    double chisquare=0;
    frame_data **backframe;
    frame_data **normcover;
    flames_err status=0;
    double ***pfibrecentre=0;
    double halfwinsize=0, phalfwinsize=0;

    flames_frame *AllFrame=0;

    allslitflats *Slit_FF=0;

    allflats *Shifted_FF=0;
    allflats *Single_FF=0;

    orderpos *Order=0;

    //char allfile[CATREC_LEN+2];
    const cpl_frameset *allfile;
    char backfile[CATREC_LEN+2];
    char infile[CATREC_LEN+2];
    //char slitsfile[CATREC_LEN+2];
    const cpl_frameset *slitsfile;
    char dummyfile[CATREC_LEN+2];
    char inorderfile[CATREC_LEN+2];
    char outorderfile[CATREC_LEN+2];
    int32_t iframe=0;
    int nval=0;
    int null=0;
    int imaxcorriters=0;
    int32_t maxcorriters=0;
    double shiftwindow=0;
    double shifttol=0;
    int icorrelxstep=0;
    int32_t correlxstep=0;
    int32_t lfibre=0, ifibre=0, firstfibre=0, lastfibre=0, iorder=0, ix=0;
    scatterswitch bkgswitch=USEALL;
    scatterswitch2 bkgswitch2=NOBADSCAN;
    int badwinxsize=0;
    int badwinysize=0;
    double badfracthres=0;
    int badtotthres=0;
    char keytype=0;
    int noelem=0;
    int bytelem=0;
    char bkgfitmethod[CATREC_LEN+1];
    double *deltas;
    frame_mask *deltamask;
    double ycentre=0;
    double decentSNR=0;
    frame_data decentSNR2=0;
    int32_t orderoffset=0;
    int32_t realfirstorder=0;
    int32_t reallastorder=0;
    int32_t slitfirstorder=0;
    int32_t slitlastorder=0;


    int32_t negativepixels=0;
    char bkginline=TRUE;

    frame_data *fdvecbuf1=0;
    frame_data *fdvecbuf2=0;
    frame_data *fdvecbuf3=0;
    frame_data *fdvecbuf4=0;
    frame_mask *fmvecbuf1=0;
    frame_mask *fmvecbuf2=0;
    frame_data pixelvalue=0;
    int32_t lastiyixindex=0;
    int32_t iorderifibreixstart=0;
    int32_t iorderifibreixend=0;
    int32_t iorderifibreindex=0;
    int32_t iorderifibreixoffset=0;
    int32_t iorderifibreixindex=0;
    int32_t ixiorderifibreindex=0;

    memset(output, 0, 200);
    memset(bkgfitmethod, 0, CATREC_LEN+1);


    bkgswitch = USEALL;

    SCSPRO("standnorm"); /* Get into MIDAS Env. */

    /* Read once and for all, here at the beginning, what MIDAS keywords we
     need */

    /* get input frame name from MIDAS env.*/
    SCKGETC(IN_A,1,CATREC_LEN+1,&nval,infile);

    /* get the fibre FF catalog name from MIDAS env.*/
    SCKGETC_fs(IN_B,1,CATREC_LEN+1,&nval,&allfile);

    /* get the slit FF catalog name from MIDAS env.*/
    SCKGETC_fs(IN_C,1,CATREC_LEN+1,&nval,&slitsfile);

    /* read background inter-order table name */
    SCKGETC(IN_D,1,160,&nval,backfile);

    /* read input order table name */
    SCKGETC(IN_E,1,160,&nval,dummyfile);
    if (striptblext(dummyfile, inorderfile) != NOERR) {
        SCTPUT("Error stripping extension from input order table file name");
        return flames_midas_fail();
    }

    /* read output order table name */
    SCKGETC(IN_F,1,160,&nval,dummyfile);
    if (striptblext(dummyfile, outorderfile) != NOERR) {
        SCTPUT("Error stripping extension from output order table file name");
        return flames_midas_fail();
    }

    /* the input and output table names must be different */
    if (strncmp(inorderfile, outorderfile, CATREC_LEN+2) == 0) {
        SCTPUT("Error: the input and output order table files are equal");
        return flames_midas_fail();
    }

    /* initialise DECENTSNR from keyword */
    if (SCKRDD(DECENTSNR, 1, 1, &actvals, &decentSNR, &unit, &null) != 0) {
        /* problems reading DECENTSNR */
        SCTPUT("Error reading the minimum acceptable SNR");
        return flames_midas_fail();
    }

    /* since it happens to have almost null fibre values on unmasked bad
     columns, try to avoid them */
    decentSNR2 = (frame_data) decentSNR*decentSNR;

    /* initialise MAXDISCARDFRACT from keyword */
    if (SCKRDD(MAXDISCARDFRACT, 1, 1, &actvals, &maxdiscardfract, &unit,
                    &null) != 0) {
        /* problems reading MAXDISCARDFRACT */
        SCTPUT("Error reading the MAXDISCARDFRACT keyword");
        return flames_midas_fail();
    }

    /* initialise MAXBACKITERS from keyword */
    if (SCKRDI(MAXBACKITERS, 1, 1, &actvals, &maxbackiters, &unit,
                    &null) != 0) {
        /* problems reading MAXBACKITERS */
        SCTPUT("Error reading the MAXBACKITERS keyword");
        return flames_midas_fail();
    }

    /* initialise MAXCORRITERS from keyword */
    if (SCKRDI(MAXCORRITERS, 1, 1, &actvals, &imaxcorriters, &unit,
                    &null) != 0) {
        /* problems reading MAXCORRITERS */
        SCTPUT("Error reading the MAXCORRITERS keyword");
        return flames_midas_fail();
    }

    /* initialise background fitting scalars */
    if (SCKRDI(BKGPOL, 1, 1, &actvals, &bxdegree, &unit, &null)
                    != 0) {
        /* problems reading xdegree */
        SCTPUT("Error reading the x degree of the background polynomial");
        return flames_midas_fail();
    }
    if (SCKRDI(BKGPOL, 2, 1, &actvals, &bydegree, &unit, &null)
                    != 0) {
        /* problems reading xdegree */
        SCTPUT("Error reading the y degree of the background polynomial");
        return flames_midas_fail();
    }

    /* Is inline background fitting and subtraction required? */
    if (SCKFND_string(BKGFITINLINE, &keytype, &noelem, &bytelem) != 0) {
        /* SCKFND failed, major problems */
        SCTPUT("Internal MIDAS error in mainopt: SCKFND failed");
        return flames_midas_fail();
    }
    switch(keytype) {
    case 'C':
        /* it exists and it is a character keyword, do read it */
        if (SCKGETC(BKGFITINLINE, 1, CATREC_LEN, &nval, bkgfitmethod)
                        != 0) {
            /* problems reading BKGFITINLINE */
            SCTPUT("Warning: error reading the BKGFITINLINE keyword, falling back \
to default");
        }
        else {
            /* is the string long enough to be unambiguous? */
            if (nval<1) {
                SCTPUT("Warning: BKGFITINLINE is ambiguous, falling back to default");
            }
            else {
                /* convert bkgfitmethod to upper case, to ease the subsequent check */
                for (i=0; i<=(nval-1); i++) bkgfitmethod[i] = toupper(bkgfitmethod[i]);
                /* compare as many letters as we have with the expected values */
                if (strncmp("YES", bkgfitmethod, (size_t) nval) == 0)
                    bkginline = TRUE;
                else if (strncmp("NO", bkgfitmethod, (size_t) nval)
                                == 0) bkginline = FALSE;
                else {
                    SCTPUT("Warning: unsupported BKGFITINLINE value, falling back to \
default");
                }
            }
        }
        break;
    case ' ':
        /* the keyword does not exist at all */
        SCTPUT("Warning: BKGFITINLINE is undefined, falling back to default");
        break;
    default:
        /* the keyword is of the wrong type */
        SCTPUT("Warning: BKGFITINLINE is not a string, falling back to default");
        break;
    }

    /* before trying to read it, make sure that BKGFITMETHOD exists and it is
     of the appropriate type */
    if (SCKFND_string(BKGFITMETHOD, &keytype, &noelem, &bytelem) != 0) {
        /* SCKFND failed, give up! */
        SCTPUT("Internal MIDAS error in mainopt: SCKFND failed");
        return flames_midas_fail();
    }
    switch(keytype) {
    case 'C':
        /* it exists and it is a character keyword, go ahead and read it */
        if (SCKGETC(BKGFITMETHOD, 1, CATREC_LEN, &nval, bkgfitmethod)
                        != 0) {
            /* problems reading BKGFITMETHOD */
            SCTPUT("Warning: error reading the BKGFITMETHOD keyword, falling back \
to default");
        }
        else {
            /* is the string long enough to be unambiguous? */
            if (nval<2) {
                /* no, fall back to the default */
                SCTPUT("Warning: BKGFITMETHOD is ambiguous, falling back to default");
            }
            else {
                /* convert bkgfitmethod to upper case, to ease the subsequent check */
                for (i=0; i<=(nval-1); i++) bkgfitmethod[i] = toupper(bkgfitmethod[i]);
                /* compare as many letters as we have with the expected values */
                if (strncmp("ALL", bkgfitmethod, (size_t) nval) == 0)
                    bkgswitch = USEALL;
                else if (strncmp("MEDIAN", bkgfitmethod, (size_t) nval)
                                == 0) bkgswitch = USEMEDIAN;
                else if (strncmp("MINIMUM", bkgfitmethod, (size_t) nval)
                                == 0) bkgswitch = USEMINIMUM;
                else if (strncmp("AVERAGE", bkgfitmethod, (size_t) nval)
                                == 0) bkgswitch = USEAVERAGE;
                else {
                    SCTPUT("Warning: unsupported BKGFITMETHOD value, falling back to \
default");
                }
            }
        }
        break;
    case ' ':
        /* the keyword does not exist at all */
        SCTPUT("Warning: BKGFITMETHOD is undefined, falling back to default");
        break;
    default:
        /* the keyword is of the wrong type */
        SCTPUT("Warning: BKGFITMETHOD is not a string, falling back to default");
        break;
    }

    /* before trying to read it, make sure that BKGFITMETHOD exists and it is
     of the appropriate type */
    if (SCKFND_string(BKGBADSCAN, &keytype, &noelem, &bytelem) != 0) {
        /* SCKFND failed, give up! */
        SCTPUT("Internal MIDAS error in mainopt: SCKFND failed");
        return flames_midas_fail();
    }
    switch(keytype) {
    case 'C':
        /* it exists and it is a character keyword, go ahead and read it */
        if (SCKGETC(BKGBADSCAN, 1, CATREC_LEN, &nval, bkgfitmethod)
                        != 0) {
            /* problems reading BKGFITMETHOD */
            SCTPUT("Warning: error reading the BKGBADSCAN keyword, falling back to \
default");
        }
        else {
            /* is the string long enough to be unambiguous? */
            if (nval<1) {
                /* no, fall back to the default */
                SCTPUT("Warning: BKGBADSCAN is ambiguous, falling back to default");
            }
            else {
                /* convert bkgfitmethod to upper case, to ease the subsequent check */
                for (i=0; i<=(nval-1); i++) bkgfitmethod[i] = toupper(bkgfitmethod[i]);
                /* compare as many letters as we have with the expected values */
                if (strncmp("NONE", bkgfitmethod, (size_t) nval) == 0)
                    bkgswitch2 = NOBADSCAN;
                else if (strncmp("FRACTION", bkgfitmethod, (size_t) nval)
                                == 0) bkgswitch2 = FRACBADSCAN;
                else if (strncmp("ABSOLUTE", bkgfitmethod, (size_t) nval)
                                == 0) bkgswitch2 = ABSBADSCAN;
                else {
                    SCTPUT("Warning: unsupported BKGBADSCAN value, falling back to \
default");
                }
            }
        }
        break;
    case ' ':
        /* the keyword does not exist at all */
        SCTPUT("Warning: BKGBADSCAN is undefined, falling back to default");
        break;
    default:
        /* the keyword is of the wrong type */
        SCTPUT("Warning: BKGBADSCAN is not a string, falling back to default");
        break;
    }

    /* if neighborhood bad pixel scanning was requested, read the other
     keywords needed */
    if (bkgswitch2 == FRACBADSCAN) {
        if ((SCKRDI(BKGBADWIN, 1, 1, &actvals, &badwinxsize, &unit, &null) != 0)
                        || (SCKRDI(BKGBADWIN, 2, 1, &actvals, &badwinysize, &unit, &null)
                                        != 0)) {
            SCTPUT("Error reading the BKGBADWIN keyword");
            return flames_midas_fail();
        }
        if (SCKRDD(BKGBADMAXFRAC, 1, 1, &actvals, &badfracthres, &unit,
                        &null) != 0) {
            SCTPUT("Error reading the BKGBADMAXFRAC keyword");
            return flames_midas_fail();
        }
        /* check the values read for consistence */
        if ((badwinxsize < 0) || (badwinysize < 0)) {
            SCTPUT("Warning: BKGBADWIN values must be non negative, disabling \
BKGBADSCAN");
            bkgswitch2 = NOBADSCAN;
        }
        if (badfracthres <= 0) {
            SCTPUT("Warning: BKGBADMAXFRAC value must be positive, disabling \
BKGBADSCAN");
            bkgswitch2 = NOBADSCAN;
        }
    }
    else if (bkgswitch2 == ABSBADSCAN) {
        if ((SCKRDI(BKGBADWIN, 1, 1, &actvals, &badwinxsize, &unit, &null) != 0)
                        || (SCKRDI(BKGBADWIN, 2, 1, &actvals, &badwinysize, &unit, &null)
                                        != 0)) {
            SCTPUT("Error reading the BKGBADWIN keyword");
            return flames_midas_fail();
        }
        if (SCKRDI(BKGBADMAXTOT, 1, 1, &actvals, &badtotthres, &unit,
                        &null) != 0) {
            SCTPUT("Error reading the BKGBADMAXTOT keyword");
            return flames_midas_fail();
        }
        /* check the values read for consistence */
        if ((badwinxsize < 0) || (badwinysize < 0)) {
            SCTPUT("Warning: BKGBADWIN values must be non negative, disabling \
BKGBADSCAN");
            bkgswitch2 = NOBADSCAN;
        }
        if (badtotthres <= 0) {
            SCTPUT("Warning: BKGBADMAXTOT value must be positive, disabling \
BKGBADSCAN");
            bkgswitch2 = NOBADSCAN;
        }
    }

    /* read the kappa factor to be used later in kappa-sigma clipping */
    if ((status=SCKRDD(SIGMA, 1, 1, &actvals, &kappa, &unit, &null))!=0) {
        /* something went wrong while reading the kappa-sigma factor */
        sprintf(output, "Error %d while reading SIGMA keyword", status);
        SCTPUT(output);
        return flames_midas_fail();
    }
    /* compute once and for all the square of kappa, as we will be using that */
    kappa2 = kappa*kappa;

    /* read the integration window size, to be used later in
     standard extraction */
    if ((status=SCKRDD(WINDOW, 1, 1, &actvals, &halfwinsize, &unit, &null))
                    !=0) {
        /* something went wrong while reading the kappa-sigma factor */
        sprintf(output, "Error %d while reading WINDOW keyword", status);
        SCTPUT(output);
        return flames_midas_fail();
    }
    /* compute once and for all halfwinsize, as we will be using that */
    halfwinsize /= 2;


    /* Link the MIDAS names of the frames to the physical ones */

    if(!(AllFrame = calloc(1, sizeof(flames_frame)))) {
        SCTPUT("Allocation error during AllFrame memory allocation");
        return flames_midas_fail();
    }

    /* let's read the Science Frame */
    sprintf(output, "I'm reading the frame %s", infile);
    SCTPUT(output);

    if (readframe(AllFrame, infile) != NOERR) {
        SCTPUT("Error while reading the frame");
        return flames_midas_fail();
    }

    /* Read the table data and then put them in the C structures */
    sprintf(output,"Reading the order/fibre table...");
    SCTPUT(output);
    if(!(Order = calloc(1, sizeof(orderpos)))) {
        SCTPUT("Allocation error during the allocation of Order structure");
        return flames_midas_fail();
    }
    /* use the readordpos function to read the descriptors */
    if (readordpos(inorderfile, Order) != NOERR) {
        SCTPUT("Error while reading the order table");
        return flames_midas_fail();
    }
    /* does the order table match the Science frame chip? */
    if(Order->chipchoice != AllFrame->chipchoice) {
        /* no, it doesn't */
        SCTPUT("Error: chip mismatch between Science frame and order table");
        return flames_midas_fail();
    }

    /* initialise firstorder and lastorder in AllFrame from Order */
    AllFrame->firstorder = Order->firstorder;
    AllFrame->lastorder = Order->lastorder;
    AllFrame->tab_io_oshift = Order->tab_io_oshift;

    /* we define the pgausswidth to be proportional to halfibrewidth,
     say a quarter of it or so, and this can be tuned by a parameter in 
     flames_uves.h */
    maxcorriters = (int32_t) imaxcorriters;

    /* read the MAXYSHIFT keyword */
    if (SCKRDD(MAXYSHIFT, 1, 1, &actvals, &shiftwindow, &unit, &null) != 0) {
        /* problems reading MAXYSHIFT */
        SCTPUT("Error reading the MAXYSHIFT (P8) parameter");
        return flames_midas_fail();
    }
    if (shiftwindow<0) {
        SCTPUT("Warning: illegal value for MAXYSHIFT (P8), fall back to 0");
        shiftwindow = 0;
    }
    /* read the CORRELTOL keyword */
    if (SCKRDD(CORRELTOL, 1, 1, &actvals, &shifttol, &unit, &null) != 0) {
        /* problems reading CORRELTOL */
        SCTPUT("Error reading the CORRELTOL keyword");
        return flames_midas_fail();
    }
    if (shifttol<0) {
        SCTPUT("Error: illegal (negative) value for CORRELTOL");
        return flames_midas_fail();
    }
    /* read the CORRELXSTEP keyword */
    if (SCKRDI(CORRELXSTEP, 1, 1, &actvals, &icorrelxstep, &unit, &null) != 0){
        /* problems reading CORRELXSTEP */
        SCTPUT("Error reading the CORRELXSTEP keyword");
        return flames_midas_fail();
    }
    if (icorrelxstep<1) {
        SCTPUT("Warning: illegal (<1) value for CORRELTOL, falling back to 1");
        icorrelxstep=1;
    }
    correlxstep = (int32_t) icorrelxstep;

    /* allocate and initialise the frame which will contain the
     estimated background. This will remain zero if no background fitting 
     was required*/
    lastiyixindex = (AllFrame->subrows*AllFrame->subcols)-1;
    backframe = fdmatrix(0, AllFrame->subrows-1, 0,
                    AllFrame->subcols-1);
    memset(&backframe[0][0], 0,
           AllFrame->subrows*AllFrame->subcols*sizeof(frame_data));

    if (bkginline==TRUE) {
        /* Inline background fitting and subtraction required */

        /* Read the data of the frames and put them in the right C structures */

        SCTPUT("*** Step 1: Background fitting and subtraction ***\n");

        sprintf(output, "I'm reading the background table %s", backfile);
        SCTPUT(output);

        /* read in the background table */
        if ((status=readback(&(AllFrame->back), backfile, bxdegree, bydegree))
                        != NOERR) {
            /* something went wrong while reading the background table */
            sprintf(output, "Error %d while reading the background table", status);
            SCTPUT(output);
            return flames_midas_fail();
        }


        /* Calculate the fit model of the background */

        SCTPUT("Start the background fitting procedure");

        if (AllFrame->back.Window_Number > 0) {
            if (scatter(AllFrame, Order, bkgswitch, bkgswitch2, badwinxsize,
                            badwinysize, badfracthres, badtotthres, kappa2,
                            (int32_t) maxbackiters, maxdiscardfract, OUTPUTI)) {
                SCTPUT("Error executing the scatter function");
                return flames_midas_fail();
            }
            /* compute the estimated background */
            if (computeback(AllFrame, backframe) != NOERR) {
                SCTPUT("Error computing fitted background");
                return flames_midas_fail();
            }
            /* subtract the estimated background from the data frame */
            /* the error of the estimated background is assumed to be negligible */
            SCTPUT("Subtracting fitted background from all fibres FF Frame");
            fdvecbuf1 = AllFrame->frame_array[0];
            fdvecbuf2 = backframe[0];
            for (ix=0; ix<=lastiyixindex; ix++) {
                fdvecbuf1[ix] -= fdvecbuf2[ix];
            }
            /* some more black magic to make the thing more robust: scan the frame
     for any negative pixel values; if any are found, compare them to the
     standard deviation: if the negative value is not compatible with 
     zero, mark that pixel as bad */
            negativepixels=0;
            fdvecbuf1 = AllFrame->frame_array[0];
            fdvecbuf2 = AllFrame->frame_sigma[0];
            fmvecbuf1 = AllFrame->badpixel[0];
            for (ix=0; ix<=lastiyixindex; ix++) {
                if (fmvecbuf1[ix]==0 && (pixelvalue=fdvecbuf1[ix])<0) {
                    if ((pixelvalue*pixelvalue)>4*fdvecbuf2[ix]) {
                        fmvecbuf1[ix]=1;
                        negativepixels++;
                    }
                }
            }
            if (negativepixels!=0) {
                sprintf(output, "Warning: %d pixels result lower than fitted \
background", negativepixels);
                SCTPUT(output);
                SCTPUT("either they are unmasked bad pixels or some contamination is");
                SCTPUT("skewing background determination");
            }
        }
        else {
            SCTPUT("Error: no regions available for background estimation");
            return flames_midas_fail();
        }

    }

    if (shiftwindow>0) {
        SCTPUT("\n*** Step 2: y shift determination and compensation ***\n");

        /* now allocate the Slit_FF structure, we'll be using it from now on */
        if(!(Slit_FF = calloc(1, sizeof(allslitflats)))) {
            SCTPUT("Allocation error during Slit_FF memory allocation");
            return flames_midas_fail();
        }

        /* let's read the slit FFs */
        sprintf(output, "I'm reading the slit FF frames");
        SCTPUT(output);
        if ((status = readslitflats(slitsfile, Slit_FF)) != NOERR) {
            /* problems reading slitflats members from disk */
            SCTPUT("Error while reading the slit Flat Field frames");
            return flames_midas_fail();
        }
        orderoffset = Slit_FF->tab_io_oshift-Order->tab_io_oshift;
        slitfirstorder = Slit_FF->firstorder;
        slitlastorder = Slit_FF->lastorder;
        realfirstorder = Order->firstorder;
        if (orderoffset > 0) realfirstorder += orderoffset;
        reallastorder = Slit_FF->lastorder+orderoffset;
        if (reallastorder > Order->lastorder) reallastorder = Order->lastorder;
        if (realfirstorder > reallastorder) {
            /* this should really never happen with sane data, complain and exit */
            strcpy(output, "Error: The orders in the slit FF(s) and in the order \
table being used do not overlap at all!");
            SCTPUT(output);
            return flames_midas_fail();
        }

        /* ycorrection is not needed here, since the order table must be
       uncorrected thus far */

        /* divide the all fibres FF frame by the slit FF frame(s), do it in place,
       to save memory. Hopefully, this step will seldom need to
       be repeated, and in those few cases we reload the original frame from 
       disk */
        sprintf(output,"Dividing all fibres FF Frame by slit FF frame(s)");
        SCTPUT(output);
        /* Let's divide the all fibres FF Frame by the full slit FF, in place */

        if (slitdivide(Slit_FF, Order, AllFrame, AllFrame)!=NOERR) {
            /* something went wrong in slitdivide */
            SCTPUT("Error while dividing the all fibres FF Frame by the slit \
frame(s)");
            return flames_midas_fail();
        }

        /* since here we already performed the final division by the
       slit FF, we do not need any more the slit FF, so free the memory
       right here. Should we need to repeat the division, we will reload 
       things later */
        if (freeslitflats(Slit_FF)!=NOERR) {
            SCTPUT("Error while freeing the memory for the Slit_FF structure");
            return flames_midas_fail();
        }
        free(Slit_FF);

        /* Allocate memory for the Single_FF structure */
        if(!(Single_FF = calloc(1, sizeof(allflats)))) {
            SCTPUT("Allocation error during Single_FF structure memory allocation");
            return flames_midas_fail();
        }

        /* let's read the fibre FFs */
        sprintf(output, "I'm reading the fibre FF frames");
        SCTPUT(output);
        if (readallff(allfile, Single_FF) != NOERR) {
            SCTPUT("Error while reading the fibre Flat Field frames");
            return flames_midas_fail();
        }

        /* is this fibre FF frames set shiftable? */
        if (Single_FF->shiftable != 'y') {
            sprintf(output, "The fibre FF set is not slit-flatfielded");
            SCTPUT(output);
            return flames_midas_fail();
        }

        /* make sure we do not attempt extraction for uncovered fibres */
        fmvecbuf1 = Single_FF->goodfibres[0][0];
        if (Order->firstorder<realfirstorder) {
            iorderifibreixstart = 0;
            iorderifibreixend = ((realfirstorder-Order->firstorder)*
                            Single_FF->maxfibres*Single_FF->subcols)-1;
            for (ix=iorderifibreixstart; ix<=iorderifibreixend; ix++) {
                fmvecbuf1[ix] = BADSLICE;
            }
        }
        if (Order->lastorder>reallastorder) {
            iorderifibreixstart = (reallastorder-Order->firstorder+1)*
                            Single_FF->maxfibres*Single_FF->subcols;
            iorderifibreixend = ((Order->lastorder-Order->firstorder)*
                            Single_FF->maxfibres*Single_FF->subcols)-1;
            for (ix=iorderifibreixstart; ix<=iorderifibreixend; ix++) {
                fmvecbuf1[ix] = BADSLICE;
            }
        }

        double oldyshift = 0;
        if (AllFrame->nflats != 0) {
            for (iframe=0; iframe<=(AllFrame->nflats-1); iframe++)
                oldyshift += AllFrame->yshift[iframe];
            oldyshift /= (double) AllFrame->nflats;
            /* I need to deallocate the yshift vector and to reallocate it with
     a different size */
            free_dvector(AllFrame->yshift, 0, AllFrame->nflats-1);
        }
        /* set AllFrame->nflats equal to Single_FF->nflats, and allocate
       AllFrame->yshift accordingly */
        AllFrame->nflats = Single_FF->nflats;
        AllFrame->yshift = dvector(0, AllFrame->nflats-1);
        /* for debugging purposes, initialise the yshifts to zero
       with the simulated data the shifts ARE zero! */
        for (iframe=0; iframe <= (Single_FF->nflats-1); iframe++)
            AllFrame->yshift[iframe]=0;

        /* calculation of the needed shifts by cross-correlation */
        SCTPUT("Doing cross-correlation calculation");
        deltas = dvector(0, Single_FF->nflats-1);
        deltamask = fmvector(0, Single_FF->nflats-1);
        if (gausscorrelFF(AllFrame, Single_FF, Order, maxcorriters, shiftwindow,
                        shifttol, correlxstep, "middumma.fits", deltas,
                        deltamask)) {
            SCTPUT("Error during cross-correlation calculation");
            return flames_midas_fail();
        }

        /* check whether we need to repeat steps later */
        repeatflag=0;
        double newyshift = 0;
        for (iframe=0; iframe<=(AllFrame->nflats-1); iframe++)
            newyshift += deltas[iframe];
        newyshift /= (double)AllFrame->nflats;
        /* do we need to repeat some steps? */
        double shifthres=1;
        if (fabs(newyshift-oldyshift)>shifthres) repeatflag=1;

        SCTPUT("Cross-correlation completed");

    }

    else {
        SCTPUT("\n*** Step 2: y shift assumed to be zero ***\n");
        repeatflag=0;

        /* now allocate the Slit_FF structure, we'll be using it from now on */
        if(!(Slit_FF = calloc(1, sizeof(allslitflats)))) {
            SCTPUT("Allocation error during Slit_FF memory allocation");
            return flames_midas_fail();
        }

        /* let's read the slit FFs */
        sprintf(output, "I'm reading the slit FF frames");
        SCTPUT(output);
        if ((status = readslitflats(slitsfile, Slit_FF)) != NOERR) {
            /* problems reading slitflats members from disk */
            SCTPUT("Error while reading the slit Flat Field frames");
            return flames_midas_fail();
        }
        orderoffset = Slit_FF->tab_io_oshift-Order->tab_io_oshift;
        slitfirstorder = Slit_FF->firstorder;
        slitlastorder = Slit_FF->lastorder;
        realfirstorder = Order->firstorder;
        if (orderoffset > 0) realfirstorder += orderoffset;
        reallastorder = Slit_FF->lastorder+orderoffset;
        if (reallastorder > Order->lastorder) reallastorder = Order->lastorder;
        if (realfirstorder > reallastorder) {
            /* this should really never happen with sane data, complain and exit */
            strcpy(output, "Error: The orders in the slit FF(s) and in the order \
table being used do not overlap at all!");
            SCTPUT(output);
            return flames_midas_fail();
        }
        /* correct for the ycorrection, if present */
        if (Order->corrected=='t') {
            for (iframe=0; iframe<=(Slit_FF->nflats-1);iframe++)
                Slit_FF->slit[iframe].yshift -= Order->ycorrection;
        }

        if (freeslitflats(Slit_FF)!=NOERR) {
            SCTPUT("Error while freeing the memory for the Slit_FF structure");
            return flames_midas_fail();
        }
        free(Slit_FF);

        /* Allocate memory for the Single_FF structure */
        if(!(Single_FF = calloc(1, sizeof(allflats)))) {
            SCTPUT("Allocation error during Single_FF structure memory allocation");
            return flames_midas_fail();
        }

        /* let's read the fibre FFs */
        sprintf(output, "I'm reading the fibre FF frames");
        SCTPUT(output);
        if (readallff(allfile, Single_FF) != NOERR) {
            SCTPUT("Error while reading the fibre Flat Field frames");
            return flames_midas_fail();
        }

        /* is this fibre FF frames set shiftable? */
        if (Single_FF->shiftable != 'y') {
            sprintf(output, "The fibre FF set is not slit-flatfielded");
            SCTPUT(output);
            return flames_midas_fail();
        }

        /* make sure we do not attempt extraction for uncovered fibres */
        fmvecbuf1 = Single_FF->goodfibres[0][0];
        if (Order->firstorder<realfirstorder) {
            iorderifibreixstart = 0;
            iorderifibreixend = (realfirstorder-Order->firstorder)*
                            Single_FF->maxfibres*Single_FF->subcols-1;
            for (ix=iorderifibreixstart; ix<=iorderifibreixend; ix++) {
                fmvecbuf1[ix] = BADSLICE;
            }
        }
        if (Order->lastorder>reallastorder) {
            iorderifibreixstart = (reallastorder-Order->firstorder+1)*
                            Single_FF->maxfibres*Single_FF->subcols;
            iorderifibreixend = (Order->lastorder-Order->firstorder)*
                            Single_FF->maxfibres*Single_FF->subcols-1;
            for (ix=iorderifibreixstart; ix<=iorderifibreixend; ix++) {
                fmvecbuf1[ix] = BADSLICE;
            }
        }

        if (AllFrame->nflats != Single_FF->nflats) {
            /* I need to deallocate the yshift vector and to reallocate it with
     a different size */
            free_dvector(AllFrame->yshift, 0, AllFrame->nflats-1);
            /* set AllFrame->nflats equal to Single_FF->nflats, and allocate
     AllFrame->yshift accordingly */
            AllFrame->nflats = Single_FF->nflats;
            AllFrame->yshift = dvector(0, AllFrame->nflats-1);
            /* initialise the yshifts to zero */
            deltas = dvector(0, Single_FF->nflats-1);
            deltamask = fmvector(0, Single_FF->nflats-1);
            for (iframe=0; iframe <= (Single_FF->nflats-1); iframe++) {
                AllFrame->yshift[iframe]=deltas[iframe]=0;
                deltamask[iframe]=TRUE;
            }
        }
        else {
            /* take the values read from disk as good ones */
            deltas = dvector(0, Single_FF->nflats-1);
            deltamask = fmvector(0, Single_FF->nflats-1);
            for (iframe=0; iframe <= (Single_FF->nflats-1); iframe++) {
                deltas[iframe]=AllFrame->yshift[iframe];
                deltamask[iframe]=TRUE;
            }
        }
    }


    /* After correl, save the shifts to disk */
    /* is there anything to save? */
    if (AllFrame->nflats > 0) {
        for (iframe=0; iframe<=(Single_FF->nflats-1); iframe++) {
            /* we measured the shifts of the all fibres FF frame with respect
     to the single fibre FF frames, but we now want to store the 
     shift of the latter with respect to the former; to say it briefly,
     we must reverse the sign */
            Single_FF->flatdata[iframe].yshift = -deltas[iframe];
            AllFrame->yshift[iframe]=0;
            /* try to open the single FF frame */
            if (SCFOPN(Single_FF->flatdata[iframe].framename, FLAMESDATATYPE, 0,
                            F_IMA_TYPE, &fileid) != 0) {
                /* I could not open the frame */
                return flames_midas_fail();
            }
            if (SCDWRD(fileid, "YSHIFT", &Single_FF->flatdata[iframe].yshift,
                            1, 1, &unit) != 0) {
                /* error writing YSHIFT */
                return flames_midas_fail();
            }
            if (SCFCLO(fileid) != 0) {
                /* I could not close the frame */
                return flames_midas_fail();
            }
        }
        /* try to open the all fibres FF frame */
        if (SCFOPN(AllFrame->framename, FLAMESDATATYPE, 0, F_IMA_TYPE,
                        &fileid) != 0) {
            /* I could not open the frame */
            return flames_midas_fail();
        }
        nflats = (int) AllFrame->nflats;
        if (SCDWRI(fileid, "NFLATS", &nflats, 1, 1, &unit) != 0) {
            /* error writing descriptor */
            return flames_midas_fail();
        }
        if (SCDWRD(fileid, "YSHIFT", AllFrame->yshift, 1,
                        nflats, &unit) != 0) {
            /* error writing descriptor */
            return flames_midas_fail();
        }
        if (SCFCLO(fileid) != 0) {
            /* I could not close the frame */
            return flames_midas_fail();
        }
        /* Now we must appropriately correct the fibre positions for the shift,
       so that the polynomial in Order trace them at zero shift */
        for (iframe=0; iframe<=Single_FF->nflats-1; iframe++) {
            for (lfibre=0; lfibre<=Single_FF->flatdata[iframe].numfibres-1;
                            lfibre++){
                ifibre=Single_FF->flatdata[iframe].fibres[lfibre];
                Order->fibrepos[ifibre] -= Single_FF->flatdata[iframe].yshift;
            }
        }

        /* find the extremal lit fibres */
        for (ifibre=0; (ifibre<=Single_FF->maxfibres-1) &&
        (Single_FF->fibremask[ifibre]!=TRUE); ifibre++);
        if (ifibre<=Single_FF->maxfibres-1) {
            firstfibre = lastfibre = ifibre;
            for (ifibre++; ifibre<=Single_FF->maxfibres-1; ifibre++)
                if (Single_FF->fibremask[ifibre]==TRUE) lastfibre=ifibre;
            ycentre = (Order->fibrepos[firstfibre]+Order->fibrepos[lastfibre])/2;
            Order->ycorrection = ycentre;
            Order->orderpol[0][0] += ycentre;
            Order->tab_io_yshift += ycentre;
            for (ifibre=0; ifibre<=Single_FF->maxfibres-1; ifibre++)
                if (Single_FF->fibremask[ifibre]==TRUE)
                    Order->fibrepos[ifibre] -= ycentre;
        }
        if ((status = TCTOPN(outorderfile, F_D_MODE, &tid)) != 0) {
            /* I could not open the output order table: protest... */
            sprintf(output, "Error: I couldn't open the %s table", outorderfile);
            SCTPUT(output);
            return flames_midas_fail();
        }
        /* now write the changed descriptors */
        if (SCDWRD(tid, "FIBREPOS", Order->fibrepos, 1, Order->maxfibres, &unit)
                        != 0) {
            SCTPUT("Error writing corrected FIBREPOS descriptor");
            return flames_midas_fail();
        }
        if (SCDWRD(tid, "COEFFD", Order->orderpol[0], 1, 1, &unit) !=0) {
            SCTPUT("Error writing corrected COEFFD descriptor");
            return flames_midas_fail();
        }
        if (SCDWRD(tid, "YCORRECTION", &ycentre, 1, 1, &unit) !=0) {
            SCTPUT("Error writing YCORRECTION descriptor");
            return flames_midas_fail();
        }
        if (SCDWRD(tid, "TAB_IN_OUT_YSHIFT", &Order->tab_io_yshift, 1, 1, &unit)
                        !=0) {
            SCTPUT("Error updating TAB_IN_OUT_YSHIFT descriptor");
            return flames_midas_fail();
        }
        if (SCDWRC(tid, "CORRECTED", 1, "t", 1, 1, &unit) != 0) {
            SCTPUT("Error updating the CORRECTED flag");
            return flames_midas_fail();
        }
        if (TCTCLO(tid) != 0) {
            SCTPUT("Error closing order table");
            return flames_midas_fail();
        }
        Order->corrected='t';
    }

    SCTPUT("Allocating the Shifted_FF");
    /* Prepare the structure for allocation of the shifted FF */
    if (!(Shifted_FF=calloc(1, sizeof(allflats)))) {
        SCTPUT("Allocation while allocating memory for the shifted FF");
        return flames_midas_fail();
    }

    /* allocate the arrays in Shifted_FF */
    if (!(initshiftedff(Single_FF,Shifted_FF)==NOERR)) {
        SCTPUT("Error while allocating internal arrays of the shifted FF \
structure");
        return flames_midas_fail();
    }

    /* produce the shifted fibre FF frames, using the yshifts previously
     determined by correl */
    SCTPUT("Shifting fibre FF Frames");
    /* to save memory, allocate just one iframe at a time, and recycle the
     memory of the unshifted frames once they have been used */
    if (Shifted_FF->nflats >= 2) {
        for (iframe=0; iframe <= (Shifted_FF->nflats-2); iframe++) {
            /* Shift FF */
            sprintf(output,"Shifting fibre FF frame number %d", iframe);
            SCTPUT(output);
            if (deltamask[iframe]==TRUE)
                shift_FF_n(Single_FF, Order, 0, iframe, Shifted_FF);
            else
                copy_FF_n(Single_FF, Order, 0, iframe, Shifted_FF);
            /* the iframe which has been shifted already is not needed any more, we
     can use it as next buffer in Shifted_FF, copy the pointers */
            Shifted_FF->flatdata[iframe+1].data = Single_FF->flatdata[iframe].data;
            Shifted_FF->flatdata[iframe+1].sigma = Single_FF->flatdata[iframe].sigma;
            Shifted_FF->flatdata[iframe+1].badpixel =
                            Single_FF->flatdata[iframe].badpixel;
            Shifted_FF->flatdata[iframe+1].framename =
                            Single_FF->flatdata[iframe].framename;
            Shifted_FF->flatdata[iframe+1].sigmaname =
                            Single_FF->flatdata[iframe].sigmaname;
            Shifted_FF->flatdata[iframe+1].badname =
                            Single_FF->flatdata[iframe].badname;
            Shifted_FF->flatdata[iframe+1].fibres =
                            Single_FF->flatdata[iframe].fibres;
        }
        /* handle the last iframe differently */
        iframe = Shifted_FF->nflats-1;
        sprintf(output,"Shifting fibre FF frame number %d", iframe);
        SCTPUT(output);
        if (deltamask[iframe]==TRUE)
            shift_FF_n(Single_FF, Order, 0, iframe, Shifted_FF);
        else
            copy_FF_n(Single_FF, Order, 0, iframe, Shifted_FF);
        /* this was the last one to shift, now turn the Single_FF in a dummy
       allflats structure which can be freed, as it is not needed any more */
        Single_FF->flatdata[0].data = Single_FF->flatdata[iframe].data;
        Single_FF->flatdata[0].sigma = Single_FF->flatdata[iframe].sigma;
        Single_FF->flatdata[0].badpixel = Single_FF->flatdata[iframe].badpixel;
        Single_FF->flatdata[0].framename = Single_FF->flatdata[iframe].framename;
        Single_FF->flatdata[0].sigmaname = Single_FF->flatdata[iframe].sigmaname;
        Single_FF->flatdata[0].badname = Single_FF->flatdata[iframe].badname;
        Single_FF->flatdata[0].fibres = Single_FF->flatdata[iframe].fibres;
    }
    else {
        /* in this case nflats appears to be at most 1, which simplifies things */
        for (iframe=0; iframe <= (Shifted_FF->nflats-1); iframe++) {
            /* Shift FF */
            sprintf(output,"Shifting fibre FF frame number %d", iframe);
            SCTPUT(output);
            if (deltamask[iframe]==TRUE)
                shift_FF_n(Single_FF, Order, 0, iframe, Shifted_FF);
            else
                copy_FF_n(Single_FF, Order, 0, iframe, Shifted_FF);
        }
    }

    /* free the (now dummy) allflats structure of the unshifted fibre FF flats */
    if (freeoneflats(Single_FF) != NOERR) {
        SCTPUT("Error freeing the dummy fibre FF frame structure");
        return flames_midas_fail();
    }
    free(Single_FF);

    /* free the previously processed all fibres FF frame, later we will need the
     original one */
    if (freeframe(AllFrame) != NOERR) {
        SCTPUT("Error freeing AllFrame");
        return flames_midas_fail();
    }

    /* free also deltas and deltamask, we do not need them any more */
    free_dvector(deltas, 0, Shifted_FF->nflats-1);
    free_fmvector(deltamask, 0, Shifted_FF->nflats-1);

    /* we must multiply the shifted FF frames by the slit FF, hence reallocate
     and reload the latter */
    if(!(Slit_FF = calloc(1, sizeof(allslitflats)))) {
        SCTPUT("Allocation error during Slit_FF memory allocation");
        return flames_midas_fail();
    }
    /* let's read the slit FFs */
    sprintf(output, "I'm reading the slit FF frames");
    SCTPUT(output);
    if ((status = readslitflats(slitsfile, Slit_FF)) != NOERR) {
        /* problems reading slitflats members from disk */
        SCTPUT("Error while reading the slit Flat Field frames");
        return flames_midas_fail();
    }
    /* the ycorrection is necessarily needed here, since we previously corrected
     the order table */
    for (iframe=0; iframe<=(Slit_FF->nflats-1);iframe++)
        Slit_FF->slit[iframe].yshift -= Order->ycorrection;
    /* do multiply the shifted fibre FF frames by the slit FF, in place
     to save memory */
    SCTPUT("Multiplying the shifted FF frame(s) by the slit FF frame(s)");
    if (ffslitmultiply(Slit_FF, Order, Shifted_FF, Shifted_FF) != NOERR) {
        SCTPUT("Error multiplying the shifted fibre FF by the slit FF");
        return flames_midas_fail();
    }
    /* copy the normalisation factors from the slit FF structure before
     freeing it for good */
    normcover = fdmatrix(0, Slit_FF->lastorder-Slit_FF->firstorder,
                    0, Slit_FF->subcols-1);
    memcpy(normcover[0], Slit_FF->normfactor[0],
           (size_t) ((Slit_FF->lastorder-Slit_FF->firstorder+1)*
                           Slit_FF->subcols)*sizeof(frame_data));
    /* finally free for good the memory for the slit FF */
    if (freeslitflats(Slit_FF)!=NOERR) {
        SCTPUT("Error while freeing the memory for the Slit_FF structure");
        return flames_midas_fail();
    }
    free(Slit_FF);

    /* Reread the original AllFrame (now including the correct yshifts),
     undivided by the slit FF */
    sprintf(output, "I'm re-reading the original frame %s", infile);
    SCTPUT(output);
    if (readframe(AllFrame, infile) != NOERR) {
        SCTPUT("Error while re-reading the all fibres FF Frame");
        return flames_midas_fail();
    }
    /* initialise firstorder and lastorder in AllFrame from Order */
    AllFrame->firstorder = Order->firstorder;
    AllFrame->lastorder = Order->lastorder;
    AllFrame->tab_io_oshift = Order->tab_io_oshift;
    if ((repeatflag != 0) && (bkginline == TRUE)) {
        /* read in the old background table */
        SCTPUT("A significant drift was detected:\n refit the background");
        if ((status=readback(&(AllFrame->back), backfile, bxdegree, bydegree))
                        != NOERR) {
            /* something went wrong while reading the background table */
            sprintf(output, "Error %d while reading the background table", status);
            SCTPUT(output);
            return flames_midas_fail();
        }
        if (AllFrame->back.Window_Number > 0) {
            if (scatter(AllFrame, Order, bkgswitch, bkgswitch2, badwinxsize,
                            badwinysize, badfracthres, badtotthres, kappa2,
                            (int32_t) maxbackiters, maxdiscardfract, OUTPUTI)) {
                SCTPUT("Error during the calculation of the scattered light");
                return flames_midas_fail();
            }
            /* compute the estimated background */
            if (computeback(AllFrame, backframe) != NOERR) {
                SCTPUT("Eccor computing fitted background");
                return flames_midas_fail();
            }
        }
        else {
            SCTPUT("Error: no regions available for background estimation");
            return flames_midas_fail();
        }
    }
    if (bkginline == TRUE) {
        /* subtract the estimated background from the data frame */
        /* the error of the estimated background is assumed to be negligible */
        SCTPUT("Subtracting fitted background from all fibres FF Frame");
        fdvecbuf1 = AllFrame->frame_array[0];
        fdvecbuf2 = backframe[0];
        for (ix=0; ix<=lastiyixindex; ix++) {
            fdvecbuf1[ix] -= fdvecbuf2[ix];
        }
        /* some more black magic to make the thing more robust: scan the frame
       for any negative pixel values; if any are found, compare them to the
       standard deviation: if the negative value is not compatible with zero, 
       mark that pixel as bad */
        negativepixels=0;
        fdvecbuf1 = AllFrame->frame_array[0];
        fdvecbuf2 = AllFrame->frame_sigma[0];
        fmvecbuf1 = AllFrame->badpixel[0];
        for (ix=0; ix<=lastiyixindex; ix++) {
            if (fmvecbuf1[ix]==0 && (pixelvalue=fdvecbuf1[ix])<0) {
                if ((pixelvalue*pixelvalue)>4*fdvecbuf2[ix]) {
                    fmvecbuf1[ix]=1;
                    negativepixels++;
                }
            }
        }
        if (negativepixels!=0) {
            sprintf(output, "Warning: %d pixels result lower than fitted \
background", negativepixels);
            SCTPUT(output);
            SCTPUT("either they are unmasked bad pixels or some contamination is");
            SCTPUT("skewing background determination");
        }
        SCTPUT("Writing fitted background frame to middumma.bdf");
        if (writeback(AllFrame, "middumma.bdf", backframe)!=NOERR)
            SCTPUT("Warning: error writing background frame to disk");
    }


    SCTPUT("\n*** Step 3: prepare merged bad pixel mask and final \
initialisations ***\n");
    /* allocate the global, merged bad pixel mask and the upper and lower
     integration limits */
    mask=fmmatrix(0,AllFrame->subrows-1,0,AllFrame->subcols-1);
    if(!mask) {
        SCTPUT("Error allocating mask matrix");
        return flames_midas_fail();
    }
    pfibrecentre = d3tensor(0, Order->lastorder-Order->firstorder,
                    0, AllFrame->maxfibres-1,
                    0, AllFrame->subcols-1);
    if(!pfibrecentre) {
        SCTPUT("Error allocating pfibrecentre tensor");
        return flames_midas_fail();
    }

    /* it is pointless to use an integration window larger than fibre width */
    if (halfwinsize>Order->halfibrewidth) {
        SCTPUT("Warning: reducing window size to fibre width");
        halfwinsize = Order->halfibrewidth;
    }
    if (fabs(AllFrame->substepy) < DEPSILON) {
        /* the y step is too small, exit */
        SCTPUT("Error: the STEP in y is too small");
        return flames_midas_fail();
    }
    phalfwinsize = fabs(halfwinsize/AllFrame->substepy);

    /* initialise the global, merged bad pixel mask to be used
     in the subsequent standard extraction, initialise the lookup tables for 
     lit fibres in the AllFrame and allocate the arrays to store the 
     allocated spectra again in AllFrame */
    if (prepstand(AllFrame, Shifted_FF, Order, pfibrecentre, normcover,
                    orderoffset, realfirstorder, reallastorder, mask,
                    phalfwinsize)!=NOERR) {
        /* something went wrong in prepstand */
        SCTPUT("Error in prepstand");
        return flames_midas_fail();
    }

    /* Going to do the real spectrum extraction */
    SCTPUT("\n*** Step 4: standard extraction proper ***\n");
    if (dostandard(AllFrame, Order, Shifted_FF, mask, pfibrecentre,
                    phalfwinsize, realfirstorder, reallastorder) != NOERR) {
        SCTPUT("Error in dostandard");
        return flames_midas_fail();
    }

    /* free here pfibrecentre, unneeded from now on */
    free_d3tensor(pfibrecentre, 0, Order->lastorder-Order->firstorder,
                  0, AllFrame->maxfibres-1,
                  0, AllFrame->subcols-1);

    /* free the old bad pixel mask, replace it with the global mask */
    free_fmmatrix(AllFrame->badpixel, 0, AllFrame->subrows-1, 0,
                  AllFrame->subcols-1);
    AllFrame->badpixel = mask;

    /* build the fitted all fibres FF Frame, do it in place to save memory */
    if (optsynth(AllFrame, Shifted_FF, Order, &backframe, &chisquare,
                    &chisqpixels, &nfittedparams)!=NOERR) {
        SCTPUT("Error computing the fitted frame");
        return flames_midas_fail();
    }
    SCKWRD(OUTPUTD, &chisquare, 1, 1, &unit);
    SCKWRI(OUTPUTI, &chisqpixels, 1, 1, &unit);
    SCKWRI(OUTPUTI, &nfittedparams, 2, 1, &unit);
    SCTPUT("Writing the synthesized data frame to middummb.bdf");
    SCTPUT("Writing the synthesized sigma frame to middummc.bdf");
    SCTPUT("Writing the overall mask to middummd.bdf");
    if (writesynth(AllFrame, "middummb.bdf", "middummc.bdf", "middummd.bdf")
                    != NOERR)
        SCTPUT("Warning: error writing dummy debugging frames");


    SCTPUT("\n*** Step 5: write normalisation factors to disk, clean up and \
exit ***\n");

    /* let's free a bit of memory */

    /* free the estimated background frame, we don't need it any more */
    free_fdmatrix(backframe, 0, AllFrame->subrows-1, 0,
                  AllFrame->subcols-1);

    /* free the normcover array */
    free_fdmatrix(normcover, 0, slitlastorder-slitfirstorder,
                  0, AllFrame->subcols-1);

    fdvecbuf1 = Shifted_FF->normfactors[0][0];
    fdvecbuf2 = Shifted_FF->normsigmas[0][0];
    fmvecbuf1 = Shifted_FF->goodfibres[0][0];
    fdvecbuf3 = AllFrame->spectrum[0][0];
    fdvecbuf4 = AllFrame->specsigma[0][0];
    fmvecbuf2 = AllFrame->specmask[0][0];
    for (ifibre=0; ifibre<=(Shifted_FF->maxfibres-1); ifibre++) {
        if ((Shifted_FF->fibremask[ifibre]==TRUE) &&
                        (AllFrame->fibremask[ifibre]==TRUE)) {
            for (iorder=0; iorder<=(Order->lastorder-Order->firstorder); iorder++) {
                iorderifibreindex = (iorder*AllFrame->maxfibres)+ifibre;
                iorderifibreixoffset = iorderifibreindex*AllFrame->subcols;
                for (ix=0; ix<=(Shifted_FF->subcols-1); ix++) {
                    iorderifibreixindex = iorderifibreixoffset+ix;
                    ixiorderifibreindex = (ix*(1+Order->lastorder-Order->firstorder)*
                                    AllFrame->maxfibres)+iorderifibreindex;
                    /* was this a decent fibre at this x, before? */
                    if (fmvecbuf1[iorderifibreixindex] != BADSLICE) {
                        pixelvalue = fdvecbuf3[ixiorderifibreindex];
                        /* was I able to extract this with a decent SNR? */
                        if ((fmvecbuf2[ixiorderifibreindex]==1) &&
                                        ((pixelvalue*pixelvalue/fdvecbuf4[ixiorderifibreindex])
                                                        >=decentSNR2)){
                            fmvecbuf1[iorderifibreixindex] = GOODSLICE;
                            fdvecbuf1[iorderifibreixindex] = fdvecbuf3[ixiorderifibreindex];
                            fdvecbuf2[iorderifibreixindex] = fdvecbuf4[ixiorderifibreindex];
                        }
                        else {
                            fmvecbuf1[iorderifibreixindex] = DEMISLICE;
                        }
                    }
                }
            }
        }
        else {
            /* This fibre could not be possibly extracted, mark it DEMISLICE
     at best */
            for (iorder=0; iorder<=(Order->lastorder-Order->firstorder); iorder++) {
                iorderifibreindex = (iorder*AllFrame->maxfibres)+ifibre;
                iorderifibreixoffset = iorderifibreindex*AllFrame->subcols;
                for (ix=0; ix<=(Shifted_FF->subcols-1); ix++) {
                    iorderifibreixindex = iorderifibreixoffset+ix;
                    /* was this a decent fibre at this x, before? */
                    if (fmvecbuf1[iorderifibreixindex] != BADSLICE)
                        fmvecbuf1[iorderifibreixindex] = DEMISLICE;
                }
            }
        }
    }

    /* now do write the normalisation factors */
    if (writenormfactors(allfile, Shifted_FF) != NOERR) {
        SCTPUT("Error in writenormfactors");
        return flames_midas_fail();
    }

    /* finally free for good the AllFrame (including the spectrum),
     Shifted_FF and Order */
    if (free_spectrum(AllFrame)!=NOERR) {
        SCTPUT("Error while freeing the spectrum in the AllFrame structure");
        return flames_midas_fail();
    }
    if (freeframe(AllFrame)!=NOERR) {
        SCTPUT("Error while freeing the memory of the AllFrame structure");
        return flames_midas_fail();
    }
    free(AllFrame);
    if (freeallflats(Shifted_FF)!=NOERR) {
        SCTPUT("Error while freeing the memory of the Shifted_FF structure");
        return flames_midas_fail();
    }
    free(Shifted_FF);
    if (freeordpos(Order)!=NOERR) {
        SCTPUT("Error while freeing the memory of the Order structure");
        return flames_midas_fail();
    }
    free(Order);

    SCTPUT("\n*** Standard extraction complete ***\n");

    return SCSEPI();

}
