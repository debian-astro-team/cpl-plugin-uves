/* $Id: recipe_main.c,v 1.3 2009-06-05 05:57:38 amodigli Exp $
 *
 * This file is part of the UVES Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2009-06-05 05:57:38 $
 * $Revision: 1.3 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <cpl.h>

#include <irplib_plugin.h>
#include <cpl_test.h>
#include <uves_dfs.h>
#include <uves_msg.h>

#ifdef UVES_TEST_NAME
#  define STRINGIFY(macro)      CPL_XSTRINGIFY(macro)
#  define LOG_FILE_NAME         STRINGIFY(UVES_TEST_NAME)"-test-run.log"
#  define TEST_NAME             STRINGIFY(UVES_TEST_NAME)
#else
#  define LOG_FILE_NAME         __FILE__
#  define TEST_NAME             __FILE__
#endif


/*----------------------------------------------------------------------------*/
/**
 * @defgroup recipe_main   General plugin tests
 */
/*----------------------------------------------------------------------------*/

/**@{*/

/*-----------------------------------------------------------------------------
                            Function definitions
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/**
  @brief    Find a plugin and submit it to some tests
  @return   0 iff succesful

 */
/*----------------------------------------------------------------------------*/
int main(void)
{

    const char * tags[] = {

       "BIAS_RED",
       "PDARK_RED",
       "DFLAT_RED",
       "ORDER_FLAT_RED",
       "ARC_LAMP_FORM_RED",
       "ARC_LAMP_RED",
       "STANDARD_RED",
       "SCIENCE_RED",

       "LINE_REFER_TABLE",
       "FLUX_STD_TABLE",
       "EXTCOEFF_TABLE",
       "INSTR_RESPONSE_REDL",
       "INSTR_RESPONSE_REDU",

       "MASTER_BIAS_REDL",
       "MASTER_BIAS_REDU",
       "MASTER_DARK_REDL",
       "MASTER_DARK_REDU",
       "MASTER_FLAT_REDL",
       "MASTER_FLAT_REDU",

       "ORDER_TABLE_REDL",
       "ORDER_TABLE_REDU",
       "LINE_TABLE_REDL",
       "LINE_TABLE_REDU",
       "ORDER_GUESS_TAB_REDL",
       "ORDER_GUESS_TAB_REDU",

       "DARK_BLUE",
       "BIAS_BLUE",
       "MASTER_BIAS_BLUE",
       "IFLAT_BLUE",
       "TFLAT_BLUE",
       "ARC_LAMP_FORM_BLUE",
       "ARC_LAMP_BLUE",
       "STANDARD_BLUE",
       "SCI_POINT_BLUE",
       "SCIENCE_BLUE",

       "FLUX_STD_TABLE",
       "EXTCOEFF_TABLE",
       "INSTR_RESPONSE_BLUE",
       "FLUX_STD_TABLE",
       "EXTCOEFF_TABLE",
       "INSTR_RESPONSE_BLUE",

       "ORDER_TABLE_BLUE",
       "ORDER_FLAT_BLUE",
       "ORDER_GUESS_TAB_BLUE",
       "MASTER_BIAS_BLUE",
       "MASTER_DARK_BLUE",
       "MASTER_PDARK_BLUE",
       "MASTER_FLAT_BLUE",
       "ORDER_TABLE_BLUE",
       "LINE_TABLE_BLUE",


       "SFLAT_RED",
       "FIB_ORDEF_RED",
       "FIB_ARC_LAMP_FORM_RED",
       "FIB_FF_ODD_RED",
       "FIB_FF_EVEN_RED",
       "FIB_FF_ALL_RED",
       "FIB_ARC_LAMP_RED",
       "FIB_FF_ODD_RED",
       "FIB_FF_EVEN_RED",
       "FIB_FF_ALL_RED",
       "FIB_ARC_LAMP_RED",
       "FIB_ORDEF_RED",
       "FIB_ARC_LAMP_FORM_RED",
       "FIB_SCI_RED",


       "FIB_ORD_GUE_REDL",
       "FIB_ORD_GUE_REDU",
       "FIB_ORD_TAB_REDL",
       "FIB_ORD_TAB_REDU",
       "MASTER_SFLAT_REDL",
       "MASTER_SFLAT_REDU",
       "MASTER_SFLAT_REDL",
       "MASTER_SFLAT_REDU",
       "MASTER_SFLAT_REDL",
       "MASTER_SFLAT_REDU",
       "FIB_ORDEF_REDL",
       "FIB_ORDEF_REDU",
       "FIB_LIN_GUE_REDL",
       "FIB_LIN_GUE_REDU",
       "FIB_ORDEF_TABLE_REDL",
       "FIB_ORDEF_TABLE_REDU",
       "FIB_FF_BPC_REDL",
       "FIB_FF_COM_REDL",
       "FIB_FF_DTC_REDL",
       "FIB_FF_NOR_REDL",
       "FIB_FF_NSG_REDL",
       "FIB_FF_SGC_REDL",
       "FIB_FF_BPC_REDU",
       "FIB_FF_COM_REDU",
       "FIB_FF_DTC_REDU",
       "FIB_FF_NOR_REDU",
       "FIB_FF_NSG_REDU",
       "FIB_FF_SGC_REDU",
       "FIB_LINE_TABLE_REDL",
       "FIB_LINE_TABLE_REDU",
       "FIB_ORDEF_TABLE_REDL",
       "FIB_ORDEF_TABLE_REDU",
       "SLIT_FF_BNC_REDL",
       "SLIT_FF_BPC_REDL",
       "SLIT_FF_COM_REDL",
       "SLIT_FF_DTC_REDL",
       "SLIT_FF_NOR_REDL",
       "SLIT_FF_SGC_REDL",
       "SLIT_FF_BNC_REDU",
       "SLIT_FF_BPC_REDU",
       "SLIT_FF_COM_REDU",
       "SLIT_FF_DTC_REDU",
       "SLIT_FF_NOR_REDU",
       "SLIT_FF_SGC_REDU"

    };

    cpl_pluginlist * pluginlist;
    const size_t ntags = sizeof(tags) / sizeof(char*);

    char * old_dir = NULL;
    char * new_dir = NULL;

    cpl_test_init_macro(LOG_FILE_NAME, PACKAGE_BUGREPORT, CPL_MSG_WARNING);
    uves_msg("Hello world!");

    /* Save the current working directory. */
    old_dir = cpl_malloc(1024*4);
    cpl_test_assert(old_dir != NULL);
    cpl_test_assert(getcwd(old_dir, 1024*4) != NULL);

    /* Create a new sub directory and change to that directory. */
    new_dir = cpl_sprintf("%s/workdir_%s_pid%d", old_dir, TEST_NAME,
                          (int)getpid());
    cpl_test_assert(new_dir != NULL);
    cpl_msg_debug(cpl_func, "Changing current working directory to: %s",
                  new_dir);
    cpl_test_assert(mkdir(new_dir, 0777) == 0);
    cpl_test_assert(chdir(new_dir) == 0);

    /* Now we can run the plugin tests in their own directory without worrying
     * that we will have any race conditions with other unit tests. */

    pluginlist = cpl_pluginlist_new();

    cpl_test(!cpl_plugin_get_info(pluginlist));

    cpl_test(!irplib_plugin_test(pluginlist, ntags, tags));

    cpl_pluginlist_delete(pluginlist);

    (void) system("rm -f "TEST_NAME"*.fits");  /* cleanup any FITS files. */

    /* Restore the current directory to the initial value and remove the
     * temporary sub-directory if there were no unit test failures. */
    cpl_msg_debug(cpl_func, "Restoring current working directory to: %s",
                  old_dir);
    cpl_test_assert(chdir(old_dir) == 0);
    cpl_free(old_dir);
    if (cpl_test_get_failed() == 0) {
      cpl_test_assert(rmdir(new_dir) == 0);
    }
    cpl_free(new_dir);

    return cpl_test_end(0);
}

/**@}*/
