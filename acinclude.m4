# LIB_CHECK_FFTW
#------------------
# Checks for the fftw library and header files.
AC_DEFUN([LIB_CHECK_FFTW],
[

    AC_MSG_CHECKING([for fftw])

    uves_fftw_check_header="sfftw.h"
    uves_fftw_check_lib="libsfftw.a"

    uves_fftw_includes=""
    uves_fftw_libraries=""

    AC_ARG_WITH(fftw,
                AS_HELP_STRING([--with-fftw],
                               [location where fftw is installed]),
                [
                    uves_with_fftw_includes=$withval/include
                    uves_with_fftw_libs=$withval/lib
                ])

    AC_ARG_WITH(fftw-includes,
                AS_HELP_STRING([--with-fftw-includes],
                               [location of the fftw header files]),
                uves_with_fftw_includes=$withval)

    AC_ARG_WITH(fftw-libs,
                AS_HELP_STRING([--with-fftw-libs],
                               [location of the fftw library]),
                uves_with_fftw_libs=$withval)

    AC_ARG_ENABLE(fftw,
                  AS_HELP_STRING([--enable-fftw],
                                 [disables checks for the fftw library and headers]),
                  uves_enable_fftw_test=yes,
                  uves_enable_fftw_test=no)


    if test "x$uves_enable_fftw_test" = xyes; then

        # Check for the fftw includes

        if test -z "$uves_with_fftw_includes"; then
            uves_fftw_incdirs="/opt/fftw/include /usr/local/include /usr/include"

            test -n "$CPLDIR" && uves_fftw_incdirs="$CPLDIR/include \
                $uves_fftw_incdirs"

            test -n "$FFTWDIR" && uves_fftw_incdirs="$FFTWDIR/include \
                $uves_fftw_incdirs"
        else
            uves_fftw_incdirs="$uves_with_fftw_includes"
        fi

        ESO_FIND_FILE($uves_fftw_check_header, $uves_fftw_incdirs,
                      uves_fftw_includes)


        # Check for the fftw library

        if test -z "$uves_with_fftw_libs"; then
            uves_fftw_libdirs="/opt/fftw/lib /usr/local/lib /usr/lib"

            test -n "$CPLDIR" && uves_fftw_libdirs="$CPLDIR/lib \
                $uves_fftw_libdirs"

            test -n "$FFTWDIR" && uves_fftw_libdirs="$FFTWDIR/lib \
                $uves_fftw_libdirs"
        else
            uves_fftw_libdirs="$uves_with_fftw_libs"
        fi

        ESO_FIND_FILE($uves_fftw_check_lib, $uves_fftw_libdirs,
                      uves_fftw_libraries)


        if test x"$uves_fftw_includes" = xno || \
            test x"$uves_fftw_libraries" = xno; then
            uves_fftw_notfound=""

            if test x"$uves_fftw_includes" = xno; then
                if test x"$uves_fftw_libraries" = xno; then
                    uves_fftw_notfound="(headers and libraries)"
                else
                    uves_fftw_notfound="(headers)"
                fi
            else
                uves_fftw_notfound="(libraries)"
            fi

            AC_MSG_ERROR([fftw $uves_fftw_notfound was not found on your system. Please check!])
        else
            AC_MSG_RESULT([libraries $uves_fftw_libraries, headers $uves_fftw_includes])
        fi

        # Set up the symbols

        FFTW_INCLUDES="-I$uves_fftw_includes"
        FFTW_LDFLAGS="-L$uves_fftw_libraries"
        LDFLAGS="$LDFLAGS $FFTW_LDFLAGS"
        AC_CHECK_LIB([sfftw], [main], [LIBFFTWC="-lsfftw"], AC_MSG_ERROR([Library fftwcblas not found]))
	AC_DEFINE_UNQUOTED(HAVE_FFTW, 1, [Define to 1 if you have FFTW v. 2.1.5])
        LIBS="$LIBS $LIBFFTWC"
        LIBFFTW="$LIBFFTWC"
    else
        AC_MSG_RESULT([disabled])
        AC_MSG_WARN([fftw checks have been disabled! This package may not build!])
        FFTW_INCLUDES=""
        FFTW_LDFLAGS=""
        LIBFFTW=""
    fi

    # To be removed after CPL switched to fftw
    FFTW_INCLUDES="$FFTW_INCLUDES"
    FFTW_LDFLAGS="$FFTW_LDFLAGS"
    LIBFFTW="$LIBFFTW"

    AC_SUBST(FFTW_INCLUDES)
    AC_SUBST(FFTW_LDFLAGS)
    AC_SUBST(LIBFFTW)

])




# UVES_SET_PREFIX(PREFIX)
#---------------------------
AC_DEFUN([UVES_SET_PREFIX],
[
    unset CDPATH
    # make $PIPE_HOME the default for the installation
    AC_PREFIX_DEFAULT($1)

    if test "x$prefix" = "xNONE"; then
        prefix=$ac_default_prefix
        ac_configure_args="$ac_configure_args --prefix $prefix"
    fi

    if test "x$exec_prefix" = "xNONE"; then
        exec_prefix=$prefix
    fi

])


# UVES_SET_VERSION_INFO(VERSION, [CURRENT], [REVISION], [AGE])
#----------------------------------------------------------------
# Setup various version information, especially the libtool versioning
AC_DEFUN([UVES_SET_VERSION_INFO],
[
    uves_version=`echo "$1" | sed -e 's/[[a-z,A-Z]].*$//'`

    uves_major_version=`echo "$uves_version" | \
        sed 's/\([[0-9]]*\).\(.*\)/\1/'`
    uves_minor_version=`echo "$uves_version" | \
        sed 's/\([[0-9]]*\).\([[0-9]]*\)\(.*\)/\2/'`
    uves_micro_version=`echo "$uves_version" | \
        sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\3/'`

    if test -z "$uves_major_version"; then uves_major_version=0
    fi

    if test -z "$uves_minor_version"; then uves_minor_version=0
    fi

    if test -z "$uves_micro_version"; then uves_micro_version=0
    fi

    UVES_VERSION="$uves_version"
    UVES_MAJOR_VERSION=$uves_major_version
    UVES_MINOR_VERSION=$uves_minor_version
    UVES_MICRO_VERSION=$uves_micro_version

    if test -z "$4"; then UVES_INTERFACE_AGE=0
    else UVES_INTERFACE_AGE="$4"
    fi

    UVES_BINARY_AGE=`expr 100 '*' $UVES_MINOR_VERSION + $UVES_MICRO_VERSION`
    UVES_BINARY_VERSION=`expr 10000 '*' $UVES_MAJOR_VERSION + \
                          $UVES_BINARY_AGE`

    AC_SUBST(UVES_VERSION)
    AC_SUBST(UVES_MAJOR_VERSION)
    AC_SUBST(UVES_MINOR_VERSION)
    AC_SUBST(UVES_MICRO_VERSION)
    AC_SUBST(UVES_INTERFACE_AGE)
    AC_SUBST(UVES_BINARY_VERSION)
    AC_SUBST(UVES_BINARY_AGE)

    AC_DEFINE_UNQUOTED(UVES_MAJOR_VERSION, $UVES_MAJOR_VERSION,
                       [UVES major version number])
    AC_DEFINE_UNQUOTED(UVES_MINOR_VERSION, $UVES_MINOR_VERSION,
                       [UVES minor version number])
    AC_DEFINE_UNQUOTED(UVES_MICRO_VERSION, $UVES_MICRO_VERSION,
                       [UVES micro version number])
    AC_DEFINE_UNQUOTED(UVES_INTERFACE_AGE, $UVES_INTERFACE_AGE,
                       [UVES interface age])
    AC_DEFINE_UNQUOTED(UVES_BINARY_VERSION, $UVES_BINARY_VERSION,
                       [UVES binary version number])
    AC_DEFINE_UNQUOTED(UVES_BINARY_AGE, $UVES_BINARY_AGE,
                       [UVES binary age])

    ESO_SET_LIBRARY_VERSION([$2], [$3], [$4])
])


# UVES_SET_PATHS
#------------------
# Define auxiliary directories of the installed directory tree.
AC_DEFUN([UVES_SET_PATHS],
[

    if test -z "$plugindir"; then
        plugindir='${libdir}/esopipes-plugins/${PACKAGE}-${VERSION}'
    fi

    if test -z "$privatelibdir"; then
        privatelibdir='${libdir}/${PACKAGE}-${VERSION}'
    fi

    if test -z "$pipedocsdir"; then
        pipedocsdir='${datadir}/doc/esopipes/${PACKAGE}-${VERSION}'
    fi

    if test -z "$apidocdir"; then
        apidocdir='${datadir}/doc/esopipes/${PACKAGE}-${VERSION}/html'
    fi

    if test -z "$configdir"; then
       configdir='${datadir}/${PACKAGE}/config'
    fi

    if test -z "$wkfextradir"; then
        wkfextradir='${datadir}/esopipes/${PACKAGE}-${VERSION}/reflex'
    fi

    if test -z "$wkfcopydir"; then
        wkfcopydir='${datadir}/reflex/workflows/${PACKAGE}-${VERSION}'
    fi

    AC_SUBST(plugindir)
    AC_SUBST(privatelibdir)
    AC_SUBST(apidocdir)
    AC_SUBST(pipedocsdir)
    AC_SUBST(configdir)
    AC_SUBST(wkfextradir)
    AC_SUBST(wkfcopydir)


    # Define a preprocesor symbol for the plugin search paths

    AC_DEFINE_UNQUOTED(UVES_PLUGIN_DIR, "esopipes-plugins",
                       [Plugin directory tree prefix])

    eval plugin_dir="$plugindir"
    plugin_path=`eval echo $plugin_dir | \
                sed -e "s/\/${PACKAGE}-${VERSION}.*$//"`

    AC_DEFINE_UNQUOTED(UVES_PLUGIN_PATH, "$plugin_path",
                       [Absolute path to the plugin directory tree])

])


# UVES_CREATE_SYMBOLS
#-----------------------
# Define include and library related makefile symbols
AC_DEFUN([UVES_CREATE_SYMBOLS],
[
    # Symbols for package include file and library search paths

    FLAMES_INCLUDES='-I$(top_srcdir)/flames'
    FLAMES_LDFLAGS='-L$(top_builddir)/flames'
    UVES_INCLUDES='-I$(top_srcdir)/uves'
    UVES_LDFLAGS='-L$(top_builddir)/uves'
    IRPLIB_INCLUDES='-I$(top_srcdir)/irplib'
    # No -L for IRPLIB which is statically linked

    QF_INCLUDES='-I$(top_srcdir)/libqfits -I$(top_srcdir)/libqfits/src'
    QF_LDFLAGS=""


    all_includes='$(QF_INCLUDES) $(FLAMES_INCLUDES) $(UVES_INCLUDES) $(IRPLIB_INCLUDES) $(CPL_INCLUDES) $(CFITSIO_INCLUDES) $(EXTRA_INCLUDES)'
    all_ldflags='$(FLAMES_LDFLAGS) $(UVES_LDFLAGS) $(CPL_LDFLAGS) $(CFITSIO_LDFLAGS) $(EXTRA_LDFLAGS) $(QF_LDFLAGS)'

    # Library aliases

    LIBFLAMES='$(top_builddir)/flames/libflames.la'
    LIBUVES='$(top_builddir)/uves/libuves.la'
    LIBIRPLIB='$(top_builddir)/irplib/libirplib.la'

    # Substitute the defined symbols

    AC_SUBST(QF_INCLUDES)
    AC_SUBST(FLAMES_INCLUDES)
    AC_SUBST(UVES_INCLUDES)
    AC_SUBST(IRPLIB_INCLUDES)	

    AC_SUBST(FLAMES_LDFLAGS)
    AC_SUBST(UVES_LDFLAGS)

    AC_SUBST(LIBFLAMES)
    AC_SUBST(LIBHDRL)
    AC_SUBST(LIBUVES)
    AC_SUBST(LIBIRPLIB)

    AC_SUBST(QF_LDFLAGS)

    # Check for CPL and user defined libraries
    AC_REQUIRE([CPL_CHECK_LIBS])
    AC_REQUIRE([ESO_CHECK_EXTRA_LIBS])

    all_includes='$(QF_INCLUDES) $(FLAMES_INCLUDES) $(UVES_INCLUDES) $(HDRL_INCLUDES) $(IRPLIB_INCLUDES) $(CPL_INCLUDES) $(CFITSIO_INCLUDES)  $(EXTRA_INCLUDES)'
    all_ldflags='$(FLAMES_LDFLAGS) $(UVES_LDFLAGS) $(HDRL_LDFLAGS) $(CPL_LDFLAGS) $(EXTRA_LDFLAGS) $(QF_LDFLAGS)'

#    all_includes="$CPL_INCLUDES $CX_INCLUDES $CFITSIO_INCLUDES $all_includes"


    AC_SUBST(all_includes)
    AC_SUBST(all_ldflags)
])
