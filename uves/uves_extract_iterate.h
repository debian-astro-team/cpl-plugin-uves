/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:03 $
 * $Revision: 1.8 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.6  2010/02/13 12:22:31  amodigli
 * removed inlines (let's do work to compiler)
 *
 * Revision 1.5  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.4  2007/05/02 13:17:25  jmlarsen
 * Allow specifying offset in optimal extraction
 *
 * Revision 1.3  2006/11/16 09:48:30  jmlarsen
 * Renamed data type position -> uves_iterate_position, for namespace reasons
 *
 * Revision 1.2  2006/09/11 14:19:28  jmlarsen
 * Updated documentation
 *
 * Revision 1.1  2006/09/08 14:03:58  jmlarsen
 * Simplified code by using iterators, sky subtraction much optimized
 *
 *
 */
#ifndef UVES_EXTRACT_ITERATE_H
#define UVES_EXTRACT_ITERATE_H

#include <uves_utils_polynomial.h>

#include <stdbool.h>

typedef struct {
    double length;
    double offset;
} slit_geometry;

/* @cond */
typedef struct
{
    /* 'public' numbers that are iterated */

    /** Order number, pixel position */
    int order;
    /** Current pixel */
    int x, y;
    /** Center of current bin */
    double ycenter;
    /** y-range of current bin */
    int ylow, yhigh;


    /* private stuff which is set
       for each iteration */

    /** x-range */
    int xmin, xmax;
    /** last order (inclusive) of
    current iteration */
    int ordermax;
    /** bad pixel map used in this iteration,
    or NULL */
    const cpl_binary *bpm;
    /** Loop over y-values (true), or loop
    just over bins (false) */
    bool loop_y;
    /** Iteration finished? */
    bool end;

    /* public stuff which is always
       constant (the geometry) */

    /** Image size */
    int nx, ny;

    /** First/last order according to order table */
    int minorder, maxorder;

    const polynomial *order_locations;
    slit_geometry sg;

} uves_iterate_position;
/* @endcond */

uves_iterate_position *
uves_iterate_new(int nx, int ny,
         const polynomial *order_locations,
         int minorder, int maxorder,
         slit_geometry sg);

void
uves_iterate_delete(uves_iterate_position **p);

void
uves_iterate_set_first(uves_iterate_position *p,
               int xmin, int xmax, 
               int ordermin, int ordermax,
               const cpl_binary *bpm,
               bool loop_y);

void
uves_iterate_increment(uves_iterate_position *p);

bool
uves_iterate_finished(const uves_iterate_position *p);

void
uves_iterate_dump(const uves_iterate_position *p, FILE *stream);

#endif
