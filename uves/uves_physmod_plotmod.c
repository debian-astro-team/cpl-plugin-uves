/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */

/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:07 $
 * $Revision: 1.13 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.11  2008/09/29 06:56:23  amodigli
 * add #include <string.h>
 *
 * Revision 1.10  2007/10/15 11:10:55  amodigli
 * fixed bug on reported chip ID in physmod plots
 *
 * Revision 1.9  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.8  2007/04/26 06:54:43  amodigli
 * small changes on title
 *
 * Revision 1.7  2007/04/25 08:38:03  amodigli
 * changed interface and plotting more info
 *
 * Revision 1.6  2007/01/15 08:43:15  jmlarsen
 * Fixed missing plots
 *
 * Revision 1.5  2007/01/13 09:52:22  amodigli
 * fixed some problems on flames QC log
 *
 * Revision 1.4  2006/11/06 15:19:41  jmlarsen
 * Removed unused include directives
 *
 * Revision 1.3  2006/07/28 14:51:26  amodigli
 * fixed some bugs on improper table selection
 *
 * Revision 1.2  2006/06/20 10:56:56  amodigli
 * cleaned output, added units
 *
 * Revision 1.1  2006/02/03 07:46:30  jmlarsen
 * Moved recipe implementations to ./uves directory
 *
 * Revision 1.5  2006/01/19 08:47:24  jmlarsen
 * Inserted missing doxygen end tag
 *
 * Revision 1.4  2006/01/09 14:05:42  amodigli
 * Fixed doxigen warnings
 *
 * Revision 1.3  2005/12/20 08:11:44  jmlarsen
 * Added CVS  entry
 *
 */

/*----------------------------------------------------------------------------*/
/**
 * @addtogroup uves_physmod
 */
/*----------------------------------------------------------------------------*/
/**@{*/
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif


/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/
#include <uves_physmod_plotmod.h>

#include <uves_plot.h>
#include <uves_msg.h>
#include <uves_error.h>
#include <string.h>
/*-----------------------------------------------------------------------------
                                Defines
 -----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
                            Functions prototypes
 ----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
                            Static variables
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                            Functions code
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
  @brief    This procedure plots results from the uves_physmod recipe
  @param    tbl    The input line table. 
  @param    tbl    The input raw data header. 
  @param    params The input recipe parameters. 

  @return   0 if everything is ok, -1 otherwise


 This procedure plots results from the uves_physmod recipe
 */
/*----------------------------------------------------------------------------*/

int 
uves_physmod_plotmod(const cpl_table* tbl,
                     const uves_propertylist* head,
                     const char* rec_id,
                     const cpl_parameterlist* params,
                     enum uves_chip chip)
{

  char title[300];
  double ech_ang_off=0;
  double cd_ang_off=0;
  double ccd_ang_off=0;
  double wcent=0;
  double temp_cam=0;
  double slit_width=0;
  double slit_length=0;
  const char* origfile=NULL;
  const char* tpl_start=NULL;
  char chip_id[5];

  strcpy(chip_id,uves_chip_tostring_lower(chip));

  check( uves_get_parameter(params, NULL,rec_id,"ech_angle_off", 
         CPL_TYPE_DOUBLE, &ech_ang_off )  , "Could not read parameter");

  check( uves_get_parameter(params, NULL, rec_id, "cd_angle_off", 
         CPL_TYPE_DOUBLE, &cd_ang_off )  , "Could not read parameter");

  check( uves_get_parameter(params, NULL, rec_id, "ccd_rot_angle_off", 
         CPL_TYPE_DOUBLE, &ccd_ang_off )  , "Could not read parameter");

  check (wcent = uves_pfits_get_gratwlen(head, chip), 
     "Could not read central wavelength setting from input header");

  check (temp_cam = uves_pfits_get_tempcam(head,chip), 
     "Could not read camera's temperature from input header");

  check (slit_width = uves_pfits_get_slitwidth(head, chip), 
     "Could not read slit width input header");
  
  check (slit_length = uves_pfits_get_slitlength(head, chip), 
     "Could not read slit length input header");

  check(tpl_start=uves_pfits_get_tpl_start(head),"Error getting TPL START");

  check(origfile=uves_pfits_get_origfile(head),"Error getting ORIGFILE");

  sprintf(title,"%s %4.1f %s %2.1f %s %s %3.1f %s ",
      "Central wavelength: ",wcent,
          " nm, slit: ",slit_length,
          " arcsec, CCD:",chip_id,temp_cam," C");


  /* 1st plot */
  check(uves_plot_table(tbl, "XMOD", "XDIF", "%s", title),
    "Plotting failed");
  
  /* 2nd plot */
  check(uves_plot_table(tbl, "XMOD", "YDIF", "%s", title),
    "Plotting failed");
  
  
  /* 3rd plot */
  check(uves_plot_table(tbl, "YMOD", "XDIF", "%s", title),
    "Plotting failed");
  
  
  /* 4th plot */
  check(uves_plot_table(tbl, "YMOD", "YDIF", "%s", title),
        "Plotting failed");
  
  
  /* 5th plot */
  check(uves_plot_table(tbl, "XDIF", "YDIF", "%s", title),
        "Plotting failed");
  
  
  /* 6th plot */
  check(uves_plot_table(tbl, "XMOD", "YMOD", "%s", title),
        "Plotting failed");
  
 cleanup:
  return 0;
}
/**@}*/
