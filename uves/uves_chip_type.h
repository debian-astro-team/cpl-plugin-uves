/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:02 $
 * $Revision: 1.3 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.1  2007/01/18 07:43:22  jmlarsen
 * Add chip type
 *
 * Revision 1.6  2007/01/16 10:26:19  jmlarsen
 * Removed obsolete declaration
 *
 */
#ifndef UVES_CHIP_TYPE_H
#define UVES_CHIP_TYPE_H

/*-----------------------------------------------------------------------------
                            Type definitions
 -----------------------------------------------------------------------------*/
typedef struct _uves_chip * uves_chip;

/*-----------------------------------------------------------------------------
                               Values
 -----------------------------------------------------------------------------*/
extern const uves_chip UVES_CHIP_BLUE;
extern const uves_chip UVES_CHIP_REDL;
extern const uves_chip UVES_CHIP_REDU;
extern const uves_chip UVES_CHIP_INVALID;

#endif
