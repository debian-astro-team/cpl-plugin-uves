/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2011-12-08 13:55:38 $
 * $Revision: 1.8 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.7  2010/09/24 09:32:07  amodigli
 * put back QFITS dependency to fix problem spot by NRI on FIBER mode (with MIDAS calibs) data
 *
 * Revision 1.5  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.4  2007/04/24 12:50:29  jmlarsen
 * Replaced cpl_propertylist -> uves_propertylist which is much faster
 *
 * Revision 1.3  2006/11/03 15:01:21  jmlarsen
 * Killed UVES 3d table module and use CPL 3d tables
 *
 * Revision 1.2  2006/08/17 09:20:43  jmlarsen
 * Get reference object ID from flux table, not raw header
 *
 * Revision 1.1  2006/02/03 07:51:04  jmlarsen
 * Moved recipe implementations to ./uves directory
 *
 * Revision 1.2  2005/12/19 16:17:55  jmlarsen
 * Replaced bool -> int
 *
 * Revision 1.1  2005/11/11 13:18:54  jmlarsen
 * Reorganized code, renamed source files
 *
 */
#ifndef UVES_RESPONSE_UTILS_H
#define UVES_RESPONSE_UTILS_H
#include <uves_cpl_size.h>
#include <uves_propertylist.h>
#include <cpl.h>
#include <stdbool.h>
#include <../hdrl/hdrl.h>
cpl_image *
uves_calculate_response(const cpl_image *spectrum, const uves_propertylist *spectrum_header,
            const cpl_table *flux_table,
            const uves_propertylist *raw_header,
            double PACCURACY,
            bool inverse,
            char **ref_obj_id);

cpl_table *
uves_align(const uves_propertylist *object_header, const cpl_table *flux_table, 
       double accuracy, char **ref_name_dynamic);
hdrl_spectrum1D *
uves_extend_hdrl_spectrum(const double wmin, const double wmax,
        const hdrl_spectrum1D * s);
#endif
