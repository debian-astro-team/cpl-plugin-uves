/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:02 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.3  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.2  2007/04/24 12:50:29  jmlarsen
 * Replaced cpl_propertylist -> uves_propertylist which is much faster
 *
 * Revision 1.1  2006/10/04 11:03:08  jmlarsen
 * Implemented QC.VRAD parameters
 *
 * Revision 1.4  2006/08/17 13:56:52  jmlarsen
 * Reduced max line length
 *
 * Revision 1.3  2006/02/28 09:15:22  jmlarsen
 * Minor update
 *
 * Revision 1.2  2005/12/19 16:17:56  jmlarsen
 * Replaced bool -> int
 *
 */
#ifndef UVES_BARYVEL_H
#define UVES_BARYVEL_H

#include <uves_propertylist.h>

void
uves_baryvel(const uves_propertylist *raw_header,
         double *barycor,
         double *helicor);

#endif  /* UVES_BARYVEL_H */
