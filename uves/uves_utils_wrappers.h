/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2012-05-01 06:28:00 $
 * $Revision: 1.67 $
 * $Name: not supported by cvs2svn $
 */
#ifndef UVES_UTILS_WRAPPERS_H
#define UVES_UTILS_WRAPPERS_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
/*-----------------------------------------------------------------------------
                    Includes
 -----------------------------------------------------------------------------*/
#include <uves_cpl_size.h>

#include <uves_propertylist.h>
#include <irplib_utils.h>

#include <cpl.h>

#include <stdbool.h>
/*-----------------------------------------------------------------------------
                             Defines
 -----------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------
                                   Prototypes
 -----------------------------------------------------------------------------*/
#define uves_sprintf(...) cpl_sprintf(__VA_ARGS__)

cpl_image * 
uves_image_filter_median(const cpl_image * img, const cpl_matrix * mx);


cpl_image * 
uves_image_filter_linear(const cpl_image *img, const cpl_matrix * mx);




const char *uves_find_frame(const cpl_frameset *frames, const char **wanted, 
                int N, int *found, const cpl_frame **frame);
cpl_error_code uves_cast_image(cpl_image **image, cpl_type to_type);
cpl_error_code uves_crop_image(cpl_image **image, int x1, int y_1, int x2, int y2);

cpl_error_code uves_get_property_value(const uves_propertylist *plist,
                       const char *keyword, cpl_type keywordtype, 
                       void *result);
cpl_error_code uves_get_parameter(const cpl_parameterlist *parameters,
                  const char *context, const char *recipe_id, 
                  const char *name, cpl_type type, void *value);
cpl_error_code uves_set_parameter(cpl_parameterlist *parameters, 
                  const char *context, const char *name,
                  cpl_type type, void *value);
cpl_error_code uves_set_parameter_default(cpl_parameterlist *parameters, 
                      const char *context,
                      const char *parname, 
                      cpl_type type, void *value);

void uves_image_reject_all(cpl_image *image);

cpl_size uves_get_nextensions(const char *filename);
int uves_select_table_rows(cpl_table *t,  const char *column,
                      cpl_table_select_operator operator, 
                      double value);
int uves_erase_invalid_table_rows(cpl_table *t, const char *column);
int uves_erase_table_rows(cpl_table *t, const char *column, 
              cpl_table_select_operator operator,
              double value);
int uves_extract_table_rows_local(cpl_table *t, const char *column,
                  cpl_table_select_operator operator, double value);

cpl_table *uves_extract_table_rows(const cpl_table *t, const char *column,
                   cpl_table_select_operator operator,
                   double value);

cpl_error_code
uves_table_sort_dfsxxxx(cpl_table *table, const uves_propertylist *reflist);

void uves_sort_table_1(cpl_table *t, const char *column1,
               bool reverse1);
void uves_sort_table_2(cpl_table *t, const char *column1, 
               const char *column2, bool reverse1, 
               bool reverse2);
void uves_sort_table_3(cpl_table *t, const char *column1, 
               const char *column2, const char *column3,
               bool reverse1, bool reverse2, bool reverse3);


cpl_error_code uves_fit(const cpl_matrix *x, const cpl_matrix *sigma_x,
            const cpl_vector *y, const cpl_vector *sigma_y,
            cpl_vector *a, const int ia[],
            int    (*f)(const double x[], const double a[], 
                    double *result),
            int (*dfda)(const double x[], const double a[], 
                    double result[]),
            double *mse,
            double *red_chisq,
            cpl_matrix **covariance);

cpl_error_code
uves_fit_1d(cpl_vector *x, const cpl_vector *sigma_x,
        cpl_vector *y, const cpl_vector *sigma_y,
        cpl_fit_mode fit_pars, bool fit_back,
        double *x0, double *sigma, double *area, double *offset, double *slope,
            double *mse, double *red_chisq,
        cpl_matrix **covariance,
        int (*f)   (const double x[], const double a[], double *result),
        int (*dfda)(const double x[], const double a[], double result[]),
        int M);

cpl_error_code
uves_fit_1d_image(const cpl_image *image, const cpl_image *noise,
          const cpl_binary *image_badmap,
          bool horizontal, bool fix_back, bool fit_back,
          int xlo, int xhi, int y_0,
          double *x0, double *sigma, double *norm, double *background,
                  double *slope,
          double *mse, double *red_chisq,
          cpl_matrix **covariance,
          int (*f)   (const double x[], const double a[], double *result),
          int (*dfda)(const double x[], const double a[], double result[]),
          int M);


void uves_propertylist_append_property(uves_propertylist *result, const cpl_property *p);

cpl_error_code uves_table_erase_selected_dfs02356(cpl_table *t);
int uves_table_and_selected_invalid(cpl_table *t, const char *column);

void uves_raise_to_median_frac(cpl_table *t, const char *column, double fraction);
void uves_free(const void *mem);
void uves_free_image(cpl_image **i);
void uves_free_mask(cpl_mask **m);
void uves_free_imagelist(cpl_imagelist **i);
void uves_free_table(cpl_table **t);
void uves_free_table_const(const cpl_table **t);
void uves_free_propertylist(uves_propertylist **p);
void uves_free_propertylist_const(const uves_propertylist **p);
void uves_free_property(cpl_property **p);
void uves_free_polynomial(cpl_polynomial **p);
void uves_free_matrix(cpl_matrix **m);
void uves_free_vector(cpl_vector **v);
void uves_free_bivector(cpl_bivector **b);
void uves_free_stats(cpl_stats **s);
void uves_unwrap_vector(cpl_vector **v);
void uves_unwrap_vector_const(const cpl_vector **v);
void uves_unwrap_bivector_vectors(cpl_bivector **b);
void uves_free_parameterlist(cpl_parameterlist **p);
void uves_free_frameset(cpl_frameset **f);
void uves_free_frame(cpl_frame **f);
void uves_free_array(cpl_array **a);
void uves_free_int(int **i);
void uves_free_int_const(const int **i);
void uves_free_float(float **f);
void uves_free_double(double **d);
void uves_free_string(char **s);
void uves_free_string_const(const char **s);
cpl_image*
uves_image_filter_mode(const cpl_image* b,
                      const cpl_matrix * ker,
                      cpl_filter_mode filter);


#endif












