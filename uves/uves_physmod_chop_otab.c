
/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */

/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:07 $
 * $Revision: 1.14 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.12  2008/11/06 14:36:27  amodigli
 * fixed mem leaks
 *
 * Revision 1.11  2008/05/13 08:44:32  amodigli
 * added check on guess order table spectral format size
 *
 * Revision 1.10  2008/05/01 09:44:10  amodigli
 * modify table after chop
 *
 * Revision 1.9  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.8  2007/05/16 16:32:55  amodigli
 * changed factor from 0.05to 0.075
 *
 * Revision 1.7  2007/04/24 12:50:29  jmlarsen
 * Replaced cpl_propertylist -> uves_propertylist which is much faster
 *
 * Revision 1.6  2006/11/06 15:19:41  jmlarsen
 * Removed unused include directives
 *
 * Revision 1.5  2006/07/28 14:51:26  amodigli
 * fixed some bugs on improper table selection
 *
 * Revision 1.4  2006/06/05 08:51:55  amodigli
 * cleaned some warnings from static checks
 *
 * Revision 1.3  2006/05/09 15:42:48  amodigli
 * safer way to do selection
 *
 * Revision 1.2  2006/05/08 15:42:16  amodigli
 * allow to specify order column label
 *
 * Revision 1.1  2006/02/03 07:46:30  jmlarsen
 * Moved recipe implementations to ./uves directory
 *
 * Revision 1.5  2006/01/19 08:47:24  jmlarsen
 * Inserted missing doxygen end tag
 *
 * Revision 1.4  2006/01/16 08:01:57  amodigli
 *
 * Added stability check
 *
 * Revision 1.3  2006/01/09 14:05:42  amodigli
 * Fixed doxigen warnings
 *
 * Revision 1.2  2006/01/03 16:57:13  amodigli
 * Fixed bug
 *
 * Revision 1.1  2006/01/03 14:47:53  amodigli
 *
 * Added uves_physmod_chop_otab.h .c to match MIDAS
 *
 * Revision 1.3  2005/12/19 16:17:55  jmlarsen
 * Replaced bool -> int
 *
 */

/*----------------------------------------------------------------------------*/
/**
 * @addtogroup uves_physmod
 */
/*----------------------------------------------------------------------------*/
/**@{*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/
#include <uves_physmod_chop_otab.h>

#include <uves_utils_wrappers.h>
#include <uves_pfits.h>
#include <uves_error.h>

/*-----------------------------------------------------------------------------
                                Defines
 -----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
                            Functions prototypes
 ----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
                            Static variables
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                            Functions code
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
  @brief    Chop off orders which are not fully in the detector chip
  @param    raw_header      An array containing the input image headers. 
                            The ordering must be the same as the ordering 
                            of images in the input image list
  @param    chip            CCD chip
  @param    col_name        Name of column of which min/max is computed
  @param    ord_tbl         The output guess order table. 
  @param    ord_min         The output guess order table minimum value 
  @param    ord_max         The output guess order table maximum value 

  @return   0 if everything is ok, -1 otherwise

 */
/*----------------------------------------------------------------------------*/

int  uves_physmod_chop_otab(const uves_propertylist * raw_header,
                            enum uves_chip  chip,
                            cpl_table** ord_tbl,
                            const char* col_name,
                            int * ord_min,
                            int * ord_max)
{

  int nx=0;
  int ny=0;

  double xbox[2]={0,0};
  double ybox[2]={0,0};
  const double fct=0.075;
  const double add=50.;
  cpl_table* tmp_tbl1=NULL;
  cpl_table* tmp_tbl2=NULL;

  /* note those are swapped */
  check (ny = uves_pfits_get_nx(raw_header,chip),
     "Could not read nx from input header");
  check (nx = uves_pfits_get_ny(raw_header,chip),
     "Could not read ny from input header");

  xbox[0]=(double)(nx/2-nx*fct);
  xbox[1]=(double)(nx/2+nx*fct);
  ybox[0]=add;
  ybox[1]=(double)(ny-add);

  uves_msg_debug("NX=%d NY=%d",nx,ny);
  uves_msg_debug("xbox=%f,%f ybox=%f,%f",xbox[0],xbox[1],ybox[0],ybox[1]);

  check(tmp_tbl1=uves_extract_table_rows(*ord_tbl,"X",CPL_GREATER_THAN,xbox[0]),
          "Error selecting X");

  check(tmp_tbl2=uves_extract_table_rows(tmp_tbl1,"X",CPL_LESS_THAN,xbox[1]),
          "Error selecting X");
  uves_free_table(&tmp_tbl1);

  check_nomsg(*ord_min=(int)cpl_table_get_column_min(tmp_tbl2,col_name));
  check_nomsg(*ord_max=(int)cpl_table_get_column_max(tmp_tbl2,col_name));
  uves_free_table(&tmp_tbl2);
  uves_msg_debug("ord_min=%d ord_max=%d",*ord_min,*ord_max);

  check(tmp_tbl1=uves_extract_table_rows(*ord_tbl,col_name,
					 CPL_NOT_GREATER_THAN,*ord_max),
	"Error selecting Order");
  uves_free_table(ord_tbl);
  check(*ord_tbl=uves_extract_table_rows(tmp_tbl1,col_name,
					 CPL_NOT_LESS_THAN,*ord_min),
	"Error selecting Order");


  cleanup:
  uves_free_table(&tmp_tbl1);
  uves_free_table(&tmp_tbl2);

  return 0;

}
/**@}*/


