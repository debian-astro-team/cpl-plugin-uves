/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:03 $
 * $Revision: 1.4 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.2  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.1  2007/02/09 13:38:13  jmlarsen
 * Enable calling from uves_cal_mkmaster
 *
 * Revision 1.2  2006/11/13 12:47:42  jmlarsen
 * Don't subtract background for FLAMES reduction
 *
 * Revision 1.1  2006/09/27 13:22:43  jmlarsen
 * Factored out flat reduction
 *
 */
#ifndef UVES_MBIAS_IMPL_H
#define UVES_MBIAS_IMPL_H

#include <cpl.h>

void
uves_mbias_exe_body(cpl_frameset *frames, 
            const cpl_parameterlist *parameters,
            const char *starttime,
            const char *recipe_id);


#endif  /* UVES_MBIAS_IMPL_H */
