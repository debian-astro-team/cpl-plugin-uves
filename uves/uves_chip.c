/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */

/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:02 $
 * $Revision: 1.8 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.6  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.5  2007/01/10 12:35:33  jmlarsen
 * Added uves_chip_tochar
 *
 * Revision 1.4  2006/08/17 13:56:52  jmlarsen
 * Reduced max line length
 *
 * Revision 1.3  2005/12/19 16:17:56  jmlarsen
 * Replaced bool -> int
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*----------------------------------------------------------------------------*/
/**
 * @defgroup uves_chip    CCD Chip
 *
 * The CCD chip abstract data type
 */
/*----------------------------------------------------------------------------*/
/**@{*/


/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#include <uves_chip.h>
#include <cpl.h>

/*-----------------------------------------------------------------------------
                            Functions prototypes
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                            Implementation
 -----------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------*/
/**
   @brief    Get first chip for blue or red arm
   @param    blue          Blue (if true) or red (if false) arm
   @return   The first (using some arbitrary but self-consistent definition of
             chip ordering)
             chip of the specified arm.

   This function is used to loop through all (in the case of UVES, 1 or 2) chips
   of an arm.

   @code
     for (chip = uves_chip_get_first(blue); 
          chip != UVES_CHIP_INVALID; 
      chip = uves_chip_get_next(chip))
     {...}
   @endcode

*/
/*----------------------------------------------------------------------------*/
enum uves_chip
uves_chip_get_first(bool blue)
{
    return (blue) ? UVES_CHIP_BLUE : UVES_CHIP_REDL;
}

/*----------------------------------------------------------------------------*/
/**
   @brief    Get next chip
   @param    chip          The current chip
   @return   The chip following the specified @em chip

   See @c uves_chip_get_first()
*/
/*----------------------------------------------------------------------------*/

enum uves_chip
uves_chip_get_next(enum uves_chip chip)
{
    return
    (chip == UVES_CHIP_REDL) ? UVES_CHIP_REDU :
    (chip == UVES_CHIP_BLUE) ? UVES_CHIP_INVALID :
    (chip == UVES_CHIP_REDU) ? UVES_CHIP_INVALID :
    UVES_CHIP_INVALID;
}

/*----------------------------------------------------------------------------*/
/**
   @brief    Convert to integer
   @param    chip          The CCD chip
   @return   The specified @em chip converted to integer (1 if REDU, otherwise 0)
*/
/*----------------------------------------------------------------------------*/
int uves_chip_get_index(enum uves_chip chip)
{
    /* 0 = REDL/BLUE, 1 = REDU */
    return (chip == UVES_CHIP_REDU) ? 1 : 0;
}

/*----------------------------------------------------------------------------*/
/**
   @brief    Convert to string
   @param    chip          The CCD chip
   @return   A lower case textual representation of the specified @em chip
*/
/*----------------------------------------------------------------------------*/

const char *
uves_chip_tostring_lower(enum uves_chip chip)
{
    return
    (chip == UVES_CHIP_BLUE) ? "blue" : 
    (chip == UVES_CHIP_REDU) ? "redu" : 
    (chip == UVES_CHIP_REDL) ? "redl" : "?chip?";
}

/*----------------------------------------------------------------------------*/
/**
   @brief    Convert to string
   @param    chip          The CCD chip
   @return   An upper case textual representation of the specified @em chip
*/
/*----------------------------------------------------------------------------*/

const char *
uves_chip_tostring_upper(enum uves_chip chip)
{
    return
    (chip == UVES_CHIP_BLUE) ? "BLUE" : 
    (chip == UVES_CHIP_REDU) ? "REDU" : 
    (chip == UVES_CHIP_REDL) ? "REDL" : "?CHIP?";
}


/*----------------------------------------------------------------------------*/
/**
   @brief    Get letters used in filenames
   @param    chip          The CCD chip
   @return   letters
*/
/*----------------------------------------------------------------------------*/
const char *uves_chip_get_det(enum uves_chip chip)
{
    return
    (chip == UVES_CHIP_BLUE) ? "be" : 
    (chip == UVES_CHIP_REDU) ? "rm" : 
    (chip == UVES_CHIP_REDL) ? "re" : "??";
}

/*----------------------------------------------------------------------------*/
/**
   @brief    Get letters used in filenames
   @param    chip          The CCD chip
   @return   character representation of chip
*/
/*----------------------------------------------------------------------------*/
char uves_chip_tochar(enum uves_chip chip)
{
    return
    (chip == UVES_CHIP_BLUE) ? 'b' : 
    (chip == UVES_CHIP_REDU) ? 'u' : 
    (chip == UVES_CHIP_REDL) ? 'l' : '?';
}

/**@}*/
