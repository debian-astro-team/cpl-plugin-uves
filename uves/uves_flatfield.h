/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2011-12-08 14:00:55 $
 * $Revision: 1.9 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.8  2010/09/24 09:32:03  amodigli
 * put back QFITS dependency to fix problem spot by NRI on FIBER mode (with MIDAS calibs) data
 *
 * Revision 1.6  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.5  2007/05/22 11:36:37  jmlarsen
 * Removed MIDAS flag for good
 *
 * Revision 1.4  2006/08/17 13:56:53  jmlarsen
 * Reduced max line length
 *
 * Revision 1.3  2005/12/19 16:17:56  jmlarsen
 * Replaced bool -> int
 *
 * Revision 1.2  2005/12/16 14:22:23  jmlarsen
 * Removed midas test data; Added sof files
 *
 * Revision 1.1  2005/11/11 13:18:54  jmlarsen
 * Reorganized code, renamed source files
 *
 */
#ifndef UVES_FLATFIELD_H
#define UVES_FLATFIELD_H
#include <uves_cpl_size.h>
#include <cpl.h>
#include <stdbool.h>

typedef enum {FF_PIXEL, FF_EXTRACT, FF_NO} flatfielding_method;

flatfielding_method
uves_get_flatfield_method(const cpl_parameterlist *parameters, 
              const char *context, const char *subcontext);

cpl_error_code
uves_flatfielding(cpl_image *image, cpl_image *noise, 
          const cpl_image *master_flat, const cpl_image *mflat_noise);

#endif
