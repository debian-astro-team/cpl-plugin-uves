/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston MA 02110-1301 USA          *
 */
 
/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:07 $
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif


/*----------------------------------------------------------------------------*/
/**
 * @defgroup uves_redchain  Recipe: Reduction Chain
 *
 * This recipe executes a reduction chain. See man-page for details.
 */
/*----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#include <uves.h>

#include <uves_parameters.h>
#include <uves_utils.h>
#include <uves_utils_wrappers.h>
#include <uves_dfs.h>
#include <uves_recipe.h>
#include <uves_error.h>
#include <uves_msg.h>

/* Library */
#include <cpl.h>
#include <string.h>
/*-----------------------------------------------------------------------------
                            Local constants
 -----------------------------------------------------------------------------*/
static const bool flames = false;  /* This recipe is only for UVES */

/*-----------------------------------------------------------------------------
                            Functions prototypes
 -----------------------------------------------------------------------------*/
static bool frame_is_needed(bool blue, const cpl_frame *f);
static cpl_error_code execute_recipe(const char *recipe_id, 
                                     cpl_frameset *frames, const cpl_parameterlist *parameters,
                                     const char *products[], int n_products, bool reclassify);
static bool is_missing(const cpl_frameset *frames, const char *frame1, const char *frame2);
static void remove_input_frame(cpl_frameset *frames, const char *tag);

static int uves_redchain_define_parameters(cpl_parameterlist *parameters);

/*-----------------------------------------------------------------------------
                            Recipe standard code
 -----------------------------------------------------------------------------*/
#define cpl_plugin_get_info uves_redchain_get_info
UVES_RECIPE_DEFINE(
    UVES_REDCHAIN_ID, UVES_REDCHAIN_DOM, uves_redchain_define_parameters,
    "Jonas M. Larsen", "cpl@eso.org",
    "Runs the full UVES reduction chain",
    "This recipe does a complete science reduction. It runs all necessary\n"
    "calibration recipes depending on the availability of raw/processed\n"
    "calibration frames.\n"
    "Input frames are all UVES raw and reference frames:\n"
    "formatchecks, ARC_LAMP_FORM_xxxx, xxxx=BLUE or RED,\n"
    "order definition frames, ORDER_FLAT_xxx,\n"
    "biases, BIAS_xxx,\n"
    "darks, DARK_xxx,\n"
    "flats, FLAT_xxx,\n"
    "arc lamps, ARC_LAMP_xxx,\n"
    "standard stars, STANDARD_xxx\n"
    "a wavelength catalogue table,LINE_REFER_TABLE, \n"
    "and optionally a wavelength table of bright lines,LINE_INTMON_TABLE, \n"
    "used only for computing Quality Control parameters.\n"
    "a reference standard star flux table, FLUX_STD_TABLE, \n"
    "a table describing the atmospheric extintion,EXTCOEFF_TABLE.\n"
    "optionally, science frames, SCIENCE_xxx, or UVES_SCI_POINT_xxx, \n"
    "or UVES_SCI_EXTND_xxx, or UVES_SCI_SLICER_xxx.\n"
    "For further details on the data reduction and the input frame types\n"
    "refer to the man page of the individual recipes.\n");

/**@{*/

/*-----------------------------------------------------------------------------
                              Functions code
 -----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/**
  @brief    Setup the recipe options    
  @param    parameters        the parameterlist to fill
  @return   0 if everything is ok
 */
/*----------------------------------------------------------------------------*/
static int
uves_redchain_define_parameters(cpl_parameterlist *parameters)
{
    const char *recipe_id = make_str(UVES_REDCHAIN_ID);
    const char *subcontext = NULL;
   
    uves_par_new_value("scired",
                       CPL_TYPE_BOOL,
                       "Whether or not to do science reduction. "
                       "If false, only master calibration frames "
                       "are created. If false, either zero or all "
                       "necessary calibration frames must be provided "
                       "for each arm",
                       true);

    /*****************
     *    General    *
     *****************/
    if (uves_define_global_parameters(parameters) != CPL_ERROR_NONE)
        {
            return -1;
        }

    /******************
     *   Master bias  *
     ******************/
    if (uves_propagate_parameters(
            make_str(UVES_MBIAS_ID), parameters, make_str(UVES_REDCHAIN_ID), NULL) != 0)
        {
            return -1;
        }


    /******************
     *   Master dark  *
     ******************/
    if (uves_propagate_parameters(
            make_str(UVES_MDARK_ID), parameters, make_str(UVES_REDCHAIN_ID), NULL) != 0)
        {
            return -1;
        }

    /******************
     * Physical model *
     ******************/
    if (uves_propagate_parameters(
            make_str(UVES_PHYSMOD_ID), parameters, make_str(UVES_REDCHAIN_ID), NULL) != 0)
        {
            return -1;
        }

    /******************
     * Order position *
     ******************/
    if (uves_propagate_parameters(
            make_str(UVES_ORDERPOS_ID), parameters, make_str(UVES_REDCHAIN_ID), NULL) != 0)
        {
            return -1;
        }

    /******************
     *   Master flat  *
     ******************/
    if (uves_propagate_parameters(
            make_str(UVES_MFLAT_ID), parameters, make_str(UVES_REDCHAIN_ID), NULL) != 0)
        {
            return -1;
        }

    
    /******************
     *   Wave.cal.    *
     ******************/
    if (uves_propagate_parameters(
            make_str(UVES_WAVECAL_ID), parameters, make_str(UVES_REDCHAIN_ID), NULL) != 0)
        {
            return -1;
        }

    /******************
     *    Response    *
     ******************/
/*
    if (uves_propagate_parameters(
             make_str(UVES_RESPONSE_ID), parameters, make_str(UVES_REDCHAIN_ID), NULL) != 0)
    {
             return -1;
    }
*/

    uves_define_background_for_response_chain_parameters(parameters);
    uves_define_extract_for_response_chain_parameters(parameters);
    uves_define_reduce_for_response_chain_parameters(parameters);
    uves_define_rebin_for_response_chain_parameters(parameters);
    uves_define_efficiency_for_response_chain_parameters(parameters);
  


    /******************
     *    Scired      *
     ******************/
    if (uves_propagate_parameters(
            make_str(UVES_SCIRED_ID), parameters, make_str(UVES_REDCHAIN_ID), NULL) != 0)
        {
            return -1;
        }

    return (cpl_error_get_code() != CPL_ERROR_NONE);
}

/*----------------------------------------------------------------------------*/
/**
  @brief    Get the command line options and execute the data reduction
  @param    parameters  the parameters list
  @param    frames      the frames list
  @return   CPL_ERROR_NONE if everything is ok


 */
/*----------------------------------------------------------------------------*/
static void
UVES_CONCAT2X(UVES_REDCHAIN_ID,exe)(cpl_frameset *frames,
                      const cpl_parameterlist *parameters, 
                      const char *starttime)
{
    cpl_frameset *blue_frames = NULL;
    cpl_frameset *red_frames = NULL;
    cpl_frameset *common_frames = NULL;
 

    bool blue;
    bool do_science;

    bool run_mbias[2];     /* index 0 (==false): red  arm */
    bool run_mdark[2];     /* index 1 (==true ): blue arm  */
    bool run_mflat[2];
    bool run_physmod[2];
    bool run_orderpos[2];
    bool run_wavecal[2];
    bool run_response[2];
    bool run_scired[2];
    bool nraw_arm[2];      /* Do we have frames used exclusively
                              for this arm? */
    const char* PROCESS_CHIP=NULL;

    /* Exceptionally, this parameter is not used because this
       recipe does not create any products on its own. Suppress
       warning about unused variable */
    starttime = starttime;

    check( uves_get_parameter(parameters, NULL, make_str(UVES_REDCHAIN_ID), "scired", 
                              CPL_TYPE_BOOL, &do_science), "Could not read parameter");

    /* Check for at least one science frame */
    assure(!do_science ||
           cpl_frameset_find(frames, UVES_SCIENCE(true ))    != NULL ||
           cpl_frameset_find(frames, UVES_SCIENCE(false))    != NULL ||
           cpl_frameset_find(frames, UVES_SCI_EXTND(true ))  != NULL ||
           cpl_frameset_find(frames, UVES_SCI_EXTND(false))  != NULL ||
           cpl_frameset_find(frames, UVES_SCI_POINT(true ))  != NULL ||
           cpl_frameset_find(frames, UVES_SCI_POINT(false))  != NULL ||
           cpl_frameset_find(frames, UVES_SCI_SLICER(true )) != NULL ||
           cpl_frameset_find(frames, UVES_SCI_SLICER(false)) != NULL,
           CPL_ERROR_DATA_NOT_FOUND, "No %s, %s, %s, %s, %s, %s, %s or %s in frame set", 
           UVES_SCIENCE(true),
           UVES_SCIENCE(false),
           UVES_SCI_EXTND(true),
           UVES_SCI_EXTND(false),
           UVES_SCI_POINT(true),
           UVES_SCI_POINT(false),
           UVES_SCI_SLICER(true),
           UVES_SCI_SLICER(false));
        
    blue_frames = cpl_frameset_new();
    red_frames = cpl_frameset_new();
    common_frames = cpl_frameset_new();

    check( uves_get_parameter(parameters, NULL, "uves", "process_chip", CPL_TYPE_STRING, &PROCESS_CHIP),
               "Could not read parameter");
    uves_string_toupper((char*)PROCESS_CHIP);

    /* Split in blue/red frames */
    {
        cpl_frame *f = NULL;
        int i=0;
        int nfrm=0;
        nfrm=cpl_frameset_get_size(frames);
        for (i=0;i<nfrm;i++)
            {

                f=cpl_frameset_get_frame(frames,i);
                if (frame_is_needed(true, f))    /* Used in blue arm? */
                    {
                        uves_msg_debug("Found blue frame: '%s'", cpl_frame_get_tag(f));
                        check( cpl_frameset_insert(blue_frames, cpl_frame_duplicate(f)),
                               "Error extracting frame '%s' from frame set",
                               cpl_frame_get_tag(f));
                    }
                if (frame_is_needed(false, f))  /* Used in red arm? */
                    {
                        uves_msg_debug("Found red frame: '%s'", cpl_frame_get_tag(f));
                        check( cpl_frameset_insert(red_frames, cpl_frame_duplicate(f)),
                               "Error extracting frame '%s' from frame set",
                               cpl_frame_get_tag(f));
                    }

                if (frame_is_needed(true, f) && 
                    frame_is_needed(false, f))  /* Used in both arms? */
                    {
                        uves_msg_debug("Found common frame: '%s'", cpl_frame_get_tag(f));
                        check( cpl_frameset_insert(common_frames, cpl_frame_duplicate(f)),
                               "Error extracting frame '%s' from frame set",
                               cpl_frame_get_tag(f));
                    }

            }
        /* Remove all frames from input frame set */
        cpl_frame *frm = NULL;
        for (i=0;i<nfrm;i++) {
            check_nomsg(frm=cpl_frameset_get_frame(frames,0));
            check_nomsg(cpl_frameset_erase_frame(frames, frm));
        }


    }

    /* Algorithm:
       (with purpose of failing early if we have to fail.)

       1) Find out which recipes to run
       2) Check for necessary input frames
       3) Execute
    */

    blue = true;
    do {
        enum uves_chip chip1 = (blue) ? UVES_CHIP_BLUE : UVES_CHIP_REDL;
        enum uves_chip chip2 = (blue) ? UVES_CHIP_BLUE : UVES_CHIP_REDU;
        
        cpl_frameset *fms = (blue) ? blue_frames : red_frames;

        nraw_arm[blue] = 
            cpl_frameset_get_size(fms) > 
            cpl_frameset_get_size(common_frames);
        
        uves_msg_debug("nraw_arm=%d (%s arm)", nraw_arm[blue], blue ? "blue" : "red");
        
        run_scired[blue] = do_science &&
            !(is_missing(fms, UVES_SCIENCE(blue), NULL) &&
              is_missing(fms, UVES_SCI_EXTND(blue), NULL) &&
              is_missing(fms, UVES_SCI_POINT(blue), NULL) &&
              is_missing(fms, UVES_SCI_SLICER(blue), NULL));
        
        /* If calibrations must be produced for this arm */
        if (run_scired[blue]
            ||
            (!do_science && nraw_arm[blue])) {
            
            /* Require master bias */
            run_mbias[blue] = is_missing(fms, 
                                         UVES_MASTER_BIAS(chip1),
                                         UVES_MASTER_BIAS(chip2)
                );
            
            /* Run master dark, only if raw frames are available */ 
            run_mdark[blue] = 
                is_missing(fms,
                           UVES_MASTER_DARK(chip1),
                           UVES_MASTER_DARK(chip2)) 
                &&
                is_missing(fms,
                           UVES_MASTER_PDARK(chip1),
                           UVES_MASTER_PDARK(chip2))
                &&(
                    !is_missing(fms, UVES_DARK(blue), NULL) ||
                    !is_missing(fms, UVES_PDARK(blue), NULL));
            
            /* Run orderpos if either order table is missing, 
               or raw frame available */
            run_orderpos[blue] = is_missing(fms,
                                            UVES_ORDER_TABLE(flames, chip1),
                                            UVES_ORDER_TABLE(flames, chip2)
                ) ||
                !is_missing(fms, UVES_ORDER_FLAT(flames, blue), NULL);
            
            /* Run master flat recipe if master flat frame is missing */
            run_mflat[blue] = 
                is_missing(fms,
                           UVES_MASTER_FLAT(chip1),
                           UVES_MASTER_FLAT(chip2))
                &&
                is_missing(fms,
                           UVES_MASTER_DFLAT(chip1),
                           UVES_MASTER_DFLAT(chip2))
                &&
                is_missing(fms,
                           UVES_MASTER_IFLAT(chip1),
                           UVES_MASTER_IFLAT(chip2))
                &&
                is_missing(fms,
                           UVES_MASTER_SCREEN_FLAT(chip1),
                           UVES_MASTER_SCREEN_FLAT(chip2))
                &&
                is_missing(fms,
                           UVES_REF_TFLAT(chip1),
                           UVES_REF_TFLAT(chip2));
            
            
            /* Line tables are used as both input and output
               for wavecal recipe. A provided line table is 
               interpreted as an input table if an arc lamp 
               frame is also available, otherwise as output.
               Line tables produce by the physmod recipe are
               input tables. The logic is
               
               if !linetable
                 physmod=yes
                 wavecal=yes
               if linetable
                 physmod=no
                 if !arclamp
                   wavecal=no   // line table is final
                 if arclamp
                   wavecal=yes  // line table is guess
            */
            
            /* Run physical model if there's no
               line table */
            run_physmod[blue] = is_missing(fms,
                                           UVES_LINE_TABLE(flames, chip1),
                                           UVES_LINE_TABLE(flames, chip2))
                &&
        is_missing(fms,
               UVES_GUESS_LINE_TABLE(flames, chip1),
               UVES_GUESS_LINE_TABLE(flames, chip2))
                &&
                (
                    is_missing(fms,
                               UVES_LINE_TABLE_MIDAS(chip1, 1), 
                               UVES_LINE_TABLE_MIDAS(chip2, 1)) ||
                    is_missing(fms,
                               UVES_LINE_TABLE_MIDAS(chip1, 2),
                               UVES_LINE_TABLE_MIDAS(chip2, 2)) ||
                    is_missing(fms,
                               UVES_LINE_TABLE_MIDAS(chip1, 3),
                               UVES_LINE_TABLE_MIDAS(chip2, 3))
                    );
                    
            /* Run wavecal if no line table,
               or if there's an arc lamp frame
            */
            run_wavecal[blue] = 
                run_physmod[blue]
                ||
        (
            is_missing(fms,
                   UVES_LINE_TABLE(flames, chip1),
                   UVES_LINE_TABLE(flames, chip2))
            &&
            (
            is_missing(fms,
                   UVES_LINE_TABLE_MIDAS(chip1, 1), 
                   UVES_LINE_TABLE_MIDAS(chip2, 1)) ||
            is_missing(fms,
                   UVES_LINE_TABLE_MIDAS(chip1, 2),
                   UVES_LINE_TABLE_MIDAS(chip2, 2)) ||
            is_missing(fms,
                   UVES_LINE_TABLE_MIDAS(chip1, 3),
                   UVES_LINE_TABLE_MIDAS(chip2, 3))
            )
            )
        ||
        (
            !is_missing(fms,
                                UVES_ARC_LAMP(flames, blue), NULL) ||
                    !is_missing(fms,
                                UVES_ECH_ARC_LAMP(blue), NULL)
                    );
        
            /* Run response only if there's a standard star.
               Otherwise no response correction is done */
            run_response[blue] = !is_missing(fms,
                                             UVES_STD_STAR(blue), NULL);
                    
                    
            uves_msg("Reduction strategy for %s arm:", (blue) ? "BLUE" : "RED");
            uves_msg("Run %-13s: %s", make_str(UVES_MBIAS_ID)   , (run_mbias[blue]   ) ? "Yes" : "No");
            uves_msg("Run %-13s: %s", make_str(UVES_MDARK_ID)   , (run_mdark[blue]   ) ? "Yes" : "No");
            uves_msg("Run %-13s: %s", make_str(UVES_PHYSMOD_ID) , (run_physmod[blue] ) ? "Yes" : "No");
            uves_msg("Run %-13s: %s", make_str(UVES_ORDERPOS_ID), (run_orderpos[blue]) ? "Yes" : "No");
            uves_msg("Run %-13s: %s", make_str(UVES_MFLAT_ID)   , (run_mflat[blue]   ) ? "Yes" : "No");
            uves_msg("Run %-13s: %s", make_str(UVES_WAVECAL_ID) , (run_wavecal[blue] ) ? "Yes" : "No");
            uves_msg("Run %-13s: %s", make_str(UVES_RESPONSE_ID), (run_response[blue]) ? "Yes" : "No");
            uves_msg("Run %-13s: %s", make_str(UVES_SCIRED_ID)  , (run_scired[blue]  ) ? "Yes" : "No");
    
        }  /* if reduce this arm */
        else {
            uves_msg("Skipping %s arm", 
                     (blue) ? "BLUE" : "RED");

            run_mbias[blue] = false;
            run_mdark[blue] = false;
            run_mflat[blue] = false;
            run_physmod[blue] = false;
            run_orderpos[blue] = false;
            run_wavecal[blue] = false;
            run_response[blue] = false;
        }
            
        blue = !blue;
    }
    while (!blue);

    /* As a service to the user, assure that required
       raw frames and catalogue calibration frames
       exist *before* doing the reduction */
    
    blue = true;
    do
        {
            cpl_frameset *fms = (blue) ? blue_frames : red_frames;

            assure( !run_mbias[blue] || !is_missing(fms, UVES_BIAS(blue), NULL), 
                    CPL_ERROR_DATA_NOT_FOUND,
                    "One or more '%s' frames needed for recipe '%s'",
                    UVES_BIAS(blue), make_str(UVES_MBIAS_ID));
            
            assure( !run_mdark[blue] || 
                    !is_missing(fms, UVES_DARK(blue), NULL) ||
                    !is_missing(fms, UVES_PDARK(blue), NULL),
                    CPL_ERROR_DATA_NOT_FOUND, 
                    "One or more '%s' or '%s' frames needed for recipe '%s'",
                    UVES_DARK(blue), UVES_PDARK(blue), make_str(UVES_MDARK_ID));
            
            assure( !run_physmod[blue] || !is_missing(fms, UVES_FORMATCHECK(flames, blue), NULL),
                    CPL_ERROR_DATA_NOT_FOUND, "Frame '%s' needed for recipe '%s'",
                    UVES_FORMATCHECK(flames, blue), make_str(UVES_PHYSMOD_ID));
            
            assure( !run_orderpos[blue] || !is_missing(fms, UVES_ORDER_FLAT(flames, blue), NULL),
                    CPL_ERROR_DATA_NOT_FOUND, "Frame '%s' needed for recipe '%s'",
                    UVES_ORDER_FLAT(flames, blue), make_str(UVES_ORDERPOS_ID));
            
            assure( !run_mflat[blue] || 
                    !is_missing(fms, UVES_FLAT(blue), NULL) ||
                    !is_missing(fms, UVES_IFLAT(blue), NULL) ||
                    !is_missing(fms, UVES_SCREEN_FLAT(blue), NULL) ||
                    !is_missing(fms, UVES_DFLAT(blue), NULL) ||
                    !is_missing(fms, UVES_TFLAT(blue), NULL),
                    CPL_ERROR_DATA_NOT_FOUND, 
                    "One or more '%s', '%s', '%s', '%s' or '%s' frames needed for recipe '%s'",
                    UVES_FLAT(blue), 
                    UVES_IFLAT(blue), 
                    UVES_SCREEN_FLAT(blue), 
                    UVES_DFLAT(blue), 
                    UVES_TFLAT(blue), 
                    make_str(UVES_MFLAT_ID));
            
            assure( !run_wavecal[blue] || (
                        !is_missing(fms, UVES_ARC_LAMP(flames, blue), NULL) ||
                        !is_missing(fms, UVES_ECH_ARC_LAMP(blue), NULL)),
                    CPL_ERROR_DATA_NOT_FOUND, "Frame '%s' or '%s' needed for recipe '%s'",
                    UVES_ARC_LAMP(flames, blue), UVES_ECH_ARC_LAMP(blue), make_str(UVES_WAVECAL_ID));
            assure( !run_wavecal[blue] || !is_missing(fms, UVES_LINE_REFER_TABLE, NULL),
                    CPL_ERROR_DATA_NOT_FOUND, "Frame '%s' needed for recipe '%s'",
                    UVES_LINE_REFER_TABLE, make_str(UVES_WAVECAL_ID));
            
            assure( !run_response[blue] || !is_missing(fms, UVES_STD_STAR(blue), NULL), 
                    CPL_ERROR_DATA_NOT_FOUND, "Frame '%s' needed for recipe '%s'",
                    UVES_STD_STAR(blue), make_str(UVES_RESPONSE_ID));
            assure( !run_response[blue] || !is_missing(fms, UVES_FLUX_STD_TABLE, NULL), 
                    CPL_ERROR_DATA_NOT_FOUND, "Frame '%s' needed for recipe '%s'",
                    UVES_FLUX_STD_TABLE, make_str(UVES_RESPONSE_ID));
            assure( !run_response[blue] || !is_missing(fms, UVES_EXTCOEFF_TABLE, NULL), 
                    CPL_ERROR_DATA_NOT_FOUND, "Frame '%s' needed for recipe '%s'",
                    UVES_EXTCOEFF_TABLE, make_str(UVES_RESPONSE_ID));
            
            blue = !blue;
        }
    while (!blue);
   
    /* We now know which recipes to run and
     * that required input frames exist. Execute
     * chain; re-classify PRODUCT->CALIB under way
     */
    
    blue = true;
    do
        {
            enum uves_chip chip1 = (blue) ? UVES_CHIP_BLUE : UVES_CHIP_REDL;
            enum uves_chip chip2 = (blue) ? UVES_CHIP_BLUE : UVES_CHIP_REDU;

            cpl_frameset *fms = (blue) ? blue_frames : red_frames;

            if (run_mbias[blue])
                {
                    const char *products[2];
                    
                    int nprod = sizeof(products) / sizeof (char *);
                    
                    products[0] = UVES_MASTER_BIAS(chip1);
                    products[1] = UVES_MASTER_BIAS(chip2);
                    
                    if (blue) nprod /= 2;
                    
                    check( execute_recipe(make_str(UVES_MBIAS_ID), fms, parameters, products, nprod, true),
                           "Recipe execution failed");
                }
            
            check( remove_input_frame(fms, UVES_BIAS(blue)), "Error removing input frames");
            
            if (run_mdark[blue])
                {
                    const char *products[4];
                    
                    int nprod = sizeof(products) / sizeof (char *);
                    
                    products[0] = UVES_MASTER_DARK(chip1);
                    products[1] = UVES_MASTER_PDARK(chip1);
                    products[2] = UVES_MASTER_DARK(chip2);
                    products[3] = UVES_MASTER_PDARK(chip2);
                    
                    if (blue) nprod /= 2;

                    check( execute_recipe(
                               make_str(UVES_MDARK_ID), fms, parameters, products, nprod, true), 
                           "Recipe execution failed");
                }
            
            check( remove_input_frame(fms, UVES_DARK(blue)), "Error removing input frames");
            check( remove_input_frame(fms, UVES_PDARK(blue)), "Error removing input frames");
            
            if (run_physmod[blue])
                {
                    const char *products[4];
                    int nprod = sizeof(products) / sizeof (char *);
                    
                    products[0] = UVES_GUESS_LINE_TABLE (flames, chip1);
                    products[1] = UVES_GUESS_ORDER_TABLE(flames, chip1);
                    products[2] = UVES_GUESS_LINE_TABLE (flames, chip2);
                    products[3] = UVES_GUESS_ORDER_TABLE(flames, chip2);
                    
                    if (blue) nprod /= 2;
                    
                    check( execute_recipe(
                               make_str(UVES_PHYSMOD_ID), 
                               fms, parameters, products, nprod, true), 
                           "Recipe execution failed");
                }
            
            check( remove_input_frame(fms, UVES_FORMATCHECK(flames, blue)), 
                   "Error removing input frames");
            
            if (run_orderpos[blue])
                {
                    const char *products[2];
                    int nprod = sizeof(products) / sizeof (char *);
                    
                    products[0] = UVES_ORDER_TABLE(flames, chip1);
                    products[1] = UVES_ORDER_TABLE(flames, chip2);
                    
                    if (blue) nprod /= 2;
                            
                    check( execute_recipe(
                               make_str(UVES_ORDERPOS_ID), 
                               fms, parameters, products, nprod, true), 
                           "Recipe execution failed");
                }

            check( remove_input_frame(fms, UVES_ORDER_FLAT(flames, blue)),
                   "Error removing input frames");
                    
            if (run_mflat[blue])
                {
                    const char *products[10];
                            
                    int nprod = sizeof(products) / sizeof (char *);

                    products[0] = UVES_MASTER_FLAT(chip1);
                    products[1] = UVES_MASTER_DFLAT(chip1);
                    products[2] = UVES_MASTER_IFLAT(chip1);
                    products[3] = UVES_MASTER_TFLAT(chip1);
                    products[4] = UVES_MASTER_SCREEN_FLAT(chip1);
                    products[5] = UVES_MASTER_FLAT(chip2);
                    products[6] = UVES_MASTER_DFLAT(chip2);
                    products[7] = UVES_MASTER_IFLAT(chip2);
                    products[8] = UVES_MASTER_TFLAT(chip2);
                    products[9] = UVES_MASTER_SCREEN_FLAT(chip2);
                            
                    if (blue) nprod /= 2;
                            
                    check( execute_recipe(make_str(UVES_MFLAT_ID), 
                                          fms, parameters, products, nprod, true), 
                           "Recipe execution failed");
                }
                    
            check( remove_input_frame(fms, UVES_FLAT(blue)), "Error removing input frames");
            check( remove_input_frame(fms, UVES_IFLAT(blue)), "Error removing input frames");
            check( remove_input_frame(fms, UVES_DFLAT(blue)), "Error removing input frames");
            check( remove_input_frame(fms, UVES_TFLAT(blue)), "Error removing input frames");
            check( remove_input_frame(fms, UVES_SCREEN_FLAT(blue)), "Error removing input frames");
                    
            if (run_wavecal[blue])
                {
                    const char *products[2];
                            
                    int nprod = sizeof(products) / sizeof (char *);

                    products[0] = UVES_LINE_TABLE(flames, chip1);
                    products[1] = UVES_LINE_TABLE(flames, chip2);
                            
                    if (blue) nprod /= 2;
                    
                    check( execute_recipe(make_str(UVES_WAVECAL_ID), 
                                          fms, parameters, products, nprod, true), 
                           "Recipe execution failed");
                }

            check( remove_input_frame(fms, UVES_ARC_LAMP(flames, blue)), 
           "Error removing input frames");
            check( remove_input_frame(fms, UVES_ECH_ARC_LAMP(blue)), 
           "Error removing input frames");
            check( remove_input_frame(fms, UVES_LINE_REFER_TABLE), 
           "Error removing input frames");
                    
            if (run_response[blue])
                {
                    const char *products[2];
                            
                    int nprod = sizeof(products) / sizeof (char *);

                    products[0] = UVES_INSTR_RESPONSE(chip1);
                    products[1] = UVES_INSTR_RESPONSE(chip2);
                            
                    if (blue) nprod /= 2;
                            
                     check( execute_recipe(make_str(UVES_RESPONSE_ID), 
                                          fms, parameters, products, nprod, true),
                           "Recipe execution failed");
                }

            check( remove_input_frame(fms, UVES_STD_STAR(blue)), "Error removing input frames");
            check( remove_input_frame(fms, UVES_FLUX_STD_TABLE), "Error removing input frames");

            if (run_scired[blue])
                {
                    const char *products[2];
                            
                    int nprod = sizeof(products) / sizeof (char *);

                    products[0] = blue ? "RED_SCIENCE_BLUE" : "RED_SCIENCE_REDL";
                    products[1] = blue ? "RED_SCIENCE_BLUE" : "RED_SCIENCE_REDU";
                            
                    if (blue) nprod /= 2;

                    check( execute_recipe(make_str(UVES_SCIRED_ID), 
                                          fms, parameters, products, nprod, false),
                           "Recipe execution failed");
                }
                    
            check( remove_input_frame(fms, UVES_SCIENCE(blue))   , "Error removing input frames");
            check( remove_input_frame(fms, UVES_SCI_EXTND(blue)) , "Error removing input frames");
            check( remove_input_frame(fms, UVES_SCI_POINT(blue)) , "Error removing input frames");
            check( remove_input_frame(fms, UVES_SCI_SLICER(blue)), "Error removing input frames");
                    
            /* Insert all product frames into recipe frame set */
            {
                cpl_frame *f = NULL;
                int i=0;
                int nfrm=0;
                nfrm=cpl_frameset_get_size(fms);
                for (i=0;i<nfrm;i++)
                    {
                     f=cpl_frameset_get_frame(fms,i);
                        if (cpl_frame_get_group(f) == CPL_FRAME_GROUP_PRODUCT)
                            {
                                check( cpl_frameset_insert(frames, cpl_frame_duplicate(f)),
                                       "Error inserting product '%s' into frame set",
                                       cpl_frame_get_tag(f));
                            }
                    }
            }
            
            blue = !blue;
        }
    while(!blue);     /* For each arm */
    
  cleanup:
    uves_free_frameset(&blue_frames);
    uves_free_frameset(&red_frames);
    uves_free_frameset(&common_frames);

    return;
}

/* Returns true, iff frame is used for blue/red arm
   (note that some frames like UVES_FLUX_STD_TABLE are
   used for both arms) */
static bool
frame_is_needed(bool blue, const cpl_frame *f)
{
    const char *tag = cpl_frame_get_tag(f);
    
    bool result = (strcmp(tag, UVES_ORDER_FLAT (flames, blue)) == 0 ||
                   strcmp(tag, UVES_BIAS       (blue)) == 0 ||
                   strcmp(tag, UVES_DARK       (blue)) == 0 ||
                   strcmp(tag, UVES_PDARK      (blue)) == 0 ||
                   strcmp(tag, UVES_FLAT       (blue)) == 0 ||
                   strcmp(tag, UVES_IFLAT      (blue)) == 0 ||
                   strcmp(tag, UVES_DFLAT      (blue)) == 0 ||
                   strcmp(tag, UVES_TFLAT      (blue)) == 0 ||
                   strcmp(tag, UVES_SCREEN_FLAT(blue)) == 0 ||
                   strcmp(tag, UVES_STD_STAR   (blue)) == 0 ||
                   strcmp(tag, UVES_FORMATCHECK(flames, blue)) == 0 ||
                   strcmp(tag, UVES_STD_STAR   (blue)) == 0 ||
                   strcmp(tag, UVES_SCIENCE    (blue)) == 0 ||
                   strcmp(tag, UVES_SCI_EXTND  (blue)) == 0 ||
                   strcmp(tag, UVES_SCI_POINT  (blue)) == 0 ||
                   strcmp(tag, UVES_SCI_SLICER (blue)) == 0 ||
                   strcmp(tag, UVES_ARC_LAMP   (flames, blue)) == 0 ||
                   strcmp(tag, UVES_ECH_ARC_LAMP(blue)) == 0);
    
    enum uves_chip chip;
    
    /* Loop through all blue or red chips  (1 or 2) */
    for (chip = uves_chip_get_first(blue); 
         chip != UVES_CHIP_INVALID; 
         chip = uves_chip_get_next(chip))
        {
            result = result || (strcmp(tag, UVES_DRS_SETUP(flames, chip)) == 0 ||
                                strcmp(tag, UVES_ORDER_TABLE(flames, chip)) == 0 ||
                                strcmp(tag, UVES_GUESS_ORDER_TABLE(flames, chip)) == 0 ||
                                strcmp(tag, UVES_MASTER_BIAS(chip)) == 0 ||
                                strcmp(tag, UVES_MASTER_DARK(chip)) == 0 ||
                                strcmp(tag, UVES_MASTER_PDARK(chip)) == 0 ||
                                strcmp(tag, UVES_MASTER_FLAT(chip)) == 0 ||
                                strcmp(tag, UVES_MASTER_DFLAT(chip)) == 0 ||
                                strcmp(tag, UVES_MASTER_IFLAT(chip)) == 0 ||
                                strcmp(tag, UVES_MASTER_TFLAT(chip)) == 0 ||
                                strcmp(tag, UVES_REF_TFLAT(chip)) == 0 ||
                                strcmp(tag, UVES_MASTER_SCREEN_FLAT(chip)) == 0 ||
                                strcmp(tag, UVES_LINE_TABLE (flames, chip)) == 0 ||
                                strcmp(tag, UVES_GUESS_LINE_TABLE(flames, chip)) == 0 ||
                                strcmp(tag, UVES_LINE_TABLE_MIDAS(chip, 1)) == 0 ||
                                strcmp(tag, UVES_LINE_TABLE_MIDAS(chip, 2)) == 0 ||
                                strcmp(tag, UVES_LINE_TABLE_MIDAS(chip, 3)) == 0 ||
                                strcmp(tag, UVES_LINE_REFER_TABLE ) == 0 ||
                                strcmp(tag, UVES_FLUX_STD_TABLE   ) == 0 ||
                                strcmp(tag, UVES_EXTCOEFF_TABLE   ) == 0);
        }
    return result;
}

/* Execute a recipe and re-classify its products as calibration frames */
static cpl_error_code
execute_recipe(const char *recipe_id, 
               cpl_frameset *frames, const cpl_parameterlist *parameters,
               const char *products[],
               int n_products,
               bool reclassify)              /* Re-classify products? */
{
  int i;
  cpl_frame *f = NULL;

  /* Remove (from frame set) any product
     frames already present */
  for (i = 0; i < n_products; i++)
      {
          if ((f = cpl_frameset_find(frames, products[i])) != NULL)
              {
                  if (cpl_frame_get_group(f) == CPL_FRAME_GROUP_PRODUCT)
                      {
                          cpl_msg_warning(__func__, "Ignoring %s frame in '%s'. "
                                          "A new %s frame will now be calculated",
                                          products[i], cpl_frame_get_filename(f),
                                          products[i]);
                          
                          cpl_frameset_erase_frame(frames, f);
                      }
              }
      }
  
  /* Execute */
  check( uves_invoke_recipe(recipe_id, parameters, frames, make_str(UVES_REDCHAIN_ID), NULL),
         "Recipe '%s' failed", recipe_id);
            
  check(cpl_dfs_update_product_header(frames),"Error updating pipe products' header");
  if (reclassify)
      {
          /* Now re-classify PRODUCT->CALIB to be used in the remaining
             reduction chain. Before doing that, we have to remove any
             calibration frame with same tag as a product (such as line tables),
             in order not to confuse the re-classified products with the
             previous calibration frames */
          
          for (i = 0; i < n_products; i++)
              {
                  if ((f = cpl_frameset_find(frames, products[i])) != NULL &&
                      cpl_frame_get_group(f) != CPL_FRAME_GROUP_PRODUCT)
                      {
                          uves_msg("Removing %s frame in '%s' from frameset. "
                                   "It is not tagged as a product",
                                   products[i], cpl_frame_get_filename(f));
                          
                          cpl_frameset_erase_frame(frames, f);
                      }
              }
          
          /*
           * Re-classify products
           */
          for (i = 0; i < n_products; i++)
              {
                  cpl_frame *found = NULL;
                  int j=0;
                  int nfrm=cpl_frameset_get_size(frames);
                  for (j=0;j<nfrm;j++)
                      {
                          f=cpl_frameset_get_frame(frames,j);
                          if (cpl_frame_get_group(f) == CPL_FRAME_GROUP_PRODUCT)
                              {
                                  if (strcmp(cpl_frame_get_tag(f), products[i]) == 0)
                                      {
                                          found = f;
                                      }
                              }
                      }
                  
                  if (found != NULL)
                      {
                          /* Re-classify the product as calibration frames */
                          uves_msg("Re-classifying %s product in '%s' PRODUCT->CALIB",
                                   products[i], cpl_frame_get_filename(found));
                          
                          cpl_frame_set_group(found, CPL_FRAME_GROUP_CALIB);
                      }
              }
          
          /*
           * Remove other products that 
           * are not used later  (e.g. BKG_FLAT_xxxx)
           */

          int k=0;
          int nfrm=cpl_frameset_get_size(frames);
          int nerased=0;
          for (k=0;k<nfrm-nerased;k++)
              {
                  f=cpl_frameset_get_frame(frames,k);

                  if (cpl_frame_get_group(f) == CPL_FRAME_GROUP_PRODUCT)
                      {
                          /* Remove this product */
                          uves_msg("Removing product %s in '%s' from frameset. "
                                   "Not needed later",
                                   cpl_frame_get_tag(f), cpl_frame_get_filename(f));
                          
                          cpl_frameset_erase_frame(frames, f);
                          nerased++;

                      }
              }  

      } /* if re-classify... */  

  cleanup:
  return cpl_error_get_code();
}
  

/* Retruns true if either frame 1 or frame 2 is not in the
   provided frame set 

   fixme: reverse the logic of this function, i.e. rename to 'contains'
*/
static bool
is_missing(const cpl_frameset *frames, const char *frame1, const char *frame2)
{
    bool result = false;
    if (cpl_frameset_find_const(frames, frame1) == NULL)
        {
            uves_msg("checking for %s... no", frame1);
            result = true;
        }
    else
        {
            uves_msg("checking for %s... yes", frame1);
        }
    
    if (frame2 != NULL && strcmp(frame1, frame2) != 0)
        {
            if (cpl_frameset_find_const(frames, frame2) == NULL)
                {
                    uves_msg("checking for %s... no", frame2);
                    result = true;
                }
            else
                {
                    uves_msg("checking for %s... yes", frame2);
                }
        }
    
  return result;
}


/* Remove input frames (e.g. bias frames) along the way */
static void
remove_input_frame(cpl_frameset *frames, const char *tag)
{
    int removed = cpl_frameset_erase(frames, tag);

    if (removed > 0)
        {
            uves_msg("Removing %d %s frame(s) from frame set", removed, tag);
        }

    return;
}
/**@}*/
