/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */

/*
 * $Author: amodigli $
 * $Date: 2012-03-02 16:38:14 $
 * $Revision: 1.19 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.18  2011/12/08 14:02:17  amodigli
 * Fix warnings with CPL6
 *
 * Revision 1.17  2010/09/24 09:32:07  amodigli
 * put back QFITS dependency to fix problem spot by NRI on FIBER mode (with MIDAS calibs) data
 *
 * Revision 1.15  2007/08/21 13:08:26  jmlarsen
 * Removed irplib_access module, largely deprecated by CPL-4
 *
 * Revision 1.14  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.13  2007/04/24 12:50:29  jmlarsen
 * Replaced cpl_propertylist -> uves_propertylist which is much faster
 *
 * Revision 1.12  2007/04/10 07:09:48  jmlarsen
 * Changed interface of uves_spline_hermite()
 *
 * Revision 1.11  2006/11/15 15:02:15  jmlarsen
 * Implemented const safe workarounds for CPL functions
 *
 * Revision 1.9  2006/11/15 14:04:08  jmlarsen
 * Removed non-const version of parameterlist_get_first/last/next which is
 * already in CPL, added const-safe wrapper, unwrapper and deallocator functions
 *
 * Revision 1.8  2006/11/07 14:04:45  jmlarsen
 * Fixed memory error by setting pointer to NULL after cpl_free
 *
 * Revision 1.7  2006/11/06 15:19:42  jmlarsen
 * Removed unused include directives
 *
 * Revision 1.6  2006/11/03 15:01:21  jmlarsen
 * Killed UVES 3d table module and use CPL 3d tables
 *
 * Revision 1.5  2006/08/17 13:56:53  jmlarsen
 * Reduced max line length
 *
 * Revision 1.4  2006/08/17 09:20:43  jmlarsen
 * Get reference object ID from flux table, not raw header
 *
 * Revision 1.3  2006/06/01 14:43:17  jmlarsen
 * Added missing documentation
 *
 * Revision 1.2  2006/03/24 13:56:13  jmlarsen
 * Changed ambigous text message
 *
 * Revision 1.1  2006/02/03 07:51:04  jmlarsen
 * Moved recipe implementations to ./uves directory
 *
 * Revision 1.2  2005/12/19 16:17:55  jmlarsen
 * Replaced bool -> int
 *
 * Revision 1.1  2005/11/11 13:18:54  jmlarsen
 * Reorganized code, renamed source files
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*----------------------------------------------------------------------------*/
/**
 * @addtogroup uves_response
 */
/*----------------------------------------------------------------------------*/
/**@{*/

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#include <uves_response_utils.h>

#include <uves_utils.h>
#include <uves_utils_wrappers.h>
#include <uves_pfits.h>
#include <uves_error.h>
#include <uves_msg.h>

#include <cpl.h>

#include <stdbool.h>

/*-----------------------------------------------------------------------------
                            Defines
 -----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
                            Functions prototypes
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                            Implementation
 -----------------------------------------------------------------------------*/
static inline cpl_size
get_before(const double wmin_spectrum, const double wmin_interval, const double step){
    const double num = (wmin_spectrum - wmin_interval) / step;
    if(num <= 0) return 0;
    return (cpl_size) ceil(num);
}

static inline cpl_size
get_after(const double wmax_spectrum, const double wmax_interval, const double step){
    const double num = (wmax_interval - wmax_spectrum) / step;
    if(num <= 0) return 0;
    return (cpl_size) ceil(num);
}
hdrl_spectrum1D * uves_extend_hdrl_spectrum(const double wmin, const double wmax,
        const hdrl_spectrum1D * s){

	cpl_ensure(wmin < wmax, CPL_ERROR_ILLEGAL_INPUT, NULL);
	cpl_ensure(s != NULL, CPL_ERROR_NULL_INPUT, NULL);

    const cpl_array * lambadas_s = hdrl_spectrum1D_get_wavelength(s).wavelength;
    const double s_wmin = cpl_array_get_min(lambadas_s);
    const double s_wmax = cpl_array_get_max(lambadas_s);

    const cpl_size sz_s = cpl_array_get_size(lambadas_s);
    const double step = (s_wmax - s_wmin)/sz_s;

    cpl_size n_before = get_before(s_wmin, wmin, step);
    cpl_size n_after = get_after(s_wmax, wmax, step);

    const cpl_size new_size = n_before + n_after + sz_s;
    cpl_array * new_lambdas = cpl_array_new(new_size,
            cpl_array_get_type(lambadas_s));
    hdrl_image * new_flux = hdrl_image_new(new_size, 1);

    double lambda = s_wmin;
    for(cpl_size i = n_before - 1; i >= 0; i--){
        lambda -= step;
        cpl_array_set(new_lambdas, i, lambda);
        hdrl_image_reject(new_flux, i + 1, 1);
    }

    lambda = s_wmax;
    for(cpl_size i = n_before + sz_s; i < new_size; i++){
            lambda += step;
            cpl_array_set(new_lambdas, i, lambda);
            hdrl_image_reject(new_flux, i + 1, 1);
    }

    for(cpl_size i = n_before; i < n_before + sz_s; i++){
        const cpl_size idx_ori = i - n_before;
        int rej = 0;
        const double lambda =
                hdrl_spectrum1D_get_wavelength_value(s, idx_ori, &rej);
        cpl_array_set(new_lambdas, i, lambda);

        if(rej) {
            hdrl_image_reject(new_flux, i + 1, 1);
            continue;
        }

        hdrl_value val = hdrl_spectrum1D_get_flux_value(s, idx_ori, NULL);
        hdrl_image_set_pixel(new_flux, i + 1, 1, val);
    }

    const hdrl_spectrum1D_wave_scale scale = hdrl_spectrum1D_get_scale(s);

    hdrl_spectrum1D * to_ret =
            hdrl_spectrum1D_create(hdrl_image_get_image(new_flux),
                    hdrl_image_get_error(new_flux),
                    new_lambdas,
                    scale);


    cpl_array_delete(new_lambdas);
    hdrl_image_delete(new_flux);
    uves_print_rec_status(0);
    return to_ret;
}


/*----------------------------------------------------------------------------*/
/**
   @brief    Calculate response
   @param    spectrum           The 1d (merged) or 2d (non-merged) spectrum
   @param    spectrum_header    Header describing the geometry of the input spectrum
   @param    flux_table         Table of standard star fluxes
   @param    raw_header         Header of the raw frame
   @param    PACCURACY          Pointing accuracy (in arcseconds) used
                                to find a unique matching object in the @em flux_table
   @param    inverse            If false, flux/std_flux is calculated, if true
                                std_flux/flux is computed.
   @param    ref_obj_id         (output) reference object ID. Must be
                                deallocated by the caller
   @return   Response curve

   Undefined values (division by zero) are set to zero.
*/
/*----------------------------------------------------------------------------*/
cpl_image *
uves_calculate_response(const cpl_image *spectrum, const uves_propertylist *spectrum_header,
            const cpl_table *flux_table,
            const uves_propertylist *raw_header,
            double PACCURACY,
            bool inverse,
            char **ref_obj_id)
{
    cpl_image *response = NULL;             /* Result */
    cpl_table *catalogue_flux = NULL;
    int nx, norders;
    
    nx      = cpl_image_get_size_x(spectrum);
    norders = cpl_image_get_size_y(spectrum);
    
    response = cpl_image_new(nx, norders, CPL_TYPE_DOUBLE);

    check( catalogue_flux = uves_align(raw_header, flux_table, PACCURACY, ref_obj_id),
       "Cannot read catalogue flux");
    /*AMO: adjusted for new STD catalog with wavelength units of nm */
    int status;
    double wave = cpl_table_get(catalogue_flux,"LAMBDA",0,&status);
    if (wave < 1000.) {
    	/* if table is in nm units put back to angstrom as UVES has extracted spectrum data in [angstrom]*/
    	cpl_table_multiply_scalar(catalogue_flux,"LAMBDA",10.);
    }


    /* Correct for atmospheric extinction, and calculate response */
    {
    double dlambda;
    int order;
    
    check( dlambda = uves_pfits_get_cdelt1(spectrum_header),
           "Error reading bin width from header");
    
    for (order = 1; order <= norders; order++)
        {
        double lambda_start;
        int x;
        
        /* If spectrum was already merged, then read cdelt1,
         * otherwise read wstart for each order
         */
        if (norders == 1)
            {
            check( lambda_start = uves_pfits_get_crval1(spectrum_header),
                   "Error reading start wavelength from header");    
            }
        else
            {
            check( lambda_start = uves_pfits_get_wstart(spectrum_header, order),
                   "Error reading start wavelength from header");    
            }
        
        for (x = 1; x <= nx; x++)
            {
            int pis_rejected;
            double lambda;            
            double flux, std_flux, resp;
                        int istart = 0;

            lambda = lambda_start + (x-1) * dlambda;
            
            check( flux = cpl_image_get(spectrum, x, order, &pis_rejected),
                   "Error reading flux");
            
            if (!pis_rejected)
                {
                /* Get interpolated catalogue flux */
                check( std_flux = uves_spline_hermite_table(
                       lambda, catalogue_flux,
                       "LAMBDA", "F_LAMBDA", &istart),
                       "Error interpolating catalogue flux");

                if (inverse)
                    {
                    resp = (flux == 0) ? 0 : std_flux / flux;
                    }
                else
                    {
                    resp = (std_flux == 0) ? 0 : flux / std_flux;
                    }
                
                check( cpl_image_set(response, x, order, resp),
                       "Error writing response image");
                }
            else
                {
                cpl_image_reject(response, x, order);
                }
            }
        }
    }

  cleanup:
    uves_free_table(&catalogue_flux);
    if (cpl_error_get_code() != CPL_ERROR_NONE)
    {
        uves_free_image(&response);
    }
    return response;
}


/*----------------------------------------------------------------------------*/
/**
   @brief    Match a star against a catalogue of stars 
   @param    object_header         Header containing the object coordinates
   @param    flux_table            The catalogue of standard star coordinates and fluxes
   @param    accuracy              The pointing accuracy in arcseconds
   @param    ref_name_dynamic      (output) reference object ID. Must be
                                   deallocated by the caller
   @return   Table containing the catalogue flux of the identified star.

   The input object coordinates are matched against the list of standard star
   coordinates.

   Exactly one reference star must match the object within the specified @em accuracy,
   or an error is set.

   The spectrum of the uniquely identified star is read from the catalogue 
   and returned as a (2D) table.
*/
/*----------------------------------------------------------------------------*/
cpl_table *
uves_align(const uves_propertylist *object_header, const cpl_table *flux_table,
       double accuracy,
       char **ref_name_dynamic)
{
    double obj_ra, obj_dec;  /* Object position in degrees    */
    int nident = 0;          /* Number of identifications     */
    int match_row = 0;       /* Catalogue row number of match */
    double min_dist = 0;     /* Accuracy of match             */
    double std_ra = 0, std_dec = 0;    /* Catalogue position  */
    const char *ref_type = NULL;

    cpl_table *result = NULL;

    int i;

    assure_nomsg( ref_name_dynamic != NULL, CPL_ERROR_NULL_INPUT );
    *ref_name_dynamic = NULL;

    check( obj_ra  = uves_pfits_get_ra (object_header), "Could not read right ascension");
    check( obj_dec = uves_pfits_get_dec(object_header), "Could not read declination");
    
    uves_msg("Object RA, DEC = (%e, %e)", obj_ra, obj_dec);

    nident = 0;
    for (i = 0; i < cpl_table_get_nrow(flux_table); i++)
    {
        double ref_ra, ref_dec;
        double dist;

        check( ref_ra  = cpl_table_get_double(flux_table, "RA_DEG", i, NULL),
           "Could not read catalogue star right ascension");
        check( ref_dec = cpl_table_get_double(flux_table, "DEC_DEG", i, NULL),
           "Could not read catalogue star declination");

        /* Calculate angular separation in arcsecs
         * cos(sep) = sin(d1)sin(d2) + cos(d1)cos(d2)cos(ra1-ra2) 
         *
         * All input angles are in degrees.
         */
        
        dist = 
        SIN_DEG(obj_dec)*SIN_DEG(ref_dec) + 
        COS_DEG(obj_dec)*COS_DEG(ref_dec)*COS_DEG(obj_ra - ref_ra);
        
        dist = uves_max_double(dist, -1);
        dist = uves_min_double(dist,  1);
        
        dist = ACOS_DEG(dist) * 3600;

        uves_msg_debug("Angular separation = %f arcseconds", dist);


        /* Keep track of best match also if it is not within the pointing accuracy */
        if (i == 0 || dist < min_dist)
        {
            min_dist = dist;
            std_ra = ref_ra;
            std_dec = ref_dec;
        }


        /* Does it match? */
        if (dist < accuracy)
        {
            nident += 1;
            match_row = i;        
            min_dist = dist;
            std_ra = ref_ra;
            std_dec = ref_dec;            
        }
    }

    assure( nident >= 1, CPL_ERROR_INCOMPATIBLE_INPUT, 
        "No catalogue object within %f arcsecs. "
        "Nearest object is %f arcsecs away at (RA, DEC) = (%f, %f)", 
        accuracy, min_dist, std_ra, std_dec);

    assure( nident <= 1, CPL_ERROR_INCOMPATIBLE_INPUT,
        "%d matching catalogue objects found. Confused. "
        "Decrease pointing accuracy (currently %f arcsecs) to get fewer matches", 
        nident, accuracy);

    check( *ref_name_dynamic = cpl_strdup(
           cpl_table_get_string(flux_table, "OBJECT", match_row)),
       "Could not read reference object name");
    
    check( ref_type = cpl_table_get_string(flux_table, "TYPE", match_row),
       "Could not read reference object type");
    
    uves_msg("Object ID is '%s', type = '%s'. Residual between header/catalogue = %f arcsecs",
         *ref_name_dynamic, ref_type, min_dist);
    
    
    /* Create (2d) flux table from catalogue table row number 'match_row' */
    {
    const char *columns[3] = {"LAMBDA", "BIN_WIDTH", "F_LAMBDA"};
    int ndata;                      /* Number of elements in column */

    check( ndata = cpl_table_get_int(flux_table, "NDATA", match_row, NULL),
           "Error reading length of flux array");
    
    result = cpl_table_new(ndata);

    for(i = 0; i < 3; i++)
        {
        const cpl_array *data;
        int indx;
        
        cpl_table_new_column(result, columns[i], CPL_TYPE_DOUBLE);
        
        data = cpl_table_get_array(flux_table, columns[i], match_row);

        /* Only the 'ndata' first elements of the array are used,
           and the array may be longer than this */
        uves_msg_debug("3d table array size = %" CPL_SIZE_FORMAT ", ndata = %d",
                   cpl_array_get_size(data), ndata);

        assure( cpl_array_get_size(data) >= ndata,
            CPL_ERROR_ILLEGAL_INPUT,
            "Flux table row %d: column '%s' depth (%" CPL_SIZE_FORMAT ") "
            "is less than NDATA (%d)",
            match_row, columns[i], cpl_array_get_size(data), ndata);

        for (indx = 0; indx < ndata; indx++)
            {
            /* 3d columns are float */
            cpl_table_set_double(result, columns[i], indx, 
                         cpl_array_get_float(data, indx, NULL));
            }
        }
    }
  
  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE)
    {
        uves_free_table(&result);
        if (ref_name_dynamic != NULL) 
        {
            cpl_free(*ref_name_dynamic);
            *ref_name_dynamic = NULL;
        }
    }

    return result;
}

/**@}*/



