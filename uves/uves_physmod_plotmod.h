/*                                                                              *
 *   This file is part of the ESO UVES  Pipeline                                *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */

/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:07 $
 * $Revision: 1.6 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.4  2007/04/25 08:37:46  amodigli
 * changed interface
 *
 * Revision 1.3  2007/01/15 08:46:59  jmlarsen
 * Fixed missing plots
 *
 * Revision 1.2  2006/11/06 15:19:41  jmlarsen
 * Removed unused include directives
 *
 * Revision 1.1  2006/02/03 07:46:30  jmlarsen
 * Moved recipe implementations to ./uves directory
 *
 * Revision 1.3  2005/12/20 08:11:44  jmlarsen
 * Added CVS  entry
 *
 */
#ifndef UVES_PHYSMOD_PLOTMOD_H
#define UVES_PHYSMOD_PLOTMOD_H

#include <uves_chip.h>
#include <uves_pfits.h>
#include <uves_utils_wrappers.h>

#include <cpl.h>
int 
uves_physmod_plotmod(const cpl_table* tbl,
                     const uves_propertylist* head,
                     const char* rec_id,
                     const cpl_parameterlist* params,
                     enum uves_chip chip);


#endif /* UVES_PHYSMOD_PLOTMOD_H */
