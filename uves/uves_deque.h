/*===========================================================================
  Copyright (C) 2001 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
    Internet e-mail: midas@eso.org
    Postal address: European Southern Observatory
            Data Management Division 
            Karl-Schwarzschild-Strasse 2
            D 85748 Garching bei Muenchen 
            GERMANY
===========================================================================*/
#ifndef UVES_DEQUE_H
#define UVES_DEQUE_H

#include <cxtypes.h>
#include <assert.h>
#include <stdlib.h>

typedef struct _uves_deque_ uves_deque;

typedef unsigned long uves_deque_const_iterator;
typedef unsigned long uves_deque_iterator;

uves_deque *
uves_deque_new(void);
void
uves_deque_push_back(uves_deque *d, cxptr what);
void uves_deque_push_front(uves_deque *d, cxptr what);
cxptr uves_deque_get(const uves_deque *d, uves_deque_const_iterator indx);
uves_deque_iterator uves_deque_erase(uves_deque *d, uves_deque_iterator indx, cx_free_func deallocate);
void uves_deque_insert(uves_deque *d, uves_deque_iterator indx, cxptr what);
cxsize
uves_deque_size(const uves_deque *d);
void uves_deque_destroy(uves_deque *d, cx_free_func deallocate);
cxbool uves_deque_empty(const uves_deque *d);
uves_deque_iterator uves_deque_begin(const uves_deque *d);
uves_deque_iterator uves_deque_end(const uves_deque *d);
uves_deque_iterator uves_deque_next(const uves_deque *d, uves_deque_const_iterator i);
#endif
