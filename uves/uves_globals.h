/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*-----------------------------------------------------------------------------
                            Defines
 ----------------------------------------------------------------------------*/

#ifndef UVES_GLOBALS_H
#define UVES_GLOBALS_H

#define MAX_NAME_SIZE 4096     /* fixme: Is this always enough? */
#define MIN_NAME_SIZE  80      /* fixme: Is this always enough? */
#define MED_NAME_SIZE 255      /* fixme: Is this always enough? */

#define cpl_frameset_get_frame cpl_frameset_get_position
#define cpl_frameset_get_frame_const cpl_frameset_get_position_const

#include <stdint.h>

#endif /* UVES_GLOBALS_H */
