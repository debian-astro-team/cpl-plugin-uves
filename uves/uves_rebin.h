/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2010-11-09 17:21:57 $
 * $Revision: 1.11 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.10  2010/09/24 09:32:07  amodigli
 * put back QFITS dependency to fix problem spot by NRI on FIBER mode (with MIDAS calibs) data
 *
 * Revision 1.8  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.7  2007/05/22 11:38:18  jmlarsen
 * Removed MIDAS flag for good
 *
 * Revision 1.6  2007/05/07 10:18:25  jmlarsen
 * Added option to enforce positive resulting values (useful for error bars)
 *
 * Revision 1.5  2007/04/24 12:50:29  jmlarsen
 * Replaced cpl_propertylist -> uves_propertylist which is much faster
 *
 * Revision 1.4  2006/04/06 08:39:36  jmlarsen
 * Added void to function prototype
 *
 * Revision 1.3  2005/12/19 16:17:56  jmlarsen
 * Replaced bool -> int
 *
 * Revision 1.2  2005/11/24 11:54:46  jmlarsen
 * Added support for CPL 3 interface
 *
 * Revision 1.1  2005/11/11 13:18:54  jmlarsen
 * Reorganized code, renamed source files
 *
 */
#ifndef UVES_REBIN_H
#define UVES_REBIN_H

#include <uves_utils_polynomial.h>
#include <cpl.h>
#include <stdbool.h>
#include <uves_chip.h>

cpl_parameterlist *uves_rebin_define_parameters(void);

cpl_image *
uves_rebin(const cpl_image *spectrum,
       const cpl_parameterlist *parameters, const char *context,
       const cpl_table *linetable, const polynomial *dispersion_relation, 
       int first_abs_order, int last_abs_order,
       int n_traces,
           bool threshold_to_positive,
           bool is_noise,
           uves_propertylist **rebinned_header,
		   enum uves_chip chip);

#endif


