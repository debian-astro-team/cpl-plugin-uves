/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */

/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:07 $
 * $Revision: 1.6 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.4  2007/04/24 12:50:29  jmlarsen
 * Replaced cpl_propertylist -> uves_propertylist which is much faster
 *
 * Revision 1.3  2006/11/06 15:19:41  jmlarsen
 * Removed unused include directives
 *
 * Revision 1.2  2006/05/08 15:42:16  amodigli
 * allow to specify order column label
 *
 * Revision 1.1  2006/02/03 07:46:30  jmlarsen
 * Moved recipe implementations to ./uves directory
 *
 * Revision 1.2  2006/01/03 16:57:13  amodigli
 * Fixed bug
 *
 * Revision 1.1  2006/01/03 14:47:53  amodigli
 *
 * Added uves_physmod_chop_otab.h .c to match MIDAS
 *
 * Revision 1.2  2005/12/19 16:17:55  jmlarsen
 * Replaced bool -> int
 *
 */
#ifndef UVES_PHYSMOD_CHOP_OTAB_H
#define UVES_PHYSMOD_CHOP_OTAB_H

#include <uves_propertylist.h>
#include <uves_chip.h>

#include <cpl.h>

int  uves_physmod_chop_otab(const uves_propertylist * rline_tbl,
                            enum uves_chip chip, 
                            cpl_table** ord_tbl,const char* col_name,
                            int * ord_min,
                            int * ord_max);
#endif /* UVES_PHYSMOD_CHOP_OTAB_H */
