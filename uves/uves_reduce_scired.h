/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:07 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.3  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.2  2007/02/09 13:41:31  jmlarsen
 * Enable calling from uves_obs_spatred
 *
 * Revision 1.1  2006/09/27 13:22:38  jmlarsen
 * Factored out scired body to enable calling it from other recipes
 *
 */
#ifndef UVES_REDUCE_SCIRED_H
#define UVES_REDUCE_SCIRED_H

#include <cpl.h>

void uves_reduce_scired(cpl_frameset *frames, const cpl_parameterlist *parameters, 
            const char *recipe_id, const char *starttime);

int uves_scired_define_parameters_body(cpl_parameterlist *parameters,
                       const char *recipe_id);

extern const char * const uves_scired_desc_short;
extern const char * const uves_scired_desc;

#endif  /* UVES_REDUCE_SCIRED_H */
