/*
 * This file is part of the UVES Pipeline
 * Copyright (C) 2002, 2003, 2004, 2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2013-04-16 15:46:18 $
 * $Revision: 1.88 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.87  2010/11/25 12:48:14  amodigli
 * added UVES_FNOISE
 *
 * Revision 1.86  2010/09/29 15:03:18  amodigli
 * added DTIME
 *
 * Revision 1.85  2010/09/29 09:39:27  amodigli
 * added UVES_DNOISE,UVES_BNOISE
 *
 * Revision 1.84  2010/09/24 09:32:06  amodigli
 * put back QFITS dependency to fix problem spot by NRI on FIBER mode (with MIDAS calibs) data
 *
 * Revision 1.82  2010/06/01 13:29:58  amodigli
 * defined UVES_TUNIT, uves_pfits_set_tunit_no(), uves_pfits_set_bunit_no()
 *
 * Revision 1.81  2009/07/13 06:37:46  amodigli
 * added uves_ccd_is_new()
 *
 * Revision 1.80  2007/06/28 09:18:19  jmlarsen
 * Added functions to write DPR keywords
 *
 * Revision 1.79  2007/06/01 14:09:55  jmlarsen
 * Exported uves_pfits_get_pixelscale
 *
 * Revision 1.78  2007/05/16 14:38:03  amodigli
 * added uves_flames_pfits_get_ident & uves_flames_pfits_get_object
 *
 * Revision 1.77  2007/05/04 08:53:49  amodigli
 * added uves_flames_pfits_get_nflats
 *
 * Revision 1.76  2007/05/03 15:21:35  jmlarsen
 * Added function to read wavecal offset
 *
 * Revision 1.75  2007/04/26 13:19:42  jmlarsen
 * Added uves_pfits_set_ocs_simcal()
 *
 * Revision 1.74  2007/04/25 08:37:19  amodigli
 * added uves_pfits_get_origfile
 *
 * Revision 1.73  2007/04/24 12:50:29  jmlarsen
 * Replaced cpl_propertylist -> uves_propertylist which is much faster
 *
 * Revision 1.72  2007/04/12 12:15:12  jmlarsen
 * Propagate keyword OS-EXPOI
 *
 * Revision 1.71  2007/03/05 10:18:18  jmlarsen
 * Write 2d extraion slit length
 *
 * Revision 1.70  2007/01/31 13:11:25  jmlarsen
 * Added SIMCAL accessor functions
 *
 * Revision 1.69  2007/01/29 12:10:21  jmlarsen
 * Exported set_history_val() for usage by FLAMES
 *
 * Revision 1.68  2007/01/26 12:33:57  amodigli
 * added function for common QC
 *
 * Revision 1.67  2007/01/17 13:27:09  jmlarsen
 * Added BITPIX accessor function
 *
 * Revision 1.66  2007/01/10 12:39:07  jmlarsen
 * Unified two get_plate_id functions
 *
 * Revision 1.65  2006/12/27 13:55:29  amodigli
 * added flames_pfits_get_plate_id and some FLAMES_ defines for FIBER mode
 *
 * Revision 1.64  2006/12/11 12:35:03  jmlarsen
 * Fixed QC bugs
 *
 * Revision 1.63  2006/12/07 08:25:09  jmlarsen
 * Added uves_pfits_get_readspeed
 *
 * Revision 1.62  2006/12/01 12:27:23  jmlarsen
 * Factored out FLAMES plate-id code
 *
 * Revision 1.61  2006/10/24 14:05:46  jmlarsen
 * Added OBSPLATE
 *
 * Revision 1.60  2006/10/10 11:29:24  jmlarsen
 * Added code to propagate TM-START
 *
 * Revision 1.59  2006/10/05 06:50:23  jmlarsen
 * Renamed function format_is_new -> uves_format_is_new
 *
 * Revision 1.58  2006/10/04 10:55:33  jmlarsen
 * Added telescope lat-/longitude accessor functions
 *
 * Revision 1.57  2006/09/27 13:14:17  jmlarsen
 * Added accessor functions for RA/DEC
 *
 * Revision 1.56  2006/09/20 10:57:36  jmlarsen
 * Propagate DATAMEAN/DATAMED/DATARMS if present
 *
 * Revision 1.55  2006/09/19 14:32:15  jmlarsen
 * Minor doc. update
 *
 * Revision 1.54  2006/09/19 06:42:49  jmlarsen
 * Added keywords
 *
 * Revision 1.53  2006/08/24 11:37:14  jmlarsen
 * Write recipe start/stop time to header
 *
 * Revision 1.52  2006/08/18 10:14:04  jmlarsen
 * Use legal FITS keywords for TRACEID/WINDOW/FABSORD/LABSORD
 *
 * Revision 1.51  2006/08/17 13:56:53  jmlarsen
 * Reduced max line length
 *
 * Revision 1.50  2006/08/16 14:24:03  jmlarsen
 * Renamed UIT QC keyword
 *
 * Revision 1.49  2006/08/09 14:23:19  jmlarsen
 * Removed unused function argument
 *
 * Revision 1.48  2006/08/07 11:32:14  jmlarsen
 * Added comment
 *
 * Revision 1.47  2006/08/04 15:11:54  amodigli
 * ESO INS DET1 UIT-->ESO DET WIN1 UIT1
 *
 * Revision 1.46  2006/07/31 06:29:26  amodigli
 * added QC on stability test
 *
 * Revision 1.45  2006/07/14 12:24:49  jmlarsen
 * Added conad, target name
 *
 * Revision 1.44  2006/06/22 08:54:28  jmlarsen
 * Use proper keyword to read number of predicted orders
 *
 * Revision 1.43  2006/06/13 11:58:55  jmlarsen
 * Bugfix: Use proper chipname keyword for REDL chip
 *
 * Revision 1.42  2006/06/01 14:43:17  jmlarsen
 * Added missing documentation
 *
 * Revision 1.41  2006/06/01 14:22:48  amodigli
 * moved format_is_new to c module and made it static
 *
 * Revision 1.40  2006/05/31 09:26:40  amodigli
 * fixed some problem dumping QC log
 *
 * Revision 1.39  2006/05/19 13:07:20  amodigli
 * added uves_pfits_get_slit3_x1encoder
 *
 * Revision 1.38  2006/05/09 15:43:13  amodigli
 * added useful key/functions
 *
 * Revision 1.37  2006/04/26 12:25:20  amodigli
 * fixed a problem with ARCFILE keyword
 *
 * Revision 1.36  2006/04/25 14:58:48  amodigli
 * added paf creation functionalities
 *
 * Revision 1.35  2006/04/20 10:48:58  amodigli
 * added uves_pfits_get_tpl_start
 *
 * Revision 1.34  2006/02/21 14:26:54  jmlarsen
 * Minor changes
 *
 * Revision 1.33  2006/01/17 13:05:58  jmlarsen
 * Changed line table trace id keyword
 *
 * Revision 1.32  2005/12/19 16:17:56  jmlarsen
 * Replaced bool -> int
 *
 */

#ifndef UVES_PFITS_H
#define UVES_PFITS_H

/*----------------------------------------------------------------------------*/
/**
 * @addtogroup uves_pfits
 *
 */
/*----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#include <uves_propertylist.h>
#include <uves_chip.h>
#include <cpl.h>


/*-----------------------------------------------------------------------------
                                Defines
 -----------------------------------------------------------------------------*/

/*
 * Recipe DRS id
 */

#define UVES_DRS_ID "ESO PRO REC1 DRS ID"

/*
 * Pre- and overscan keywords
 */
/* Use detector prefix 'ESO DET OUT4' for REDL chips, old format. Otherwise 'ESO DET OUT1' */

#define UVES_PRESCANX(new_format, chip) \
 ((!(new_format) && (chip) == UVES_CHIP_REDL) ? "ESO DET OUT4 PRSCX" : "ESO DET OUT1 PRSCX")
#define UVES_PRESCANY(new_format, chip) \
 ((!(new_format) && (chip) == UVES_CHIP_REDL) ? "ESO DET OUT4 PRSCY" : "ESO DET OUT1 PRSCY")

#define UVES_OVRSCANX(new_format, chip) \
 ((!(new_format) && (chip) == UVES_CHIP_REDL) ? "ESO DET OUT4 OVSCX" : "ESO DET OUT1 OVSCX")
#define UVES_OVRSCANY(new_format, chip) \
 ((!(new_format) && (chip) == UVES_CHIP_REDL) ? "ESO DET OUT4 OVSCY" : "ESO DET OUT1 OVSCY")

/* 
 *  Readout noise, gain
 */

#define UVES_RON(new_format, chip)  \
 ((!(new_format) && (chip) == UVES_CHIP_REDL) ? "ESO DET OUT4 RON"  : "ESO DET OUT1 RON")
#define UVES_GAIN(new_format, chip) \
 ((!(new_format) && (chip) == UVES_CHIP_REDL) ? "ESO DET OUT4 GAIN" : "ESO DET OUT1 GAIN")
#define UVES_CONAD(new_format, chip) \
 ((!(new_format) && (chip) == UVES_CHIP_REDL) ? "ESO DET OUT4 CONAD" : "ESO DET OUT1 CONAD")


#define UVES_NX(new_format, chip) \
 ((!(new_format) && (chip) == UVES_CHIP_REDL) ? "ESO DET OUT4 NX" : "ESO DET OUT1 NX")
#define UVES_NY(new_format, chip) \
 ((!(new_format) && (chip) == UVES_CHIP_REDL) ? "ESO DET OUT4 NY" : "ESO DET OUT1 NY")

/*
 *  Exposure time
 */
#define UVES_EXPTIME "EXPTIME"

/*
 *  User defined subintegration time
 */
#define UVES_UIT(new_format)  "ESO DET WIN1 UIT1"
/*#define UVES_QC_UIT(new_format, chip) \
  ((!(new_format) && (chip) == UVES_CHIP_REDL) ? "ESO DET4 OUT4 UIT1"  : "ESO DET1 OUT1 UIT1")
*/

/* This is probably what we want to do (use the same keyword name for reading/writing):
   #define UVES_QC_UIT(new_format, chip) \
   ((chip) == UVES_CHIP_REDL ? "ESO DET WIN4 UIT1"  : "ESO DET WIN1 UIT1")

   but we do like MIDAS: 

   MIDAS uses the name ESO.DET1.WIN1.UIT1 when writing to the product header
*/
#define UVES_QC_UIT(new_format, chip) \
 ((chip) == UVES_CHIP_REDL ? "ESO INS DET4 UIT"  : "ESO INS DET1 UIT")



/*
 *  Start time template
 */
#define UVES_TPL_START "ESO TPL START"
#define UVES_TMSTART "TM-START"     /* MIDAS internal? */


/*
 *  Airmass
 */
#define UVES_AIRMASS_START "ESO TEL AIRM START"
#define UVES_AIRMASS_END   "ESO TEL AIRM END"
#define UVES_AIRMASS       "AIRMASS"

/*
 * Image type
 */
#define UVES_IMAGETYP "IMAGETYP"

/*
 * Observation time
 */
#define UVES_UTC "UTC"
#define UVES_UT "UT"
#define UVES_ST "ST"

/*
 * latitude/longitude
 */
#define UVES_GEOLAT "ESO TEL GEOLAT"
#define UVES_GEOLON "ESO TEL GEOLON"

/*
 *  Extension name (new format)
 */
#define UVES_EXTNAME "EXTNAME"

/*
 *  Observation date
 */
#define UVES_MJDOBS "MJD-OBS"
#define UVES_DATE "DATE"

/* Observer id */
#define UVES_OS_EXPOI "OS-EXPOI"

/*
 *   Object coordinates
 */
#define UVES_RA "RA"
#define UVES_DEC "DEC"

/*
 *   Object name
 */
#define UVES_TARG_NAME "ESO OBS TARG NAME"

/*
 *  Binning factors
 */

#define UVES_BINX "ESO DET WIN1 BINX"
#define UVES_BINY "ESO DET WIN1 BINY"

/*
 *  Arcseconds per pixel
 */
#define UVES_PIXELSCALE "ESO INS PIXSCALE"

/*
 * Encoder steps
 */

#define  UVES_ENCODER_REF1  "ESO INS SLIT3 X1ENC"
#define  UVES_ENCODER_REF2  "ESO INS SLIT3 X2ENC"

/*
 * Extra keys for bias and dark noise values
 */
#define UVES_BNOISE "ESO BNOISE"
#define UVES_DNOISE "ESO DNOISE"
#define UVES_FNOISE "ESO FNOISE"
#define UVES_DTIME  "ESO DTIME"

/* CD matrix */
#define UVES_CD1 "CD1"
#define UVES_CD11 "CD1_1"
#define UVES_CD12 "CD1_2"
#define UVES_CD21 "CD2_1"
#define UVES_CD22 "CD2_2"

/*
 *  Slit name, length/width (arcseconds)
 */
#define UVES_SLIT1NAME   "ESO INS SLIT1 NAME"
#define UVES_ARM(chip) (((chip) == UVES_CHIP_BLUE) ? "BLUE" : "RED")
#define UVES_ARMID(chip) (((chip) == UVES_CHIP_BLUE) ? "b" : "r")
#define UVES_DETECTOR(chip) (((chip) == UVES_CHIP_BLUE) ? "EEV" : "MIT-LL")
#define UVES_SLITLENGTH(chip) \
 (((chip) == UVES_CHIP_BLUE) ? "ESO INS SLIT2 LEN" : "ESO INS SLIT3 LEN")
#define UVES_SLITWIDTH(chip) \
 (((chip) == UVES_CHIP_BLUE) ? "ESO INS SLIT2 WID" : "ESO INS SLIT3 WID")

/*
 *   Grating
 */

#define UVES_GRATWLEN(chip) \
 (((chip) == UVES_CHIP_BLUE) ? "ESO INS GRAT1 WLEN" : "ESO INS GRAT2 WLEN")
#define UVES_GRATID(chip) \
 (((chip) == UVES_CHIP_BLUE) ? "ESO INS GRAT1 ID"   : "ESO INS GRAT2 ID")
#define UVES_GRATNAME(chip) \
 (((chip) == UVES_CHIP_BLUE) ? "ESO INS GRAT1 NAME" : "ESO INS GRAT2 NAME")

/*
  BLUE,old CHIP1 NAME
  BLUE,new CHIP1 NAME
  REDL,old CHIP2 NAME
  REDL,new CHIP2 NAME
  REDU,old CHIP1 NAME
  REDU,new CHIP1 NAME
*/
#define UVES_CHIP_NAME(chip) \
 (((chip) == UVES_CHIP_REDL) ? "ESO DET CHIP2 NAME" : "ESO DET CHIP1 NAME")
#define UVES_CHIP_ID(chip) \
 (((chip) == UVES_CHIP_REDL) ? "ESO DET CHIP2 ID"   : "ESO DET CHIP1 ID")

/* Is this correct? Do we really want to use different
   FITS keywords when reading inputs and writing QC files?

   No, this is wrong:

#define UVES_QC_CHIP_NAME(chip) \
 (((chip) == UVES_CHIP_BLUE) ? "ESO DET CHIP1 NAME" : "ESO DET CHIP2 NAME")
#define UVES_QC_CHIP_ID(chip) \
 (((chip) == UVES_CHIP_BLUE) ? "ESO DET CHIP1 ID"   : "ESO DET CHIP2 ID")
#define UVES_QC_CHIP_VAL(chip) \
 (((chip) == UVES_CHIP_REDU) ? "MIT" : "EEV")
*/

/*
 *
 */
#define UVES_CCDID       "ESO DET ID"
#define UVES_PRESSURE    "ESO INS SENS26 MEAN"
#define UVES_TEMPCAM(chip) \
 (((chip) == UVES_CHIP_BLUE) ? "ESO INS TEMP1 MEAN"   : "ESO INS TEMP2 MEAN")

#define UVES_HUMIDITY    "ESO INS TEMP31 MEAN"

/*
 * Keyword in DRS table for predicted number of orders
 */

/* #define UVES_ORD_PRED "ESO PRO QC ORDEF ORD PRED" */
#define UVES_ORD_PRED "NBORDI"

#define UVES_INSMODE  "ESO INS MODE"
#define UVES_INSPATH  "ESO INS PATH"


/* Images statistics */
#define UVES_PRO_DATAAVG  "ESO PRO DATAAVG"
#define UVES_PRO_DATARMS  "ESO PRO DATARMS"
#define UVES_PRO_DATAMED  "ESO PRO DATAMED"
#define UVES_DATAMIN  "DATAMIN"
#define UVES_DATAMAX  "DATAMAX"
#define UVES_DATAMEAN "DATAMEAN"
#define UVES_DATAMED  "DATAMED"
#define UVES_DATARMS  "DATARMS"

/* Object */
#define UVES_OBJECT "OBJECT"

/* Line table trace and window ID */
#define UVES_TRACEID      "TRACEID"
#define UVES_TRACE_OFFSET "OFFSET"
#define UVES_WINDOWNUMBER "WINDOW"

/* Line table order numbers */
#define UVES_FIRSTABSORDER "FABSORD"
#define UVES_LASTABSORDER  "LABSORD"

/* Image FITS keywords */
#define UVES_BUNIT "BUNIT"
#define UVES_BSCALE "BSCALE"
#define UVES_TUNIT "TUNIT"

#define UVES_CTYPE1 "CTYPE1"
#define UVES_CTYPE2 "CTYPE2"
#define UVES_CUNIT1 "CUNIT1"
#define UVES_CUNIT2 "CUNIT2"
#define UVES_CRVAL1 "CRVAL1"
#define UVES_CRVAL2 "CRVAL2"
#define UVES_CRPIX1 "CRPIX1"
#define UVES_CRPIX2 "CRPIX2"
#define UVES_CDELT1 "CDELT1"
#define UVES_CDELT2 "CDELT2"

#define UVES_BITPIX "BITPIX"
#define UVES_NAXIS  "NAXIS"
#define UVES_NAXIS1 "NAXIS1"
#define UVES_NAXIS2 "NAXIS2"
#define UVES_STARTX "STARTX"
#define UVES_STARTY "STARTY"
#define UVES_TEL_ALT "ESO TEL ALT"
#define UVES_OUT1NX "ESO DET OUT1 NX"
#define UVES_OUT1NY "ESO DET OUT1 NY"
#define UVES_OUT4NX "ESO DET OUT4 NX"
#define UVES_OUT4NY "ESO DET OUT4 NY"

#define UVES_READ_SPEED "ESO DET READ SPEED"

/* 2d extraction slit length */
#define UVES_HS "HS"

/* Rebinned spectrum wavelength offset */
#define UVES_WSTART "WSTART"
#define UVES_WEND   "WEND"

/* Number of input raw frames */
#define UVES_DATANCOM     "ESO PRO DATANCOM"
#define UVES_DATANCOM_OLD "ESO PRO QC DATANCOM"

/* QC - Bad pixel corrected */
#define UVES_QC_BADPIXCORR "ESO QC BADPIX CORR"

/* Reduction level */
#define UVES_REDLEVEL "ESO PRO REDLEVEL"

/*
 *   Note   : Currently we always write to the RECi keyword 
 *            with i = 1, like the cpl_setup_product_header
 *            function. Maybe this needs to be generalized
 *            later (if a product of one recipe is further processed
 *            by another recipe).
 */
#define UVES_STATUS "ESO PRO REC1 STATUS"

#define UVES_START "ESO PRO REC1 START"
#define UVES_STOP  "ESO PRO REC1 STOP"

#define UVES_DPR_TECH "ESO DPR TECH"
#define UVES_DPR_TYPE "ESO DPR TYPE"
#define UVES_DPR_CATG "ESO DPR CATG"
#define UVES_OCS_SIMCAL "ESO OCS SIMCAL"

/*
 *  FLAMES
 */

#define FLAMES_NFLATS  "NFLATS"

#define FLAMES_OBS_PLATE_ID "ESO INS SLIT3 PLATE"
#define FLAMES_INS_SHUT09   "ESO INS SHUT9 ST"
#define FLAMES_INS_SHUT10   "ESO INS SHUT10 ST"
#define FLAMES_PLATEID      "ESO INS1 TUMB POS"
#define FLAMES_NEWPLATEID   "ESO INS OBSPLATE"
#define FLAMES_DIT          "ESO DET WIN1 DIT1"
#define FLAMES_CCFPOSMAX    "ESO QC CCF POSMAX"

/**@{*/

/*-----------------------------------------------------------------------------
                           Functions prototypes
-----------------------------------------------------------------------------*/
bool uves_format_is_new(const uves_propertylist * plist);
void
uves_pfits_set_history_val(uves_propertylist *plist, const char *name, const char *format, ...)
#ifdef __GNUC__
    __attribute__((format (printf, 3, 4)))
#endif
    ;

void uves_pfits_set_cd11(uves_propertylist * plist, double value);
void uves_pfits_set_cd12(uves_propertylist * plist, double value);
void uves_pfits_set_cd21(uves_propertylist * plist, double value);
void uves_pfits_set_cd22(uves_propertylist * plist, double value);

bool uves_ccd_is_new(const uves_propertylist * plist);
int uves_pfits_get_slit3_x1encoder(const uves_propertylist * plist);
int uves_pfits_get_slit3_x2encoder(const uves_propertylist * plist);
double uves_pfits_get_startx(const uves_propertylist * plist);
double uves_pfits_get_starty(const uves_propertylist * plist);
double uves_pfits_get_tel_alt_start(const uves_propertylist * plist);
int uves_pfits_get_maxfibres(const uves_propertylist * plist);
char  uves_pfits_get_chipchoice(const uves_propertylist * plist);
const char * uves_pfits_get_origfile(const uves_propertylist * plist);
const char * uves_pfits_get_badpxframe(const uves_propertylist * plist);
const char * uves_chop_eso_prefix(const char* key);
const char * uves_pfits_get_rec1raw1name(const uves_propertylist * plist);
const char * uves_pfits_get_arcfile(const uves_propertylist * plist);
const char * uves_pfits_get_pipefile(const uves_propertylist * plist);
const char * uves_pfits_get_templateid(const uves_propertylist * plist);
const char * uves_pfits_get_pro_catg(const uves_propertylist * plist) ;
const char * uves_pfits_get_dpr_catg(const uves_propertylist * plist) ;
const char * uves_pfits_get_dpr_tech(const uves_propertylist * plist) ;
const char * uves_pfits_get_dpr_type(const uves_propertylist * plist) ;
void uves_pfits_set_dpr_catg(uves_propertylist * plist, const char *);
void uves_pfits_set_dpr_tech(uves_propertylist * plist, const char *);
void uves_pfits_set_dpr_type(uves_propertylist * plist, const char *);
const char * uves_pfits_get_date_obs(const uves_propertylist * plist) ;
double uves_pfits_get_airm_mean (const cpl_propertylist * plist);
const char* uves_pfits_get_chipid(const uves_propertylist * plist, enum uves_chip chip);
const char* uves_pfits_get_chip_name(const uves_propertylist * plist, enum uves_chip chip);
const char *uves_pfits_get_drs_id(const uves_propertylist * plist);
const char *uves_pfits_get_tpl_start(const uves_propertylist * plist);
const char* uves_pfits_get_insmode(const uves_propertylist * plist);
const char* uves_pfits_get_inspath(const uves_propertylist * plist);
const char *uves_pfits_get_targ_name(const uves_propertylist * plist);
int uves_pfits_get_ocs_simcal(const uves_propertylist * plist);
void uves_pfits_set_ocs_simcal(uves_propertylist * plist, int simcal);

int uves_pfits_get_prescanx(const uves_propertylist * plist, enum uves_chip chip);
int uves_pfits_get_ovrscanx(const uves_propertylist * plist, enum uves_chip chip);
int uves_pfits_get_prescany(const uves_propertylist * plist, enum uves_chip chip);
int uves_pfits_get_ovrscany(const uves_propertylist * plist, enum uves_chip chip);

int uves_pfits_get_ordpred(const uves_propertylist * plist);
int uves_pfits_get_binx(const uves_propertylist * plist);
int uves_pfits_get_biny(const uves_propertylist * plist);
double uves_pfits_get_mjdobs(const uves_propertylist * plist);
double uves_pfits_get_utc(const uves_propertylist * plist);
double uves_pfits_get_ra(const uves_propertylist * plist);
double uves_pfits_get_dec(const uves_propertylist * plist);
double uves_pfits_get_geolon(const uves_propertylist * plist);
double uves_pfits_get_geolat(const uves_propertylist * plist);
double uves_pfits_get_ron_adu(const uves_propertylist * plist, enum uves_chip chip);
double uves_pfits_get_ambipress(const uves_propertylist * plist);
double uves_pfits_get_gain(const uves_propertylist * plist, enum uves_chip chip);
double uves_pfits_get_conad(const uves_propertylist * plist, enum uves_chip chip);
double uves_pfits_get_exptime(const uves_propertylist * plist);
double uves_pfits_get_uit(const uves_propertylist * plist);
double uves_pfits_get_airmass_start(const uves_propertylist * plist);
double uves_pfits_get_airmass_end(const uves_propertylist * plist);
cpl_error_code uves_pfits_set_exptime(uves_propertylist *plist, double exptime);
void uves_pfits_set_dec(uves_propertylist *plist, double dec);
void uves_pfits_set_ra(uves_propertylist *plist, double ra);
void uves_pfits_set_firstabsorder(uves_propertylist *plist, int first_abs_order);
int uves_pfits_get_firstabsorder(const uves_propertylist * plist);
void uves_pfits_set_lastabsorder(uves_propertylist *plist, int last_abs_order);
int uves_pfits_get_lastabsorder(const uves_propertylist * plist);
double uves_pfits_get_offset(const uves_propertylist * plist);
void uves_pfits_set_traceid(uves_propertylist * plist, int trace_id);
void uves_pfits_set_offset(uves_propertylist * plist, double trace_offset);
int            uves_pfits_get_traceid(const uves_propertylist * plist);
void uves_pfits_set_windownumber(uves_propertylist * plist, int window_number);
int            uves_pfits_get_windownumber(const uves_propertylist * plist);
cpl_error_code uves_pfits_set_data_average(uves_propertylist * plist, double average);
cpl_error_code uves_pfits_set_data_stddev (uves_propertylist * plist, double stddev);
cpl_error_code uves_pfits_set_data_median (uves_propertylist * plist, double median);
cpl_error_code uves_pfits_set_data_min    (uves_propertylist * plist, double min);
cpl_error_code uves_pfits_set_data_max    (uves_propertylist * plist, double max);
cpl_error_code uves_pfits_set_object(uves_propertylist * plist, const char *object);
cpl_error_code uves_pfits_set_ordpred(uves_propertylist *plist, int nord);
cpl_error_code uves_pfits_set_badpixcorr(uves_propertylist * plist, const char *corr);
cpl_error_code uves_pfits_set_redlevel(uves_propertylist * plist, const char *redlevel);
cpl_error_code uves_pfits_set_status(uves_propertylist * plist, const char *status);
cpl_error_code uves_pfits_set_starttime(uves_propertylist * plist, const char *start_time);
cpl_error_code uves_pfits_set_stoptime(uves_propertylist * plist, const char *stop_time);
int            uves_pfits_get_datancom(const uves_propertylist * plist);
const char* uves_pfits_get_ccdid(const uves_propertylist * plist);
double uves_pfits_get_pressure(const uves_propertylist * plist);
double uves_pfits_get_tempcam(const uves_propertylist * plist,enum uves_chip chip);
double uves_pfits_get_humidity(const uves_propertylist * plist);
double uves_pfits_get_slitlength_pixels(const uves_propertylist * plist, enum uves_chip chip);
double uves_pfits_get_slitwidth(const uves_propertylist * plist, enum uves_chip chip);
double uves_pfits_get_slitlength(const uves_propertylist * plist, enum uves_chip chip);
double uves_pfits_get_gratwlen(const uves_propertylist * plist, enum uves_chip chip);
const char* uves_pfits_get_gratid(const uves_propertylist * plist, enum uves_chip chip);
const char* uves_pfits_get_gratname(const uves_propertylist * plist, enum uves_chip chip);
const char* uves_pfits_get_readspeed(const uves_propertylist * plist);

const char* uves_pfits_get_slit1_name(const uves_propertylist * plist);
const char* uves_pfits_get_bunit(const uves_propertylist * plist);
double uves_pfits_get_bscale(const uves_propertylist * plist);
const char* uves_pfits_get_ctype1(const uves_propertylist * plist);
const char* uves_pfits_get_ctype2(const uves_propertylist * plist);
const char* uves_pfits_get_cunit1(const uves_propertylist * plist);
const char* uves_pfits_get_cunit2(const uves_propertylist * plist);
int uves_pfits_get_bitpix(const uves_propertylist * plist);
int uves_pfits_get_naxis(const uves_propertylist * plist);
int uves_pfits_get_naxis1(const uves_propertylist * plist);
int uves_pfits_get_naxis2(const uves_propertylist * plist);
int uves_pfits_get_out1nx(const uves_propertylist * plist);
int uves_pfits_get_out1ny(const uves_propertylist * plist);
int uves_pfits_get_out4nx(const uves_propertylist * plist);
int uves_pfits_get_out4ny(const uves_propertylist * plist);
int uves_pfits_get_nx(const uves_propertylist * plist,enum uves_chip chip);
int uves_pfits_get_ny(const uves_propertylist * plist,enum uves_chip chip);

double uves_pfits_get_crval1(const uves_propertylist * plist);
double uves_pfits_get_crval2(const uves_propertylist * plist);
double uves_pfits_get_crpix1(const uves_propertylist * plist);
double uves_pfits_get_crpix2(const uves_propertylist * plist);
double uves_pfits_get_cdelt1(const uves_propertylist * plist);
double uves_pfits_get_cdelt2(const uves_propertylist * plist);
double uves_pfits_get_wstart(const uves_propertylist * plist, int order);
double uves_pfits_get_wend(const uves_propertylist * plist, int order);
double uves_pfits_get_wlen1(const uves_propertylist * plist);

cpl_error_code uves_pfits_set_wlen1(uves_propertylist *plist, double wlen1);
cpl_error_code uves_pfits_set_extname(uves_propertylist * plist, const char *extname);
cpl_error_code uves_pfits_set_bunit(uves_propertylist * plist, const char *bunit);
cpl_error_code uves_pfits_set_bscale(uves_propertylist * plist, const double bscale);
cpl_error_code uves_pfits_set_ctype1(uves_propertylist * plist, const char *ctype1);
cpl_error_code uves_pfits_set_ctype2(uves_propertylist * plist, const char *ctype2);
cpl_error_code uves_pfits_set_cunit1(uves_propertylist * plist, const char *ctype1);
cpl_error_code uves_pfits_set_cunit2(uves_propertylist * plist, const char *ctype2);
cpl_error_code uves_pfits_set_crval1(uves_propertylist * plist, double crval1);
cpl_error_code uves_pfits_set_crval2(uves_propertylist * plist, double crval2);
cpl_error_code uves_pfits_set_crpix1(uves_propertylist * plist, double crpix1);
cpl_error_code uves_pfits_set_crpix2(uves_propertylist * plist, double crpix2);
cpl_error_code uves_pfits_set_cdelt1(uves_propertylist * plist, double cdelt1);
cpl_error_code uves_pfits_set_cdelt2(uves_propertylist * plist, double cdelt2);
cpl_error_code uves_pfits_set_wstart(uves_propertylist * plist, int order, double wstart);
cpl_error_code uves_pfits_set_wend(uves_propertylist * plist, int order, double wend);

cpl_error_code uves_pfits_set_tunit_no(uves_propertylist * plist, int col_no, const char* value);
cpl_error_code uves_pfits_set_bunit_no(uves_propertylist * plist, int axis_no, const char* value);


void uves_pfits_set_hs(uves_propertylist * plist, int hs);
void uves_flames_pfits_set_ccfposmax(uves_propertylist *plist, double ccfposmax);
double uves_flames_pfits_get_dit(const uves_propertylist *raw_header);
int uves_flames_pfits_get_plateid(const uves_propertylist *raw_header);
void uves_flames_pfits_set_newplateid(uves_propertylist * plist, int plate_no);
int uves_flames_pfits_get_nflats(const uves_propertylist * plist);
const char * uves_flames_pfits_get_ident(const uves_propertylist * plist);
const char * uves_flames_pfits_get_object(const uves_propertylist * plist);
double uves_pfits_get_pixelscale(const uves_propertylist *plist);

#endif

/**@}*/
