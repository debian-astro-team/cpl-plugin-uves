/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2002,2003 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */

/*
 * $Author: amodigli $
 * $Date: 2013-08-08 13:36:46 $
 * $Revision: 1.9 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.8  2013/07/01 15:35:42  amodigli
 * Rename DEBUG to debug_mode to remove compiler error on some platforms (that name is reserved to special compiler options)
 *
 * Revision 1.7  2011/12/08 13:56:20  amodigli
 * include uves_cpl_size.h for CPL6
 *
 * Revision 1.6  2010/09/24 09:32:02  amodigli
 * put back QFITS dependency to fix problem spot by NRI on FIBER mode (with MIDAS calibs) data
 *
 * Revision 1.4  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.3  2007/04/24 12:50:29  jmlarsen
 * Replaced cpl_propertylist -> uves_propertylist which is much faster
 *
 * Revision 1.2  2007/02/09 13:36:40  jmlarsen
 * Added comment
 *
 * Revision 1.1  2007/02/08 11:38:39  jmlarsen
 * Added cd_align recipe
 *
 * Revision 1.4  2006/12/07 08:29:56  jmlarsen
 * Compute correct Ynew column for FLAMES
 *
 * Revision 1.3  2006/11/24 16:24:53  jmlarsen
 * Added window offset parameter
 *
 * Revision 1.2  2006/02/15 13:19:15  jmlarsen
 * Reduced source code max. line length
 *
 * Revision 1.1  2006/02/03 07:46:30  jmlarsen
 * Moved recipe implementations to ./uves directory
 *
 * Revision 1.8  2005/12/20 08:11:44  jmlarsen
 * Added CVS  entry
 *
 */
#ifndef UVES_CD_ALIGN_IMPL_H
#define UVES_CD_ALIGN_IMPL_H
#include <uves_cpl_size.h>
#include <uves_propertylist.h>
#include <uves_chip.h>
#include <cpl.h>
#include <stdbool.h>

/* For unit testing */
cpl_table *
uves_cd_align_process(const cpl_image *im1,
              const cpl_image *im2,
              const uves_propertylist *rotated_header1,
              const uves_propertylist *rotated_header2,
              int steps,
              int xborder,
              int window,
              bool debug_mode,
              enum uves_chip chip);
#endif
