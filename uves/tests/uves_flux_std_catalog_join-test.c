/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */
 
/*
 */

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <uves_error.h>
#include <cpl_test.h>
#include <cpl.h>
#include <uves_pfits.h>
#include <uves_msg.h>
#include <getopt.h>
/*-----------------------------------------------------------------------------
                                Defines
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                            Functions prototypes
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/**
 * @defgroup  uves_flux_std_catalog_update  UVES library unit tests
 */
/*----------------------------------------------------------------------------*/
/**@{*/
/**
@brief this function test the gaussian fitting
*/

/*----------------------------------------------------------------------------*/
/**
  @brief   Various tests of low-level library functions

  Currently, only 2d gaussian fitting (@c uves_fit_gaussian_2d which calls
  @c uves_fit_gaussian_1d_image which
  calls @c irplib_fit_gaussian_1d which calls Levenberg-Marquardt routine) is 
  tested.
**/
/*----------------------------------------------------------------------------*/

int main(int argc, char * const argv[])
{
    /* Initialize CPL + UVES messaging */
    cpl_test_init(PACKAGE_BUGREPORT, CPL_MSG_WARNING);

    int print_usage = 0;

 	int opt = 0;
 	const char* flux_std_cat_name=NULL;
 	const char* flux_std_add_name=NULL;
	int flux_std_cat_extid=2;
	int flux_std_add_extid=2;
	
 	uves_msg_warning("argc=%d",argc);
 	if (argc < 5)
 	{
 		print_usage = 1;
 	}
 	else
 	{
 		flux_std_cat_name = argv[1];
 		flux_std_add_name = argv[2];
 		flux_std_cat_extid = atoi(argv[3]);
 		flux_std_add_extid = atoi(argv[4]);
 		

 	}
 	uves_msg_warning("flux_std_cat_name=%s",flux_std_cat_name);
 	uves_msg_warning("flux_std_add_name=%s",flux_std_add_name);
 	uves_msg_warning("flux_std_cat_extid=%d",flux_std_cat_extid);
 	uves_msg_warning("flux_std_add_extid=%d",flux_std_add_extid);
	uves_msg_warning("ok1");
 	if (flux_std_cat_name == NULL || flux_std_add_name == NULL)
 	{
 		print_usage = 1;
 	}
	uves_msg_warning("ok2");
 	if(print_usage)
 	{
 		fprintf(stderr, "usage: %s cat_ref_name cat_add_name ref_extid add_extid \n", argv[0]);
 		return 0;
 	}
	uves_msg_warning("ok3");
	cpl_frame* frm1=cpl_frame_new();
	cpl_frame_set_filename(frm1, flux_std_cat_name);
	cpl_propertylist* phead=cpl_propertylist_load(flux_std_cat_name,0);
	cpl_propertylist* xhead;
	int next = cpl_frame_get_nextensions(frm1);
	cpl_table* tab;
	cpl_image* ima=cpl_image_load(flux_std_cat_name,CPL_TYPE_DOUBLE,0,0);
	cpl_image_save(ima,"new.fits",CPL_BPP_IEEE_FLOAT,phead,CPL_IO_DEFAULT);
	uves_msg_warning("ok4");
	for(int i=1;i<next;i++) {
	  tab=cpl_table_load(flux_std_cat_name,i,0);
	  xhead=cpl_propertylist_load(flux_std_cat_name,i);
	  if(i==flux_std_add_extid) {
              cpl_table* tab2=cpl_table_load(flux_std_add_name,flux_std_add_extid,0);
              int nrow=cpl_table_get_nrow(tab);
              cpl_table_insert(tab,tab2,nrow);
              cpl_table_delete(tab2);
	  }
          cpl_table_save(tab,phead,xhead,"new.fits",CPL_IO_EXTEND);
   	  cpl_table_delete(tab);
	  cpl_propertylist_delete(xhead);
	}
	uves_msg_warning("ok5");
	cpl_propertylist_delete(phead);
	cpl_frame_delete(frm1);
	uves_msg_warning("ok6");
	
  cleanup:
	uves_msg_warning("ok7");
    return cpl_test_end(0);
}


/**@}*/
