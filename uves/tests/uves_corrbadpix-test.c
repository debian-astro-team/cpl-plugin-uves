/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */
 
/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:31:49 $
 * $Revision: 1.7 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.5  2009/11/04 06:58:26  amodigli
 * fixed API error
 *
 * Revision 1.4  2009/06/05 05:49:02  amodigli
 * updated init/end to cpl5
 *
 * Revision 1.3  2007/05/23 06:43:23  jmlarsen
 * Removed unused variables
 *
 * Revision 1.2  2007/04/24 12:50:29  jmlarsen
 * Replaced cpl_propertylist -> uves_propertylist which is much faster
 *
 * Revision 1.1  2007/03/15 12:27:18  jmlarsen
 * Moved unit tests to ./uves/tests and ./flames/tests
 *
 * Revision 1.3  2007/02/27 14:04:14  jmlarsen
 * Move unit test infrastructure to IRPLIB
 *
 * Revision 1.2  2007/01/29 12:17:54  jmlarsen
 * Support setting verbosity from command line
 *
 * Revision 1.1  2007/01/16 10:25:11  jmlarsen
 * Added test of bad pixel correction
 *
 * Revision 1.1  2006/11/28 08:26:35  jmlarsen
 * Added QC log unit test
 *
 */

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <uves_corrbadpix.h>
#include <uves_chip.h>
#include <uves_utils_wrappers.h>
#include <uves_error.h>
#include <cpl_test.h>

#include <cpl.h>
/*-----------------------------------------------------------------------------
                                Defines
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                            Functions prototypes
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/**
 * @defgroup uves_corrbadpix_test  UVES bad pixel correction unit tests
 */
/*----------------------------------------------------------------------------*/
/**@{*/

/*----------------------------------------------------------------------------*/
/**
   @brief Call the functions which had many memory errors
 */
/*----------------------------------------------------------------------------*/
static void
test_bad_corr(void)
{
    cpl_image *master_bias = NULL;
    uves_propertylist *header = uves_propertylist_new();
    int **map = NULL;
    int mark_bad;
    int blue, binx, biny;
    
    /* Empty map */
    map = cpl_calloc(1, sizeof(int*));
    map[0] = cpl_calloc(4, sizeof(int));
    map[0][0] = -1;
    map[0][1] = -1;
    map[0][2] = -1;
    map[0][3] = -1;
    uves_badmap_free(&map);
    
    /* 2 row map */
    map = cpl_calloc(3, sizeof(int*));
    map[0] = cpl_calloc(4, sizeof(int));
    map[0][0] = 1;
    map[0][1] = 2;
    map[0][2] = 3;
    map[0][3] = 4;
    map[1] = cpl_calloc(4, sizeof(int));
    map[1][0] = 5;
    map[1][1] = 6;
    map[1][2] = 7;
    map[1][3] = 8;
    map[2] = cpl_calloc(4, sizeof(int));
    map[2][0] = -1;
    map[2][1] = -1;
    map[2][2] = -1;
    map[2][3] = -1;
    uves_badmap_free(&map);

    for (blue = 0; blue <= 1; blue++)
    for (binx = 1; binx <= 3; binx++)
    for (biny = 1; biny <= 3; biny++)
	if (!(
		(binx == 1 && biny == 3) ||
		(binx == 2 && biny == 1) ||
		(binx == 3 && biny == 1) ||
		(binx == 3 && biny == 2) ||
		(binx == 3 && biny == 3)
		)){
	    enum uves_chip chip;
	    for (chip = uves_chip_get_first(blue);
		 chip != UVES_CHIP_INVALID;
		 chip = uves_chip_get_next(chip))
		{
		    uves_free_image(&master_bias);
		    master_bias = cpl_image_new(4096/biny, 2048/binx, CPL_TYPE_DOUBLE);
		    assure_mem( master_bias );

		    for (mark_bad = 0; mark_bad <= 1; mark_bad++)
			{
			    int expected_size;

			    /* Call low level function */
			    check( map = uves_get_badpix(chip, binx, biny, mark_bad,0),
				   "Error getting bad pixel map");
			    uves_badmap_free(&map);    
			    
			    /* Call higher level wrapper */
			    uves_propertylist_empty(header);
			    check( uves_correct_badpix_all(master_bias, header,
							   chip,
							   binx, biny, mark_bad,0),
				   "Error in bad pixel correction");
			    
			    /* Should write one key:  correct = true   (or something like that) */
			    if (chip == UVES_CHIP_BLUE)
				{
				    expected_size = 0;
				}
			    else
				{
				    expected_size = 1;
				}
			    assure( uves_propertylist_get_size(header) == expected_size,
				    CPL_ERROR_ILLEGAL_OUTPUT, "Wrong output header size: %ld "
				    "(%d expected)",
				    uves_propertylist_get_size(header), expected_size);
			}

		}
	}

  cleanup:
    uves_badmap_free(&map);
    uves_free_propertylist(&header);
    uves_free_image(&master_bias);
    return;
}

   
/*----------------------------------------------------------------------------*/
/**
  @brief   Test of uves_corrbadpix
**/
/*----------------------------------------------------------------------------*/

int main(void)
{
    /* Initialize CPL + UVES messaging */
    cpl_test_init(PACKAGE_BUGREPORT, CPL_MSG_WARNING);

    check( test_bad_corr(),
	   "Test failed");

  cleanup:
    return cpl_test_end(0);
}


/**@}*/
