/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */
 
/*
 */

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <uves_error.h>
#include <cpl_test.h>
#include <cpl.h>
#include <uves_pfits.h>

/*-----------------------------------------------------------------------------
                                Defines
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                            Functions prototypes
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/**
 * @defgroup uves_test  UVES library unit tests
 */
/*----------------------------------------------------------------------------*/
/**@{*/
/**
@brief this function test the gaussian fitting
*/
static cpl_error_code
uves_save_vector(void)
{
    cpl_image* ima;
    int nx=100;
    int ny=100;
    cpl_propertylist* plist;

    ima = cpl_image_new(nx,ny,CPL_TYPE_DOUBLE);
    cpl_image_add_scalar(ima,10.);
    plist=cpl_propertylist_new();
    uves_pfits_set_bunit(plist,"erg/s");
    uves_pfits_set_bscale(plist,1e-16);
    if(uves_pfits_get_bunit(plist)) {
        uves_msg("found bunit");
        uves_msg_warning("found bunit");
    }
    cpl_image_save(ima, "ima.fits", CPL_TYPE_DOUBLE, plist, CPL_IO_CREATE);

    cpl_image_delete(ima);
    cpl_propertylist_delete(plist);

}
/*----------------------------------------------------------------------------*/
/**
  @brief   Various tests of low-level library functions

  Currently, only 2d gaussian fitting (@c uves_fit_gaussian_2d which calls
  @c uves_fit_gaussian_1d_image which
  calls @c irplib_fit_gaussian_1d which calls Levenberg-Marquardt routine) is 
  tested.
**/
/*----------------------------------------------------------------------------*/

int main(void)
{
    /* Initialize CPL + UVES messaging */
    cpl_test_init(PACKAGE_BUGREPORT, CPL_MSG_WARNING);

    uves_save_vector();
    //uves_save_table();
    //uves_save_image();
    //uves_save_imagelist();


  cleanup:
    return cpl_test_end(0);
}


/**@}*/
