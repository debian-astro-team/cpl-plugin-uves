/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */
 
/*
 * $Author: amodigli $
 * $Date: 2013-08-08 13:36:14 $
 * $Revision: 1.6 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.5  2013/07/02 12:42:30  amodigli
 * Rename DEBUG to debug_mode to remove compiler error on some platforms (that name is reserved to special compiler options)
 *
 * Revision 1.4  2009/06/05 05:49:02  amodigli
 * updated init/end to cpl5
 *
 * Revision 1.3  2007/08/30 07:56:05  amodigli
 * fixed some doxygen warnings
 *
 * Revision 1.2  2007/05/23 06:43:23  jmlarsen
 * Removed unused variables
 *
 * Revision 1.1  2007/03/15 12:27:18  jmlarsen
 * Moved unit tests to ./uves/tests and ./flames/tests
 *
 * Revision 1.2  2007/02/27 14:04:14  jmlarsen
 * Move unit test infrastructure to IRPLIB
 *
 * Revision 1.1  2007/02/08 11:38:56  jmlarsen
 * Added cd_align recipe
 *
 * Revision 1.5  2007/01/31 15:11:09  jmlarsen
 * Test of inf+nan when saving FITS files
 */

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <uves_cd_align_impl.h>
#include <uves_utils_wrappers.h>
#include <uves_error.h>
#include <cpl_test.h>

#include <cpl.h>

#include <float.h>
/*-----------------------------------------------------------------------------
                                Defines
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                            Functions prototypes
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/**
 * @defgroup uves_dfs  UVES CD align unit tests
 */
/*----------------------------------------------------------------------------*/
/**@{*/

/*----------------------------------------------------------------------------*/
/**
   @brief      evaluate Gaussian
   @param      x      where
   @param      my     centroid
   @param      sigma  stdev
   @param      norm   normalization
   @param      back   constant term
 */
/*----------------------------------------------------------------------------*/
static double eval_gauss(double x, double my, double sigma, double norm, double back)
{
    double result;
    double a[5];
    double xa[1];

    xa[0] = x;

    a[0] = my;
    a[1] = sigma;
    a[2] = norm;
    a[3] = back;
    a[4] = 0.01;  /* linear background term */

    /* Use moffat, to have something that is not perfectly Gaussian  */
    assure( uves_moffat(xa, a, &result) == 0,
	    CPL_ERROR_ILLEGAL_OUTPUT,
	    "Moffat evalutation failed");
    
  cleanup:
    return result;
}


/*----------------------------------------------------------------------------*/
/**
   @brief      test core processing part of CD align recipe
 */
/*----------------------------------------------------------------------------*/
static void
test_process(void)
{
    const int nx = 100;
    const int ny = 100;
    const double maxrow = 61.1;
    const double sigma = 2;
    const double norm = 6000;
    const double background = 200;
    cpl_image *im[2] = {NULL, NULL};
    cpl_table *cd_align = NULL;
    double shift;

    /* Create data */
    im[0] = cpl_image_new(nx, ny, CPL_TYPE_DOUBLE);
    im[1] = cpl_image_new(nx, ny, CPL_TYPE_DOUBLE);

    assure_mem( im[0] );
    assure_mem( im[1] );



    for (shift = -20; shift < 5; shift = (shift < -5) ? shift/1.5 : shift + 0.7)
	{
	    int x, y;
	    for (y = 1; y <= ny; y++)
		{
		    for (x = 1; x <= nx; x++)
			{
			    cpl_image_set(im[0], x, y,
					  eval_gauss(y, maxrow, sigma, norm, background));
			    cpl_image_set(im[1], x, y,
					  eval_gauss(y, maxrow+shift, sigma, norm, background));
			}
		}
	    
	    /* Call function */
	    {
		int steps = 10;
		int xborder = 0;
		int window = 20;
		
		bool debug_mode = false;
		enum uves_chip chip = UVES_CHIP_BLUE;   /* not used */
		
		uves_free_table(&cd_align);
		check( cd_align = uves_cd_align_process(
			   im[0],
			   im[1],
			   NULL, NULL,
			   steps, xborder, window, debug_mode, chip),
		       "Processing failed");
	    }

	    /* Check results */
	    assure_nomsg( cpl_table_has_column(cd_align, "X"    ), CPL_ERROR_ILLEGAL_OUTPUT);
	    assure_nomsg( cpl_table_has_column(cd_align, "YCEN1"), CPL_ERROR_ILLEGAL_OUTPUT);
	    assure_nomsg( cpl_table_has_column(cd_align, "YCEN2"), CPL_ERROR_ILLEGAL_OUTPUT);
	    assure_nomsg( cpl_table_has_column(cd_align, "SIGMA1"), CPL_ERROR_ILLEGAL_OUTPUT);
	    assure_nomsg( cpl_table_has_column(cd_align, "SIGMA2"), CPL_ERROR_ILLEGAL_OUTPUT);
	    assure_nomsg( cpl_table_has_column(cd_align, "BACK1"), CPL_ERROR_ILLEGAL_OUTPUT);
	    assure_nomsg( cpl_table_has_column(cd_align, "BACK2"), CPL_ERROR_ILLEGAL_OUTPUT);
	    assure_nomsg( cpl_table_has_column(cd_align, "NORM1"), CPL_ERROR_ILLEGAL_OUTPUT);
	    assure_nomsg( cpl_table_has_column(cd_align, "NORM2"), CPL_ERROR_ILLEGAL_OUTPUT);
	    assure_nomsg( cpl_table_has_column(cd_align, "YDIFF"), CPL_ERROR_ILLEGAL_OUTPUT);
	    
	    uves_msg("Shift: %f pixels. Measured shift: %f pixels",
		     shift, cpl_table_get_column_mean(cd_align, "YDIFF"));

	    {
		double abs_tolerance = 0.1; /* pixels */

                cpl_test_rel(cpl_table_get_column_mean(cd_align, "YDIFF"), 
                                shift, abs_tolerance);
	    }
    
	} /* for shift */

  cleanup:
    uves_free_image(&im[0]);
    uves_free_image(&im[1]);
    uves_free_table(&cd_align);

    return;
}

/*----------------------------------------------------------------------------*/
/**
  @brief   Test of uves_cal_cdalign recipe
**/
/*----------------------------------------------------------------------------*/

int main(void)
{
    /* Initialize CPL + UVES messaging */
    cpl_test_init(PACKAGE_BUGREPORT, CPL_MSG_WARNING);

    check( test_process(),
	   "Test of CD align failed");

  cleanup:
    return cpl_test_end(0);
}


/**@}*/
