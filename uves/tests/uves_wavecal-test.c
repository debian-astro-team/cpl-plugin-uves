/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */
 
/*
 * $Author: amodigli $
 * $Date: 2009-06-05 05:49:02 $
 * $Revision: 1.4 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.3  2007/08/13 12:15:43  amodigli
 * support of CPL4
 *
 * Revision 1.2  2007/05/23 06:43:23  jmlarsen
 * Removed unused variables
 *
 * Revision 1.1  2007/03/15 12:27:18  jmlarsen
 * Moved unit tests to ./uves/tests and ./flames/tests
 *
 * Revision 1.3  2007/02/27 14:04:14  jmlarsen
 * Move unit test infrastructure to IRPLIB
 *
 * Revision 1.2  2007/01/29 12:17:54  jmlarsen
 * Support setting verbosity from command line
 *
 * Revision 1.1  2006/11/28 08:26:35  jmlarsen
 * Added QC log unit test
 *
 */

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <uves_wavecal_identify.h>
#include <uves_wavecal_utils.h>
#include <uves_utils_wrappers.h>
#include <uves_error.h>
#include <cpl_test.h>

#include <cpl.h>
#include <math.h>
/*-----------------------------------------------------------------------------
                                Defines
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                            Functions prototypes
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/**
 * @defgroup uves_wavecal_test  UVES wavelength calibration unit tests
 */
/*----------------------------------------------------------------------------*/
/**@{*/

/*----------------------------------------------------------------------------*/
/**
   @brief test point pattern matching
 */
/*----------------------------------------------------------------------------*/
static void
test_ppm(void)
{
    int nlines = 50;
    int minorder = 94;
    int maxorder = 97;
    const int norders = maxorder - minorder + 1;
    int i, order;

    cpl_table *linetable  = cpl_table_new(norders * nlines);
    cpl_table *line_refer = cpl_table_new(nlines);

    cpl_table_new_column(line_refer, "Wave", CPL_TYPE_DOUBLE);
 
    for (i = 0; i < nlines; i++)
        {
            cpl_table_set_double(line_refer, "Wave", i, 1000 + 500*sin(i));
        }

    cpl_test_eq( cpl_error_get_code(), CPL_ERROR_NONE );
    
    cpl_table_new_column(linetable, "X"              , CPL_TYPE_DOUBLE);
    cpl_table_new_column(linetable, "Order"          , CPL_TYPE_INT);
    cpl_table_new_column(linetable, LINETAB_LAMBDAC  , CPL_TYPE_DOUBLE);
    cpl_table_new_column(linetable, LINETAB_PIXELSIZE, CPL_TYPE_DOUBLE);
    
    for (order = minorder; order <= maxorder; order++)
        {
            for (i = 0; i < nlines; i++)
                {
                    int row = (order-minorder)*nlines + i;

                    cpl_table_set_int(linetable, "Order", row, 
                                      order);

                    cpl_table_set_double(linetable, LINETAB_PIXELSIZE, row, 1);

                    /* Set X = catalogue wavelength */
                    cpl_table_set_double(linetable, "X", row, 
                                         cpl_table_get_double(line_refer, "Wave", i, NULL));
                    
                    cpl_table_set_double(linetable, LINETAB_LAMBDAC, row, 
                                         cpl_table_get_double(line_refer, "Wave", i, NULL));
                }
        }

    cpl_test_eq( cpl_error_get_code(), CPL_ERROR_NONE );

/*
  cpl_table_dump_structure(linetable, stdout);
  cpl_table_dump(linetable, 0, cpl_table_get_nrow(linetable), stdout);
  cpl_table_dump(line_refer, 0, cpl_table_get_nrow(line_refer), stdout);
*/

    uves_wavecal_identify_lines_ppm(linetable, 
                                    line_refer);

    cpl_test( cpl_table_has_column(linetable, "Ident_ppm") );

    uves_free_table(&linetable);
    uves_free_table(&line_refer);    

    return;
}

   
/*----------------------------------------------------------------------------*/
/**
  @brief   Test of uves_wavecal
**/
/*----------------------------------------------------------------------------*/

int main(void)
{
    cpl_test_init(PACKAGE_BUGREPORT, CPL_MSG_WARNING);
    cpl_errorstate initial_errorstate = cpl_errorstate_get();
    test_ppm();

    if (cpl_error_get_code() != CPL_ERROR_NONE)
        {
	  cpl_errorstate_dump(initial_errorstate,CPL_FALSE,NULL);
        }
    return cpl_test_end(0);
}


/**@}*/
