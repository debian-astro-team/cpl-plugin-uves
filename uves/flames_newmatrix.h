/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jtaylor $
 * $Date: 2012-11-19 09:16:16 $
 * $Revision: 1.9 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.8  2010/09/24 09:32:02  amodigli
 * put back QFITS dependency to fix problem spot by NRI on FIBER mode (with MIDAS calibs) data
 *
 * Revision 1.6  2009/04/14 07:01:07  amodigli
 * added to CVS (moded from flames tree)
 *
 * Revision 1.8  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.7  2007/06/06 07:22:20  jmlarsen
 * Changed return type of nrerror()
 *
 * Revision 1.6  2007/05/07 06:52:17  amodigli
 * fixed compilation warnings
 *
 * Revision 1.5  2007/01/10 11:08:34  jmlarsen
 * Don't define DRS_USE_ORDEF
 *
 * Revision 1.4  2006/10/17 12:33:42  jmlarsen
 * Moved FLAMES source to flames directory
 *
 * Revision 1.4  2006/10/12 11:59:23  jmlarsen
 * Conform to source code template
 *
 * Revision 1.4  2006/08/17 13:56:52  jmlarsen
 * Reduced max line length
 *
 * Revision 1.3  2006/02/28 09:15:22  jmlarsen
 * Minor update
 *
 * Revision 1.2  2005/12/19 16:17:56  jmlarsen
 * Replaced bool -> int
 *
 */
#ifndef FLAMES_NEWMATRIX_H
#define FLAMES_NEWMATRIX_H

#include <flames_uves.h>

void nrerror(const char* error_text);
float *vector(int32_t nl, int32_t nh);
int *ivector(int32_t nl, int32_t nh);
unsigned int *uivector(int32_t nl, int32_t nh);
char *cvector(int32_t nl, int32_t nh);
unsigned char *ucvector(int32_t nl, int32_t nh);
int32_t *lvector(int32_t nl, int32_t nh);
uint32_t *ulvector(int32_t nl, int32_t nh);
double *dvector(int32_t nl, int32_t nh);
frame_data *fdvector(int32_t nl, int32_t nh);
frame_mask *fmvector(int32_t nl, int32_t nh);
float **matrix(int32_t nrl, int32_t nrh, int32_t ncl, int32_t nch);
char **cmatrix(int32_t nrl, int32_t nrh, int32_t ncl, int32_t nch);
double **dmatrix(int32_t nrl, int32_t nrh, int32_t ncl, int32_t nch);
int **imatrix(int32_t nrl, int32_t nrh, int32_t ncl, int32_t nch);
uint32_t **ulmatrix(int32_t nrl, int32_t nrh, int32_t ncl, int32_t nch);
int32_t **lmatrix(int32_t nrl, int32_t nrh, int32_t ncl, int32_t nch);
frame_data **fdmatrix(int32_t nrl, int32_t nrh, int32_t ncl, int32_t nch);
frame_mask **fmmatrix(int32_t nrl, int32_t nrh, int32_t ncl, int32_t nch);
float **submatrix(float **a, int32_t oldrl, int32_t oldrh, int32_t oldcl,
    int32_t newrl, int32_t newcl);
float **convert_matrix(float *a, int32_t nrl, int32_t nrh, int32_t ncl, int32_t nch);
float ***f3tensor(int32_t nrl, int32_t nrh, int32_t ncl, int32_t nch, int32_t ndl, int32_t ndh);
double ***d3tensor(int32_t nrl, int32_t nrh, int32_t ncl, int32_t nch, int32_t ndl, int32_t ndh);
frame_data ***fd3tensor(int32_t nrl, int32_t nrh, int32_t ncl, int32_t nch, int32_t ndl, 
            int32_t ndh);
frame_mask ***fm3tensor(int32_t nrl, int32_t nrh, int32_t ncl, int32_t nch, int32_t ndl, 
            int32_t ndh);
uint32_t ***ul3tensor(int32_t nrl, int32_t nrh, int32_t ncl, int32_t nch, 
                   int32_t ndl, int32_t ndh);
int32_t ***l3tensor(int32_t nrl, int32_t nrh, int32_t ncl, int32_t nch, int32_t ndl, 
             int32_t ndh);
int32_t ****l4tensor(int32_t nal, int32_t nah, int32_t nrl, int32_t nrh, int32_t ncl, 
              int32_t nch, int32_t ndl, int32_t ndh);
void free_vector(float *v, int32_t nl, int32_t nh);
void free_ivector(int *v, int32_t nl, int32_t nh);
void free_uivector(unsigned int *v, int32_t nl, int32_t nh);
void free_cvector(char *v, int32_t nl, int32_t nh);
void free_ucvector(unsigned char *v, int32_t nl, int32_t nh);
void free_lvector(int32_t *v, int32_t nl, int32_t nh);
void free_ulvector(uint32_t *v, int32_t nl, int32_t nh);
void free_dvector(double *v, int32_t nl, int32_t nh);
void free_fdvector(frame_data *v, int32_t nl, int32_t nh);
void free_fmvector(frame_mask *v, int32_t nl, int32_t nh);
void free_matrix(float **m, int32_t nrl, int32_t nrh, int32_t ncl, int32_t nch);
void free_cmatrix(char **m, int32_t nrl, int32_t nrh, int32_t ncl, int32_t nch);
void free_dmatrix(double **m, int32_t nrl, int32_t nrh, int32_t ncl, int32_t nch);
void free_imatrix(int **m, int32_t nrl, int32_t nrh, int32_t ncl, int32_t nch);
void free_ulmatrix(uint32_t **m, int32_t nrl, int32_t nrh, int32_t ncl, 
           int32_t nch);
void free_lmatrix(int32_t **m, int32_t nrl, int32_t nrh, int32_t ncl, int32_t nch);
void free_fdmatrix(frame_data **m, int32_t nrl, int32_t nrh, int32_t ncl, int32_t nch);
void free_fmmatrix(frame_mask **m, int32_t nrl, int32_t nrh, int32_t ncl, int32_t nch);
void free_submatrix(float **b, int32_t nrl, int32_t nrh, int32_t ncl, int32_t nch);
void free_convert_matrix(float **b, int32_t nrl, int32_t nrh, int32_t ncl, int32_t nch);
void free_f3tensor(float ***t, int32_t nrl, int32_t nrh, int32_t ncl, int32_t nch,
    int32_t ndl, int32_t ndh);
void free_d3tensor(double ***t, int32_t nrl, int32_t nrh, int32_t ncl, int32_t nch,
    int32_t ndl, int32_t ndh);
void free_fd3tensor(frame_data ***t, int32_t nrl, int32_t nrh, int32_t ncl, int32_t nch,
    int32_t ndl, int32_t ndh);
void free_fm3tensor(frame_mask ***t, int32_t nrl, int32_t nrh, int32_t ncl, int32_t nch,
    int32_t ndl, int32_t ndh);
void free_ul3tensor(uint32_t ***t, int32_t nrl, int32_t nrh, int32_t ncl, 
            int32_t nch,
    int32_t ndl, int32_t ndh);
void free_l3tensor(int32_t ***t, int32_t nrl, int32_t nrh, int32_t ncl, int32_t nch,
    int32_t ndl, int32_t ndh);
void free_l4tensor(int32_t ****t, int32_t nal, int32_t nah, int32_t nrl, int32_t nrh, 
           int32_t ncl, int32_t nch, int32_t ndl, int32_t ndh);
void matrix_product(double **, double **, double **, int , int , int );
void matrix_sum(double **, double **, int , int );


#endif
