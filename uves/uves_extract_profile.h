/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:03 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.3  2010/02/13 12:22:31  amodigli
 * removed inlines (let's do work to compiler)
 *
 * Revision 1.2  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.1  2007/05/02 13:42:23  jmlarsen
 * Added header
 *
 * Revision 1.28  2007/04/24 12:50:29  jmlarsen
 * Replaced cpl_propertylist -> uves_propertylist which is much faster
 *
 * Revision 1.27  2006/11/15 14:04:08  jmlarsen
 * Removed non-const version of parameterlist_get_first/last/next which is already
 * in CPL, added const-safe wrapper, unwrapper and deallocator functions
 *
 * Revision 1.26  2006/09/19 07:15:35  jmlarsen
 * Added chip to argument list of uves_extract()
 *
 * Revision 1.25  2006/08/17 13:56:53  jmlarsen
 * Reduced max line length
 *
 * Revision 1.24  2006/05/16 12:13:07  amodigli
 * added QC log
 *
 * Revision 1.23  2006/05/12 15:04:13  jmlarsen
 * Changed gauss/moffat/virtual profile measuring methods to use 
 * global polynomials (rather than one polynomial per order)
 *
 * Revision 1.22  2006/04/24 09:21:18  jmlarsen
 * Implemented virtual resampling algorithm
 *
 * Revision 1.21  2006/02/28 09:15:22  jmlarsen
 * Minor update
 *
 * Revision 1.20  2005/12/19 16:17:56  jmlarsen
 * Replaced bool -> int
 *
 */
#ifndef UVES_EXTRACT_PROFILE_H
#define UVES_EXTRACT_PROFILE_H

#include <uves_extract_iterate.h>

typedef struct _uves_extract_profile uves_extract_profile;

uves_extract_profile *uves_extract_profile_new(
    int (*f)   (const double x[], const double a[], double *result),
    int (*dfda)(const double x[], const double a[], double result[]),
    int M,
    double slit_length,
    int sampling_factor);

uves_extract_profile *
uves_extract_profile_new_constant(double slit_length);

void uves_extract_profile_delete(uves_extract_profile **p);

double
uves_extract_profile_evaluate(const uves_extract_profile *profile,
                              const uves_iterate_position *pos);

void uves_extract_profile_set(const uves_extract_profile *p, 
                  uves_iterate_position *pos,
                  int *warnings);
double
uves_extract_profile_get_y(uves_iterate_position *pos,
               double bin,
               int sampling_factor);

double
uves_extract_profile_get_bin(const uves_iterate_position *pos,
                 int sampling_factor);

int uves_extract_profile_get_nbins(double slit_length, int sampling_factor);




/* This should be defined to 0, and is used only for testing.
 * If set to 1 the profile is measured order by order (like the MIDAS algorithm
 * which is less robust)
 */
#define ORDER_PER_ORDER 0

/* The spatial profile   @cond 
   fixme: avoid exporting this definition,
   currently needed for uves_extract module
*/
struct _uves_extract_profile
{
    /* There are three types of profiles.
     *
     * If constant == true
     *    assume a constant spatial profile
     *    only the member current_area is used
     * else if f != NULL  (zero resampling)
     *     y0(x,m) and sigma(x,m) are the parameters
     *     of the assumed profile (gaussian/moffat)
     *     red_chisq(x,m) is the smoothed reduced
     *     chi^2 of the fits
     * 
     * else if f == NULL  (virtual resampling)
     *     dy is an array of polynomials.
     *     dy_i(x,m) is the profile of the i'th 
     *     spatial bin. 
     */

    bool constant;

    int (*f)   (const double x[], const double a[], double *result);
    int (*dfda)(const double x[], const double a[], double result[]);
    int M;

#if ORDER_PER_ORDER
    polynomial **y0;   /* Polynomial for each order */
    polynomial **sigma;
    polynomial **red_chisq;
#else
    polynomial *y0;
    polynomial *sigma;
    polynomial *red_chisq;
#endif

    double current_y0;      /* This is for a "performance hack",      */
    double current_sigma;   /* so that we don't have to evaluate the  */
    double current_area;    /* polynomials too often. See also
                               uves_extract_profile_set().            */

    /* Virtual resampling */
    int spatial_bins;
    double slit_length;
    int sampling_factor;


    bool *is_zero_degree;  /* For efficiency: use polynomials or simple doubles
                              as necessary */
    polynomial **dy_poly;  /* Polynomial for each position along slit */
    double *dy_double;

    double *current_profile;   /* Array with profile at current x */
    double *current_ypos;      /* Array with y-positions where profile is known,
                                  at current x */
    double *current_interpolated;    /* Interpolated profile at current x */

};
/* @endcond */

#endif
