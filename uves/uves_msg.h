/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:04 $
 * $Revision: 1.14 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.12  2006/02/28 09:15:22  jmlarsen
 * Minor update
 *
 * Revision 1.11  2006/02/21 14:26:54  jmlarsen
 * Minor changes
 *
 * Revision 1.10  2005/12/19 16:17:56  jmlarsen
 * Replaced bool -> int
 *
 */
#ifndef UVES_MSG_H
#define UVES_MSG_H

#include <uves_utils.h>

/* Nothing bad happens if user also calls cpl_msg_info()
 * but maybe prevent it as a service to the user of this module
#define cpl_msg_info(...)  use__uves_msg__instead__of__cpl_msg_info
#define cpl_msg_indent()
 */

/*----------------------------------------------------------------------------*/
/**
 * @addtogroup uves_msg
 *
 */
/*----------------------------------------------------------------------------*/
/**@{*/

/*----------------------------------------------------------------------------*/
/**
   @brief    Print an error message
   @param    ...             Message to print

   This function is used instead of @c cpl_msg_error(), and saves
   the user from typing the calling function name.
*/
/*----------------------------------------------------------------------------*/
#define uves_msg_error(...) cpl_msg_error(__func__, __VA_ARGS__)

/*----------------------------------------------------------------------------*/
/**
   @brief    Print a progress message
   @param    i           See @c cpl_msg_progress()
   @param    iter        See @c cpl_msg_progress()
   @param    ...         Message to print

   This function is used instead of @c cpl_msg_progress(), and saves
   the user from typing the calling function name.
*/
/*----------------------------------------------------------------------------*/
#define uves_msg_progress(i, iter, ...) cpl_msg_progress(__func__, (i), (iter), __VA_ARGS__)
/*----------------------------------------------------------------------------*/
/**
   @brief    Print an warning message
   @param    ...             Message to print

   This function is used instead of @c cpl_msg_warning(), and saves
   the user from typing the calling function name.
*/
/*----------------------------------------------------------------------------*/
#define uves_msg_warning(...) uves_msg_warning_macro(__func__, __VA_ARGS__)
/*----------------------------------------------------------------------------*/
/**
   @brief    Print a debug message
   @param    ...             Message to print

   This function is used instead of @c cpl_msg_debug(), and saves
   the user from typing the calling function name.
*/
/*----------------------------------------------------------------------------*/
#define uves_msg_debug(...) cpl_msg_debug(__func__, __VA_ARGS__)

/*----------------------------------------------------------------------------*/
/**
   @brief    Print a message on a lower message level
   @param    ...             Message to print
*/
/*----------------------------------------------------------------------------*/
#define uves_msg_low(...)  do {                     \
                           uves_msg_softer();       \
                           uves_msg(__VA_ARGS__);   \
                           uves_msg_louder();       \
                           } while (false)

/*----------------------------------------------------------------------------*/
/**
   @brief    Print a message on 'info' or 'debug' level
   @param    ...             Message to print

   See also @c uves_msg_macro().
*/
/*----------------------------------------------------------------------------*/
#define uves_msg(...) uves_msg_macro(__func__, __VA_ARGS__)

/*----------------------------------------------------------------------------*/
/**
   @brief    Decrease message volume
*/
/*----------------------------------------------------------------------------*/
#define uves_msg_softer() uves_msg_softer_macro(__func__)

/*----------------------------------------------------------------------------*/
/**
   @brief    Increase message volume
*/
/*----------------------------------------------------------------------------*/
#define uves_msg_louder() uves_msg_louder_macro(__func__)


void uves_msg_init(int olevel, const char *dom);


void uves_msg_set_level(int olevel);

const char *uves_msg_get_domain(void);
void uves_msg_set_domain(const char *d);
void uves_msg_macro(const char *fct, const char *format, ...)
#ifdef __GNUC__
__attribute__((format (printf, 2, 3)))
#endif
;

void uves_msg_warning_macro(const char *fct, const char *format, ...)
#ifdef __GNUC__
__attribute__((format (printf, 2, 3)))
#endif
;

int uves_msg_get_warnings(void);
void uves_msg_add_warnings(int n);

void uves_msg_softer_macro(const char *fct);
void uves_msg_louder_macro(const char *fct);

#endif /* UVES_MSG_H */

/**@}*/
