/*                                                                            *
 *   This file is part of the ESO UVES Pipeline                               *
 *   Copyright (C) 2004,2005 European Southern Observatory                    *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2011-12-08 14:05:02 $
 * $Revision: 1.21 $
 * $Name: not supported by cvs2svn $
 */

/*----------------------------------------------------------------------------*/
/**
 * @addtogroup uves_physmod
 */
/*----------------------------------------------------------------------------*/
/**@{*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/
#include <uves_physmod_msrawxy.h>

#include <uves_physmod_center_gauss.h>
#include <uves_pfits.h>
#include <uves_utils_polynomial.h>
#include <uves_utils_wrappers.h>
#include <uves_msg.h>
#include <uves_error.h>

/*-----------------------------------------------------------------------------
                            Functions code
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
  @brief    This procedure measures the raw XY positions and widths
        of Th-Ar lines on an ThAr arc echelle exposure. 
        It computes the differences with the predicted positions

  @param    raw_image    The input raw frame (splitted)
  @param    raw_header   An array containing the input image headers. 
                         The ordering must be the same as the ordering 
                         of images in the input image list
  @param    recipe_id    name of calling recipe
  @param    parameters   The recipe parameter list
  @param    mod_tbl      Input model table from uves_physmod_create_table()
  @param    l_tbl        Temporary table
  @param    m_tbl        Temporary table
  @param    p_tbl        Temporary table
  @param    rline_tbl    Temporary table with predicted line positions
  @param    mline_tbl    Temporary table with corrected predicted line positions
  @param    npline_tbl   Temporary table

  @return   0 if everything is ok, -1 otherwise

  This function creates tables rline_tbl containing predicted positions
  (XMOD,YMOD) and the differences, fits with a polynomial of degree 2,2 
  on X,Ythis difference depending on the positions (X,Y), computes 
  the projection map with a polynomial fit of degree 2,2 on X,Y and save 
  them in the table npline_tbl.


 */
/*----------------------------------------------------------------------------*/


int uves_physmod_msrawxy(const cpl_image *raw_image, 
             const uves_propertylist *raw_header, 
             const char *recipe_id,
             const cpl_parameterlist* parameters, 
             cpl_table* mod_tbl,
             cpl_table** l_tbl,
             cpl_table** m_tbl,
             cpl_table** p_tbl,
             cpl_table** rline_tbl,
             cpl_table** mline_tbl,
             cpl_table** npline_tbl)
    
{
  double scl_fct=0;
  double add_fct=0;
  double      X_AXIS_SCALE=0;    
  double      Y_AXIS_SCALE=0;
  double      OFFSET_X=0;    
  double      OFFSET_Y=0;


  int status=0;


  double limit[4]={0.,0.,0.,0.};
  int      MBOX_X=0;
  int      MBOX_Y=0;
 
  double tmp_x=0;
  double tmp_y=0;
  double tmp_val_x=0;
  double tmp_val_y=0;



  polynomial* poly2d_x=NULL;
  polynomial* poly2d_y=NULL;
  double mean_err_xdif=0;
  double mean_err_ydif=0;
 
  double binx=0;
  double biny=0;

  double mbox_x=0;
  double mbox_y=0;
  double outputd[3][3];
  int k=0;

  cpl_table * tmp_tbl=NULL;
  cpl_table * tmp_tbl1=NULL;
  cpl_table * tmp_tbl2=NULL;
  cpl_table * tmp_tbl3=NULL;
  cpl_table * tmp_tbl4=NULL;
 
  uves_msg_debug("start msrawxy");

  check (biny = uves_pfits_get_binx(raw_header),
	 "Could not read x binning factor from input header");

  check (binx = uves_pfits_get_biny(raw_header), 
     "Could not read x binning factor from input header");

  check( uves_get_parameter(parameters, NULL, recipe_id, 
           "mbox_x", CPL_TYPE_INT, &MBOX_X )  , 
       "Could not read parameter");

  check( uves_get_parameter(parameters, NULL, recipe_id, 
           "mbox_y", CPL_TYPE_INT, &MBOX_Y )  , 
       "Could not read parameter");

  mbox_x=MBOX_X/binx;
  mbox_y=MBOX_Y/biny;

    /* IN MIDAS we always use the default offset=0,0
    check( uves_get_parameter(parameters, NULL, recipe_id, 
           "offset_x", CPL_TYPE_DOUBLE, &OFFSET_X )  , 
       "Could not read parameter");

    check( uves_get_parameter(parameters, NULL, recipe_id, 
           "offset_y", CPL_TYPE_DOUBLE, &OFFSET_Y )  ,
       "Could not read parameter");

    */
    /* msrawxy specific pars */
    /* Removed params x_axis_scale,y_axis_scale, always set to 0 */
    /*
    check( uves_get_parameter(parameters, NULL, recipe_id, 
           "x_axis_scale", CPL_TYPE_DOUBLE, &X_AXIS_SCALE )  , 
           "Could not read parameter");

    check( uves_get_parameter(parameters, NULL, recipe_id, 
           "y_axis_scale", CPL_TYPE_DOUBLE, &Y_AXIS_SCALE ),
       "Could not read parameter");
    */

    check(cpl_table_and_selected_double(mod_tbl,"IDENT",CPL_GREATER_THAN,0),
          "Error selecting IDENT");
  *l_tbl=cpl_table_extract_selected(mod_tbl);

  uves_msg_debug("xs=%f ofx=%f bx=%f",(1.+X_AXIS_SCALE),OFFSET_X,mbox_x/2);
  uves_msg_debug("ys=%f ofy=%f by=%f",(1.+Y_AXIS_SCALE),OFFSET_Y,mbox_y/2);

  scl_fct=(1.+X_AXIS_SCALE);
  cpl_table_erase_column(*l_tbl,"XSTART");
  cpl_table_duplicate_column(*l_tbl,"XSTART",*l_tbl,"XMOD");
  cpl_table_multiply_scalar(*l_tbl,"XSTART",scl_fct);
  add_fct=OFFSET_X-(double)mbox_x/2;

  uves_msg_debug("add_fct=%f",add_fct);
  cpl_table_add_scalar(*l_tbl,"XSTART",add_fct);

  cpl_table_erase_column(*l_tbl,"XEND");
  cpl_table_duplicate_column(*l_tbl,"XEND",*l_tbl,"XMOD");
  cpl_table_multiply_scalar(*l_tbl,"XEND",scl_fct);
  add_fct=OFFSET_X+(double)mbox_x/2;
  uves_msg_debug("add_fct=%f",add_fct);
  cpl_table_add_scalar(*l_tbl,"XEND",add_fct);

  scl_fct=(1.+Y_AXIS_SCALE);
  cpl_table_erase_column(*l_tbl,"YSTART");
  cpl_table_duplicate_column(*l_tbl,"YSTART",*l_tbl,"YMOD");
  cpl_table_multiply_scalar(*l_tbl,"YSTART",scl_fct);
  add_fct=OFFSET_Y-(double)mbox_y/2;
  uves_msg_debug("add_fct=%f",add_fct);
  cpl_table_add_scalar(*l_tbl,"YSTART",add_fct);

  cpl_table_erase_column(*l_tbl,"YEND");
  cpl_table_duplicate_column(*l_tbl,"YEND",*l_tbl,"YMOD");
  cpl_table_multiply_scalar(*l_tbl,"YEND",scl_fct);
  add_fct=OFFSET_Y+(double)mbox_y/2;
  uves_msg_debug("add_fct=%f",add_fct);
  cpl_table_add_scalar(*l_tbl,"YEND",add_fct);
 

 
  limit[2] = mbox_x/2;  /* limit(3) */
  check(tmp_tbl1=uves_extract_table_rows(*l_tbl,"XSTART",CPL_GREATER_THAN,
          limit[2]),"Error selecting XSTART");

  check_nomsg(limit[0] = cpl_image_get_size_x(raw_image)-mbox_x/2); /* limit(1) */

  check(tmp_tbl2=uves_extract_table_rows(tmp_tbl1,"XSTART",CPL_LESS_THAN,
          limit[0]),"Error selecting XSTART");

  limit[3] = mbox_y/2; /* limit(4) */

  check(tmp_tbl3=uves_extract_table_rows(tmp_tbl2,"YSTART",CPL_GREATER_THAN,
          limit[3]),"Error selecting YSTART");

  check_nomsg(limit[1] = cpl_image_get_size_y(raw_image)-mbox_y/2); /* limit(2) */

  check(tmp_tbl4=uves_extract_table_rows(tmp_tbl3,"YSTART",CPL_LESS_THAN,
          limit[1]),"Error selecting YSTART");

  uves_msg_debug("limits %f %f %f %f",
               limit[0],limit[1],limit[2],limit[3]);
  check_nomsg(*m_tbl     = cpl_table_extract_selected(tmp_tbl4));
  check_nomsg(*rline_tbl = cpl_table_extract_selected(tmp_tbl4));

  /* this is to finally get in l_tbl the result of the selection */
  check_nomsg(tmp_tbl=cpl_table_extract_selected(*l_tbl));
  uves_free_table(l_tbl);
  *l_tbl=cpl_table_duplicate(tmp_tbl);
  uves_free_table(&tmp_tbl);


  assure( !cpl_table_has_invalid(*m_tbl,"XMOD") &&
      !cpl_table_has_invalid(*m_tbl,"YMOD"),
      CPL_ERROR_ILLEGAL_INPUT, 
      "Invalid content in table m_tbl");

  uves_msg_debug("min=%f max=%f std=%f",
          cpl_image_get_min(raw_image),
          cpl_image_get_max(raw_image),
          cpl_image_get_stdev(raw_image));

  check_nomsg( uves_physmod_center_gauss(raw_image,m_tbl) );
  /*
  cpl_table_dump(*m_tbl,0,20,stdout);
  */


  check_nomsg(
      (cpl_table_duplicate_column(*rline_tbl,"XMES",*m_tbl,"XCEN"),
       cpl_table_duplicate_column(*rline_tbl,"YMES",*m_tbl,"YCEN"),
       cpl_table_duplicate_column(*rline_tbl,"STATUS",*m_tbl,"STATUS")));

  /* save measured positions without check for quality check */
  *mline_tbl=cpl_table_duplicate(*rline_tbl);

  uves_msg_debug("nraw=%" CPL_SIZE_FORMAT "",
     cpl_table_and_selected_int(*rline_tbl,"STATUS",CPL_EQUAL_TO,0.));


  check_nomsg( *p_tbl=cpl_table_extract_selected(*rline_tbl) );
  /* the following two lines to keep in *rline_tbl the result of selection */
  uves_free_table(rline_tbl);
  *rline_tbl=cpl_table_duplicate(*p_tbl);

  uves_msg_debug("middump nraw=%" CPL_SIZE_FORMAT "",cpl_table_get_nrow(*p_tbl));

  cpl_table_select_all(*rline_tbl);
  uves_free_table(rline_tbl);
  *rline_tbl=cpl_table_duplicate(*p_tbl);
  uves_msg_debug("rline nraw=%" CPL_SIZE_FORMAT "",cpl_table_get_nrow(*rline_tbl));


  check_nomsg( (cpl_table_duplicate_column(*rline_tbl,"XDIF",*rline_tbl,"XMES"),
                cpl_table_subtract_columns(*rline_tbl,"XDIF","XMOD")) );
  uves_msg_debug("rline1 nraw=%" CPL_SIZE_FORMAT "",cpl_table_get_nrow(*rline_tbl));


  cpl_table_duplicate_column(*rline_tbl,"YDIF",*rline_tbl,"YMES");
  cpl_table_subtract_columns(*rline_tbl,"YDIF","YMOD");

  uves_msg_debug("rline2 nraw=%" CPL_SIZE_FORMAT "",cpl_table_get_nrow(*rline_tbl));
  /* Fits the residual differences */
  /* Now we do the polynomial fit */
  check(poly2d_x=uves_polynomial_regression_2d(*rline_tbl,"XMOD","YMOD","XDIF",
                         NULL,2,2,"XDIFREG",NULL,NULL,
                         &mean_err_xdif,NULL,NULL,-1,-1),
        "2D poly regression to fit XDIF failed. Try to increase mbox_x value, "
        "and possibly the mbox_y value");

  outputd[0][0]=uves_polynomial_get_coeff_2d(poly2d_x,0,0);
  outputd[1][0]=uves_polynomial_get_coeff_2d(poly2d_x,1,0);
  outputd[0][1]=uves_polynomial_get_coeff_2d(poly2d_x,0,1);
  outputd[1][1]=uves_polynomial_get_coeff_2d(poly2d_x,1,1);
  outputd[2][0]=uves_polynomial_get_coeff_2d(poly2d_x,2,0);
  outputd[0][2]=uves_polynomial_get_coeff_2d(poly2d_x,0,2);
  outputd[1][2]=uves_polynomial_get_coeff_2d(poly2d_x,1,2);
  outputd[2][1]=uves_polynomial_get_coeff_2d(poly2d_x,2,1);
  outputd[2][2]=uves_polynomial_get_coeff_2d(poly2d_x,2,2);

  check(poly2d_y=uves_polynomial_regression_2d(*rline_tbl,"XMOD","YMOD","YDIF",
                         NULL,2,2,"YDIFREG",NULL,NULL,
                         &mean_err_ydif,NULL,NULL,-1,-1),
        "2D poly regression to fit YDIF failed. Try to increase mbox_y value");


  outputd[0][0]=uves_polynomial_get_coeff_2d(poly2d_y,0,0);
  outputd[1][0]=uves_polynomial_get_coeff_2d(poly2d_y,1,0);
  outputd[0][1]=uves_polynomial_get_coeff_2d(poly2d_y,0,1);
  outputd[1][1]=uves_polynomial_get_coeff_2d(poly2d_y,1,1);
  outputd[2][0]=uves_polynomial_get_coeff_2d(poly2d_y,2,0);
  outputd[0][2]=uves_polynomial_get_coeff_2d(poly2d_y,0,2);
  outputd[1][2]=uves_polynomial_get_coeff_2d(poly2d_y,1,2);
  outputd[2][1]=uves_polynomial_get_coeff_2d(poly2d_y,2,1);
  outputd[2][2]=uves_polynomial_get_coeff_2d(poly2d_y,2,2);


  uves_msg_debug("rline3 nraw=%" CPL_SIZE_FORMAT "",cpl_table_get_nrow(*rline_tbl));
   cpl_table_duplicate_column(*rline_tbl,"XMODREG",*rline_tbl,"XMOD");
   cpl_table_add_columns(*rline_tbl,"XMODREG","XDIFREG");

   cpl_table_duplicate_column(*rline_tbl,"YMODREG",*rline_tbl,"YMOD");
   cpl_table_add_columns(*rline_tbl,"YMODREG","YDIFREG");

  uves_msg_debug("rline4 nraw=%" CPL_SIZE_FORMAT "",cpl_table_get_nrow(*rline_tbl));
  /*
   cpl_table_dump(*rline_tbl,0,10,stdout);
  */


  /* was   */
  *npline_tbl=cpl_table_extract_selected(mod_tbl);

  /* we now unselect */
  cpl_table_select_all(mod_tbl); 
  /*
  *npline_tbl=cpl_table_extract_selected(mod_tbl);
  */
  uves_msg_debug("npline nraw=%" CPL_SIZE_FORMAT "",cpl_table_get_nrow(*npline_tbl));
  /* to prevent a problem of duplicating columns from tables 
     with different size we use a temporary table and extend 
     the size */ 
  tmp_tbl=cpl_table_new(cpl_table_get_nrow(*rline_tbl));
  cpl_table_duplicate_column(tmp_tbl,"XDIF",*rline_tbl,"XDIF");
  cpl_table_duplicate_column(tmp_tbl,"YDIF",*rline_tbl,"YDIF");
  
  cpl_table_set_size(tmp_tbl,cpl_table_get_nrow(*npline_tbl));

  cpl_table_duplicate_column(*npline_tbl,"XDIF",tmp_tbl,"XDIF");
  cpl_table_duplicate_column(*npline_tbl,"YDIF",tmp_tbl,"YDIF");
  uves_free_table(&tmp_tbl);

  cpl_table_new_column(*npline_tbl,"XDIFREG",CPL_TYPE_DOUBLE);
  cpl_table_new_column(*npline_tbl,"YDIFREG",CPL_TYPE_DOUBLE);
 
   for (k=0;k<cpl_table_get_nrow(*npline_tbl);k++) {

     tmp_x=cpl_table_get_double(*npline_tbl,"XMOD",k,&status);
     tmp_y=cpl_table_get_double(*npline_tbl,"YMOD",k,&status);

     tmp_val_x=uves_polynomial_evaluate_2d(poly2d_x,tmp_x,tmp_y);
     tmp_val_y=uves_polynomial_evaluate_2d(poly2d_y,tmp_x,tmp_y);

     cpl_table_set_double(*npline_tbl,"XDIFREG",k,tmp_val_x);
     cpl_table_set_double(*npline_tbl,"YDIFREG",k,tmp_val_y);
     
   }

   cpl_table_add_columns(*npline_tbl,"XMOD","XDIFREG");
   cpl_table_add_columns(*npline_tbl,"YMOD","YDIFREG");


   cleanup:
   uves_polynomial_delete(&poly2d_x);
   uves_polynomial_delete(&poly2d_y);
   uves_free_table(&tmp_tbl);
   uves_free_table(&tmp_tbl1);
   uves_free_table(&tmp_tbl2);
   uves_free_table(&tmp_tbl3);
   uves_free_table(&tmp_tbl4);
   
    uves_msg_debug("end msrawxy");
  return 0;
}
/**@}*/
