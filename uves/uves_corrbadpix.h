/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:02 $
 * $Revision: 1.8 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.6  2009/10/29 17:17:28  amodigli
 * added param to specify if red cdd is new/old
 *
 * Revision 1.5  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.4  2007/04/24 12:50:29  jmlarsen
 * Replaced cpl_propertylist -> uves_propertylist which is much faster
 *
 * Revision 1.3  2007/01/10 12:35:57  jmlarsen
 * Added uves_get_badpix
 *
 * Revision 1.2  2006/07/03 12:58:34  jmlarsen
 * Support flagging instead of interpolating bad pixels
 *
 * Revision 1.1  2005/11/11 13:18:54  jmlarsen
 * Reorganized code, renamed source files
 *
 */
#ifndef UVES_CORRBADPIX_H
#define UVES_CORRBADPIX_H

#include <uves_propertylist.h>
#include <uves_chip.h>
#include <cpl.h>

int uves_correct_badpix_all(cpl_image *master_bias, uves_propertylist *mbias_header,
                enum uves_chip chip,
                            int binx, int biny, int mark_bad, bool red_ccd_new);

int **
uves_get_badpix(enum uves_chip, 
                int binx, int biny, int mark_bad,bool uves_ccd_new);

void
uves_badmap_free(int ***badmap);

#endif
