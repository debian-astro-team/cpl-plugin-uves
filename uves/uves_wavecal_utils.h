/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:10 $
 * $Revision: 1.10 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.8  2007/06/06 08:17:34  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.7  2007/03/05 10:47:00  jmlarsen
 * Reject outliers based on line FWHM and fit residual
 *
 * Revision 1.6  2006/10/10 11:20:11  jmlarsen
 * Renamed line table columns to match MIDAS
 *
 * Revision 1.5  2006/07/14 12:52:57  jmlarsen
 * Exported/renamed function find_nearest
 *
 * Revision 1.4  2006/06/01 14:43:17  jmlarsen
 * Added missing documentation
 *
 * Revision 1.3  2006/04/21 12:29:30  jmlarsen
 * Write QC parameters to line table
 *
 * Revision 1.2  2006/02/15 13:19:15  jmlarsen
 * Reduced source code max. line length
 *
 * Revision 1.1  2006/02/03 07:46:30  jmlarsen
 * Moved recipe implementations to ./uves directory
 *
 * Revision 1.2  2005/12/19 16:17:55  jmlarsen
 * Replaced bool -> int
 *
 * Revision 1.1  2005/11/11 13:18:54  jmlarsen
 * Reorganized code, renamed source files
 *
 */
#ifndef UVES_WAVECAL_UTILS_H
#define UVES_WAVECAL_UTILS_H

#include <uves_utils_polynomial.h>
#include <cpl.h>
#include <stdbool.h>

/*-----------------------------------------------------------------------------
                                Defines
 -----------------------------------------------------------------------------*/
/* Use #defines to have consistent column names in all recipes */
#define LINETAB_PIXELSIZE "Pixel"
#define LINETAB_RESIDUAL  "Residual"  /* in wlu, not pixels */
#define LINETAB_LAMBDAC   "WaveC"     /* computed wavelength */

/*-----------------------------------------------------------------------------
                                Typedefs
 -----------------------------------------------------------------------------*/

/**
   A 'line table' is not just a table. It consists of
   (for each (window, trace))

   a table
   a dispersion relation,
   a map from (pixel, pixel) to (absolute order number)
   absolute order number limits
*/
typedef struct
{
    /** Number of windows */
    int windows;

    /** Number of traces */
    int traces;

    /** The table */
    cpl_table        **table;

    /** f(x,m) = lambda*m  */
    polynomial       **dispersion_relation;

    /** m = abs_order(x,y) */
    polynomial       **absolute_order;

    /** Conversion from relative to absolute (physical) order number */
    int *first_absolute_order;

    /** Last absolute order */
    int *last_absolute_order;

} lt_type;


int uves_wavecal_find_nearest(const cpl_table *line_refer, double lambda, int lo, int hi);

cpl_error_code uves_draw_lines(cpl_image *image,
                   polynomial *dispersion, 
                   const polynomial *order_locations,
                   const cpl_table *t,
                   const char *lambda_column, const char *abs_order,
                   const int *relative_order, 
                   int minorder, int maxorder, bool vertical, int offset);

int uves_delete_bad_lines(cpl_table *table, double TOLERANCE, double kappa);


lt_type *uves_lt_new(int windows, int traces);
void uves_lt_delete(lt_type **lt);
cpl_table **uves_lt_get_table(const lt_type *lt, int window, int trace);
polynomial **uves_lt_get_disprel(const lt_type *lt, int window, int trace);
polynomial **uves_lt_get_absord(const lt_type *lt, int window, int trace);
int *uves_lt_get_firstabs(const lt_type *lt, int window, int trace);
int *uves_lt_get_lastabs(const lt_type *lt, int window, int trace);

#endif

