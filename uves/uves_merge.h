/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2011-12-08 14:00:44 $
 * $Revision: 1.16 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.15  2010/12/08 11:06:20  amodigli
 * added chip parameter to uves_merge_orders() to have proper filenames
 *
 * Revision 1.14  2010/09/24 09:32:04  amodigli
 * put back QFITS dependency to fix problem spot by NRI on FIBER mode (with MIDAS calibs) data
 *
 * Revision 1.12  2008/05/13 09:59:58  amodigli
 * added delt1,delt2
 *
 * Revision 1.11  2008/02/21 07:50:07  amodigli
 * added method noappend
 *
 * Revision 1.10  2007/09/19 11:38:44  amodigli
 * added MERGE_FLAMES
 *
 * Revision 1.9  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.8  2007/04/24 12:50:29  jmlarsen
 * Replaced cpl_propertylist -> uves_propertylist which is much faster
 *
 * Revision 1.7  2006/08/17 13:56:53  jmlarsen
 * Reduced max line length
 *
 * Revision 1.6  2005/12/16 14:22:23  jmlarsen
 * Removed midas test data; Added sof files
 *
 * Revision 1.5  2005/11/24 15:09:06  jmlarsen
 * Implemented 2d extraction/rebinning/merging
 *
 * Revision 1.4  2005/11/18 10:52:06  jmlarsen
 * Split into optimal/sum merge methods
 *
 * Revision 1.3  2005/11/11 13:18:54  jmlarsen
 * Reorganized code, renamed source files
 *
 */
#ifndef UVES_MERGE_H
#define UVES_MERGE_H
#include <uves_cpl_size.h>
#include <uves_propertylist.h>
#include <cpl.h>
#include <uves_chip.h>

typedef enum {MERGE_OPTIMAL, MERGE_SUM, MERGE_NOAPPEND, MERGE_FLAMES} merge_method;

cpl_image *
uves_merge_orders(const cpl_image *spectrum, 
                  const cpl_image *spectrum_noise,
                  const uves_propertylist *spectrum_header,
                  merge_method m_method,
                  int n_traces,
		  uves_propertylist **merged_header,
		  const double delt1, 
                  const double delt2,
                  enum uves_chip chip,
                  cpl_image **merged_noise);

merge_method
uves_get_merge_method(const cpl_parameterlist *parameters, 
              const char *context, const char *subcontext);

#endif
