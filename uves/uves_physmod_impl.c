/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */
 
/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:07 $
 * $Revision: 1.38 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.36  2008/03/28 08:54:22  amodigli
 * IRPLIB_CONCAT2X-->UVES_CONCAT2X
 *
 * Revision 1.35  2007/06/11 13:28:26  jmlarsen
 * Changed recipe contact address to cpl at eso.org
 *
 * Revision 1.34  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.33  2007/02/09 13:39:51  jmlarsen
 * Use defines for recipe id
 *
 * Revision 1.32  2006/11/06 15:19:41  jmlarsen
 * Removed unused include directives
 *
 * Revision 1.31  2006/10/24 14:12:16  jmlarsen
 * Parametrized recipe id to support FLAMES recipe
 *
 * Revision 1.30  2006/10/19 13:53:25  jmlarsen
 * Changed guess line table tag to LINE_GUESS_TAB
 *
 * Revision 1.29  2006/10/17 12:33:02  jmlarsen
 * Added semicolon at UVES_RECIPE_DEFINE invocation
 *
 * Revision 1.28  2006/10/11 12:22:36  amodigli
 * now the stability check consist only i the msrawxy and the table comparison, as in MIDAS
 *
 * Revision 1.27  2006/10/09 13:01:13  jmlarsen
 * Use macro to define recipe interface functions
 *
 * Revision 1.26  2006/09/19 14:31:38  jmlarsen
 * uves_insert_frame(): use bitmap to specify which image statistics keywords must be computed
 *
 * Revision 1.25  2006/09/19 06:55:39  jmlarsen
 * Changed interface of uves_frameset to optionally write image statistics kewwords
 *
 * Revision 1.24  2006/08/24 11:36:37  jmlarsen
 * Write recipe start/stop time to header
 *
 * Revision 1.23  2006/08/18 13:35:42  jmlarsen
 * Fixed/changed QC parameter formats
 *
 * Revision 1.22  2006/08/17 13:56:53  jmlarsen
 * Reduced max line length
 *
 * Revision 1.21  2006/08/11 14:56:05  amodigli
 * removed Doxygen warnings
 *
 * Revision 1.20  2006/08/07 11:35:35  jmlarsen
 * Disabled parameter environment variable mode
 *
 * Revision 1.19  2006/08/01 14:42:34  amodigli
 * fixed bugs getting raw header from master formatcheck
 *
 * Revision 1.18  2006/07/31 06:29:26  amodigli
 * added QC on stability test
 *
 * Revision 1.17  2006/07/28 14:51:26  amodigli
 * fixed some bugs on improper table selection
 *
 * Revision 1.16  2006/07/14 12:19:28  jmlarsen
 * Support multiple QC tests per product
 *
 * Revision 1.15  2006/07/03 12:46:34  amodigli
 * updated description
 *
 * Revision 1.14  2006/06/28 13:28:29  amodigli
 * improved output
 *
 * Revision 1.13  2006/06/20 09:06:39  amodigli
 * correct input tag info in man page
 *
 * Revision 1.12  2006/06/16 08:25:45  jmlarsen
 * Manually propagate ESO.DET. keywords from 1st/2nd input header
 *
 * Revision 1.11  2006/06/13 11:57:02  jmlarsen
 * Check that calibration frames are from the same chip ID
 *
 * Revision 1.10  2006/06/07 13:06:28  jmlarsen
 * Changed doxygen tag addtogroup -> defgroup
 *
 * Revision 1.9  2006/06/07 09:01:28  amodigli
 * added some doc
 *
 * Revision 1.8  2006/05/08 15:42:16  amodigli
 * allow to specify order column label
 *
 * Revision 1.7  2006/04/20 10:47:39  amodigli
 * added qclog
 *
 * Revision 1.6  2006/04/07 07:11:12  jmlarsen
 * Minor doc. fix
 *
 * Revision 1.5  2006/04/06 09:48:15  amodigli
 * changed uves_frameset_insert interface to have QC log
 *
 * Revision 1.4  2006/04/06 08:42:19  jmlarsen
 * Changed indentation
 *
 * Revision 1.3  2006/03/03 13:54:11  jmlarsen
 * Changed syntax of check macro
 *
 * Revision 1.2  2006/02/28 09:15:22  jmlarsen
 * Minor update
 *
 * Revision 1.1  2006/02/03 07:46:30  jmlarsen
 * Moved recipe implementations to ./uves directory
 *
 * Revision 1.66  2006/01/25 10:09:18  jmlarsen
 * Added doxygen end marker
 *
 * Revision 1.65  2006/01/20 10:36:25  amodigli
 *
 * Fixed warings from doxigen
 *
 * Revision 1.64  2006/01/19 10:03:06  amodigli
 * Fixed leaks
 *
 * Revision 1.62  2006/01/16 13:52:58  jmlarsen
 * Removed memory leak
 *
 * Revision 1.61  2006/01/16 08:01:57  amodigli
 *
 * Added stability check
 *
 * Revision 1.60  2006/01/13 13:43:15  jmlarsen
 * Removed memory leak
 *
 * Revision 1.59  2006/01/13 09:54:42  amodigli
 * Fixed some bugs: improved agreement with MIDAS version
 *
 * Revision 1.58  2006/01/09 15:23:06  jmlarsen
 * Removed some warnings
 *
 * Revision 1.57  2006/01/09 14:05:42  amodigli
 * Fixed doxigen warnings
 *
 * Revision 1.56  2006/01/03 16:57:13  amodigli
 * Fixed bug
 *
 * Revision 1.55  2006/01/03 14:47:53  amodigli
 *
 * Added uves_physmod_chop_otab.h .c to match MIDAS
 *
 * Revision 1.54  2005/12/19 16:17:55  jmlarsen
 * Replaced bool -> int
 *
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*----------------------------------------------------------------------------*/
/**
 * @defgroup uves_physmod  Recipe: UVES Physical Model
 *
 * This recipe computes a guess order and line table by
 * using a physical model for UVES
 * See man-page for details.
 */
/*----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#include <uves.h>
#include <uves_physmod_body.h>
#include <uves_recipe.h>
#include <uves_error.h>

#include <cpl.h>

/*-----------------------------------------------------------------------------
                                Defines
 -----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
                            Functions prototypes
 ----------------------------------------------------------------------------*/
static int uves_physmod_define_parameters(cpl_parameterlist *parameters);

/*-----------------------------------------------------------------------------
                            Recipe standard code
 -----------------------------------------------------------------------------*/
#define cpl_plugin_get_info uves_physmod_get_info
UVES_RECIPE_DEFINE(
    UVES_PHYSMOD_ID, UVES_PHYSMOD_DOM, uves_physmod_define_parameters,
    "Andrea Modigliani", "cpl@eso.org",
    uves_physmod_desc_short,
    uves_physmod_desc);

/**@{*/
/*-----------------------------------------------------------------------------
                              Functions code
 -----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/**
  @brief    Setup the recipe options    
  @param    parameters        the parameterlist to fill
  @return   0 if everything is ok
 */
/*----------------------------------------------------------------------------*/
static int
uves_physmod_define_parameters(cpl_parameterlist *parameters)
{
    return uves_physmod_define_parameters_body(parameters, make_str(UVES_PHYSMOD_ID));
}

/*----------------------------------------------------------------------------*/
/**
  @brief    Get the command line options and execute the data reduction
  @param    parameters  the parameters list
  @param    frames      the frames list
  @return   CPL_ERROR_NONE if everything is ok

 */
/*----------------------------------------------------------------------------*/
static void
UVES_CONCAT2X(UVES_PHYSMOD_ID,exe)(cpl_frameset *frames, 
                     const cpl_parameterlist *parameters,
                     const char *starttime)
{
    bool flames = false;
    uves_physmod_exe_body(frames, flames, make_str(UVES_PHYSMOD_ID),
              parameters, starttime);
}
/**@}*/
