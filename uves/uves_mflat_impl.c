/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston MA 02110-1301 USA          *
 */
 
/*
 * $Author: amodigli $
 * $Date: 2010-04-08 08:06:18 $
 * $Revision: 1.45 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*----------------------------------------------------------------------------*/
/**
 * @defgroup uves_mflat  Recipe: Master Flat
 *
 * This recipe calculates the master flat frame.
 * See man-page for details.
 */
/*----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#include <uves_reduce_mflat.h>

#include <uves_parameters.h>
#include <uves_recipe.h>
#include <uves.h>
#include <uves_error.h>

#include <cpl.h>

/*-----------------------------------------------------------------------------
                            Functions prototypes
 -----------------------------------------------------------------------------*/

static int uves_mflat_define_parameters(cpl_parameterlist *parameters);

/*-----------------------------------------------------------------------------
                            Recipe standard code
 -----------------------------------------------------------------------------*/
#define cpl_plugin_get_info uves_mflat_get_info
UVES_RECIPE_DEFINE(
    UVES_MFLAT_ID, UVES_MFLAT_DOM, uves_mflat_define_parameters,
    "Jonas M. Larsen", "cpl@eso.org",
    "Creates the master flat field frame",
    uves_mflat_desc);

/**@{*/
/*-----------------------------------------------------------------------------
                              Functions code
  ----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/**
  @brief    Setup the recipe options    
  @param    parameters        the parameterlist to fill
  @return   0 if everything is ok
 */
/*----------------------------------------------------------------------------*/
static int
uves_mflat_define_parameters(cpl_parameterlist *parameters)
{
    return uves_mflat_define_parameters_body(parameters, 
                         make_str(UVES_MFLAT_ID));
}

/*----------------------------------------------------------------------------*/
/**
   @brief    Execute the data reduction
   @param    frames      the frames list
   @param    parameters  the parameters list
   @param    starttime   start of execution
   @return   CPL_ERROR_NONE if everything is ok

*/
/*----------------------------------------------------------------------------*/
static void
UVES_CONCAT2X(UVES_MFLAT_ID,exe)(cpl_frameset *frames, 
                   const cpl_parameterlist *parameters,
                   const char *starttime)
{
    uves_mflat_exe_body(frames, parameters, starttime, make_str(UVES_MFLAT_ID));
    return;
}

/**@}*/
