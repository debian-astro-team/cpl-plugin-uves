/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2002,2003 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */

/*
 * $Author: amodigli $
 * $Date: 2011-12-08 13:56:30 $
 * $Revision: 1.9 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.8  2010/09/24 09:32:05  amodigli
 * put back QFITS dependency to fix problem spot by NRI on FIBER mode (with MIDAS calibs) data
 *
 * Revision 1.6  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.5  2007/04/17 09:34:40  jmlarsen
 * Parametrize the assumption about consecutive orders (for FLAMES support)
 *
 * Revision 1.4  2006/08/17 13:56:53  jmlarsen
 * Reduced max line length
 *
 * Revision 1.3  2006/06/08 08:42:53  jmlarsen
 * Added support for computing Hough transform on image subwindow
 *
 * Revision 1.2  2006/02/15 13:19:15  jmlarsen
 * Reduced source code max. line length
 *
 * Revision 1.1  2006/02/03 07:46:30  jmlarsen
 * Moved recipe implementations to ./uves directory
 *
 * Revision 1.8  2005/12/19 16:17:55  jmlarsen
 * Replaced bool -> int
 *
 */
#ifndef UVES_ORDERPOS_HOUGH_H
#define UVES_ORDERPOS_HOUGH_H
#include <uves_cpl_size.h>
#include <cpl.h>

#include <stdbool.h>

cpl_table *uves_hough(const cpl_image *image, int ymin, int ymax, int NORDERS, 
              bool norders_is_guess,
              int SAMPLEWIDTH, double PTHRES, double MINSLOPE, 
              double MAXSLOPE,
              int SLOPERES, bool consecutive,
              cpl_image **htrans, 
              cpl_image **htrans_original);

cpl_error_code uves_draw_orders(const cpl_table *, cpl_image *);

#endif /* UVES_ORDERPOS_HOUGH_H */
