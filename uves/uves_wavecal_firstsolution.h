/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2002,2003 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */

/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:09 $
 * $Revision: 1.8 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.6  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.5  2007/03/05 10:22:35  jmlarsen
 * Fixed bug in computation of max/min physical order number
 *
 * Revision 1.4  2006/12/07 08:29:56  jmlarsen
 * Compute correct Ynew column for FLAMES
 *
 * Revision 1.3  2006/11/24 16:24:53  jmlarsen
 * Added window offset parameter
 *
 * Revision 1.2  2006/02/15 13:19:15  jmlarsen
 * Reduced source code max. line length
 *
 * Revision 1.1  2006/02/03 07:46:30  jmlarsen
 * Moved recipe implementations to ./uves directory
 *
 * Revision 1.8  2005/12/20 08:11:44  jmlarsen
 * Added CVS  entry
 *
 */
#ifndef UVES_WAVECAL_FIRSTSOLUTION_H
#define UVES_WAVECAL_FIRSTSOLUTION_H

#include <uves_utils_polynomial.h>

#include <cpl.h>

#include <stdbool.h>

polynomial *
uves_wavecal_firstsolution(cpl_table *linetable, 
               const cpl_table *guess,
               polynomial **absolute_order, 
               const cpl_table *order_table,
               const polynomial *order_locations,
               bool flames,
               double offset, int **relative_order, int DEGREE,
               double CORREL_RANGE, double CORREL_STEP, double CORREL_TOLERANCE, 
               double TOLERANCE, int *first_abs_order, int *last_abs_order);

#endif
