/*                                                                              *
 *   This file is part of the ESO UVES  Pipeline                                *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */

/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:07 $
 * $Revision: 1.10 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.8  2009/07/13 06:39:34  amodigli
 * change uves_physmod_create_table API
 *
 * Revision 1.7  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.6  2007/04/24 12:50:29  jmlarsen
 * Replaced cpl_propertylist -> uves_propertylist which is much faster
 *
 * Revision 1.5  2007/01/08 16:59:28  amodigli
 * changes to make flames-uves iterations to recover physical model
 *
 * Revision 1.4  2006/11/06 15:19:41  jmlarsen
 * Removed unused include directives
 *
 * Revision 1.3  2006/10/24 14:12:24  jmlarsen
 * Parametrized recipe id to support FLAMES recipe
 *
 * Revision 1.2  2006/10/05 11:16:41  jmlarsen
 * Declared parameter list const
 *
 * Revision 1.1  2006/02/03 07:46:30  jmlarsen
 * Moved recipe implementations to ./uves directory
 *
 * Revision 1.2  2005/12/20 08:11:44  jmlarsen
 * Added CVS  entry
 *
 */
#ifndef UVES_PHYSMOD_CREATE_TABLE_H
#define UVES_PHYSMOD_CREATE_TABLE_H

#include <uves_propertylist.h>
#include <uves_chip.h>

#include <cpl.h>

int 
uves_physmod_create_table(const uves_propertylist *raw_header, 
                          enum uves_chip chip, 
			  bool flames,
			  const char *recipe_id,
                          const cpl_parameterlist* parameters,
                          cpl_table* line_refer,
                          const double physmod_shift_x,
                          const double physmod_shift_y,
                          cpl_table** mod_tbl,
                          cpl_table** fsr_tbl);

#endif /* UVES_PHYSMOD_CREATE_TABLE_H */
