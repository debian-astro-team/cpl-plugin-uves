/*
 * This file is part of the UVES Pipeline
 * Copyright (C) 2002, 2003, 2004, 2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:07 $
 * $Revision: 1.24 $
 * $Name: not supported by cvs2svn $
 *
 */

#ifndef UVES_QCLOG_H
#define UVES_QCLOG_H

#include <uves_propertylist.h>
#include <uves_globals.h>
#include <uves_chip.h>

#include <cpl.h>

#include <stdbool.h>

#define KEY_NAME_PRO_REC1_RAW1_NAME        "ESO PRO REC1 RAW1 NAME"

#define KEY_NAME_PRO_DRSID                 "ESO PRO REC1 DRS ID"
#define PAF_NAME_PRO_DRSID                 "PRO REC1 DRS ID"
#define KEY_HELP_PRO_DRSID                 "Data Reduction System identifier"

#define KEY_NAME_PIPE_ID                   "ESO PRO REC1 PIPE ID"
#define PAF_NAME_PIPE_ID                   "PRO REC1 PIPE ID"
#define KEY_HELP_PIPE_ID                   "Pipeline (unique) identifier"
#define KEY_NAME_PIPEFILE                  "PIPEFILE"
#define KEY_HELP_PIPEFILE                  "Filename of data product"

#define KEY_NAME_QC_DID                    "ESO QC DID"
#define PAF_NAME_QC_DID                    "QC DID"
#define KEY_HELP_QC_DID                    "Data dictionary for QC"

#define KEY_NAME_PRO_TYPE                  "ESO PRO TYPE"
#define PAF_NAME_PRO_TYPE                  "PRO TYPE"
#define KEY_HELP_PRO_TYPE                  "Product Type"

#define KEY_NAME_DATE_OBS                  "DATE-OBS"
#define KEY_HELP_DATE_OBS                  "Observing date"

#define KEY_NAME_PRO_RECID                 "ESO PRO REC1 ID"
#define PAF_NAME_PRO_RECID                 "PRO REC1 ID"
#define KEY_HELP_PRO_RECID                 "Pipeline recipe(unique) identifier"

#define KEY_NAME_DPR_TYPE                  "ESO DPR TYPE"
#define KEY_NAME_DPR_CATG                  "ESO DPR CATG"
#define KEY_NAME_DPR_TECH                  "ESO DPR TECH"
#define KEY_NAME_PRO_CATG                  "ESO PRO CATG"
#define KEY_NAME_TPL_ID                    "ESO TPL ID"
#define KEY_NAME_ARCFILE                   "ARCFILE"

#define PAF_NAME_DPR_TYPE                  "DPR TYPE"
#define PAF_NAME_DPR_CATG                  "DPR CATG"
#define PAF_NAME_DPR_TECH                  "DPR TECH"
#define PAF_NAME_PRO_CATG                  "PRO CATG"
#define PAF_NAME_TPL_ID                    "TPL ID"
#define PAF_NAME_PRO_DRSID                 "PRO REC1 DRS ID"

#define KEY_HELP_DPR_TYPE                  "Observation type"
#define KEY_HELP_DPR_CATG                  "Observation category"
#define KEY_HELP_DPR_TECH                  "Observation technique"
#define KEY_HELP_PRO_CATG                  "Category of pipeline product frame"
#define KEY_HELP_TPL_ID                    "Template sig"
#define KEY_HELP_ARCFILE                   "Archive file name"
#define KEY_HELP_PRO_DRSID                 "Data Reduction System identifier"

#define PORT_ID(chip) (((chip) == UVES_CHIP_REDL) ? 4 : 1)

char * uves_get_rootname(const char * filename);
void uves_get_paf_name(const char* in, int paf_no, char** paf);

int uves_blank2dot(const char * in, char* ou);

int uves_save_paf(const char* filename, 
          int paf_no,
                  const char* rec_id,  
                  const cpl_table* qclog,
                  uves_propertylist*   plist,
                  const uves_propertylist*   rhead,
                  const char*      pro_catg);

cpl_table *
uves_qclog_init(const uves_propertylist *raw_header,
        enum uves_chip chip);
int
uves_qclog_add_int(cpl_table* table,
                 const char* name,  
                 const int   value,
                 const char* help,
         const char* format);

int
uves_qclog_add_bool(cpl_table* table,
                 const char* name,  
                 const char  value,
                 const char* help,
         const char* format);


int
uves_qclog_add_float(cpl_table* table,
                 const char* name,  
                 const float value,
                 const char* help,
         const char* format);


int
uves_qclog_add_double(cpl_table* table,
                 const char* name,  
                 const double value,
                 const char* help,
         const char* format);


int
uves_qclog_add_string(cpl_table* table,
                 const char* name,  
                 const char* value,
                 const char* help,
         const char* format);


int uves_qclog_delete(cpl_table** table);

int uves_pfits_put_qc(uves_propertylist * plist, const cpl_table * qclog);

void uves_qclog_add_sci(cpl_table *qclog,
            const uves_propertylist *raw_header,
            const cpl_image *raw_image,
            double slit,
            const cpl_table *info_tbl);

void 
uves_qclog_add_common_wave(const uves_propertylist *raw_header,
               enum uves_chip chip,
               cpl_table *qclog);

const char *
uves_qclog_get_qc_name(const char *name,
               bool flames, int trace_number);

int
uves_qclog_dump_common(const uves_propertylist *plist,
               enum uves_chip chip, 
                       cpl_table* qclog);

int
uves_qclog_dump_common_wave(const uves_propertylist *plist,
                    enum uves_chip chip, 
                            cpl_table* qclog);


#endif
