/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */
/* ===========================================================================
 * $Id: uves_physmod_utils.h,v 1.6 2010-09-24 09:32:07 amodigli Exp $
 * $Name: not supported by cvs2svn $
 * $Revision: 1.6 $
 * $Log: not supported by cvs2svn $
 * Revision 1.4  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.3  2006/08/17 13:56:53  jmlarsen
 * Reduced max line length
 *
 * Revision 1.2  2006/04/10 12:38:43  jmlarsen
 * Minor layout change
 *
 * Revision 1.1  2006/02/03 07:46:30  jmlarsen
 * Moved recipe implementations to ./uves directory
 *
 * Revision 1.7  2005/12/05 16:11:47  amodigli
 * Fixed some warning
 *
 * Revision 1.6  2005/12/02 12:56:09  amodigli
 * Fixed compilation problems on mac
 *
 * Revision 1.5  2005/11/28 15:36:34  amodigli
 * Renamed global variables and global function with prefix uves_physmod
 *
 * Revision 1.4  2005/11/25 08:05:15  amodigli
 *  Added plotting facility in physmod. Commented out some strange things in utils.
 *
 * Revision 1.3  2005/08/29 10:05:08  jmlarsen
 * Conversion to/from electrons when calculating photonic noise
 *
 * Revision 1.2  2005/08/29 08:27:58  amodigli
 * Put into repository updates on physical model
 *
 * Revision 1.1  2005/06/17 11:34:34  amodigli
 * First release
 *
 * ===========================================================================
 */
#ifndef UVES_PHYSMOD_UTILS_H
#define UVES_PHYSMOD_UTILS_H


#include <cpl.h>


#define ARM_UNDEF 0
#define ARM_BLUE  1
#define ARM_RED   2
 


/*
  double fc;
  double l;
  int m;
  char uves_arm;


  double uves_beta_ech;
  double uves_beta_cd ;
  char uves_ccd_id;
  double uves_physmod_rcd;
  double uves_physmod_rech ;
  double uves_physmod_x_off;
  double uves_physmod_y_off;
  double x;
  double y;

*/

/*
  double lm, k, theta, *palpha, *pbeta;
  int disp;
  float binx, biny;
  double waveCent;
  double ccdrotred, ccdrotblue;
  double echred, echblue, xred, xblue;
  double p, t, w;
  double lambda;
  double alpha, beta;
  double *puves_beta_ech, *puves_beta_cd, *pfc;
  double *puves_physmod_rech, *puves_physmod_rcd, *pblz;
  double wave, order, blz;
  double *pbinsize,*ppixscale,*ppixscalCD,*plinewidpx,*plinelenpx,*plinewidth,*presol;
  double *px, *py;
  double *plambda;
  double *pdm;
  int *pm;
  double *lambdaC, *fsrStart, *fsrEnd;
  int *nx, *ny;
*/

void uves_physmod_set_incidence(double echred, double echblue, double xred, double xblue);
void uves_set_ccd_rot(double* uves_ccd_rot, double ccdrotred, double ccdrotblue);
void uves_physmod_set_offset(double offset_x, double offset_y,
                 double uves_physmod_x_off, double uves_physmod_y_off);
void uves_air_config(double p, double t, double w);
double uves_air_index(double lambda);
int  uves_config(char uves_arm, char uves_ccd_id, int disp, double waveCent, 
                float binx, float biny);

int  uves_config_cpl(int blue, int upper, int disp, double waveCent, 
             float binx, float biny);
int  uves_config_cpl_new(int blue, int upper, int disp, double waveCent, 
             float binx, float biny);

int uves_physmod_find_order(double lm);

double uves_physmod_find_beta(int m, double k, double l, double alpha);

void uves_physmod_lambda_order2beta(double lambda, int m, 
                    double* puves_beta_ech, double* puves_beta_cd, 
             double* pfc);
void uves_beta2xy(double uves_beta_cd, double uves_beta_ech, double fc, double* px, double* py);
void uves_physmod_photo_beta(double lambda, double uves_beta_ech, 
                 double uves_beta_cd, double* puves_physmod_rech, 
               double* puves_physmod_rcd, double* pblz);
void uves_physmod_pixscl(double wave, int order, double uves_physmod_rech, 
             double uves_physmod_rcd, float binx, 
            float biny, double fc, double slitwidth, double slitlength,
            double* pbinsize, double* ppixscale, double* ppixscalCD, 
        double* plinewidpx, double* plinelenpx, double* plinewidth, 
            double* presol);
void uves_physmod_xy_model(double lambda, int m, double* px, double* py);

void uves_physmod_lambda_order_model(double* plambda, int* pm, double x, double y);
void uves_physmod_find_FSR(int m, double* lambdaC, double* fsrStart, double* fsrEnd);
void uves_ccd_size(int* nx, int* ny);
void uves_physmod_xy_regres(double x,double y,double* px,double* py);



/*
  Global Variables and Constants
*/

extern float    uves_bin[2];
extern double   uves_ccd_rot[2];

extern int      uves_cfg_indx;
extern int      uves_x_disp_id;
extern double   uves_alpha0_cd, uves_beta0_cd;
extern char     uves_arm_id;

 
extern double   uves_physmod_offsetx[6];
extern double   uves_physmod_offsety[6];

#endif
