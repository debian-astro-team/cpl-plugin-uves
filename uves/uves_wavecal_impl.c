/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */
 
/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:09 $
 * $Revision: 1.56 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.54  2008/03/28 08:54:57  amodigli
 * IRPLIB_CONCAT2X-->UVES_CONCAT2X
 *
 * Revision 1.53  2007/06/11 13:28:26  jmlarsen
 * Changed recipe contact address to cpl at eso.org
 *
 * Revision 1.52  2007/06/08 13:06:16  jmlarsen
 * Send bug reports to Andrea
 *
 * Revision 1.51  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.50  2007/05/09 14:48:06  jmlarsen
 * Read slitlength from header
 *
 * Revision 1.49  2007/02/09 13:44:42  jmlarsen
 * Use defines for recipe id
 *
 * Revision 1.48  2006/12/07 08:30:19  jmlarsen
 * Support different slit for UVES, FLAMES
 *
 * Revision 1.47  2006/11/13 12:42:55  jmlarsen
 * Factored out common UVES/FLAMES wavecal code
 *
 * Revision 1.37  2006/11/06 15:19:41  jmlarsen
 * Removed unused include directives
 *
 * Revision 1.36  2006/10/25 07:21:52  jmlarsen
 * Documentation bug fix
 *
 * Revision 1.35  2006/10/24 14:02:38  jmlarsen
 * Factored out common UVES/FLAMES code
 *
 * Revision 1.34  2006/10/17 12:33:02  jmlarsen
 * Added semicolon at UVES_RECIPE_DEFINE invocation
 *
 * Revision 1.33  2006/10/09 13:01:13  jmlarsen
 * Use macro to define recipe interface functions
 *
 * Revision 1.32  2006/09/20 12:53:57  jmlarsen
 * Replaced stringcat functions with uves_sprintf()
 *
 * Revision 1.31  2006/09/19 14:31:10  jmlarsen
 * uves_insert_frame(): use bitmap to specify which image statistics keywords must be computed
 *
 * Revision 1.30  2006/09/19 06:55:11  jmlarsen
 * Changed interface of uves_frameset to optionally write image statistics kewwords
 *
 * Revision 1.29  2006/08/24 11:36:37  jmlarsen
 * Write recipe start/stop time to header
 *
 * Revision 1.28  2006/08/18 13:35:42  jmlarsen
 * Fixed/changed QC parameter formats
 *
 * Revision 1.27  2006/08/11 14:56:05  amodigli
 * removed Doxygen warnings
 *
 * Revision 1.26  2006/08/07 11:35:35  jmlarsen
 * Disabled parameter environment variable mode
 *
 * Revision 1.25  2006/07/14 12:19:28  jmlarsen
 * Support multiple QC tests per product
 *
 * Revision 1.24  2006/07/03 14:20:39  jmlarsen
 * Exclude bad pixels from order tracing
 *
 * Revision 1.23  2006/07/03 13:09:24  amodigli
 * adjusted description display layout
 *
 * Revision 1.22  2006/07/03 12:46:34  amodigli
 * updated description
 *
 * Revision 1.21  2006/06/22 12:13:10  amodigli
 * removed ESO prefix
 *
 * Revision 1.20  2006/06/22 06:44:06  amodigli
 * added some QC param
 *
 * Revision 1.19  2006/06/16 08:25:34  jmlarsen
 * Do the order tracing on non-median filtered frame
 *
 * Revision 1.18  2006/06/13 11:57:02  jmlarsen
 * Check that calibration frames are from the same chip ID
 *
 * Revision 1.17  2006/06/08 11:40:50  amodigli
 * added check to have output order table as input guess, if provided
 *
 * Revision 1.16  2006/06/08 08:42:53  jmlarsen
 * Added support for computing Hough transform on image subwindow
 *
 * Revision 1.15  2006/06/07 13:06:28  jmlarsen
 * Changed doxygen tag addtogroup -> defgroup
 *
 * Revision 1.14  2006/06/07 09:01:28  amodigli
 * added some doc
 *
 * Revision 1.13  2006/06/06 08:40:10  jmlarsen
 * Shortened max line length
 *
 * Revision 1.12  2006/05/09 15:42:00  amodigli
 * added QC log
 *
 * Revision 1.11  2006/05/08 15:41:32  amodigli
 * added order table chopping (commented out)
 *
 * Revision 1.10  2006/05/05 13:55:17  jmlarsen
 * Minor doc update
 *
 * Revision 1.9  2006/04/20 10:47:39  amodigli
 * added qclog
 *
 * Revision 1.8  2006/04/06 09:48:15  amodigli
 * changed uves_frameset_insert interface to have QC log
 *
 * Revision 1.7  2006/04/06 08:46:40  jmlarsen
 * Changed default polynomial degrees to auto
 *
 * Revision 1.6  2006/03/24 14:04:14  jmlarsen
 * Changed background subtraction sample density default parameter value
 *
 * Revision 1.5  2006/03/09 10:53:41  jmlarsen
 * Changed default bivariate degrees to MIDAS values
 *
 * Revision 1.4  2006/03/03 13:54:11  jmlarsen
 * Changed syntax of check macro
 *
 * Revision 1.3  2006/02/28 09:15:22  jmlarsen
 * Minor update
 *
 * Revision 1.2  2006/02/21 14:26:54  jmlarsen
 * Minor changes
 *
 * Revision 1.1  2006/02/03 07:46:30  jmlarsen
 * Moved recipe implementations to ./uves directory
 *
 * Revision 1.63  2006/01/19 08:47:24  jmlarsen
 * Inserted missing doxygen end tag
 *
 * Revision 1.62  2006/01/05 14:31:31  jmlarsen
 * Checking for guess DRS table before guess order table
 *
 * Revision 1.61  2005/12/20 16:10:32  jmlarsen
 * Added some documentation
 *
 * Revision 1.60  2005/12/19 16:17:55  jmlarsen
 * Replaced bool -> int
 *
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*----------------------------------------------------------------------------*/
/**
 * @defgroup uves_wavecal  Recipe: Order Position
 *
 * This recipe determines the echelle order locations.
 * See man-page for details.
 */
/*----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#include <uves_wavecal_body.h>
#include <uves_recipe.h>
#include <uves.h>
#include <uves_error.h>

#include <cpl.h>

/*-----------------------------------------------------------------------------
                            Forward declarations
 -----------------------------------------------------------------------------*/
static int uves_wavecal_define_parameters(cpl_parameterlist *parameters);

/*-----------------------------------------------------------------------------
                            Recipe standard code
 -----------------------------------------------------------------------------*/
#define cpl_plugin_get_info uves_wavecal_get_info
UVES_RECIPE_DEFINE(
    UVES_WAVECAL_ID, UVES_WAVECAL_DOM, uves_wavecal_define_parameters,
    "Jonas M. Larsen", "cpl@eso.org",
    uves_wavecal_desc_short,
    uves_wavecal_desc);

/*-----------------------------------------------------------------------------
                              Functions code
 -----------------------------------------------------------------------------*/
/**@{*/
/*----------------------------------------------------------------------------*/
/**
  @brief    Setup the recipe options    
  @param    parameters        the parameterlist to fill
  @return   0 if everything is ok
 */
/*----------------------------------------------------------------------------*/
static int
uves_wavecal_define_parameters(cpl_parameterlist *parameters)
{
    double slit_length_in_pixels = -1; /* Use header value */
    return uves_wavecal_define_parameters_body(parameters, make_str(UVES_WAVECAL_ID),
                           slit_length_in_pixels);
}

/*----------------------------------------------------------------------------*/
/**
  @brief    Get the command line options and execute the data reduction
  @param    parameters  the parameters list
  @param    frames      the frames list
  @return   CPL_ERROR_NONE if everything is ok

 */
/*----------------------------------------------------------------------------*/
static void
UVES_CONCAT2X(UVES_WAVECAL_ID,exe)(cpl_frameset *frames, 
                     const cpl_parameterlist *parameters,
                     const char *starttime)
{
    bool flames = false;

    uves_wavecal_exe_body(frames, flames, make_str(UVES_WAVECAL_ID),
              parameters, starttime);
}
/**@}*/


