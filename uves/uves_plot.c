/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */

/*
 * $Author: amodigli $
 * $Date: 2012-03-02 16:49:48 $
 * $Revision: 1.38 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*----------------------------------------------------------------------------*/
/**
 * @defgroup uves_plot  Plot CPL tables/images
 *
 * Plot CPL tables and/or images using an external plotter specified by the
 * user. This is useful for debugging, and might also
 * be useful for the user. This module provides only
 * basic functionality for plotting CPL objects; for more advanced 
 * purposes, a dedicated FITS viewing tool is recommended.
 *
 * This module depends on the availability of the system call setenv.
 * This is *not* defined in ANSI-C (getenv() is; setenv() is not) and
 * therefore might fail for various reasons on different platforms. Note that
 * the plotting/imaging functionality can be easily switched off by setting
 * the plotting/imaging commands to 'no'.
 *
 * If a plotting call fails for
 * 'external' reasons (i.e. when communication with the external process), a warning
 * message is printed, but success status is returned. A failure exit status is
 * returned only in cases of invalid input (null pointers etc.). 
 *
 */
/*----------------------------------------------------------------------------*/

/* If we can link to setenv but it is not declared, then declare it manually */
#if defined HAVE_SETENV && HAVE_SETENV
#if defined HAVE_DECL_SETENV && !HAVE_DECL_SETENV
int setenv(const char *name, const char *value, int overwrite);
#endif
#endif

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#include <uves_plot.h>

#include <uves_dump.h>
#include <uves_utils_wrappers.h>
#include <uves_error.h>
#include <uves_msg.h>

#include <irplib_utils.h>

#include <cpl.h>

#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>   /* setenv */

/*-----------------------------------------------------------------------------
                            Functions prototypes
 -----------------------------------------------------------------------------*/
static char *title_string(const char *title, int npoints);
/*-----------------------------------------------------------------------------
                            Defines
 -----------------------------------------------------------------------------*/
#define MAXTITLELENGTH 10000
#define RECOVER_FROM_ERROR(EXTERNAL_COMMAND) do {  \
    if (cpl_error_get_code() != CPL_ERROR_NONE)    \
    {                                              \
       uves_msg_error("Could not send plot to "    \
              "command '%s': "             \
               "%s in '%s'",               \
                       EXTERNAL_COMMAND,           \
               cpl_error_get_message(),    \
               cpl_error_get_where());     \
       cpl_error_reset();                          \
       goto cleanup;                               \
    } } while (false)


static char title[MAXTITLELENGTH];
static bool plotting_enabled = false;           /* If caller forgets to call 
                           the initializer, plotting
                           will be disabled */
static const char *plotter = "";

/**@{*/

/*-----------------------------------------------------------------------------
                        Implementation
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
   @brief    Set plotting/imaging commands
   @param    plotter_command       The plotter command
   @return   CPL_ERROR_NONE iff okay

   The specified commands are written to the environment variables
   CPL_PLOTTER and IRPLIB_IMAGER, which are used by the irplib.

   Some additional checks are made to see if the specified commands are
   available on the current platform. See code for details.
*/
/*----------------------------------------------------------------------------*/
cpl_error_code
uves_plot_initialize(const char *plotter_command)
{
    char *test_cmd = NULL;
    char *first_word = NULL;

    plotting_enabled = (strcmp(plotter_command, "no") != 0);
    
    /* Note that 'setenv' is *not* ANSI C. If it does not exist, tell user to
     *  define the environment variable him-/herself.
     */

    if (plotting_enabled)
    {
        const char *env = "CPL_PLOTTER";

        /* Check if 'which x' returns non-zero.
           x is the first word of plotting command.
           Note: this assumes the environment understands
           'which' and '> /dev/null'. If not,
           plotting will be disabled.
        */
        first_word = uves_sprintf("%s ", plotter_command);
        
        assure( strtok(first_word, " ") != NULL, CPL_ERROR_ILLEGAL_OUTPUT,
            "Error splitting string '%s'", first_word);
        
        test_cmd = uves_sprintf("which %s > /dev/null", first_word);
        
#if defined HAVE_SETENV && HAVE_SETENV

        if (setenv(env, plotter_command, 1) != 0)
        {
            uves_msg_warning("Could not set environment variable '%s'. "
                     "Plotting disabled!", env);
            plotting_enabled = false;
        }
        /* popen may return non-NULL even when the external command
           is not available. This causes the recipe to crash when writing
           to an invalid FILE pointer.
           Therefore, check (using 'which') that the
           command is available.
        */
        else if (system(test_cmd) != 0)
        {
            uves_msg_debug("Command '%s' returned non-zero", test_cmd);
            uves_msg_warning("Command '%s' failed. Plotting disabled!", test_cmd);
            plotting_enabled = false;
        }
        else
        {
            /* Setenv succeeded, remember command */
            uves_msg_debug("setenv %s='%s' succeeded", env, plotter_command);
            uves_msg_debug("Command '%s' returned zero", test_cmd);

            plotter = plotter_command;
        }
#else
        uves_msg_warning("setenv() is not available on this platform. You have to manually "
                 "set the environment variable '%s' to '%s'", env, plotter_command);

        plotter = plotter_command;

#endif
    }   
   
  cleanup:
    cpl_free(test_cmd);
    cpl_free(first_word);

    return cpl_error_get_code();
}

/*----------------------------------------------------------------------------*/
/**
   @brief    Plot one image row
   @param    image           Image to plot
   @param    first_row       First row to plot (counting from 1)
   @param    last_row        Last row to plot (counting from 1)
   @param    step            Plot only every n'th row where n = @em step
   @param    xtitle          Label on x-axis
   @param    ytitle          Label on y-axis
   @param    format          Title of plot, a printf-style format string
   @return   CPL_ERROR_NONE iff okay

   The input is sent to the previously specified plotter command.

*/
/*----------------------------------------------------------------------------*/
cpl_error_code
uves_plot_image_rows(const cpl_image *image, int first_row, int last_row, int step, 
             const char *xtitle, const char *ytitle, const char *format, ...)
{
    va_list al;
    
    char *pre = NULL;
    char *options = NULL;
    const char *post = "";
    cpl_image *thresholded = NULL;

    assure( image != NULL, CPL_ERROR_NULL_INPUT, "Null image");
    if (xtitle == NULL) xtitle = "";
    if (ytitle == NULL) ytitle = "";
    assure( 1 <= first_row && first_row <= last_row && 
        last_row <= cpl_image_get_size_y(image), 
        CPL_ERROR_ILLEGAL_INPUT,
        "Illegal rows: %d - %d; rows in image = %" CPL_SIZE_FORMAT "",
        first_row, last_row, cpl_image_get_size_y(image));
    
    assure( step >= 1, CPL_ERROR_ILLEGAL_INPUT,
        "Illegal step size: %d", step);

    if (plotting_enabled)
    {
        const char *pre_format;
        int row;
        
        /* Create pre string */
        pre_format = "set grid; set xlabel '%s'; set ylabel '%s';";
        pre = cpl_calloc(strlen(pre_format) + 
                 strlen(xtitle) + strlen(ytitle) + 1,
                 sizeof(char));
        sprintf(pre, pre_format, xtitle, ytitle);

        
        va_start(al, format);
        vsnprintf(title, MAXTITLELENGTH - 1, format, al);
        va_end(al);
        title[MAXTITLELENGTH - 1] = '\0';
        
        options = title_string(title, cpl_image_get_size_x(image));

        /* Threshold each row */
        thresholded = cpl_image_duplicate(image);
        for (row = first_row; row <= last_row; row++)
        {
            int nx = cpl_image_get_size_x(thresholded);
            double median = cpl_image_get_median_window(thresholded,
                                1, first_row,
                                nx, last_row);
            double stdev = cpl_image_get_stdev_window(thresholded,
                                  1, first_row,
                                  nx, last_row);

            double locut = median - 3*stdev;
            double hicut = median + 3*stdev;
            
            int x, pis_rejected;

            for (x = 1; x <= nx; x++)
            {
                double data = 
                cpl_image_get(thresholded, x, row, &pis_rejected);
                if (data < locut) data = locut;
                if (data > hicut) data = hicut;
                cpl_image_set(thresholded, x, row, data);
            }
        }
        
        cpl_plot_image_row(pre,
                  (strcmp(options, "t '%s'") == 0) ? "" : options, 
                  post, 
                  thresholded,
                  first_row, last_row, step);
        
        RECOVER_FROM_ERROR(plotter);
    }
        
  cleanup:
    uves_free_image(&thresholded);
    cpl_free(pre);
    cpl_free(options);

    return cpl_error_get_code();
}

/*----------------------------------------------------------------------------*/
/**
   @brief    Plot one image column
   @param    image           Image to plot
   @param    first_column    First column to plot (counting from 1)
   @param    last_column     Last column to plot (counting from 1)
   @param    step            Plot only every n'th column where n = @em step
   @param    xtitle          Label on x-axis
   @param    ytitle          Label on y-axis
   @param    format          Title of plot, a printf-style format string
   @return   CPL_ERROR_NONE iff okay

   The input is sent to the previously specified plotter command.

   Each row is thresholded to median +- 3 sigma before plotting.

   See also @c uves_plot_image_rows().
*/
/*----------------------------------------------------------------------------*/
cpl_error_code
uves_plot_image_columns(const cpl_image *image, int first_column, int last_column, int step,
            const char *xtitle, const char *ytitle, const char *format, ...)
{
    va_list al;
    
    char *pre = NULL;
    char *options = NULL;
    const char *post = "";
    cpl_image *thresholded = NULL;

    assure( image != NULL, CPL_ERROR_NULL_INPUT, "Null image");
    if (xtitle == NULL) xtitle = "";
    if (ytitle == NULL) ytitle = "";
    assure( 1 <= first_column && first_column <= last_column &&
        last_column <= cpl_image_get_size_x(image), 
        CPL_ERROR_ILLEGAL_INPUT,
        "Illegal columns: %d - %d; columns in image = %" CPL_SIZE_FORMAT "",
        first_column, last_column, cpl_image_get_size_x(image));
    
    assure( step >= 1, CPL_ERROR_ILLEGAL_INPUT,
        "Illegal step size: %d", step);
    
    if (plotting_enabled)
    {
        const char *pre_format;
        int col;

        /* Create pre string */
        pre_format = "set grid; set xlabel '%s'; set ylabel '%s';";
        pre = cpl_calloc(strlen(pre_format) + 
                 strlen(xtitle) + strlen(ytitle) + 1,
                 sizeof(char));
        sprintf(pre, pre_format, xtitle, ytitle);
        
        va_start(al, format);
        vsnprintf(title, MAXTITLELENGTH - 1, format, al);
        va_end(al);
        title[MAXTITLELENGTH - 1] = '\0';
        
        options = title_string(title, cpl_image_get_size_y(image));

        /* Threshold each column */
        thresholded = cpl_image_duplicate(image);
        for (col = first_column; col <= last_column; col++)
        {
            int ny = cpl_image_get_size_x(thresholded);
            double median = cpl_image_get_median_window(thresholded,
                                first_column, 1,
                                last_column, ny);
            double stdev = cpl_image_get_stdev_window(thresholded,
                                  first_column, 1,
                                  last_column, ny);

            double locut = median - 3*stdev;
            double hicut = median + 3*stdev;
            
            int y, pis_rejected;

            for (y = 1; y <= ny; y++)
            {
                double data = cpl_image_get(thresholded, col, y, &pis_rejected);
                if (data < locut) data = locut;
                if (data > hicut) data = hicut;
                cpl_image_set(thresholded, col, y, data);
            }
        }
        
        
        check( cpl_plot_image_col(pre,
                     (strcmp(options, "t '%s'") == 0) ? "" : options, 
                     post, 
                     image,
                     first_column, last_column, step), 
           "Error plotting image");
        
        RECOVER_FROM_ERROR(plotter);
    }
    
  cleanup:
    uves_free_image(&thresholded);
    cpl_free(pre);
    cpl_free(options);

    return cpl_error_get_code();
}

/*----------------------------------------------------------------------------*/
/**
   @brief    Plot bivectors
   @param    bivectors    to plot. The values in all arrays
                          will be thresholded. The threshold limits
              are computed based on the values in the first bivector
   @param    titles       array of titles, same length as bivector array
   @param    N            number of bivectors  
   @param    xtitle       x label
   @param    ytitle       y label
   @return   CPL_ERROR_NONE iff okay

   The input is sent to the previously specified plotter command.
*/
/*----------------------------------------------------------------------------*/
void
uves_plot_bivectors(cpl_bivector **bivectors, char **titles, 
            int N, const char *xtitle,
            const char *ytitle)
{
    char *pre = NULL;
    char **options = NULL;
    const char *post = "";

    options = cpl_calloc(N, sizeof(char *)); /* Initialized to NULL */
    assure_mem( options );

    if (plotting_enabled)
    {
        int npoints, i;
        cpl_bivector *temp;
        char *temps;

        /* Create options strings */
        
        npoints = 0;
        for (i = 0; i < N; i++)
        {
            npoints += cpl_bivector_get_size(bivectors[i]);
        }
        for (i = 0; i < N; i++)
        {        
            options[i] = title_string(titles[i], npoints);
        }
        
        
        {
        double datamax = cpl_vector_get_max(cpl_bivector_get_y(bivectors[0]));
        double datamin = cpl_vector_get_min(cpl_bivector_get_y(bivectors[0]));

        double locut = datamin - 0.2*(datamax-datamin);
        double hicut = datamax + 0.2*(datamax-datamin);
        
        for (i = 0; i < N; i++)
            {
            int j;
            for (j = 0; j < cpl_bivector_get_size(bivectors[i]); j++)
                {
                if (cpl_bivector_get_y_data(bivectors[i])[j] < locut)
                    {
                    cpl_bivector_get_y_data(bivectors[i])[j] = locut;
                    }
                if (cpl_bivector_get_y_data(bivectors[i])[j] > hicut)
                    {
                    cpl_bivector_get_y_data(bivectors[i])[j] = hicut;
                    }
                }
            }
        }

        /* Swap first/last bivectors */
        temp = bivectors[0];
        bivectors[0] = bivectors[N-1];
        bivectors[N-1] = temp;
        
        temps = options[0];
        options[0] = options[N-1];
        options[N-1] = temps;
        
        pre = uves_sprintf(
        "set grid; set xlabel '%s'; set ylabel '%s';", xtitle, ytitle);
        
        cpl_plot_bivectors(pre,
                  (const char **)options,
                  post,
                  (const cpl_bivector **)bivectors, N);
        
        RECOVER_FROM_ERROR(plotter);
    }

  cleanup:
    cpl_free(pre);
    {
    int i;
    for (i = 0; i < N; i++)
        {        
        cpl_free(options[i]);
        }
    }
    cpl_free(options);
    return;
}

/*----------------------------------------------------------------------------*/
/**
   @brief    Plot two table columns
   @param    table        Table to plot
   @param    colx         Name of x-column
   @param    coly         Name of y-column
   @param    format       Title of plot, a printf-style format string
   @return   CPL_ERROR_NONE iff okay

   The column names are also used as the label names, and the column
   comment strings are not used.

   Values outside median +- 3sigma are not plotted.

   The input is sent to the previously specified plotter command.
*/
/*----------------------------------------------------------------------------*/
cpl_error_code
uves_plot_table(const cpl_table *table, const char *colx, const char *coly, 
        const char *format, ...)
{
    va_list al;
    
    char *pre = NULL;
    char *options = NULL;
    const char *post = "";
    cpl_table *thresholded = NULL;

    assure( table != NULL, CPL_ERROR_NULL_INPUT, "Null table");
    assure( colx  != NULL, CPL_ERROR_NULL_INPUT, "Null x column");
    assure( coly  != NULL, CPL_ERROR_NULL_INPUT, "Null y column");
    assure( cpl_table_has_column(table, colx), CPL_ERROR_ILLEGAL_INPUT,
        "No such column: '%s'", colx);
    assure( cpl_table_has_column(table, coly), CPL_ERROR_ILLEGAL_INPUT,
        "No such column: '%s'", coly);
    
    assure( cpl_table_get_column_type(table, colx) == CPL_TYPE_INT    ||
        cpl_table_get_column_type(table, colx) == CPL_TYPE_FLOAT  ||
        cpl_table_get_column_type(table, colx) == CPL_TYPE_DOUBLE, 
        CPL_ERROR_TYPE_MISMATCH,
        "Column '%s' has type '%s'. Numerical type expected",
        colx,
        uves_tostring_cpl_type(cpl_table_get_column_type(table, colx)));
    
    assure( cpl_table_get_column_type(table, coly) == CPL_TYPE_INT    ||
        cpl_table_get_column_type(table, coly) == CPL_TYPE_FLOAT  ||
        cpl_table_get_column_type(table, coly) == CPL_TYPE_DOUBLE, 
        CPL_ERROR_TYPE_MISMATCH,
        "Column '%s' has type '%s'. Numerical type expected",
        coly,
        uves_tostring_cpl_type(cpl_table_get_column_type(table, coly)));
    
    if (plotting_enabled)
    {
        const char *pre_format;

        /* Create options string */
        va_start(al, format);
        vsnprintf(title, MAXTITLELENGTH - 1, format, al);
        va_end(al);
        title[MAXTITLELENGTH - 1] = '\0';
        
        options = title_string(title, cpl_table_get_nrow(table));
        
        /* Create pre string */
        pre_format = "set grid; set xlabel '%s'; set ylabel '%s';";
        pre = cpl_calloc(strlen(pre_format) + strlen(colx) + strlen(coly) + 1, 
                 sizeof(char));  
                             /* It's a couple of bytes more than enough */
        sprintf(pre, pre_format, colx, coly);


        /* Threshold y-values to median +- 3 sigma before plotting */
        {
        double median, sigma, locut, hicut;
        int i;
        
        median = cpl_table_get_column_median(table, coly);
        sigma  = cpl_table_get_column_stdev(table, coly);
        
        locut = median - 3*sigma;
        hicut = median + 3*sigma;
        
        /* Copy the data we need, then threshold */
        thresholded = cpl_table_new(cpl_table_get_nrow(table));
        cpl_table_duplicate_column(thresholded, coly, table, coly);
        cpl_table_duplicate_column(thresholded, colx, table, colx);

        for (i = 0; i < cpl_table_get_nrow(thresholded); i++)
            {
            double data = cpl_table_get(thresholded, coly, i, NULL); /* polymorphic */
            
            if (data < locut && data > hicut)
                {
                cpl_table_set_invalid(thresholded, coly, i);
                }
            }

        }
        cpl_table_erase_invalid(thresholded);

        cpl_plot_column(pre,
                  (strcmp(options, "t '%s'") == 0) ? "" : options, 
                  post,
                  thresholded, colx, coly);

        RECOVER_FROM_ERROR(plotter);
    }

  cleanup:
    uves_free_table(&thresholded);
    cpl_free(pre);
    cpl_free(options);

    return cpl_error_get_code();
}


/*----------------------------------------------------------------------------*/
/**
   @brief    Generate a gnuplot-syntax title string
   @param    plot_title      Title of plot
   @param    npoints    Number of points in plot
   @return   The dynamically allocated initialization string

   Also the plotting style (isolated points or connected points)
   is defined, depending on the number of points in the plot.
*/
/*----------------------------------------------------------------------------*/
static char *
title_string(const char *plot_title, int npoints)
{
    /* Option to choose plotting style
     * depending on the number of points 
     */
    const char *options = (npoints > 100) ?
    "w points pointsize 1" :
    "w linespoints pointsize 1";
    /* If less than, say, 100 points, connect them with lines */

    size_t length = strlen("t '' ") + strlen(plot_title) + strlen(options) + 1;
    char *result = cpl_calloc(length, sizeof(char));
    
    snprintf(result, length, "t '%s' %s", plot_title, options);
    
    return result;
}

/**@}*/
