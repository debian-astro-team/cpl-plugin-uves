/*                                                                              *
 *   This file is part of the ESO UVES  Pipeline                                *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */

/*
 * $Author: amodigli $
 * $Date: 2011-12-08 14:04:14 $
 * $Revision: 1.8 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.7  2010/09/24 09:32:04  amodigli
 * put back QFITS dependency to fix problem spot by NRI on FIBER mode (with MIDAS calibs) data
 *
 * Revision 1.5  2007/06/28 09:18:03  jmlarsen
 * Return actualy polynomial degree used
 *
 * Revision 1.4  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.3  2007/03/28 14:02:23  jmlarsen
 * Removed unused parameter
 *
 * Revision 1.2  2007/03/28 11:39:11  jmlarsen
 * Killed MIDAS flag, removed dead code
 *
 * Revision 1.1  2006/02/03 07:46:30  jmlarsen
 * Moved recipe implementations to ./uves directory
 *
 * Revision 1.10  2005/12/19 16:17:55  jmlarsen
 * Replaced bool -> int
 *
 */
#ifndef UVES_ORDERPOS_FOLLOW_H
#define UVES_ORDERPOS_FOLLOW_H
#include <uves_cpl_size.h>
#include <uves_utils_polynomial.h>
#include <cpl.h>
#include <stdbool.h>

cpl_table *uves_locate_orders(const cpl_image *inputimage, const cpl_image *noise,
                  cpl_table *ordertable, int TRACESTEP,
                  double MINTHRESH, double MAXGAP,
                  double MAXRMS, int *DEFPOL1, int *DEFPOL2, double KAPPA,
                  polynomial **bivariate_fit, int *orders_traced);

#endif /* UVES_ORDERPOS_FOLLOW_H */
