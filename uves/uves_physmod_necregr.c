/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */

/*
 * $Author: amodigli $
 * $Date: 2010-09-24 09:32:07 $
 * $Revision: 1.7 $
 * $Name: not supported by cvs2svn $
 *
 */

/*----------------------------------------------------------------------------*/
/**
 * @addtogroup uves_physmod
 */
/*----------------------------------------------------------------------------*/
/**@{*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/
#include <uves_physmod_necregr.h>

#include <uves_error.h>
#include <uves_msg.h>

/*-----------------------------------------------------------------------------
                                Defines
 -----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
                            Functions prototypes
 ----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
                            Static variables
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                            Functions code
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
  @brief    This procedure does a linear fit of independent orders 


  @param    ord_tbl    The input order table
  @param    reg_tbl    The output regression table. 

  @return   0 if everything is ok, -1 otherwise


 This procedure does a linear fit of independent orders and writes the rms in a table
 */
/*----------------------------------------------------------------------------*/

int
uves_physmod_necregr(cpl_table** ord_tbl, cpl_table** reg_tbl)

{

  int  order=0;
  int  nb_order=0;

  int  order_nb=0;
  int  row=0;
  int ncol=0;
  int  nrow=0;

  int  selected=0;
  int  null=0;
  int ord_min=0;
  int ord_max=0;

  double    x=0., y=0.;
  double    det=0., a=0., b=0., rms=0.;

  uves_msg_debug("start %s",__func__);
  nrow=cpl_table_get_nrow(*ord_tbl);
  ncol=cpl_table_get_ncol(*ord_tbl);

  uves_msg_debug("nrow=%d ncol=%d",nrow,ncol);
  ord_min=cpl_table_get_column_min(*ord_tbl,"ORDER");
  ord_max=cpl_table_get_column_max(*ord_tbl,"ORDER");
  nb_order=ord_max-ord_min+1;
  *reg_tbl=cpl_table_new(100);
  cpl_table_new_column(*reg_tbl,"ORDER",CPL_TYPE_INT);
  cpl_table_new_column(*reg_tbl,"RMS",CPL_TYPE_DOUBLE);
  
  row = 0;
  selected=1;

  for (order=0; order<nb_order; order++) {

    double cnt=0., sx=0., sy=0., sx2=0., sxy=0., sy2 = 0.;
    order_nb=cpl_table_get_int(*ord_tbl,"ORDER",row,&null);

    int present_order = order_nb;
      
    while (present_order == order_nb) {

      if (selected) {

    x=cpl_table_get_double(*ord_tbl,"X",row,&null);
    y=cpl_table_get_double(*ord_tbl,"Y",row,&null);

    cnt += 1., sx += x, sy += y, sx2 += x*x, sy2 += y*y, sxy += x*y;
      }

      if (row >= (nrow-1)) break;
      row++;
      present_order=cpl_table_get_int(*ord_tbl,"ORDER",row,&null);

    }
      

    if (cnt >= 3) {
      det = cnt*sx2 - sx*sx;
      a   = (sy*sx2 - sx*sxy)/det;
      b   = (cnt*sxy - sx*sy)/det;
      rms = (sy2 - a*a*cnt - 2.*b*a*sx - b*b*sx2)/cnt;
      if (rms < 0. && rms > -0.05) rms = 0.;
      rms = sqrt(rms);
    }
    else    rms = 99999.;

    cpl_table_set_int(*reg_tbl,"ORDER",order,order_nb);
    cpl_table_set_double(*reg_tbl,"RMS",order,rms);

  }
  cpl_table_erase_invalid_rows(*reg_tbl);
  uves_msg_debug("end %s",__func__);
  return 0;
}
/**@}*/

