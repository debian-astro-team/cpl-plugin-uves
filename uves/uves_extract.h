/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2013-08-08 13:36:46 $
 * $Revision: 1.38 $
 * $Name: not supported by cvs2svn $
 *
 */
#ifndef UVES_EXTRACT_H
#define UVES_EXTRACT_H
#include <uves_cpl_size.h>

#include <uves_utils_polynomial.h>
#include <uves_chip.h>

#include <cpl.h>

#include <stdbool.h>

typedef enum {EXTRACT_AVERAGE,
          EXTRACT_LINEAR,
          EXTRACT_2D,
          EXTRACT_ARCLAMP,
          EXTRACT_WEIGHTED,
          EXTRACT_OPTIMAL} extract_method;


cpl_parameterlist *uves_extract_define_parameters(void);

extract_method
uves_get_extract_method(const cpl_parameterlist *parameters, 
            const char *context, const char *subcontext);

cpl_image *
uves_extract(cpl_image *image, 
             cpl_image *image_noise, 
         const uves_propertylist *image_header,
             const cpl_table *ordertable, 
             const polynomial *order_locations_raw,
             double slit_length, 
             double offset,
             const cpl_parameterlist *parameters, 
             const char *context,
             const char *mode,
             bool extract_partial,
         bool debug_mode,
         enum uves_chip chip,
             uves_propertylist **header, 
             cpl_image **spectrum_noise,
             cpl_image **sky_spectrum,
             cpl_image **sky_spectrum_noise,
             cpl_table **cosmic_mask,
             cpl_image **cosmic_image,
             cpl_table **profile_table,
             cpl_image **weights,
             cpl_table **info_tbl,
             cpl_table **order_trace);

#endif
