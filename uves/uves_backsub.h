/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2011-12-08 13:56:43 $
 * $Revision: 1.14 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.13  2010/09/24 09:32:02  amodigli
 * put back QFITS dependency to fix problem spot by NRI on FIBER mode (with MIDAS calibs) data
 *
 * Revision 1.11  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.10  2007/05/22 11:29:39  jmlarsen
 * Removed MIDAS flag for good
 *
 * Revision 1.9  2007/04/24 12:50:29  jmlarsen
 * Replaced cpl_propertylist -> uves_propertylist which is much faster
 *
 * Revision 1.8  2007/03/28 11:38:23  jmlarsen
 * Killed MIDAS flag, removed dead code
 *
 * Revision 1.7  2006/08/17 13:56:52  jmlarsen
 * Reduced max line length
 *
 * Revision 1.6  2006/03/24 13:54:24  jmlarsen
 * Use different smoothing default values depending on type of frame (flat or science)
 *
 * Revision 1.5  2006/02/28 09:15:22  jmlarsen
 * Minor update
 *
 * Revision 1.4  2005/12/19 16:17:56  jmlarsen
 * Replaced bool -> int
 *
 */
#ifndef UVES_BACKSUB_H
#define UVES_BACKSUB_H

/*-----------------------------------------------------------------------------
                    Includes
 -----------------------------------------------------------------------------*/
#include <uves_cpl_size.h>
#include <uves_utils_polynomial.h>
#include <uves_chip.h>

#include <cpl.h>
#include <stdbool.h>

/*-----------------------------------------------------------------------------
                             Typedefs
 -----------------------------------------------------------------------------*/

typedef enum {BM_MEDIAN,
          BM_MINIMUM,
              BM_NO} background_measure_method;

/*-----------------------------------------------------------------------------
                             Defines
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                                   Prototypes
 -----------------------------------------------------------------------------*/

cpl_parameterlist *uves_backsub_define_parameters(void);

background_measure_method
uves_get_bm_method(const cpl_parameterlist *parameters, 
           const char *context, const char *subcontext);

cpl_error_code uves_backsub_smooth(cpl_image *image, int RADX, int RADY, int ITER);
cpl_error_code uves_backsub_poly(cpl_image *image,
                 const cpl_table *orders, 
                 const polynomial *order_locations, 
                 background_measure_method BM_METHOD,
                 int NPOINTS,
                 int radius_y,
                 int DEGX, 
                 int DEGY,
                 double KAPPA);

cpl_error_code uves_backsub_spline(cpl_image *image, const uves_propertylist *raw_header,
                   const cpl_table *ordertable, 
                   const polynomial *order_locations,
                   const cpl_parameterlist *parlist, 
                   const char *context, 
                   enum uves_chip chip,
                   bool flat_field,
                   cpl_image **background);
#endif
