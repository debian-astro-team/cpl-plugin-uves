/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */

/*
 * $Author: amodigli $
 * $Date: 2012-03-02 17:01:40 $
 * $Revision: 1.25 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.24  2011/12/08 14:04:33  amodigli
 * Fix warnings with CPL6
 *
 * Revision 1.23  2010/09/24 09:32:05  amodigli
 * put back QFITS dependency to fix problem spot by NRI on FIBER mode (with MIDAS calibs) data
 *
 * Revision 1.21  2010/01/04 14:02:54  amodigli
 * less verbose Hough transform computation
 *
 * Revision 1.20  2007/08/27 12:31:08  amodigli
 * uves_msg_progress should go from 0 to ymax as ymax is actually the maximum y pixel value where the Hoght transform should be computed. i fixed this and added more check statements
 *
 * Revision 1.19  2007/08/21 13:08:26  jmlarsen
 * Removed irplib_access module, largely deprecated by CPL-4
 *
 * Revision 1.18  2007/06/06 08:17:33  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.17  2007/04/24 12:50:29  jmlarsen
 * Replaced cpl_propertylist -> uves_propertylist which is much faster
 *
 * Revision 1.16  2007/04/20 14:44:47  jmlarsen
 * Minor output message change
 *
 * Revision 1.15  2007/04/17 09:34:38  jmlarsen
 * Parametrize the assumption about consecutive orders (for FLAMES support)
 *
 * Revision 1.14  2007/04/12 12:02:41  jmlarsen
 * Added assertions for documentation purposes
 *
 * Revision 1.13  2007/04/10 07:08:30  jmlarsen
 * Make sure that detected orders are always consecutive
 *
 * Revision 1.12  2006/11/15 15:02:14  jmlarsen
 * Implemented const safe workarounds for CPL functions
 *
 * Revision 1.10  2006/11/15 14:04:08  jmlarsen
 * Removed non-const version of parameterlist_get_first/last/next which is
 * already in CPL, added const-safe wrapper, unwrapper and deallocator functions
 *
 * Revision 1.9  2006/11/06 15:19:41  jmlarsen
 * Removed unused include directives
 *
 * Revision 1.8  2006/08/17 13:56:53  jmlarsen
 * Reduced max line length
 *
 * Revision 1.7  2006/06/08 08:42:53  jmlarsen
 * Added support for computing Hough transform on image subwindow
 *
 * Revision 1.6  2006/06/01 14:43:17  jmlarsen
 * Added missing documentation
 *
 * Revision 1.5  2006/04/06 08:45:15  jmlarsen
 * Changed number of significant digits when printing percentage
 *
 * Revision 1.4  2006/03/09 13:58:26  jmlarsen
 * Minor optimization of Hough calculation
 *
 * Revision 1.3  2006/03/03 13:54:11  jmlarsen
 * Changed syntax of check macro
 *
 * Revision 1.2  2006/02/15 13:19:15  jmlarsen
 * Reduced source code max. line length
 *
 * Revision 1.1  2006/02/03 07:46:30  jmlarsen
 * Moved recipe implementations to ./uves directory
 *
 * Revision 1.24  2005/12/19 16:17:55  jmlarsen
 * Replaced bool -> int
 *
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*----------------------------------------------------------------------------*/
/**
 * @addtogroup uves_orderpos
 */
/*----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#include <uves_orderpos_hough.h>

#include <uves_utils.h>
#include <uves_utils_wrappers.h>
#include <uves_error.h>
#include <uves_msg.h>

#include <cpl.h>

/*-----------------------------------------------------------------------------
                                Defines
 -----------------------------------------------------------------------------*/

/* Define macros that map from x-coordinate (integer) in Hough space to slope 
   in image space (double) and the inverse function  */
#define SLOPE(hx)    (   MINSLOPE + ( ((double)(hx)) / SLOPERES  )   ) * (MAXSLOPE - MINSLOPE)
#define SLOPEINV(a) \
   uves_round_double( SLOPERES * ( ((double)(a)) - MINSLOPE ) / (MAXSLOPE - MINSLOPE))

/* Convert from pixel coordinate to intersept, and the other way */
#define INTERSEPT(hy) (minintersept + hy)
#define INTERSEPTINV(b) (b - minintersept)

/**@{*/
/*-----------------------------------------------------------------------------
                                Forward declarations
 -----------------------------------------------------------------------------*/

static cpl_table *detect_lines(cpl_image *htrans, int minintersept,
                   const cpl_image *inputimage, 
                   int NORDERS, bool norders_is_guess, int SAMPLEWIDTH, 
                   double PTHRES, double MINSLOPE, double MAXSLOPE, int SLOPERES,
                               bool consecutive);
static cpl_error_code delete_peak(cpl_image *htrans, int minintersept, int hxmax, int hymax,
                  int SPACING, int imagewidth, int SAMPLEWIDTH, 
                  double MINSLOPE, double MAXSLOPE, int SLOPERES);
static int firsttrace(int nx, int SAMPLEWIDTH);
static int calculate_spacing(const cpl_image *, int x);
static double autocorr(const cpl_image *image, int x, int shift);
static cpl_error_code update_max(const cpl_image *htrans, /* Hough image */
                 int *xmax,               /* peak location */
                 int *ymax,
                 int SPACING,       /* inter-order seperation */
                 int imagewidth,    /* width of input image */
                 int SAMPLEWIDTH,   /* seperation of traces in input image */
                 double MINSLOPE,
                 double MAXSLOPE,
                 int SLOPERES);

/*----------------------------------------------------------------------------*/
/**
   @brief    Compute Hough transform and detect lines
   @param    image             Input image
   @param    ymin              First image row to consider (inclusive, FITS convention)
   @param    ymax              Last image row to consider (inclusive, FITS convention)
   @param    NORDERS           The number of orders to detect, or zero
   @param    norders_is_guess  Flag indicating if we are allowed detect fewer
                               orders than specified by @em NORDERS.
   @param    SAMPLEWIDTH       The seperation of sample traces in input image
   @param    PTHRES            In automatic mode, the ratio between the dimmest
                               and next-dimmest line
   @param    MINSLOPE          Minimum line slope
   @param    MAXSLOPE          Maximum line slope
   @param    SLOPERES          Width of Hough image (number of slopes represented)
   @param    consecutive       If true, orders are assumed to be consecutive
   @param    htrans            (output) computed Hough transform with peaks deleted
   @param    htrans_original   (output) computed Hough transform (returned)
   @return   Table describing the detected lines (see @c detect_lines() for details )
   
   This function computes the Hough transform of the image @em image. The input image 
   is traced along vertical lines seperated by @em SAMPLEWIDTH.

   See also @c detect_lines().

   Possible order line slopes range from @em MINSLOPE to @em MAXSLOPE .
   The function assumes that all slopes are positive and small. To detect lines 
   with slopes larger than 0.3 - 0.5 or so,
   the algorithm should be changed to use polar (i.e.
   rotation invariant) coordinates in Hough space.
   
**/
/*----------------------------------------------------------------------------*/

cpl_table *uves_hough(const cpl_image *image, int ymin, int ymax, int NORDERS, 
              bool norders_is_guess,
              int SAMPLEWIDTH, double PTHRES, double MINSLOPE, double MAXSLOPE,
              int SLOPERES, bool consecutive,
                      cpl_image **htrans, cpl_image **htrans_original)
{

    cpl_table *ordertable = NULL;   /* The result table */
    
    int nx = 0;   /* Dimensions of input image */
    int ny = 0;
    int minintersept = 0;  /* The intersepts represented by the Hough image are 
                  all integer values */
    int maxintersept = 0;  /* in the interval ] minintersept ; maxintersept ] */
    int firstcol;
    const double *image_data = NULL;    /* For efficiency */
    double *htrans_data = NULL;

    *htrans = NULL;                /* Hough transform image (peaks deleted), returned */
    *htrans_original = NULL;       /* Hough transform image, returned */
    
    /* Check input */
    assure_nomsg( image != NULL, CPL_ERROR_NULL_INPUT);
    assure( cpl_image_get_type(image) == CPL_TYPE_DOUBLE, CPL_ERROR_INVALID_TYPE, 
        "Input image has wrong type. Must be of type double");
    assure( 0 <= MINSLOPE, CPL_ERROR_ILLEGAL_INPUT,
            "minslope = %f must be non-negative", MINSLOPE);
    assure( 0 <= MAXSLOPE, CPL_ERROR_ILLEGAL_INPUT, 
            "maxslope = %f must be non-negative", MAXSLOPE);
    assure( MINSLOPE < MAXSLOPE, CPL_ERROR_INCOMPATIBLE_INPUT, "minslope = %f; maxslope = %f",
            MINSLOPE, MAXSLOPE);
    assure( 0 < SLOPERES, CPL_ERROR_ILLEGAL_INPUT,
            "Hough image width = %d, must be positive", SLOPERES);

    /* For efficiency don't support bad pixels (possible to do
       later if needed) */
    assure (cpl_image_count_rejected(image) == 0,
        CPL_ERROR_UNSUPPORTED_MODE, "Input image has %" CPL_SIZE_FORMAT " bad pixels",
        cpl_image_count_rejected(image));


    if (MAXSLOPE > 0.5) 
    {
        uves_msg_warning("Max possible slope is %f, which is larger than 0.5. "
                 "Results might be unreliable", MAXSLOPE);
    }
    
    nx = cpl_image_get_size_x(image);
    ny = cpl_image_get_size_y(image);

    assure( 1 <= ymin && ymin <= ymax && ymax <= ny, CPL_ERROR_ILLEGAL_INPUT,
        "Illegal y-range: %d - %d (image height is %d)", ymin, ymax, ny);

    /* Calculate min. and max. intersepts represented.
       For simplicity, the Hough image is always full size,
       even if not (ymin == 1 && ymax == ny)  */
    maxintersept = ny;
    minintersept = uves_round_double(0 - nx*MAXSLOPE);

    /* Create the Hough image. Pixels are initialsed to zero */
    check( *htrans = cpl_image_new(SLOPERES,                      /* Image width */
                   maxintersept - minintersept,   /* Image height */
                   CPL_TYPE_DOUBLE),
       "Could not create image");
    
    check_nomsg( image_data = cpl_image_get_data_double_const(image) );
    check_nomsg( htrans_data = cpl_image_get_data_double(*htrans) );

    uves_msg("Calculating Hough transform");
    
    /* Locate the leftmost trace column */
    check_nomsg(firstcol = firsttrace(nx, SAMPLEWIDTH));    

    check_nomsg(UVES_TIME_START("The loop"));

    /* Loop through input image subwindow and calculate the Hough image */
    {
    int x, y;
    for (y = ymin; y <= ymax; y += 1)
        {
    //if use_guess_tab =1 it may happen that ymax< ny
    //uves_msg_progress should have first argoment which goes from 0 till the
    //second argument value, in our case from 0 to ymax. 
	  check_nomsg(uves_msg_debug("Calculating Hough transform %d %d",
				   y - 1, ymax));
	 
        for (x = firstcol; x <= nx; x += SAMPLEWIDTH)
            {
            /* Transform image point at (x,y) to line in Hough space */
            double pixelvalue;
            int hx, hy;

            for (hx = 1; hx <= SLOPERES; hx++)
                {
                check_nomsg(hy = INTERSEPTINV(uves_round_double(y - x*SLOPE(hx))));
                /* y = intersept + slope * x */
                
                                /* Slow: check( pixelvalue = 
                   cpl_image_get(image, x, y, &pis_rejected),
                   "Could not read pixel at (%d, %d) in input image", x, y); */
                check_nomsg(pixelvalue = image_data[(x-1) + (y-1)*nx]);
                
                /* Add the pixelvalue to Hough image (hx, hy) */
                /* Not supported for now: if (!pis_rejected) */
                
                /* Slow: check( current = 
                   cpl_image_get(*htrans, hx, hy, &pis_rejected),
                   "Could not read pixel at (%d, %d) in Hough image", hx, hy);
                   check(           
                   cpl_image_set(*htrans, hx, hy, current + pixelvalue), 
                   "Could not update pixel at (%d, %d) in Hough image", 
                   hx, hy); */

                check_nomsg(htrans_data[(hx-1) + (hy-1)*SLOPERES] += pixelvalue);
                }
            }
        }
    }

    UVES_TIME_END;
    
    check( *htrans_original = cpl_image_duplicate(*htrans), "Error copying hough image");

    /* Calculate order table from Hough image */
    check( ordertable = detect_lines(*htrans,
                     minintersept,
                     image,
                     NORDERS,
                     norders_is_guess,
                     SAMPLEWIDTH,
                     PTHRES,
                     MINSLOPE,
                     MAXSLOPE,
                     SLOPERES,
                                     consecutive),
       "Could not detect lines in hough image");
    
    passure( cpl_table_get_ncol(ordertable) == 4, "%" CPL_SIZE_FORMAT "", cpl_table_get_ncol(ordertable));
    passure( cpl_table_has_column(ordertable, "Slope"), " ");
    passure( cpl_table_has_column(ordertable, "Intersept"), " ");
    passure( cpl_table_has_column(ordertable, "Spacing"), " ");
    passure( cpl_table_has_column(ordertable, "Order"), " ");
    
  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE)
    {
        uves_free_image(htrans);
        uves_free_image(htrans_original);
        uves_free_table(&ordertable);
    }
    return ordertable;
}

/*----------------------------------------------------------------------------*/
/**
   @brief    Detect order lines from the Hough image
   @param    htrans            The Hough transform image (detected features will be deleted)
   @param    minintersept      Minimum intersept represented in Hough image
   @param    inputimage        The raw image
   @param    NORDERS           The number of orders to detect
   @param    norders_is_guess  Flag indicating if we are allowed detect fewer
                               orders than specified by @em NORDERS.
   @param    SAMPLEWIDTH       The seperation of sample traces in the raw image
   @param    PTHRES            If @em NORDERS is zero, or if @em norders_is_guess is true, 
                               this is the threshold used for peak detection (detection 
                               stops when intensity drops to below PTHRES * (previous intensity))
   @param    MINSLOPE          Minimum line slope to search for
   @param    MAXSLOPE          Maximum line slope to search for
   @param    SLOPERES          Width of Hough image
   @param    consecutive       If true, orders are not assumed to be consecutive
   @return   CPL_ERROR_NONE    A table describing the detected lines

   
   This function locates the peaks in the input Hough image.
   After detecting a peak, the peak itself and the area around it are deleted to 
   avoid several detections of the same feature (see @c delete_peak() ).

   The peak locations are initially defined as the local maximum points but 
   are refined by calculating the centroid of the local area around the maximum
   (see @c update_max() ).
        
   The number of order lines to detect is specified by the parameter @em NORDERS. 
   If this parameter is set to zero, the function will detect lines until the intensity 
   of the next candidate drops to below @em PTHRES times the dimmest line (@em PTHRES
   must be between 0 and 1; if @em norders_is_guess is false, the parameter
   @em PTHRES is ignored).

   An important parameter for the peak removal to work is the (approximate) 
   interorder spacing. This parameter is estimated as the raw image height
   divided by the predicted number of orders (@em NORDERS). In automatic mode
   it is estimated as the first minimum of the auto-correlation function along
   the column in the Hough image containing the global maximum. Note that these 
   methods assume that the inter order spacing does not vary too much as function
   of order number.

   The results of the detection are written to the returned table in the
   columns @em 'Slope' (slope of order), @em 'Intersept' (y-intersept of order)
   and @em 'Spacing' (appropriate interorder distance at this order). The table
   is sorted according to @em 'Intersept'. The column @em 'Order' is a label of
   the order number starting from 1.
   
**/
/*----------------------------------------------------------------------------*/

static cpl_table *detect_lines(cpl_image *htrans, int minintersept, 
                   const cpl_image *inputimage, int NORDERS,
                   bool norders_is_guess,
                   int SAMPLEWIDTH, double PTHRES, double MINSLOPE, 
                   double MAXSLOPE, int SLOPERES,
                               bool consecutive)
{
    cpl_table *results = NULL;   /* The result order table */
    
    /* Local variables */
    cpl_stats *stats = NULL;      /* Used for finding peaks in Hough image */
    uves_propertylist *pl = NULL;  /* Used for sorting the order table */
    
    bool automatic = false;        /* Flag indicating automatic mode */
    int tablesize = 0;
    double intensity = 0;         /* Line intensity */
    double prev_intensity = 0;    /* Intensity of previously detected line */
    int norders_detected = 0;     /* Peaks detected so far */
    int SPACING = 0;              /* Interorder spacing   */
    double globmax = 0;           /* Global maximum value */

    /* Check input */
    passure( htrans != NULL, " ");
    passure( inputimage != NULL, " ");
    passure( NORDERS >= 0, "%d", NORDERS);
    passure( SAMPLEWIDTH > 0, "%d", SAMPLEWIDTH);
    passure( 0 <= PTHRES && PTHRES <= 1, "%f", PTHRES);
    passure( SLOPERES > 0, "%d", SLOPERES);

    /* Do we know how many orders to detect?
       If not, enter automatic mode */
    if (NORDERS == 0)
    {
        uves_msg("Could not find information about predicted number of orders. "
             "Entering automatic mode (threshold = %f)", PTHRES);
        automatic = true;
    }
    else
    {
        uves_msg("Searching for %d (%s) order lines", 
             NORDERS, (norders_is_guess) ? "or less" : "exactly");
        automatic = false;
    }
    
    /* Allocate memory */
    if (automatic)
    {
        /* The input image height is a (conservative) upper limit
           on the number of echelle orders in the image */
        tablesize = cpl_image_get_size_y(inputimage);
    }
    else 
    {
        tablesize = NORDERS;
    }
    
    /* Initialize order table */
    check(( results = cpl_table_new(tablesize),
        cpl_table_new_column(results, "Slope", CPL_TYPE_DOUBLE),
        cpl_table_new_column(results, "Intersept", CPL_TYPE_DOUBLE),
        cpl_table_new_column(results, "Spacing", CPL_TYPE_INT)),
       "Could not initialize order table");
    
    /* Find maximum in Hough image */
    check( stats = cpl_stats_new_from_image(htrans, CPL_STATS_MAX | CPL_STATS_MAXPOS), 
       "Could not get statistics on Hough image");
    
    /*
     *  Remember the first (i.e. global) maximum.
     *  In 1st iteration, prev_intensity == intensity 
     */
    check( globmax = cpl_stats_get_max(stats), 
       "Could not locate first maximum in hough image" );

    prev_intensity = globmax;

    /*  Repeat until the predicted number of orders is found,
     *   or (in automatic mode) until the line intensity is less than threshold 
     */
    while (   (!automatic && 
           (norders_detected < NORDERS &&
        (!norders_is_guess || cpl_stats_get_max(stats) >= PTHRES*prev_intensity)))
          || (automatic 
          && cpl_stats_get_max(stats) >= PTHRES*prev_intensity)
    )
    {
        int xmax = 0;
        int ymax = 0;
        double slope = 0;
        double isept = 0;
        
        norders_detected += 1;
        check((intensity = cpl_stats_get_max(stats),
           xmax      = cpl_stats_get_max_x(stats),
           ymax      = cpl_stats_get_max_y(stats)),
        "Could not locate maximum");
        
        /* Print (normalized) intensity of detection */
        uves_msg_debug("%d. detection: intensity = %f",
               norders_detected, intensity/globmax * 100);

        /* Warn if intensity suddenly dropped */
        if (intensity < PTHRES * prev_intensity)
        {
            uves_msg_warning("Intensity of %d. line is only "
                     "%f %% of %d. line. Detecting too many orders?",
                     norders_detected, intensity / prev_intensity * 100,
                     norders_detected - 1);
        }
        prev_intensity = intensity;
        
        /* After detecting the first peak, estimate the approximate average order spacing */
        if (norders_detected == 1) 
        {
            if (!automatic) 
            {
                SPACING = uves_round_double( 
                cpl_image_get_size_y(inputimage) / NORDERS );
            }
            else
            {  /* If the number of orders to detect is unknown, 
                  derive the interorder spacing from the peak locations
                  in the Hough image */
                check( SPACING = calculate_spacing(htrans, xmax), 
                   "Could not estimate interorder spacing");
            }
            
            uves_msg("Estimated order spacing is %d pixels", SPACING);
        }
        
        /* Get a more precise peak location */
        check( update_max(htrans,
                  &xmax,
                  &ymax,
                  SPACING,
                  cpl_image_get_size_x(inputimage),
                  SAMPLEWIDTH,
                  MINSLOPE,
                  MAXSLOPE,
                  SLOPERES), "Could not update peak position");
        
        check( delete_peak(htrans, 
                   minintersept, xmax, ymax, SPACING,
                   cpl_image_get_size_x(inputimage), 
                   SAMPLEWIDTH, 
                   MINSLOPE, MAXSLOPE, SLOPERES), 
           "Could not delete peak in hough image");

        slope = SLOPE(xmax);
        isept = minintersept + ymax;

        /* Make sure that the detection terminates if caller specified 'bad' parameters */
        assure( norders_detected <= tablesize, CPL_ERROR_ILLEGAL_OUTPUT,
            "%d orders detected. This is way too many. "
            "Try to decrease NORDERS (from %d) or increase PTHRES (from %f)", 
            norders_detected, NORDERS, PTHRES);
        
        check(( cpl_table_set_double(results, "Slope"       , norders_detected - 1, slope),
            cpl_table_set_double(results, "Intersept"   , norders_detected - 1, isept),
            cpl_table_set_int   (results, "Spacing"     , norders_detected - 1, SPACING)),
           "Could add order line to order table");
        
        /* Locate the next potential line */
        check(( uves_free_stats(&stats),
            stats = cpl_stats_new_from_image(htrans, CPL_STATS_MAX | CPL_STATS_MAXPOS)), 
            "Could not get statistics on hough image");
    }
    
    uves_msg("The intensity of the faintest line is %f of "
         "the intensity of the brightest line", intensity / globmax);
    uves_msg("Intensity of next (undetected) line is %f of the "
         "intensity of the brightest line", cpl_stats_get_max(stats)/globmax);

    if ( cpl_stats_get_max(stats) > 0.5 * intensity )
    {
        uves_msg_warning("Brightest undetected line with intensity %.2f %% "
                 "of faintest line. Detecting too few orders?",
                 cpl_stats_get_max(stats) / intensity * 100);
    }
    
    /* Clean up table */
    check( cpl_table_set_size(results, norders_detected), 
       "Could not remove extra rows from order table");

    /* Sort the order table so that order numbers increase from 
       bottom (low y) to top (high y) of image */
    check( uves_sort_table_1(results, "Intersept", false),    /* reverse flag = false */
       "Could not sort order table");

    /* Number orders, starting from 1 */
    {
    int i;
    cpl_table_new_column(results, "Order", CPL_TYPE_INT);
    for (i = 0; i < cpl_table_get_nrow(results); i++)
        {
            cpl_table_set_int(results, "Order", i, i+1);
        }
    }

    if (consecutive)
    /* Make sure we have consecutive orders.
       This assumes that the order separation varies with less than
       50 % from one order to the next */
    {
        int i;
        double dist = 0;
        int minorder, maxorder;
        int n_removed;

        /* From middle and up */
        maxorder = -1;        
        for (i = cpl_table_get_nrow(results)/2;
             i <= cpl_table_get_nrow(results) - 2 && maxorder < 0; 
             i++)
            {
                if (i == cpl_table_get_nrow(results)/2)
                    /* initialize dist */
                    {
                        dist =
                            cpl_table_get_double(results, "Intersept", i+1, NULL)-
                            cpl_table_get_double(results, "Intersept", i, NULL);
                    }
                else
                    {
                        double new_dist = 
                            cpl_table_get_double(results, "Intersept", i+1, NULL)-
                            cpl_table_get_double(results, "Intersept", i, NULL);
                        
                        uves_msg_debug("Order %d - %d separation = %.4f pixels", 
                                       cpl_table_get_int(results, "Order", i, NULL),
                                       cpl_table_get_int(results, "Order", i+1, NULL),
                                       new_dist);

                        if (0.5*dist <= new_dist && new_dist <= 1.5*dist)
                            {
                                /* OK. It's probably the next consecutive order */
                            }
                        else
                            {
                                /* Something's wrong. Stop */
                                maxorder = cpl_table_get_int(results, "Order", i, NULL);
                                
                                uves_msg_warning("Order separation jumps from %.2f pixels to "
                                                 "%.2f pixels. Discarding order(s) %d and above",
                                                 dist, new_dist, 
                                                 maxorder+1);
                            }
                    }
            }

        /* From middle and down */
        minorder = -1;
        for (i = cpl_table_get_nrow(results)/2;
             i >= 1 && minorder < 0;
             i--)
            {
                if (i == cpl_table_get_nrow(results)/2)
                    /* initialize dist */
                    {
                        dist =
                            cpl_table_get_double(results, "Intersept", i, NULL)-
                            cpl_table_get_double(results, "Intersept", i-1, NULL);
                    }
                else
                    {
                        double new_dist = 
                            cpl_table_get_double(results, "Intersept", i, NULL)-
                            cpl_table_get_double(results, "Intersept", i-1, NULL);
                        
                        uves_msg_debug("Order %d - %d separation = %.4f pixels", 
                                       cpl_table_get_int(results, "Order", i-1, NULL),
                                       cpl_table_get_int(results, "Order", i, NULL),
                                       new_dist);

                        if (0.5*dist <= new_dist && new_dist <= 1.5*dist)
                            {
                                /* OK. It's probably the next consecutive order */
                            }
                        else
                            {
                                /* Something's wrong. Stop */
                                minorder = cpl_table_get_int(results, "Order", i, NULL);
                                
                                uves_msg_warning("Order separation jumps from %.2f pixels to "
                                                 "%.2f pixels. Discarding order(s) %d and below",
                                                 dist, new_dist, 
                                                 minorder-1);
                            }
                    }
            }

        n_removed = 0;
        if (maxorder > 0)
            {
                check_nomsg( n_removed += uves_erase_table_rows(results, "Order",
                                                                CPL_GREATER_THAN, maxorder));
            }
        if (minorder > 0)
            {
                check_nomsg( n_removed += uves_erase_table_rows(results, "Order",
                                                                CPL_LESS_THAN, minorder));
            }
        
        uves_msg_debug("%d order(s) removed", n_removed);
        norders_detected -= n_removed;
    }

    /* Renumber orders, starting from 1 */
    {
    int i;
    for (i = 0; i < cpl_table_get_nrow(results); i++)
        {
            cpl_table_set_int(results, "Order", i, i+1);
        }
    }
    
    uves_msg("Hough transform detected %d orders", norders_detected);
    
    passure( norders_detected == cpl_table_get_nrow(results), "%d %" CPL_SIZE_FORMAT "", 
         norders_detected, cpl_table_get_nrow(results));

  cleanup:
    uves_free_stats(&stats);
    uves_free_propertylist(&pl);
    if (cpl_error_get_code() != CPL_ERROR_NONE)
    {
        uves_free_table(&results);
    }
    
    return results;
}

    
/*----------------------------------------------------------------------------*/
/**
   @brief    Improve position of a peak in the Hough image
   @param    htrans        The Hough transform
   @param    xmax          Peak x-position which will be updated
   @param    ymax          Peak Y-position which will be updated
   @param    SPACING       Spacing of order lines
   @param    imagewidth    Width of raw image
   @param    SAMPLEWIDTH   Separation between traces in raw image
   @param    MINSLOPE      Minimum line slope to search for
   @param    MAXSLOPE      Maximum line slope to search for
   @param    SLOPERES      Width of Hough image
   @return   CPL_ERROR_NONE iff okay

   This function improves a peak position (from the max. pixel value) to the 
   center of mass of a line passing through the middle of the 'butterfly wings'
   in hough space. The slope of this line in Hough space
   is ( -(width of input image) / 2 ).

   Only pixels with values above ( (1 - 0.5 / @em NOT) * @em peak_value ) 
   contribute to the calculation of the centroid. (Here @em NOT is the number
   of sample traces in the input image, and @em peak_value is the pixel value 
   of the detected maximum.)
*/
/*----------------------------------------------------------------------------*/
static cpl_error_code update_max(const cpl_image *htrans,
                 int *xmax,              
                 int *ymax,
                 int SPACING,
                 int imagewidth,
                 int SAMPLEWIDTH,
                 double MINSLOPE,
                 double MAXSLOPE,
                 int SLOPERES)
{
    const int nx = cpl_image_get_size_x(htrans);
    const int ny = cpl_image_get_size_y(htrans);
    const int slope = -imagewidth/2;             /* slope of line in hough space */
    const double numberoftraces = 1 + imagewidth/SAMPLEWIDTH;
    int pis_rejected;

    /* Only look at pixel values above this threshold  */
    double threshold = (1 - 0.5 / numberoftraces) * 
    cpl_image_get(htrans, *xmax, *ymax, &pis_rejected);
    if (threshold < 0.99) threshold = 0.99;
    
    assure (cpl_error_get_code() == CPL_ERROR_NONE, cpl_error_get_code(), 
        "Could not read Hough image data");
    
    {
    double sum = 0;
    double mx  = 0;
    double my  = 0;
    int hx, hy;    
    for (hx = 1; hx <= SLOPERES; hx++)
        {
        int rowcenter = 
            uves_round_double(
            *ymax + slope*(hx - *xmax)/((double)SLOPERES)*(MAXSLOPE - MINSLOPE));
        
        /* It would be more correct to look at pixels from (rowcenter - LINEWIDTH/2)
           to (rowcenter + LINEWIDTH/2) where LINEWIDTH is the echelle line width 
           (i.e. different from SPACING) But empirically it doesn't really make
           a difference if we just use the pixel at rowcenter (for each x)    */

        for (hy = rowcenter - 0*SPACING/2; hy <= rowcenter + 0*SPACING/2; hy++) 
            {
            if (1 <= hx && hx <= nx &&
                1 <= hy && hy <= ny)
                {
                double pixelvalue = cpl_image_get(
                    htrans, hx, hy, &pis_rejected);
                if (!pis_rejected && pixelvalue >= threshold) 
                    {
                    mx  += hx*pixelvalue;
                    my  += hy*pixelvalue;
                    sum +=    pixelvalue;
                    }
                }
            }
        
        }
    
    uves_msg_debug("Peak position in Hough space changed from (%d, %d)", *xmax, *ymax);
    *xmax = uves_round_double(mx/sum);
    *ymax = uves_round_double(my/sum);
    uves_msg_debug("to (%d, %d)", *xmax, *ymax);
    }
    
  cleanup:
    return cpl_error_get_code();
}

/*----------------------------------------------------------------------------*/
/**
   @brief    Estimate spacing of order lines
   @param    image         The Hough transform
   @param    x             The column to look at
   @return   The estimated order spacing

   The function estimates the interorder spacing as two times the first
   minimum point of the auto-correlation function along the column at @em x
   in the Hough image.
*/
/*----------------------------------------------------------------------------*/

static int calculate_spacing(const cpl_image *image, int x)
{
    int shift = 0;
    double autoc = autocorr(image, x, shift);
    double previous;
    assure (cpl_error_get_code() == CPL_ERROR_NONE, cpl_error_get_code(), 
        "Could not calculate autocorrelation function");

    uves_msg_debug("Autocorrelation(shift=%d) = %f", shift, autoc);
    
    do{
    previous = autoc;
    shift += 1;
    check( autoc = autocorr(image, x, shift), 
           "Could not calculate autocorrelation function");
    uves_msg_debug("Autocorrelation(shift=%d) = %f", shift, autoc);
    } while (autoc <= previous);
    
  cleanup:
    return 2*(shift - 1);
    /* First minimum of the autocorrelation function is half spacing */
}


/*----------------------------------------------------------------------------*/
/**
   @brief    Calculate auto-correlation function of image column
   @param    image         The image
   @param    x             The column to look at
   @param    shift         Where to evaluate the auto-correlation function
   @return   The auto-correlation function

   The function returns 
      image(@em x, @em y) * image(@em x, @em y + @em shift))
   The average is over @em y-values ranging from 1 to @em ny - @em shift 
   (both inclusive).
*/
/*----------------------------------------------------------------------------*/
static double autocorr(const cpl_image *image, const int x, const int shift)
{
    double result = 0;
    const int ny = cpl_image_get_size_y(image);
    assure (cpl_error_get_code() == CPL_ERROR_NONE, cpl_error_get_code(), 
        "Could not read image dimensions");
    
    if( shift >= ny ) return 0;
    
    {
    double sum = 0;
    int y;    
    int number_of_points = 0;
    for (y = 1; y <= ny - shift; y++){
        int pis_rejected;
        double pixelvalue;

        pixelvalue = cpl_image_get(image, x, y,         &pis_rejected) *
                 cpl_image_get(image, x, y + shift, &pis_rejected);

        if (!pis_rejected){
        sum += pixelvalue;
        number_of_points += 1;
        }
    }
    assure( cpl_error_get_code() == CPL_ERROR_NONE, cpl_error_get_code(),
        "Error reading image pixel values");
    
    if (number_of_points > 0) 
        {
        result = sum / number_of_points;
        }
    else
        {
        result = 0;
        }
    }
    
  cleanup:
    return result;
}

/*----------------------------------------------------------------------------*/
/**
   @brief    Get the first trace column
   @param    nx            Width of raw image
   @param    SAMPLEWIDTH   Separation between traces in raw image
   @return   The x-position of the first trace column

   The function returns the location of the first trace column of the raw image.
   The location is defined smallest value greater than 0 of (@em nx / 2 - N * @em SAMPLEWIDTH),
   where N is an integer. This ensures that there is always a sample column at @em nx/2.

*/
/*----------------------------------------------------------------------------*/
static int firsttrace(int nx, int SAMPLEWIDTH)
{
    int result = nx/2;

    while(result - SAMPLEWIDTH >= 1)
    {
        result -= SAMPLEWIDTH;
    }

    return result;
}

/*----------------------------------------------------------------------------*/
/**
   @brief    Delete peak in Hough image
   @param    htrans        The Hough transform
   @param    minintersept  The minimum intersept represented in Hough space
   @param    hxmax         X-position of peak in Hough space
   @param    hymax         Y-position of peak in Hough space
   @param    SPACING       Spacing of order lines
   @param    imagewidth    Width of raw image
   @param    SAMPLEWIDTH   Width between traces on raw image
   @param    MINSLOPE      Minimum line slope to search for
   @param    MAXSLOPE      Maximum line slope to search for
   @param    SLOPERES      Width of Hough image
   @return   CPL_ERROR_NONE iff okay

   This function deletes a (previously detected) peak in Hough space, and the
   butterfly shaped area around the peak.

   The peak location (in Hough space) is mapped to a line in the input image (using
   the inverse Hough transformation). The Hough transform of this (one-dimensional)
   line is a (two-dimensional) butterfly-shaped area, which is then removed.
   See code for details.
*/
/*----------------------------------------------------------------------------*/
static cpl_error_code delete_peak(cpl_image *htrans, int minintersept,
                  int hxmax, int hymax, 
                  int SPACING, int imagewidth, int SAMPLEWIDTH, 
                  double MINSLOPE, double MAXSLOPE, int SLOPERES)
{
    const int ny = cpl_image_get_size_y(htrans);
    int tracecol;
    int firstcol = firsttrace(imagewidth, SAMPLEWIDTH);
    
    assure (cpl_error_get_code() == CPL_ERROR_NONE, cpl_error_get_code(), 
        "Could not read hough image data");
    
    /* We reverse the calculation of the Hough transform */
    for (tracecol = firstcol; tracecol <= imagewidth; tracecol += SAMPLEWIDTH){
    /* Get y-coordinate in raw image */
    double slope = SLOPE(hxmax);
    double intersept = minintersept + hymax;
    double imagey = intersept + slope*tracecol;
    
    /* Now erase all points in the Hough image that were caused
       by the point (tracecol, imagey) in the input image */
    int hx, hy;
    for (hx = 1; hx <= SLOPERES; hx++){
        slope = SLOPE(hx);
        intersept = imagey - slope*tracecol;
        for (hy = (intersept - minintersept) - SPACING/3;
         hy <= (intersept - minintersept) + SPACING/3; 
         hy++) {
        if (0 < hy && hy <= ny) {
            check( cpl_image_set(htrans, hx, hy, 0), 
               "Could not write pixel at (%d, %d)", hx, hy);
        }
        }
    }
    }
    
  cleanup:
    return cpl_error_get_code();
}

/*----------------------------------------------------------------------------*/
/**
   @brief    Draw detected order lines
   @param    ordertable    The order table
   @param    image         The image to draw on
   @return   CPL_ERROR_NONE iff okay

   This function draws on the input @em image the lines listed in the provided
   order table. The intersept and slope of the lines are read from the order
   table.
*/
/*----------------------------------------------------------------------------*/
cpl_error_code uves_draw_orders(const cpl_table *ordertable, cpl_image *image)
{
    double penvalue;
    int nx;
    int ny;
    cpl_stats *stats = NULL;
    int nrows;
    int i;

    /* Check input */
    passure( image != NULL, " ");
    passure( ordertable != NULL, " ");
    passure( cpl_table_has_column(ordertable, "Intersept"), " ");
    passure( cpl_table_has_column(ordertable, "Slope"    ), " ");

    nx = cpl_image_get_size_x(image);
    ny = cpl_image_get_size_y(image);
    
    /* Calculate pen value */
    check( stats = cpl_stats_new_from_image(image, CPL_STATS_MAX), 
       "Could not get statistics on input image");
    check( penvalue = 2*cpl_stats_get_max(stats),
       "Could not find image maximum value" );

    /* Draw lines in ordertable on image */
    check  ( nrows = cpl_table_get_nrow(ordertable), 
         "Could not read number of rows in ordertable");
    for (i = 0; i < nrows; i++)
    {
        int x;
        double intersept, slope;
        
        check(( intersept = cpl_table_get_double(ordertable, "Intersept", i, NULL),
            slope     = cpl_table_get_double(ordertable, "Slope", i, NULL)),
           "Could not read 'Intersept' and 'Slope' from ordertable");
        
        for (x = 1; x <= nx; x++){
        double yd = intersept + x * slope;
        int y = uves_round_double(yd);
        
        if (0 < y && y <= ny)
            {
            cpl_image_set(image, x, y, penvalue);
            }
        }
        assure( cpl_error_get_code() == CPL_ERROR_NONE, cpl_error_get_code(),
            "Could not draw order in table row #%d", i);
    }
    
  cleanup:
    uves_free_stats(&stats);
    return cpl_error_get_code();
}
/**@}*/
