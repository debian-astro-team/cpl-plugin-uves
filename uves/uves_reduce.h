/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2013-08-08 13:36:46 $
 * $Revision: 1.28 $
 * $Name: not supported by cvs2svn $
 *
 */
#ifndef UVES_REDUCE_H
#define UVES_REDUCE_H
#include <uves_cpl_size.h>
#include <uves_utils_polynomial.h>
#include <uves_chip.h>

#include <cpl.h>

#include <stdbool.h>

cpl_parameterlist *uves_reduce_define_parameters(void);

cpl_error_code uves_reduce(const cpl_image *raw_image, 
                           const uves_propertylist *raw_header, 
                           const uves_propertylist *rotated_header,
               const cpl_image *master_bias,
                           const uves_propertylist *mbias_header,
               const cpl_image *master_dark, 
                           const uves_propertylist *mdark_header, 
               const cpl_image *master_flat, 
                           const uves_propertylist *mflat_header,
               const cpl_table *ordertable, 
                           const polynomial *order_locations,
               const cpl_table *linetable[3], 
                           const uves_propertylist *linetable_header[3], 
                           const polynomial *dispersion_relation[3],
               enum uves_chip chip,
               /* General */
               bool   debug_mode,
               /* Backsub */
               /* Flat fielding */
               /* Extraction */
               /* Rebinning  */
               const cpl_parameterlist *parameters, 
                           const char *rec_id,
                           const char *mode,
               /* Output */
               cpl_image **x, uves_propertylist **x_header,
               cpl_image **fx,
               cpl_table **cosmic_mask,
               cpl_image **wave_map,
               cpl_image **background,
               cpl_image **flatfielded_variance,
               uves_propertylist **flatfielded_variance_header,
               cpl_image **resampled_spectrum,
               cpl_image **resampled_mf,
               cpl_image **merged_sky,
               cpl_image **rebinned_spectrum, 
                           cpl_image **rebinned_noise, 
                           uves_propertylist **rebinned_header,
               cpl_image **merged_spectrum,   
                           cpl_image **merged_noise, 
                           uves_propertylist **merged_header,
               cpl_image **reduced_rebinned_spectrum, 
                           cpl_image **reduced_rebinned_noise,
               cpl_image **reduced_spectrum , 
                           cpl_image **reduced_noise,
                           cpl_table **info_tbl,
               double *extraction_slit,
               cpl_table **order_trace);

#endif  /* UVES_REDUCE_H */
