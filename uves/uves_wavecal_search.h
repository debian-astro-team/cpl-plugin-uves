/*                                                                              *
 *   This file is part of the ESO UVES Pipeline                                 *
 *   Copyright (C) 2002,2003 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */

/*
 * $Author: amodigli $
 * $Date: 2011-12-08 13:53:50 $
 * $Revision: 1.10 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.9  2011/03/23 09:52:36  amodigli
 * changed uves_wavecal_search() API to allow QC log
 *
 * Revision 1.8  2010/09/24 09:32:10  amodigli
 * put back QFITS dependency to fix problem spot by NRI on FIBER mode (with MIDAS calibs) data
 *
 * Revision 1.6  2007/06/06 08:17:34  amodigli
 * replace tab with 4 spaces
 *
 * Revision 1.5  2007/05/02 13:20:00  jmlarsen
 * Take error bars into account in line searching if arclamp was flat-fielded
 *
 * Revision 1.4  2007/04/24 12:50:29  jmlarsen
 * Replaced cpl_propertylist -> uves_propertylist which is much faster
 *
 * Revision 1.3  2007/03/05 10:41:48  jmlarsen
 * Use CCD binning when determining search window width
 *
 * Revision 1.2  2006/02/15 13:19:15  jmlarsen
 * Reduced source code max. line length
 *
 * Revision 1.1  2006/02/03 07:46:30  jmlarsen
 * Moved recipe implementations to ./uves directory
 *
 * Revision 1.8  2005/12/20 08:11:44  jmlarsen
 * Added CVS  entry
 *
 */

#ifndef UVES_WAVECAL_SEARCH_H
#define UVES_WAVECAL_SEARCH_H
#include <uves_cpl_size.h>
#include <uves_utils_polynomial.h>
#include <cpl.h>
#include <stdbool.h>

typedef enum {CENTERING_GAUSSIAN, CENTERING_GRAVITY} centering_method;

cpl_table *
uves_wavecal_search(const cpl_image *spectrum, const cpl_image *noise,
            const uves_propertylist *spectrum_header,
                    bool flat_fielded,
            const polynomial *order_locations, cpl_image *arcframe,
            int RANGE, int MINLINES, int MAXLINES,
            centering_method CENTERING_METHOD,
                    int bin_disp,
                    const int trace,const int window,  cpl_table* qclog);

#endif
